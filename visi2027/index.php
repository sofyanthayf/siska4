<br>
<h2>Workshop Perumusan Nilai Unggul dan Visi STMIK KHARISMA 2027</h2><hr>

<table width="100%">
  <tr>
    <td>


<ul>

<li>
  <a href='Draft Visi 2027.pdf' target='_blank'>
    Draft Visi STMIK KHARISMA 2027</a>
</li>

<br>

<li>
  <a href='TERM OF REFERENCE.pdf' target='_blank'>
  Term of Reference (TOR)</a>
</li>

</ul>

<br>

<ul>

<li>
  <a href='Draft Visi 2029 - Review 14-09-2017.pdf' target='_blank'>
    Draft Visi STMIK KHARISMA 2029 (Hasil Review 14/09/2017)</a>
</li>

</ul>

<b>Pandangan Umum:</b> Sabtu, 26 Agustus 2017 pk.08:30-11.45 [SELESAI]<br>
<b>Pembahasan Internal:</b> Senin, 28 Agustus 2017 pk.08:30-11.45 [SELESAI]<br>
<br>
<b>Pembahasan 3:</b> Senin, 04 September 2017 pk.08:30-12.00 <br>
<b>Pembahasan 4:</b> Kamis, 14 September 2017 pk.08:30-12.00 <br>

<br>

</td>
<td align="right" valign="top">
  <img src="visi2027.png">
</td>
</tr>
</table>

<br>
<small><i>Dokumen masih bersifat confidential karena masih berupa draft dan belum layak untuk dipublikasi<br>
Hanya untuk lingkungan internal atau pihak terkait saja</i></small>
<hr>
<small><strong>&copy;STMIK KHARISMA Makassar, 2017</strong></small>
