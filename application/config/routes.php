<?php
defined('BASEPATH') or exit('No direct script access allowed');

/** HOME  **/
$route['home']                     = 'home';
$route['default_controller']       = 'home';

/** AUTH **/
$route['login/(:any)']             = 'auth/index/$1';
$route['login']                    = 'auth';
$route['signin']                   = 'auth/signin';
$route['signout']                  = 'auth/signout';
$route['lupapassword']             = 'auth/lupapassword';
$route['lupapassword/(:any)']      = 'auth/lupapassword/$1';   // $1 = kode error
$route['resetpassword']            = 'auth/resetpassword';
$route['gantipassword']            = 'auth/gantipassword';

/** MAHASISWA **/
$route['mhs']                      = 'mahasiswa';           // daftar mahasiswa
$route['mhs/(:any)']               = 'mahasiswa/data/$1';   // data mahasiswa $1=NIM

/** KRS ***
 *  $1 = NIM Mahasiswa
 *  $2 = kode semester
 *  $3 = [0]baru | [1]pembatalan | [5]review PA |
 *       [91]simpan | [92]Update Mhs | [95]Update PA | [99]hapus
 ***/
$route['krs']                      = 'krs';
$route['krs/prodi']                = 'krs/krsprodi';
$route['krs/extend/(:any)/(:any)'] = 'krs/ext_request/$1/$2';
$route['krs/ext_validate/(:any)/(:any)'] = 'krs/ext_validate/$1/$2';
$route['krs/ext_accepted']         = 'krs/ext_accept';
$route['krs/konfirmasi/(:any)']    = 'krs/konfirmasi_bayar/$1';  // $1 = id_krs
$route['krs/konfirmasi/(:any)/(:any)']    = 'krs/konfirmasi_bayar/$1/$2';  // $1 = id_krs  $2 = status
$route['krs/konfirm_upload']       = 'krs/konfirm_upload';
$route['krs/konfirm_slip_request'] = 'krs/konfirm_slip_request';
$route['krs/new/(:any)/(:any)']    = 'krs/requestnew/$1/$2'; // $1 = kodesmt  $2 = status
$route['krs/(:any)']               = 'krs/overview/$1';
$route['krs/(:any)/(:any)']        = 'krs/$1/$2';
$route['krs/(:any)/(:any)/(:any)'] = 'krs/edit/$1/$2/$3';

$route['mhs/kklp']                 = 'mahasiswa/kklp';
$route['mhs/ta']                   = 'mahasiswa/ta';
$route['mhs/request/(:any)']       = 'mahasiswa/request/$1';

/** DOSEN **/
$route['dsn']                      = 'dosen';               // daftar dosen
$route['dsn/kinerja']              = 'dosen/kinerja';       // kinerja sendiri
$route['dsn/kinerja/(:any)']       = 'dosen/kinerja/$1';    // kinerja dosen tertentu
$route['dsn/perkuliahan']          = 'dosen/perkuliahan';   // daftar kelas perkuliahan
$route['dsn/perkuliahan/(:any)']   = 'dosen/perkuliahan/$1';    // data kelas perkuliahan, $1 = kodekelas

$route['dsn/penelitian']           = 'litabmas/daftar_penelitian';   // daftar penelitian dosen
$route['dsn/penelitian/(:any)']    = 'litabmas/daftar_penelitian/$1';   // daftar penelitian dosen
$route['dsn/editpenelitian/(:any)'] = 'litabmas/edit_penelitian/$1';   // daftar penelitian dosen, $1=id_penelitian

$route['dsn/publikasi']            = 'litabmas/daftar_publikasi';   // daftar publikasi dosen
$route['dsn/publikasi/(:any)']     = 'litabmas/daftar_publikasi/$1';   // daftar publikasi dosen

$route['dsn/pkm']                  = 'litabmas/daftar_pkm';   // daftar pkm dosen
$route['dsn/pkm/(:any)']           = 'litabmas/daftar_pkm/$1';   // daftar pkm dosen

$route['dsn/perwalian']            = 'dosen/perwalian';     // daftar mahasiswa wali
$route['dsn/perwalian/(:any)']     = 'dosen/perwalian/$1';  // $1 = NIM Mahasiswa
$route['dsn/(:any)']               = 'dosen/data/$1';        // $1 = NIDN Dosen


/** BAAK **/
$route['baak/kelas']               = 'kelas/list_kelas';
$route['baak/kelas/(:any)']        = 'kelas/list_kelas/$1';
$route['baak/kelasxls/(:any)/(:any)']     = 'kelas/export2xls/$1/$2';  // $1 = prodi; $2 = semester


/** KEUANGAN **/
$route['keu/krs']                  = 'keuangan/transaksi_krs';
$route['keu/report_krs']           = 'keuangan/report_krs';
$route['keu/reportxls_krs/(:any)'] = 'keuangan/reportxls_krs/$1';
$route['keu/krs_invoice/(:any)'] = 'keuangan/invoice_krs/$1';   // $1 = id_krs
$route['keu/krs_invoice_prev/(:any)'] = 'keuangan/invoice_krs_preview/$1';   // $1 = id_krs
$route['keu/krs_invoice_prev/(:any)/(:any)'] = 'keuangan/invoice_krs_preview/$1/$2';   // $1 = id_krs; $2 = status
$route['keu/krs_invoice_mail/(:any)'] = 'keuangan/invoice_krs_mail/$1';   // $1 = id_krs


/** FEEDBACK **/
$route['feedback/respond'] = 'feedback/respond';
$route['feedback/results'] = 'feedback/results';
$route['feedback/results/(:any)'] = 'feedback/results/$1'; // $1-id angket
$route['feedback/(:any)/(:any)'] = 'feedback/survey/$1/$2';  // $1=kodeangket; $2=data-data-data-...
// $1=kodeangket; $2=kodesmt; $3=kodekelas; $4=nidn; $5kodemk



/********************* PMB *************************/
$route['pmb']                       = 'pmb';                 // login page PMB
$route['pmb/signin']                = 'pmb/signin';          // proses login PMB
$route['pmb/signin/(:any)']         = 'pmb/index/$1';              // login page PMB with status
$route['pmb/signout']               = 'pmb/signout';         // proses login PMB
$route['pmb/gantipassword']         = 'pmb/gantipassword';         // ganti password
$route['pmb/lupapassword']          = 'pmb/lupapassword';          // lupa password
$route['pmb/lupapassword/(:any)']   = 'pmb/lupapassword/$1';          // lupa password

$route['pmb/reward/(:any)']         = 'pmb/rewardprogram/$1'; // info reward program /tahun

$route['pmb/a/reg']                 = 'pmb/regagent';        // register agent
$route['pmb/a/register']            = 'pmb/registeragent';        // register agent
$route['pmb/a/syarat']              = 'pmb/syaratagen';        // syarat dan ketentuan agent
$route['pmb/a']                     = 'pmb/viewagent';       // page agent
$route['pmb/a/profil']              = 'pmb/profilagent';       // page profil agent
$route['pmb/a/(:any)']              = 'pmb/viewagent/$1';       // page agent

$route['pmb/m']                     = 'pmb/viewmaba';        // page Maba
$route['pmb/m/reg']                 = 'pmb/regmaba';        // register Maba
$route['pmb/m/register']            = 'pmb/registermaba';        // proses register Maba

$route['pmb/m/syarat']              = 'pmb/syaratmaba';        // persyaratan maba
$route['pmb/m/jadwal']              = 'pmb/jadwalmaba';        // jadwal penerimaan Maba
$route['pmb/m/biaya']               = 'pmb/biayamaba';        // Pembiayaan Maba

$route['pmb/m/profil']              = 'pmb/profilmaba';       // page profil agent
$route['pmb/m/reg/(:any)']          = 'pmb/regmaba/$1';        // register Maba dengan Referrer
$route['pmb/m/(:any)']              = 'pmb/viewmaba/$1';       // page Maba

$route['pmb/dokumen']               = 'pmb/dokumen';          // upload dokumen
$route['pmb/dokumen/(:any)']        = 'pmb/dokumen/$1';       // upload dokumen dengan status upload terakhir

$route['pmb/verify/email']          = 'pmb/infoverifyemail';       // informasi verifikasi email
$route['pmb/verify/emailsukses']    = 'pmb/suksesverifyemail';       // informasi sukses verifikasi email
$route['pmb/verify/emailinvalid']   = 'pmb/gagalverifyemail';       // informasi gagal verifikasi email
$route['pmb/verify/request']        = 'pmb/requestverifyemail';       // request kode verifikasi email

$route['pmb/verify/sms']            = 'pmb/verifysms';       // informasi sukses verifikasi sms
$route['pmb/verify/smssukses']      = 'pmb/suksesverifysms';       // informasi sukses verifikasi sms
$route['pmb/verify/smspininvalid']  = 'pmb/gagalverifysms';       // informasi gagal verifikasi sms
$route['pmb/verify/smsrequest']     = 'pmb/requestverifysms';       // request kode verifikasi sms
$route['pmb/verify/smspinreset']    = 'pmb/requestverifysmssent';       // informasi reset kode verifikasi sms

$route['pmb/verify/(:any)']         = 'pmb/verify/$1';       // verifikasi email/sms

// $route['pmb/reqgantinomorhp']       = 'pmb/request_gantinomorhp';
$route['pmb/gantinomorhp']          = 'pmb/gantinomorhp';
$route['pmb/gantinomorhp/(:any)']   = 'pmb/gantinomorhp/$1';

// $route['pmb/reqgantiemail']         = 'pmb/request_gantiemail';
$route['pmb/gantiemail']            = 'pmb/gantialamatemail';
$route['pmb/gantiemail/(:any)']     = 'pmb/gantialamatemail/$1';

$route['pmb/pan']                   = 'pmb_panitia';
$route['pmb/pan/maba']              = 'pmb_panitia/listmaba';
$route['pmb/pan/maba/(:any)']       = 'pmb_panitia/detailmaba/$1';
$route['pmb/pan/agen']              = 'pmb_panitia/listagen';
$route['pmb/pan/agen/(:any)']       = 'pmb_panitia/detailagen/$1';
$route['pmb/pan/validasi']          = 'pmb_panitia/validasi';
$route['pmb/pan/jadwalseleksi']     = 'pmb_panitia/penjadwalanseleksi';
$route['pmb/pan/jadwalseleksi/(:any)'] = 'pmb_panitia/penjadwalanseleksi/1';
$route['pmb/pan/jadwalbaru']        = 'pmb_panitia/penjadwalanbaru';

$route['pmbajax/updateprofilmaba']  = 'pmb_ajax/updateprofilmaba';
$route['pmbajax/updateprofilagen']  = 'pmb_ajax/updateprofilagen';
$route['pmbajax/emailexist']        = 'pmb_ajax/emailexist';
$route['pmbajax/cellularexist']     = 'pmb_ajax/cellularexist';

/** PDF  **/
// $route['pdf']                    = 'pdfdok';       //test pdf
$route['pdf/mylinkreferrer']        = 'pdfdok/agen_linkref';
$route['pdf/kartutest/(:any)']      = 'pdfdok/maba_kartutest/$1';
$route['pdf/kartutest/(:any)/(:any)'] = 'pdfdok/maba_kartutest/$1/$2';

$route['pdf/slipkrs/(:any)']        = 'pdfdok/krs_slip/$1';
$route['pdf/krsextend/(:any)/(:any)'] = 'pdfdok/krs_extend/$1/$2';


/** AJAX **/
$route['ajax/listprodimhs/(:any)']  = 'ajax/listprodimhs/$1';
$route['ajax/getmahasiswa']         = 'ajax/getmahasiswa';
$route['ajax/getlistmahasiswa']     = 'ajax/getlistmahasiswa';
$route['ajax/search/mhs']           = 'ajax/searchmahasiswa';
$route['ajax/listangkatan']         = 'ajax/listangkatan';
$route['ajax/listangkatanalumni']   = 'ajax/listangkatanalumni';

$route['ajax/getstaf']              = 'ajax/getstaf';

$route['ajax/listpropinsi']         = 'ajax/listpropinsi';
$route['ajax/listkotakab']          = 'ajax/listkotakab';
$route['ajax/listkecamatan']        = 'ajax/listkecamatan';
$route['ajax/listdesakelurahan']    = 'ajax/listdesakelurahan';
$route['ajax/listsekolahkota']      = 'ajax/listsekolahkota';
$route['ajax/listjurusansmk']       = 'ajax/listjurusansmk';

$route['pmb/testsms']               = 'pmb/send_sms_verifikasi';       // informasi sukses verifikasi email


$route['kurikulum']                 = 'publik/kurikulum';
$route['kurikulum/(:any)']          = 'publik/kurikulum/$1';


$route['umpanbalik']                = 'publik/fresults';
$route['umpanbalik/(:any)']         = 'publik/fresults/$1';
$route['(:any)']                    = 'publik/index/$1';


// $route['log/(:any)/(:any)/(:any)']  = 'Siska_log/index/$1/$2/$3';   //$1=type; $2=status; $3=description


/** API **/
$route['api']                       = 'api';
$route['api/req']                   = 'api/request';    // form request for API-key
$route['api/reg']                   = 'api/regist';     // processing API-key request

$route['api/auth']                  = 'api/auth';       // auth_post
$route['api/listmhs']               = 'api/listmhs';    // listmhs_get


/** DEFAULT **/
$route['404_override']              = '';
$route['translate_uri_dashes']      = FALSE;
