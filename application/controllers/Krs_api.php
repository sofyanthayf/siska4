<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

// class Mysiska_api extends CI_Controller {
class Krs_api extends REST_Controller {

  public function __construct($config = 'rest') {

    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    parent::__construct();

    $this->load->model('api_model');
    $this->load->model('mahasiswa_model');
    $this->load->model('matakuliah_model');
    $this->load->model('kuliah_model');
    $this->load->model('krs_model');
    $this->load->model('prodi_model');
  }

  public function krssemester_get() {
    $idk = $this->get('idk');

    $dkrs = $this->krs_model->krssemester_byID( $idk );
    $dkrs['belanja_krs'] = $this->krs_model->belanja_krssemester( $idk, $dkrs['prodi'] );

    if( !$dkrs ){
      $this->response( 'No Data', 204  );
    } else {
      $i=0;
      foreach ($dkrs['belanja_krs'] as $krs) {
        $dkrs['belanja_krs'][$i]['matakuliah'] = $this->matakuliah_model->matakuliah($dkrs['belanja_krs'][$i]['kodemk']);
        $i++;
      }
      $this->response( [ "krs_semester" => $dkrs ] , 200  );
    }

  }

  public function krsedit_get(){
    $nimhs = $this->get('nim');
    $kodesmt = $this->get('smt');
    $tipe = $this->get('tipe');

    $prevkrs = $this->krs_model->getPrevKrs( $nimhs, $kodesmt, $tipe );
    $mkkrs = $this->krs_model->belanja_krssemester( $prevkrs['id_krs'], $prevkrs['prodi'] );
    // var_dump($mkkrs);
    if(!$mkkrs){
      $this->response( 'No Data', 204  );
    } else {
      $this->response( [ 'krs_semester' => $mkkrs ] , 200  );
    }
  }

  /* -----------------------------------------------------------
  * Fungsi : krssemester_post()
  *          menyimpan KRS mahasiswa
  * API request: /krs_api/krssemester
  * method : POST
  * param  : data 	JSON string data KRS
  *
  * created by : Sofyan Thayf
  * ------------------------------------------------------------ */
  public function krssemester_post()
  {
    $dkrs = (array) json_decode( $this->post('data') );
    $dmhs = $this->mahasiswa_model->setNim( $dkrs['nimhs'] );

    $res = $this->krs_model->saveKRS($dmhs, $dkrs);
    if( !$res ){
      $this->response( $res, 500  );
    } else {
      $this->response( 'Success' , 200  );
    }

  }

  public function krsmahasiswawali_get()
  {
    $nidn = $this->get('nidn');
    $kodesmt = $this->get('smt');
    $res = $this->krs_model->listKrsMahasiswa( $kodesmt, '', $nidn );
    if( !$res ){
      $this->response( $res, 500  );
    } else {
      $i = 0;
      foreach ($res as $krsmhs) {
        $res[$i]['mahasiswa'] = $this->mahasiswa_model->detailmhsminimalis($krsmhs['nimhs']);
        $i++;
      }
      $this->response( ['listkrsmhs'=> $res ] , 200  );
    }
  }

  public function krsmahasiswa_get()
  {
    $kodesmt = $this->get('smt');
    $prodi = $this->get('prodi');
    $res = $this->krs_model->listKrsMahasiswa( $kodesmt, $prodi );
    if( !$res ){
      $this->response( $res, 500  );
    } else {
      $i = 0;
      foreach ($res as $krsmhs) {
        $res[$i]['mahasiswa'] = $this->mahasiswa_model->detailmhsminimalis($krsmhs['nimhs']);
        $i++;
      }
      $this->response( ['listkrsmhs'=> $res ] , 200  );
    }
  }

  public function semesterkrswali_get()
  {
    $nidn = $this->get('pa');
    $res = $this->krs_model->listSemesterKrs( $nidn );
    $i = 0;
    foreach ($res as $smt) {
      $res[$i]['stringsmt'] = $this->siska->stringSemester( $smt['kodesmt'],"" );
      $i++;
    }
    $this->response( ['semesterkrs'=> $res ] , 200  );
  }

  public function semesterkrs_get()
  {
    $res = $this->krs_model->listSemesterKrs();
    $i = 0;
    foreach ($res as $smt) {
      if ($smt['kodesmt'] != "") {
        $res[$i]['stringsmt'] = $this->siska->stringSemester( $smt['kodesmt'],"" );
      }
      $i++;
    }
    $this->response( ['semesterkrs'=> $res ] , 200  );
  }

  public function prodikrsaktif_get()
  {
    $prodi = array();
    $kodesmt = $this->get('smt');
    $res = $this->krs_model->listProdiKrsAktif( $kodesmt );
    foreach ($res as $pr) {
      $prodi[] = $this->prodi_model->getProdi( $pr['prodi'] );
    }
    $this->response( [ 'listprodi' => $prodi ] , 200 );
  }

  public function delkrs_post()
  {
    $idkrs = $this->post('id');
    $this->krs_model->deleteKrs($idkrs);
    $this->response( ['id'=> $idkrs ] , 200  );
  }

  public function updateslip_post()
  {
    $idkrs = $this->post('id');

    $dkrs = array( 'biaya_sks' => $this->post('bsks'),
                   'biaya_adm' => $this->post('badm'),
                   'tanggal_cetak_slip' => date('Y-m-d H:i:s'),
                   'staf_cetak_slip' => $this->session->uid
                 );

    if($this->post('slp') != ""){
      // BEASISWA
      $dkrs['nomorslip'] = $this->post('slp');

      $dkrs['metode_bayar'] = 5;
      $dkrs['status'] = 3;
      $dkrs['tanggal_validasi_keu'] = date('Y-m-d H:i:s');
      $dkrs['id_validasi_keu'] = $this->session->uid;
      $dkrs['kode_validasi_keu'] = md5( $idkrs . $this->session->pin . $dkrs['tanggal_validasi_keu'] );

    } else {
      $dkrs['nomorslip'] = $this->krs_model->nomorSlip( $this->post('smt') );
    }

    $this->krs_model->updateValidasiKrs( $idkrs, $dkrs );
  }

  public function validasikeu_post()
  {
    $idkrs = $this->post('id');
    $tglkonfirm = date('Y-m-d H:i:s');

    $dkrs = array( 'metode_bayar' => $this->post('mbyr'),
                   'tanggal_bayar' => $this->post('tbyr'),
                   'tanggal_validasi_keu' => $tglkonfirm,
                   'id_validasi_keu' => $this->session->uid,
                   'kode_validasi_keu' => md5( $idkrs . $this->session->pin . $tglkonfirm )
                 );

    // $dkrs = array( 'metode_bayar' => $this->post('mbyr'),
    //                'tanggal_bayar' => $this->post('tbyr'),
    //                'status' => 3,
    //                'tanggal_validasi_keu' => $tglkonfirm,
    //                'id_validasi_keu' => $this->session->uid,
    //                'kode_validasi_keu' => md5( $idkrs . $this->session->pin . $tglkonfirm )
    //              );

    $this->krs_model->updateValidasiKrs( $idkrs, $dkrs );
  }

  public function validasiprodi_post()
  {
    $idkrs = $this->post('id');
	  $krs = $this->krs_model->krssemester_byID( $idkrs );

    $tglkonfirm = date('Y-m-d H:i:s');

    //validasi prodi
    $dkrs = array( 'status' => 5,
                   'tanggal_validasi_prodi' => $tglkonfirm,
                   'id_validasi_prodi' => $this->session->uid,
                   'kode_validasi_prodi' => md5( $idkrs . $this->session->pin . $tglkonfirm )
                 );
    $this->krs_model->updateValidasiKrs( $idkrs, $dkrs );

    // daftar mata kuliah semester
    $dkrs = $this->krs_model->belanja_krssemester( $krs['id_krs'], $krs['prodi'] );
    $this->kuliah_model->registerMKMahasiswaSemester( $dkrs );

  }

  public function krsextendlist_get()
  {
    $kodesmt = $this->get('smt');
    $ext_list = $this->krs_model->getExtendList($kodesmt);
    $this->response( ['krs_extend'=> $ext_list ] , 200  );
  }

  public function krsextend_get()
  {
    // code...
  }

  public function krsextend_post()
  {
    // var_dump( $_REQUEST );
    $kodesmt = $this->post('smt');
    $nimhs = $this->post('mhs');
    $tipekrs = $this->post('type');
    $alasan = $this->post('alasan');

    $ext_req = $this->krs_model->getExtendRequest($kodesmt,$nimhs);

    $data = array(
      'kodesmt' => $kodesmt,
      'tipe_krs' => $tipekrs,
      'nimhs' => $nimhs,
      'tanggal_request' => date('Y-m-d H:i:s'),
      'alasan_extend' => $alasan
    );

    if( $ext_req ){
      $data['id'] = $ext_req->id;
      if( $ext_req->tanggal_accept === NULL ){
        $this->krs_model->updateExtendRequest($data);
      }
    } else {
      $data['id'] = date('U');
      $this->krs_model->insertExtendRequest($data);
      $ext_req = $this->krs_model->getExtendRequest($kodesmt,$nimhs);
    }

    $this->response( ['krsextend'=> $ext_req ] , 200  );
  }



}


?>
