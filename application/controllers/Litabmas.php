<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Litabmas extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$_SESSION['controller'] = $this->router->fetch_class();

    $this->load->model('litabmas_model');
	}


  public function daftar_penelitian( $nidn = '' )
  {
    $data['judul1'] = "Penelitian";
    $data['judul2'] = "";

    if($nidn != ''){
      $data['nidn'] = $nidn;
    }else{
      $data['nidn'] = $this->session->uid;
    }

    $this->load->template( 'litabmas/penelitian', $data );

  }

  public function edit_penelitian( $id_penelitian )
  {
    $data['judul1'] = "Penelitian";
    $data['judul2'] = "Edit";

    $data['penelitian'] = $this->litabmas_model->penelitian($id_penelitian);
    $data['peneliti'] = $this->litabmas_model->peneliti($id_penelitian);

    $this->load->template( 'litabmas/editlp', $data );
  }

  public function daftar_publikasi( $nidn = '' )
  {
    $data['judul1'] = "Publikasi";
    $data['judul2'] = "";

    if($nidn != ''){
      $data['nidn'] = $nidn;
    }else{
      $data['nidn'] = $this->session->uid;
    }

    $this->load->template( 'litabmas/publikasi', $data );
  }

  public function daftar_pkm( $nidn = '' )
  {
    $data['judul1'] = "Pengabdian Masyarakat";
    $data['judul2'] = "";

    if($nidn != ''){
      $data['nidn'] = $nidn;
    }else{
      $data['nidn'] = $this->session->uid;
    }

    $this->load->template( 'litabmas/pkm', $data );

  }



}
