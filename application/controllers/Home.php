<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
		$this->load->model('feedback_model');

		$_SESSION['controller'] = $this->router->fetch_class();
	}

	public function index()
	{
		$data['judul1'] = "Home";
		$data['judul2'] = "Overview";
		$this->load->template('home/home', $data);
	}
}
