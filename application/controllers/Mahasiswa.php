<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mahasiswa extends CI_Controller {

	public $mhs;

	public function __construct()
	{
		parent::__construct();
		$_SESSION['controller'] = $this->router->fetch_class();

		$this->load->model('mahasiswa_model');
		$this->load->model('prodi_model');
		$this->load->model('region_model');
	}

	public function index()
	{
		$_SESSION['page'] = 'mhs';

		$data['judul1'] = "Mahasiswa";
		$data['judul2'] = "Program Studi";
		$this->load->template( 'mhs/listmahasiswa', $data );
	}

	public function data( $nim ){

		if( $this->mhs = $this->mahasiswa_model->setNim( $nim ) ) {
			$_SESSION['page'] = 'dmhs';

			$data['judul1'] = "Mahasiswa";
			$data['judul2'] = $this->mhs['namamhs'] . " - " . $nim;

			if(!empty($this->mhs['distrik_rumah']))
				$this->mhs['region'] = $this->region_model->getRegion( $this->mhs['distrik_rumah'] );
			if(!empty($this->mhs['sekolah_asal']))
				$this->mhs['asalsekolah'] = $this->region_model->getRegionSekolah( $this->mhs['sekolah_asal'] );

			$data['mhs'] = $this->mhs;

			/* info status */
			$data['status']['last_update'] = $this->mhs['modified_date'];
			$data['status']['last_login'] = $this->mhs['last_login'];
			$data['status']['ip_address'] = $this->mhs['ip'];

			$this->load->template( 'mhs/mahasiswa', $data );
		} else {
			redirect('/');
		}

	}




}
