<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends CI_Controller {

    public function __construct()
	{
		parent::__construct();
        $this->load->model('user_model');
		$this->load->model('mahasiswa_model');
		$this->load->model('prodi_model');
		$this->load->model('region_model');
    }

    public function listprodimhs() {
		$this->ajax_out( $this->prodi_model->getProdiTahun( $this->input->post('angkatan') ) );
//        $_SESSION['view_mhs']['angkatan'] = $angkatan;
	}

    public function getmahasiswa(){
        $this->ajax_out( $this->mahasiswa_model->setNim( $this->input->post('nim') ) );
    }

    public function getlistmahasiswa(){
        $prodi = $this->input->post('prodi');
        $tahun = $this->input->post('tahun');

        $_SESSION['view_mhs_prodi'] = $prodi;
        $_SESSION['view_mhs_tahun'] = $tahun;

        if( null !== $this->input->post('dataroot')  ){
            $this->ajax_out( $this->mahasiswa_model->getListMahasiswa( $prodi, $tahun ), $this->input->post('dataroot') );
        } else {
            $this->ajax_out( $this->mahasiswa_model->getListMahasiswa( $prodi, $tahun ) );
        }
    }

    public function searchmahasiswa(){
        $key = $this->input->post('key');
        if( null !== $this->input->post('dataroot')  )
            $this->ajax_out( $this->mahasiswa_model->searchMahasiswa( $key ), $this->input->post('dataroot') );
        else
            $this->ajax_out( $this->mahasiswa_model->searchMahasiswa( $key ) );
    }

    public function listmkmahasiswa(){
        $mhs = $this->input->post('nim');
        $prodi = $this->input->post('prodi');
        $tahunkur = $this->input->post('tahunkur');
        $kompetensi = $this->input->post('kompetensi');
        $smt = $this->input->post('smt');
        $this->ajax_out( $this->mahasiswa_model->getMataKuliah( $mhs, $prodi, $tahunkur, $kompetensi, $smt ), $this->input->post('dataroot') );
    }

    public function listangkatan(){
        $this->ajax_out( $this->mahasiswa_model->getListAngkatan() );
    }

    public function listangkatanalumni(){
        $this->ajax_out( $this->mahasiswa_model->getListAngkatan(true) );
    }

    public function getstaf(){
        $id = $this->input->post('id');

        if( $this->user_model->isDosen($id) ){
            $this->ajax_out( $this->user_model->getUser('siska_dosen', 'nidn', $id ) );
        } elseif ( $this->user_model->isStaf($id) ){
            $this->ajax_out( $this->user_model->getUser('siska_staf', 'nip', $id ) );
        } else {
            echo "false";
        }
    }

    public function listpropinsi(){
        $this->ajax_out( $this->region_model->getListPropinsi() );
    }

    public function listkotakab(){
        $this->ajax_out( $this->region_model->getListKotaKabupaten( $this->input->post('idprop')) );
    }

    public function listkecamatan(){
        $this->ajax_out( $this->region_model->getListKecamatan( $this->input->post('idkota')) );
    }

    public function listdesakelurahan(){
        $this->ajax_out( $this->region_model->getListDesaKelurahan( $this->input->post('idkecamatan')) );
    }

    public function listsekolahkota(){
        $this->ajax_out( $this->region_model->getListSekolahKota( $this->input->post('idkota')) );
    }

    public function listjurusansmk(){
        $this->ajax_out( $this->region_model->getListJurusanSMK( $this->input->post('tahunlulus')) );
    }


    /***
     *  function: ajax_out()
     *  @param: $data hasil query dari model
     *  Mengirim output JSON ke browser sebagai AJaX response
     *  dengan proteksi menggunakan csrf_token
     **/
    private function ajax_out( $data, $jsonroot = "" ){
        if (empty($_SESSION['csrf_token'])) {
            if (function_exists('mcrypt_create_iv')) {
                $_SESSION['csrf_token'] = bin2hex(mcrypt_create_iv(32, MCRYPT_DEV_URANDOM));
            } else {
                $_SESSION['csrf_token'] = bin2hex(openssl_random_pseudo_bytes(32));
            }
        }

        // $headers = apache_request_headers();
        // if (isset($headers['CsrfToken'])) {
        //     if ($headers['CsrfToken'] !== $_SESSION['csrf_token']) {
        //         exit(json_encode(['error' => 'Wrong CSRF token.']));
        //     }
        // } else {
        //     exit(json_encode(['error' => 'No CSRF token.']));
        // }

        if( $jsonroot == "" ) echo json_encode( $data );
        else  print_r( "{\"$jsonroot\": " . json_encode( $data ) . " }" );
    }

}
