<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

// class Mysiska_api extends CI_Controller {
class Mysiska_api extends REST_Controller {

    public function __construct($config = 'rest') {

      header('Access-Control-Allow-Origin: *');
      header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
  		parent::__construct();

  		$this->load->model('user_model');
  		$this->load->model('prodi_model');
  		$this->load->model('region_model');
    }

    /************************************** USER *************************************/

    public function user_get( $id ){

      if( $this->user_model->setId( $id ) ){
        $datauser = $this->user_model->data;
        $this->response(  [ "data" => $datauser ], 200  );
      } else {
        $this->response(  "No Data", 204  );
      }

    }



    /************************************** DOSEN *************************************/

    public function dosen_get(){
      if($id == ""){

      } else {

      }

    }


}
