<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pmb_panitia extends CI_Controller {

	private $data;
	private $tahun;

	public function __construct(){
		parent::__construct();
		$_SESSION['controller'] = $this->router->fetch_class();

		// $this->load->model('user_model');
		// $this->load->model('prodi_model');
		$this->load->model('region_model');
		$this->load->model('pmbuser_model');
		$this->load->model('prodi_model');
		$this->load->library('siska');
		$this->load->library('pmb_lib');

		// bukan panitia dilarang masuk
		if( !$this->pmb_lib->panitia( $_SESSION['token'], $_SESSION['pan'] ) ) redirect("/pmb/a");

		$this->tahun = $this->pmb_lib->tahun;

		$this->data['judul1'] = "Panitia PMB ".$this->tahun;
		$this->data['judul2'] = "";
		$this->data['page'] = "panpmb";
		$this->data['public'] = false;

		$this->data['tahun'] = $this->tahun;
	}

    public function index(){

    }

    public function listmaba(){
        $this->data['judul2'] = "Daftar Calon Mahasiswa Baru";

        $this->load->view('pmb/pmbheader', $this->data );
        $this->load->view('pmbpan/listmaba', $this->data );
        $this->load->view('pmb/pmbfooter', $this->data );
    }

    public function detailmaba( $id ){
		//hanya boleh diakses oleh panitia dan agen
		$this->data['public'] = true;
		if( $_SESSION['klas'] != 'agen' ) redirect("/pmb/m");

        $this->data['judul2'] = "Calon Mahasiswa Baru";
		$this->data['maba'] = $this->pmbuser_model->maba( $id );

        $this->load->view('pmb/pmbheader', $this->data );
        $this->load->view('pmbpan/detailmaba', $this->data );
        $this->load->view('pmb/pmbfooter', $this->data );
    }

    public function listagen(){
        $this->data['judul2'] = "Daftar Agen Reward Point";

        $this->load->view('pmb/pmbheader', $this->data );
        $this->load->view('pmbpan/listagen', $this->data );
        $this->load->view('pmb/pmbfooter', $this->data );
    }

    public function detailagen( $id ){
        $this->data['judul2'] = "Agen Reward Point";
		$this->data['agen'] = $this->pmbuser_model->agen( $id );

        $this->load->view('pmb/pmbheader', $this->data );
        $this->load->view('pmbpan/detailagen', $this->data );
        $this->load->view('pmb/pmbfooter', $this->data );
    }

	public function validasi(){
		$this->data['judul2'] = "Dokumen Calon Mahasiswa Baru";

        $this->load->view('pmb/pmbheader', $this->data );
        $this->load->view('pmbpan/validasi', $this->data );
        $this->load->view('pmb/pmbfooter', $this->data );
    }

	public function penjadwalanseleksi($kode = '0'){
		$this->data['judul2'] = "Penjadwalan Seleksi";
		$this->data['kode'] = $kode;

        $this->load->view('pmb/pmbheader', $this->data );
        $this->load->view('pmbpan/jadwalseleksi', $this->data );
        $this->load->view('pmb/pmbfooter', $this->data );
    }

	public function penjadwalanbaru(){
		// var_dump( $this->input->post() );
		// var_dump( $this->input->post('maba') );
		if( count($this->input->post('maba')) == 0 ) redirect("/pmb/pan/jadwalseleksi/1");

		$id_ujian = date('U');
		$this->pmbuser_model->jadwalujianbaru( $id_ujian );

		foreach ( $this->input->post('maba') as $maba ) {
			$this->pmbuser_model->jadwalujianmaba( $id_ujian, $maba );
		}

		redirect("/pmb/pan/jadwalseleksi");
    }

	public function buatkartuujian(){
		$maba = $this->input->post('id');
		$this->pmbuser_model->maba( $maba );

		$data['maba'] = $this->pmbuser_model->data;
		$this->pdf->load_view('pdf/pmbmaba_kartutest', $data );

		$dokpath = 'assets/dokumen/maba/2017/';
		// $filepdf = FCPATH . str_replace('/','\\', $dokpath) . $maba . "_11.pdf";
		$filepdf = FCPATH . $dokpath . $maba . "_11.pdf";

		// buat file pdf kartu peserta
		$this->pdf->render();
		file_put_contents( $filepdf, $this->pdf->output() );

		// catat data dokumen
		$this->pmbuser_model->dokumenKartuPeserta( $maba );

		// kirim email notifikasi
	}



}

?>
