<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pdfdok extends CI_Controller {

  public function __construct()
	{
		parent::__construct();
        $this->load->model('user_model');
		$this->load->model('region_model');
		$this->load->model('pmbuser_model');
		$this->load->model('prodi_model');
		$this->load->model('mahasiswa_model');
		$this->load->model('krs_model');
		$this->load->library('siska');
		$this->load->library('pmb_lib');

        // if( !isset($_SESSION['id']) ) redirect("/pmb");
	}

  public function index(){
      $this->pdf->load_view('pdf/pdftest');
      $this->pdf->render();
      $this->pdf->stream("test.pdf");
  }

  private function pdfrender( $file ){
      $this->pdf->render();
      $this->pdf->stream( $file, array('Attachment'=>0) );
  }

  private function pdfoutput( $path ){
      $this->pdf->render();
      file_put_contents( $path, $this->pdf->output());
  }


/******************** SISKA *************************/


  public function krs_slip( $idkrs )
  {
    $dkrs = $this->krs_model->krssemester_byID( $idkrs );
    $dmhs = $this->mahasiswa_model->setNim( $dkrs['nimhs'] );
    $file = $dkrs['kodesmt']."_".$dkrs['nimhs']."_".$dkrs['nomorslip'];

    $data['dkrs'] = $dkrs;
    $data['dkrs']['terbilang'] = $this->siska->terbilang( $dkrs['biaya_sks'] + $dkrs['biaya_adm'] );
    $data['dmhs'] = $dmhs;

    $this->pdf->load_view('pdf/slip_krs', $data );
    $this->pdfrender( $file . ".pdf" );

  }

  public function krs_extend( $nim, $kodesmt )
  {
    $dkrs = (array) $this->krs_model->getExtendRequest( $kodesmt, $nim );
    $dmhs = $this->mahasiswa_model->setNim( $nim );
    $file = "krs-extend_".$dkrs['kodesmt']."_".$nim;
    $data['kodesmt'] = $kodesmt;
    $data['dkrs'] = $dkrs;
    $data['dmhs'] = $dmhs;

    $this->pdf->load_view('pdf/krs_extend', $data );
    $this->pdfrender( $file . ".pdf" );

  }




/********************* PMB *************************/

  public function agen_linkref(){
      $this->pmbuser_model->agen($_SESSION['id']);

      $data['bitly'] = $this->pmbuser_model->data['bitly_url'];
      $data['myqrimg'] = base_url() . 'assets/img/qr/agen/' . $_SESSION['id'] . '.png';

      $this->pdf->load_view('pdf/pmbagen_linkref', $data );
      $this->pdfrender("mylinkref.pdf");
  }

  public function maba_kartutest( $id, $preview ='1' ){
      $this->pmbuser_model->maba( $id );
      $dokpath = 'assets/dokumen/maba/2017/';

      $data['maba'] = $this->pmbuser_model->data;
      $this->pdf->load_view('pdf/pmbmaba_kartutest', $data );
      // $this->load->view('pdf/pmbmaba_kartutest', $data );

      if( $preview == '1' ) {
          $this->pdfrender( $id."_11.pdf" );
      } else {
          $dokpath = 'assets/dokumen/maba/2017/';
          $filepdf = FCPATH . str_replace('/','\\', $dokpath) . $id . "_11.pdf";

          $this->pdfoutput( $filepdf );
      }
  }


}
