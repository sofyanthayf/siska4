<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Keuangan extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$_SESSION['controller'] = $this->router->fetch_class();

		if ($this->session->uid != '31165' && $this->session->uid != '0929067001') {
			redirect(base_url());
		}

		$this->load->model('mahasiswa_model');
		$this->load->model('matakuliah_model');
		$this->load->model('prodi_model');
		$this->load->model('krs_model');
		$this->load->model('keuangan_model');
	}

	public function index()
	{
	}

	public function transaksi_krs()
	{
		$data['judul1'] = "Transaksi ";
		$data['judul2'] = "Pembayaran KRS";

		$this->load->template('transaksi/tr_krs', $data);
	}

	public function invoice_krs($idkrs)
	{
		$data['krs'] = $this->krs_model->krssemester_byID($idkrs);
		$data['mhs'] = $this->mahasiswa_model->setNim($data['krs']['nimhs']);

		$data['krs']['kodetransfer'] = $data['krs']['nimhs'];
		$data['krs_baru'] = $this->krs_model->krsSekarang() ? $this->krs_model->krsSekarang() : $this->krs_model->krsExtend($data['krs']['nimhs']);
		$data['belanja'] = $this->krs_model->belanja_krssemester($data['krs']['id_krs'], $data['krs']['prodi']);

		$data['tampilkanvisi'] = TRUE;

		$this->load->view("emails/mail_header.php", $data);
		$this->load->view("emails/krs_invoice.php");
		$this->load->view("emails/mail_footer.php");
	}

	public function invoice_krs_preview($idkrs, $stat = 0)
	{
		$data['judul1'] = "Transaksi ";
		$data['judul2'] = "Invoice Pembayaran KRS";
		$data['stat'] = $stat;

		$data['krs'] = $this->krs_model->krssemester_byID($idkrs);
		$data['mhs'] = $this->mahasiswa_model->setNim($data['krs']['nimhs']);
		$data['krs']['kodetransfer'] = $data['krs']['nimhs'];

		$this->load->template("krs/krsinvoice.php", $data);
	}

	public function invoice_krs_mail($idkrs)
	{
		$data['krs'] = $this->krs_model->krssemester_byID($idkrs);
		$data['mhs'] = $this->mahasiswa_model->setNim($data['krs']['nimhs']);

		$data['krs']['kodetransfer'] = $data['krs']['nimhs'];
		$data['krs_baru'] = $this->krs_model->krsSekarang() ? $this->krs_model->krsSekarang() : $this->krs_model->krsExtend($data['krs']['nimhs']);
		$data['belanja'] = $this->krs_model->belanja_krssemester($data['krs']['id_krs'], $data['krs']['prodi']);

		$data['tampilkanvisi'] = TRUE;

		$mailmessage = $this->load->view("emails/mail_header.php", $data, TRUE);
		$mailmessage .= $this->load->view("emails/krs_invoice.php", $data, TRUE);
		$mailmessage .= $this->load->view("emails/mail_footer.php", $data, TRUE);

		$this->email->from('siska@kharisma.ac.id', 'SISKA - Sistem Informasi Akademik STMIK KHARISMA');
		$this->email->to($data['mhs']['email']);
		// $this->email->to( 'sofyan.thayf@kharisma.ac.id' );
		$this->email->set_mailtype('html');
		$this->email->subject('KRS Anda disetujui, lanjut pembayaran');

		$this->email->message($mailmessage);
		// echo $mailmessage;
		// $this->email->send();
		if (!$this->email->send()) {

			$mail_err = '';
			foreach ($this->email->get_debugger_messages() as $debugger_message) $mail_err .= $debugger_message;

			$this->siska_log->setLog('M', '3', 'Invoice untuk mahasiswa ' . $data['mhs']['namamhs'] . ' GAGAL dikirim ke alamat email: ' . $data['mhs']['email'] . ': ' . $mail_err);
			$this->email->clear_debugger_messages();

			redirect('/keu/krs_invoice_prev/' . $idkrs . '/2');
		} else {
			$this->siska_log->setLog('M', '0', 'Invoice untuk mahasiswa ' . $data['mhs']['namamhs'] . ' telah dikirim ke alamat email: ' . $data['mhs']['email']);
			redirect('/keu/krs_invoice_prev/' . $idkrs . '/1');
		}
	}


	public function report_krs($kodesmt = '')
	{
		$this->session->page = 'baak';

		if ($kodesmt == '') $kodesmt = $this->krs_model->semesternow();

		$data['judul1'] = "Laporan Transaksi Rencana Studi";
		$data['judul2'] = "";

		$this->load->template('transaksi/report_krs', $data);
	}


	public function reportxls_krs($kodesmt)
	{
		$result = $this->keuangan_model->transaksi_krs($kodesmt);;
		$keys = array_keys(get_object_vars($result[0]));

		$filename = "Laporan Transaksi KRS " . $kodesmt;

		//header info for browser
		header("Content-Type: application/xls");
		header("Content-Disposition: attachment; filename=$filename.xls");
		header("Pragma: no-cache");
		header("Expires: 0");

		// /*******Start of Formatting for Excel*******/
		// define separator (defines columns in excel & tabs in word)
		$sep = "\t"; //tabbed character
		//start of printing column names as names of MySQL fields
		for ($i = 0; $i < count($keys); $i++) {
			echo $keys[$i] . "\t";
		}
		echo "Jumlah\t";
		print("\n");
		//end of printing column names

		//start while loop to get data
		$n = 1;
		foreach ($result as $trx) {
			$n++;
			$schema_insert = "";
			foreach ($keys as $key) {
				$schema_insert .= $trx->$key . $sep;
			}
			$schema_insert .= "=G" . $n . "+H" . $n . $sep;
			$schema_insert = str_replace($sep . "$", "", $schema_insert);
			$schema_insert = preg_replace("/\r\n|\n\r|\n|\r/", " ", $schema_insert);
			$schema_insert .= "\t";
			print(trim($schema_insert));
			print "\n";
		}
	}
}
