<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

// class Mysiska_api extends CI_Controller {
class Mahasiswa_api extends REST_Controller {

  public function __construct($config = 'rest') {

    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    parent::__construct();

    $this->load->model('mahasiswa_model');
  }


  public function ips_get()
  {
    $nimhs = $this->get('nim');
    $ips = $this->mahasiswa_model->getIPS( $nimhs );

    if( !$ips ){
      $this->response( 'No Data', 204  );
    } else {
      $this->response( [ "ip_semester" => $ips ] , 200  );
    }
  }



}
