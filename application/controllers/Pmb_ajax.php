<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pmb_ajax extends CI_Controller {

    public function __construct()
	{
		parent::__construct();
        $this->load->model('pmbuser_model');
		$this->load->model('prodi_model');
		$this->load->model('region_model');
		$this->load->library('pmb_lib');

    $this->tahun = $this->pmb_lib->tahun;

    }

    public function updateprofilmaba(){
        $this->pmbuser_model->updateprofilmaba();
    }

    public function updateprofilagen(){
        $this->pmbuser_model->updateprofilagen();
    }

    public function emailexist(){
        if( $this->pmbuser_model->email_registered( $this->input->post('email') ) ) echo "1";
        else echo "0";
    }

    public function cellularexist(){
        if( $this->pmbuser_model->nomorhp_registered( $this->input->post('nomorhp') ) ) echo "1";
        else echo "0";
    }

    public function mailmemyref(){
        $this->email->from('layanan_pmb@kharisma.ac.id', 'PMB - STMIK KHARISMA');
        $this->email->to( $_SESSION['email'] );
        $this->email->set_mailtype('html');
        $this->email->subject('Link Referensi Registrasi MABA');

        $data['link'] = $this->input->post('mylink');
        $data['qrimg'] = $this->input->post('myqrimg');
        $data['bitly'] = $this->input->post('mybitly');
        $data['tampilkanvisi'] = FALSE;

        $mailmessage = $this->load->view('emails/mail_header', $data, true);
        $mailmessage .= $this->load->view('emails/pmb_mailref', $data, true);
        $mailmessage .= $this->load->view('emails/mail_footer', $data, true);
        $this->email->message( $mailmessage );

        $this->email->send();
    }

    public function setpanitia(){
        if( $this->pmb_lib->panitia( $_SESSION['token'], $_SESSION['pan'] )) {
            $this->pmbuser_model->setpanitia( $this->input->post('id') );
            echo "1";
        } else {
            echo "0";
        }
    }

    public function notpanitia(){
        if( $this->pmb_lib->panitia( $_SESSION['token'], $_SESSION['pan'] )) {
            $this->pmbuser_model->notpanitia( $this->input->post('id') );
            echo "1";
        } else {
            echo "0";
        }
    }

    public function getlistagen(){
        print_r( "{\"listagen\": ".json_encode( $this->pmbuser_model->listagen() )." }" );
    }

    public function getlistmaba(){
        print_r( "{\"listmaba\": ".json_encode( $this->pmbuser_model->listmaba($this->tahun) )." }" );
    }

    public function getlistdokumen(){
        print_r( "{\"listdokumen\": ".json_encode( $this->pmbuser_model->listdokumen( true ) )." }" );
    }

    public function getlistjadwal(){
        print_r( "{\"listjadwal\": ".json_encode( $this->pmbuser_model->listjadwal() ) ." }" );
    }

    public function getwaitinglistkartupeserta(){
        print_r( "{\"listkartupeserta\": ".json_encode( $this->pmbuser_model->listkartupeserta() ) ." }" );
    }

    public function validasidokumen(){
        return $this->pmbuser_model->validasidokumen( $this->input->post('id_dokumen'));
    }

    public function waitinglistmaba(){
        $this->ajax_out( $this->pmbuser_model->mabawaitinglist( $this->input->post( 'tipe' ) ) );
    }


    public function reqgantinomorhp() {
		$this->email->from('layanan_pmb@kharisma.ac.id', 'PMB - STMIK KHARISMA');
		$this->email->to( $_SESSION['email'] );
		$this->email->set_mailtype('html');
		$this->email->subject('Verifikasi Permintaan Perubahan Nomor HP');

		$data['tampilkanvisi'] = FALSE;
		$data['link'] = $this->pmb_lib->link_request('R');

		$mailmessage = $this->load->view('emails/mail_header', $data, true);
		$mailmessage .= $this->load->view('emails/pmb_request_gantinomorhp', $data, true);
		$mailmessage .= $this->load->view('emails/mail_footer', $data, true);
		$this->email->message( $mailmessage );
// echo $mailmessage;
		$this->email->send();
	}

    public function reqgantiemail() {
		$this->email->from('layanan_pmb@kharisma.ac.id', 'PMB - STMIK KHARISMA');
		$this->email->to( $_SESSION['email'] );
		$this->email->set_mailtype('html');
		$this->email->subject('Verifikasi Permintaan Perubahan Alamat E-Mail');

		$data['tampilkanvisi'] = FALSE;
		$data['link'] = $this->pmb_lib->link_request('S');

		$mailmessage = $this->load->view('emails/mail_header', $data, true);
		$mailmessage .= $this->load->view('emails/pmb_request_gantiemail', $data, true);
		$mailmessage .= $this->load->view('emails/mail_footer', $data, true);
		$this->email->message( $mailmessage );
// echo $mailmessage;
		$this->email->send();
	}


    /***
     *  function: ajax_out()
     *  @param: $data hasil query dari model
     *  Mengirim output JSON ke browser sebagai AJaX response
     *  dengan proteksi menggunakan csrf_token
     **/
    private function ajax_out( $data ){
        if (empty($_SESSION['csrf_token'])) {
            if (function_exists('mcrypt_create_iv')) {
                $_SESSION['csrf_token'] = bin2hex(mcrypt_create_iv(32, MCRYPT_DEV_URANDOM));
            } else {
                $_SESSION['csrf_token'] = bin2hex(openssl_random_pseudo_bytes(32));
            }
        }

        // $headers = apache_request_headers();
        // if (isset($headers['CsrfToken'])) {
        //     if ($headers['CsrfToken'] !== $_SESSION['csrf_token']) {
        //         exit(json_encode(['error' => 'Wrong CSRF token.']));
        //     }
        // } else {
        //     exit(json_encode(['error' => 'No CSRF token.']));
        // }
        echo json_encode( $data );
    }

}
