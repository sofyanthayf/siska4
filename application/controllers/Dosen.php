<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dosen extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$_SESSION['controller'] = $this->router->fetch_class();

		$this->load->model('dosen_model');
		$this->load->model('prodi_model');
		$this->load->model('region_model');
		$this->load->model('feedback_model');
	}

	public function index()
	{
		$_SESSION['page'] = 'dsn';

		$data['judul1'] = "Dosen";
		$data['judul2'] = "";
		$this->load->template('dsn/listdosen', $data);
	}

	public function data($nidn)
	{

		if ($this->dsn = $this->dosen_model->setNidn($nidn)) {
			$_SESSION['page'] = 'ddsn';

			$data['judul1'] = "Dosen";
			$data['judul2'] = $this->dsn['gelar_depan'] . " " . $this->dsn['nama'] . ", " . $this->dsn['gelar_belakang'] . " - " . $nidn;

			if (!empty($this->dsn['distrik_rumah']))
				$this->dsn['region'] = $this->region_model->getRegion($this->dsn['distrik_rumah']);

			$data['dsn'] = $this->dsn;

			/* info status */
			$data['status']['last_update'] = $this->dsn['modified_date'];
			$data['status']['last_login'] = $this->dsn['last_login'];
			$data['status']['ip_address'] = $this->dsn['ip'];

			$this->load->template('dsn/dosen', $data);
		} else {

			redirect('/');
		}
	}

	public function perkuliahan($nim = '')
	{
		if ($nim == '') {
		} else {
		}
	}

	public function perwalian()
	{
		$_SESSION['page'] = 'dsn';

		$data['judul1'] = "Perwalian Akademik";
		$data['judul2'] = "";
		$this->load->template('dsn/perwalian', $data);
	}

	public function kinerja($nidn = '')
	{
		if ($nidn == '') {
		} else {
		}
	}
}
