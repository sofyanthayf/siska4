<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Feedback extends CI_Controller
{

	public $feedback_kelas = '00300-0001';

	public function __construct()
	{
		parent::__construct();
		$_SESSION['controller'] = $this->router->fetch_class();

		$this->load->model('feedback_model');
		$this->load->model('mahasiswa_model');
		$this->load->model('dosen_model');
		$this->load->model('kelas_model');
		$this->load->model('matakuliah_model');
	}

	public function survey($id, $paramdata)
	{
		$data = array();
		$data['angket'] = $this->feedback_model->angket($id);
		$data['question'] = $this->feedback_model->questions($id);

		$angket_data = explode(";", $data['angket']->data);
		$param_data = explode("-", $paramdata);
		$d = 0;
		foreach ($angket_data as $ad) {
			$field = $this->getDataField($ad);
			$data['angket_data'][] = $this->getAngketData($ad, $param_data[$d]);
			$d++;
		}
		// var_dump($data);


		$data['judul1'] = "Feedback";
		$data['judul2'] = $data['angket']->title;

		$this->load->template('feedback/angket', $data);
	}

	public function respond()
	{
		// var_dump($_POST);
		// foreach ($_POST as $key => $value) {
		// 	echo $key . "<br>";
		// }

		$id_response = date('U');

		$angket = $this->feedback_model->angket($_POST['id']);
		$questions = $this->feedback_model->questions($_POST['id']);
		// var_dump($angket);

		$angket_response = array();
		$angket_response[] = array(
			'id_angket' => $_POST['id'],
			'id_response' => $id_response,
			'response_date' => date('Y-m-d H:i:s')
		);

		$angket_data = array();
		$datakey = explode(";", $angket->data);
		foreach ($datakey as $key) {
			$angket_data[] = array(
				'id_angket' => $angket->id_angket,
				'id_response' => $id_response,
				'data_label' => $key,
				'data_value' => $_POST[$key]
			);
		}

		$response_data = array();
		foreach ($questions as $quest) {
			if ($quest->klas != '1') {
				$response_data[] = array(
					'id_angket' => $_POST['id'],
					'id_response' => $id_response,
					'id_quest' => $quest->id_quest,
					'quest_order' => $quest->quest_order,
					'score' => $_POST[$quest->id_quest]
				);
			}
		}

		$tosave = array(
			array('table' => 'siskafeedback_response', 'data' => $angket_response),
			array('table' => 'siskafeedback_data', 'data' => $angket_data),
			array('table' => 'siskafeedback_responsedetail', 'data' => $response_data),
		);
		$this->feedback_model->save_response($tosave);

		$donetable = json_decode($angket->done_table, true);

		// WHERE subtitute
		foreach ($donetable as $key => $table) {
			$where = array();
			foreach ($table['where'] as $col => $value) {
				$where[] = array($col => $_POST[$value]);
			}
			$donetable[$key]['where'] = $where;

			if ($table['afterwhere'] != '') {
				$afterwhere = array();
				foreach ($table['afterwhere'] as $col2 => $value2) {
					$afterwhere[] = array($col2 => $_POST[$value2]);
				}
				$donetable[$key]['afterwhere'] = $afterwhere;
			}
		}

		foreach ($donetable as $dkey => $done) {
			if (
				$done['after'] == '' ||
				$this->feedback_model->done_after($done['after'], $done['afterwhere'])
			) {
				$this->feedback_model->feedback_done($dkey, $done['where']);
			}
		}
		// echo '<pre>' . var_export($test, true) . '</pre>';

		redirect('/mhs/' . $_POST['nimhs']);
	}

	public function results($id = '')
	{
		$data['judul1'] = "Feedback Results";

		if ($id == '') {
			$data['angket'] = $this->feedback_model->angket_list();
			$data['judul2'] = "All Surveys";
		} else {
			$data['id'] = $id;
			$data['angket'] = $this->feedback_model->angket($id);
			$data['judul2'] = $data['angket']->title;
		}

		$this->load->template('feedback/results', $data);
	}


	private function getAngketData($field, $value)
	{
		switch ($field) {
			case 'nidn':
				return array(
					'field' => 'dosen',
					'key' => 'nidn',
					'label' => 'Dosen',
					'data' => $this->dosen_model->setNidn($value)
				);
				break;

			case 'nimhs':
				return array('field' => 'mahasiswa', 'key' => 'nimhs', 'label' => 'Mahasiswa', 'data' => $this->mahasiswa_model->detailmhsminimalis($value));
				break;

			case 'kodemk':
				return array('field' => 'matakuliah', 'key' => 'kodemk', 'label' => 'Mata Kuliah', 'data' => $this->matakuliah_model->matakuliah($value));
				break;

			case 'kodekelas':
				return array('field' => 'kelas', 'key' => 'kodekelas', 'label' => 'Kode Kelas', 'data' => array('kodekelas' => $value));
				break;

			case 'kodesmt':
				return array('field' => 'semester', 'key' => 'kodesmt', 'label' => 'Semester', 'data' => array('kodesmt' => $value));
				break;
		}
	}

	private function getDataField($datainput)
	{
		switch ($datainput) {
			case 'nidn':
				return 'dosen';
				break;

			case 'nimhs':
				return 'mahasiswa';
				break;

			case 'kodesmt':
				return 'semester';
				break;

			case 'kodemk':
				return 'matakuliah';
				break;

			case 'kodekelas':
				return 'kelas';
				break;
		}
	}
}
