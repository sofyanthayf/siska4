<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelas extends CI_Controller {

	public $mhs;

	public function __construct()
	{
		parent::__construct();
		$_SESSION['controller'] = $this->router->fetch_class();

		$this->load->model('mahasiswa_model');
		$this->load->model('matakuliah_model');
		$this->load->model('prodi_model');
		$this->load->model('krs_model');
		$this->load->model('kelas_model');
	}

	public function index()
	{
		redirect( '/krs/'.$this->session->uid );
  }


  public function list_kelas($kodesmt = '' )
  {
      $this->session->page = 'baak';

      if( $kodesmt == '' ) $kodesmt = $this->krs_model->semesternow();

      $data['judul1'] = "Kelas Mata Kuliah";
      $data['judul2'] = "Semester " . $this->siska->stringSemester($kodesmt);

      $this->load->template( 'baak/kelas', $data );
  }

	public function export2xls( $prodi, $kodesmt )
	{
		$result = $this->kelas_model->kelasMKMhs($prodi, $kodesmt);
		$keys = array_keys(get_object_vars($result[0]));

		$filename = "Peserta Mata Kuliah ".$kodesmt."-".$prodi;

		//header info for browser
		header("Content-Type: application/xls");
		header("Content-Disposition: attachment; filename=$filename.xls");
		header("Pragma: no-cache");
		header("Expires: 0");

		// /*******Start of Formatting for Excel*******/
		// define separator (defines columns in excel & tabs in word)
		$sep = "\t"; //tabbed character
		//start of printing column names as names of MySQL fields
		for ($i = 0; $i < count($keys); $i++) {
			echo $keys[$i] . "\t";
		}
		print("\n");
		//end of printing column names

		//start while loop to get data
    foreach ($result as $kelasmhs) {
        $schema_insert = "";
        foreach($keys as $key)
        {
            if(!isset($kelasmhs->$key))
                $schema_insert .= "NULL".$sep;
            elseif ($kelasmhs->$key != "")
                $schema_insert .= $kelasmhs->$key.$sep;
            else
                $schema_insert .= "".$sep;
        }
        $schema_insert = str_replace($sep."$", "", $schema_insert);
        $schema_insert = preg_replace("/\r\n|\n\r|\n|\r/", " ", $schema_insert);
        $schema_insert .= "\t";
        print(trim($schema_insert));
        print "\n";
    }

	}




}
