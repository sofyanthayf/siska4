<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pmb extends CI_Controller {

	private $data;
	private $tahun;

	public function __construct()
	{
		parent::__construct();
		$_SESSION['controller'] = $this->router->fetch_class();

		// $this->load->model('user_model');
		// $this->load->model('prodi_model');
		$this->load->model('region_model');
		$this->load->model('pmbuser_model');
		$this->load->model('prodi_model');
		$this->load->library('siska');
		$this->load->library('pmb_lib');

		$this->tahun = $this->pmb_lib->tahun;

		$this->data['judul1'] = "PMB ".$this->tahun."-".($this->tahun+1);
		$this->data['judul2'] = "";
		$this->data['page'] = "pmb";
		$this->data['public'] = false;

		$this->data['tahun'] = $this->tahun;
	}

	/*
	 * @param $status jika login gagal untuk memberikan info yang sesuai
	 */
    public function index($status = 0){
		$this->data['kode'] = $status;
		$this->data['page'] = "login";
		$this->data['public'] = true;

		if( isset($_SESSION['id']) && isset($_SESSION['klas'])){
			if($_SESSION['klas']=='maba') redirect('/pmb/m');
			if($_SESSION['klas']=='agen') redirect('/pmb/a');
		}

        $this->load->view('pmb/pmbheader', $this->data );
        $this->load->view('pmb/pmblogin', $this->data );
        $this->load->view('pmb/pmbfooter', $this->data );
    }

	public function signin(){
		$uid = $this->input->post('uid');
		$pass = $this->input->post('pass');

		if( $this->pmbuser_model->email_registered( $uid ) ){
			if( $this->pmbuser_model->agen( $uid ) || $this->pmbuser_model->maba( $uid ) ) {
				$_SESSION['klas'] = $this->pmbuser_model->data['klas'];

				if( $this->pmbuser_model->data['status_aktif']=='A' ) {
					if( password_verify( $pass, $this->pmbuser_model->data['password'] ) ) {
						$_SESSION['id'] = $this->pmbuser_model->data['id'];
						$_SESSION['nama'] = $this->pmbuser_model->data['nama_lengkap'];
						$_SESSION['email'] = $this->pmbuser_model->data['email'];
						$_SESSION['token'] = $this->pmbuser_model->data['token'];

						$gotoprofil = "";
						if( $this->pmbuser_model->data['telepon_valid']=='0000-00-00 00:00:00' ) $gotoprofil = "/profil";

						if( $_SESSION['klas'] == 'agen') {
							$_SESSION['pan'] = $this->pmbuser_model->data['pan'];
							// redirect('/pmb/a'.$gotoprofil);
							redirect('/pmb/pan/maba');
						}

						if( $_SESSION['klas'] == 'maba') redirect('/pmb/m'.$gotoprofil);

					} else {
						session_destroy();
						redirect('/pmb/signin/2');
					}
				} else {
					redirect('/pmb/signin/4');
				}
			}
		} else {
			redirect('/pmb/signin/1');
		}
	}

	public function signout(){
		session_destroy();
		redirect('/pmb');
	}

	public function rewardprogram( $tahun ){
		$this->data['public'] = true;

		$this->data['tahun'] = $tahun;
		$this->load->view('pmb/pmbheader', $this->data );
		$this->load->view('pmb/pmbrewardprogram', $this->data );
		$this->load->view('pmb/pmbfooter', $this->data );

	}

	public function syaratagen(){
		$this->data['public'] = true;
		// $this->data['tahun'] = $tahun;
		$this->data['judul1'] = "Reward Program PMB 2017";
		$this->data['judul2'] = "Syarat dan Ketentuan";
		$this->load->view('pmb/pmbheader', $this->data );

		// echo $this->markdown->parse_file( '/assets/text/syaratagen.md' );

		$this->load->view('pmb/pmbsyaratagen', $this->data );
		$this->load->view('pmb/pmbfooter', $this->data );

	}

	public function syaratmaba(){
		$this->data['public'] = true;
		// $this->data['tahun'] = $tahun;
		$this->data['judul2'] = "Persyaratan Calon Mahasiswa";
		$this->load->view('pmb/pmbheader', $this->data );
		$this->load->view('pmb/pmbmaba_syarat', $this->data );
		$this->load->view('pmb/pmbfooter', $this->data );

	}

	public function jadwalmaba(){
		$this->data['public'] = true;
		// $this->data['tahun'] = $tahun;
		$this->data['judul2'] = "JADWAL";
		$this->load->view('pmb/pmbheader', $this->data );
		$this->load->view('pmb/pmbmaba_jadwal', $this->data );
		$this->load->view('pmb/pmbfooter', $this->data );

	}

	public function biayamaba(){
		$this->data['public'] = true;
		// $this->data['tahun'] = $tahun;
		$this->data['judul2'] = "Pembiayaan";
		$this->load->view('pmb/pmbheader', $this->data );
		$this->load->view('pmb/pmbmaba_biaya', $this->data );
		$this->load->view('pmb/pmbfooter', $this->data );

	}

	public function regagent(){
		$this->data['public'] = true;

		$this->data['judul2'] = "Registrasi Reward Program";
		$this->load->view('pmb/pmbheader', $this->data );
		$this->load->view('pmb/regagent', $this->data );
		$this->load->view('pmb/pmbfooter', $this->data );
	}

	public function registeragent(){
		$this->pmbuser_model->registerAgen();

		$this->send_sms_verifikasi();
		$this->requestverifyemail('agen');
	}

	public function profilagent(){
		$this->pmbuser_model->agen($_SESSION['id']);
		$this->data['agen'] = $this->pmbuser_model->data;
		$_SESSION['nama'] = $this->pmbuser_model->data['nama_lengkap'];

		$this->data['judul2'] = "Agen: ".$_SESSION['nama'];
		$this->data['page'] = 'profilagen';

		$this->load->view('pmb/pmbheader', $this->data );
		$this->load->view('pmb/profilagen', $this->data );
		$this->load->view('pmb/pmbfooter', $this->data );
	}

	public function viewagent(){
		if( !isset($_SESSION['id']) ) redirect("/pmb");

		$this->data['judul2'] = "Agen: ".$_SESSION['nama'];
		$this->pmbuser_model->agen($_SESSION['id']);

		$refpath = base_url().'pmb/m/reg/';
		$qrpath = 'assets/img/qr/agen/';

		if( !empty($this->pmbuser_model->data['kode_referrer']) ) {
			$kode_referrer = $this->pmbuser_model->data['kode_referrer'];
			$mybitly = $this->pmbuser_model->data['bitly_url'];
		} else {
			$kode_referrer = $this->pmb_lib->kode_referrer();
			$mybitly = $this->bitly->shorten( $refpath.$kode_referrer );

			// generate QR-Code
			$myqrlink = $refpath . $this->pmb_lib->kode_referrer( true );
			$params['data'] = $myqrlink;
			$params['level'] = 'M';
			$params['size'] = 6;
			// $params['savename'] = FCPATH . str_replace('/','\\', $qrpath) . $_SESSION['id'] . '.png';
			$params['savename'] = FCPATH . $qrpath . $_SESSION['id'] . '.png';
			$this->ciqrcode->generate($params);

			// simpan Kode Referrer dan Bitly URL ke database
			$this->pmbuser_model->updatelink( $kode_referrer, $mybitly );
		}

		$mylink = $refpath . $kode_referrer;
		$myqrimg = base_url() . $qrpath . $_SESSION['id'] . '.png';

		$this->data['mylink'] = $mylink;
		$this->data['mybitly'] = $mybitly;
		$this->data['myqrimg'] = $myqrimg;

		$this->load->view('pmb/pmbheader', $this->data );
		$this->load->view('pmb/pmbviewagen', $this->data );
		$this->load->view('pmb/pmbfooter', $this->data );
	}

	public function viewmaba(){
		if( !isset($_SESSION['id']) ) redirect("/pmb");

		$this->pmbuser_model->maba($_SESSION['id']);
		$this->data['maba'] = $this->pmbuser_model->data;
		$_SESSION['nama'] = $this->pmbuser_model->data['nama_lengkap'];

		$this->data['judul1'] = "Calon MABA ".$this->tahun;
		$this->data['judul2'] = $_SESSION['nama'];

		$this->load->view('pmb/pmbheader', $this->data );
		$this->load->view('pmb/pmbviewmaba', $this->data );
		$this->load->view('pmb/pmbfooter', $this->data );
	}

	public function profilmaba(){
		if( !isset($_SESSION['id']) ) redirect("/pmb");

		$this->pmbuser_model->maba($_SESSION['id']);
		$this->data['maba'] = $this->pmbuser_model->data;
		$_SESSION['nama'] = $this->pmbuser_model->data['nama_lengkap'];

		$this->data['judul1'] = "Calon MABA ".$this->tahun;
		$this->data['judul2'] = $_SESSION['nama'];
		$this->data['page'] = 'profilmaba';

		$this->load->view('pmb/pmbheader', $this->data );
		$this->load->view('pmb/profilmaba', $this->data );
		$this->load->view('pmb/pmbfooter', $this->data );
	}

	public function regmaba($ref=''){
		$this->data['public'] = true;

		// jika ada referrer
		if( $ref != '') {
			$this->verify_referrer( $ref );
		} else {
			session_destroy();
		}

		$this->data['judul2'] = "Registrasi Calon Mahasiswa";
		$this->load->view('pmb/pmbheader', $this->data );
		$this->load->view('pmb/regmaba', $this->data );
		$this->load->view('pmb/pmbfooter', $this->data );

	}

	public function registermaba(){
		$this->pmbuser_model->registerMaba();
		//$this->requestverifysms();
		//$this->send_sms_verifikasi();
		// $this->requestverifyemail('maba');  /* temporary inactivated due to security and email problem */

		$this->data['judul2'] = "Sukses Registrasi";
		$this->load->view('pmb/pmbheader', $this->data );
		$this->load->view('pmb/pmbregistrasisukses', $this->data );
		$this->load->view('pmb/pmbfooter', $this->data );
	}

	private function verify_referrer( $ref ){
		$dekode = base64_decode( substr( $ref, 1, strlen($ref)-1 ) );

		$urlr = $this->agent->referrer();
		if( substr( $ref, 0, 1 ) == 'Q' ) $urlr = 'QR';

		$referrer = $this->pmbuser_model->agen( substr( $dekode, -10) );
		if( md5( $referrer['id'].$referrer['token'] ) == substr( $dekode, 0, strlen($dekode)-10 ) ) {
			$_SESSION['ref'] = $ref;
			$_SESSION['ref_id'] = $referrer['id'];
			$_SESSION['ref_token'] = $referrer['token'];

			$this->pmbuser_model->referrer_log( $urlr );
		}
	}

	public function requestverifyemail( $who ){

		$link = $this->pmb_lib->link_verifikasi_email();
		$this->send_email_verifikasi( $who, $_SESSION['email'], $link);

		//redirect
		redirect("/pmb/verify/email");
		// echo $link;    // DEBUG ONLY, comment-out line above
	}


	public function send_sms_verifikasi(){

		$hp_data = $this->pmbuser_model->get_hp_data( $_SESSION['email'] );

		$this->sms_global->to( $hp_data['telepon'] );
		$this->sms_global->message("Masukkan kode ini: ".$hp_data['telepon_pin'].
							       "\nuntuk proses verifikasi nomor HP Anda pada Layanan PMB SISKA\n--DO NOT REPLY!--");
		$this->sms_global->send();

		// $id = $this->sms_global->get_sms_id(); // this is the sms id
// echo $id;
		// $this->sms_global->print_debugger(); // only use this to output the message details on screen for debugging
	}

	public function infoverifyemail(){
		$this->data['public'] = true;

		$this->data['judul2'] = "Verifikasi Email";
		$this->load->view('pmb/pmbheader', $this->data );
		$this->load->view('pmb/pmbverifikasiemail', $this->data );
		$this->load->view('pmb/pmbfooter', $this->data );
	}

	public function suksesverifyemail(){
		$this->data['public'] = true;

		$this->data['judul2'] = "Sukses Verifikasi Email";
		$this->load->view('pmb/pmbheader', $this->data );
		$this->load->view('pmb/pmbverifikasiemailsukses', $this->data );
		$this->load->view('pmb/pmbfooter', $this->data );
	}

	public function gagalverifyemail(){
		$this->data['public'] = true;

		$this->data['judul2'] = "Verifikasi Email Tidak Berhasil";
		$this->load->view('pmb/pmbheader', $this->data );
		$this->load->view('pmb/pmbverifikasiemailgagal', $this->data );
		$this->load->view('pmb/pmbfooter', $this->data );
	}

	public function suksesverifysms(){
		$this->data['public'] = true;

		if( isset($_SESSION['id'] ) ) {
			if( $_SESSION['klas'] == 'agen') redirect("pmb/a/profil");
			else redirect("pmb/m/profil");
		}

		$this->data['judul2'] = "Sukses Verifikasi PIN SMS";
		$this->load->view('pmb/pmbheader', $this->data );
		$this->load->view('pmb/pmbverifikasismssukses', $this->data );
		$this->load->view('pmb/pmbfooter', $this->data );
	}

	public function gagalverifysms(){
		$this->data['public'] = true;

		$this->data['judul2'] = "Gagal Verifikasi PIN SMS";
		$this->load->view('pmb/pmbheader', $this->data );
		$this->load->view('pmb/pmbverifikasismsgagal', $this->data );
		$this->load->view('pmb/pmbfooter', $this->data );
	}

	public function requestverifysms(){
		// reset PIN sms
		if( $this->pmbuser_model->reset_sms_pin( $_SESSION['email'] ) ) {
			// kirim sms verifikasi
			$this->send_sms_verifikasi();
		}
		redirect("/pmb/verify/smspinreset");
	}

	public function requestverifysmssent(){
		$this->data['public'] = true;

		$this->data['judul2'] = "Verifikasi PIN SMS";
		$this->load->view('pmb/pmbheader', $this->data );
		$this->load->view('pmb/pmbverifikasismsreset', $this->data );
		$this->load->view('pmb/pmbfooter', $this->data );
	}


	public function verifysms(){
		if ( $this->pmbuser_model->verifikasi_sms_pin( $_SESSION['email'], $this->input->post('inputpin'), 'agen' ) ){
			if( !empty( $this->input->post('page') ) ) redirect("/pmb/a/".$this->input->post('page') );
			else redirect("/pmb/verify/smssukses");
		} elseif ( $this->pmbuser_model->verifikasi_sms_pin( $_SESSION['email'], $this->input->post('inputpin'), 'maba' ) ){
			if( !empty( $this->input->post('page') ) ) redirect("/pmb/m/".$this->input->post('page'));
			else redirect("/pmb/verify/smssukses");
		} else {
			redirect("/pmb/verify/smspininvalid");
		}
	}

	public function verify( $kode ){
		$verkode = explode(";", base64_decode( $kode ));

		switch ($verkode[0]) {
			case 'E':	// verifikasi email AGEN atau MABA
					if ( $this->pmbuser_model->verifikasi_mail_token( $verkode[1], $verkode[2], 'agen' ) ){
						redirect("/pmb/verify/emailsukses");
					} elseif ( $this->pmbuser_model->verifikasi_mail_token( $verkode[1], $verkode[2], 'maba' ) ){
						$this->send_email_welcomemaba();
						redirect("/pmb/verify/emailsukses");
					} else {
						redirect("/pmb/verify/emailinvalid");
					}
				break;
			case 'R':	// verifikasi Request Ganti Nomor HP
				if ( $this->pmbuser_model->verifikasi_token( $verkode[1], $verkode[2], 'agen' ) ||
					 $this->pmbuser_model->verifikasi_token( $verkode[1], $verkode[2], 'maba' )
					) redirect("/pmb/gantinomorhp");
				else {
					session_destroy();
					redirect("/pmb");
				}
				break;
			case 'S':	// verifikasi Request Ganti Alamat Email
				if ( $this->pmbuser_model->verifikasi_token( $verkode[1], $verkode[2], 'agen' ) ||
					 $this->pmbuser_model->verifikasi_token( $verkode[1], $verkode[2], 'maba' )
					) echo redirect("/pmb/gantiemail");
				else {
					session_destroy();
					redirect("/pmb");
				}
				break;
			case 'T':	// verifikasi kartu test MABA
				break;
		}
	}


	public function gantipassword(){
		if( $this->pmbuser_model->agen( $_SESSION['id'] ) || $this->pmbuser_model->maba( $_SESSION['id'] ) ) {
			if( password_verify( $this->input->post('oldpassword'), $this->pmbuser_model->data['password'] ) ) {
				if( $this->pmbuser_model->updatePassword( $_SESSION['klas'], $_SESSION['id'], $this->input->post('password') ) ){
					echo "0";
				} else {
					echo "2";
				}

			} else {
				echo "1";
			}
		} else {
			echo "2";
		}
	}

	public function lupapassword( $kode = '0'){
		$this->data['public'] = true;
		$this->data['kode'] = $kode;
		$this->data['judul2'] = "Reset Password";

		$this->load->view('pmb/pmbheader', $this->data );
		$this->load->view('pmb/pmblupapassword', $this->data );
		$this->load->view('pmb/pmbfooter', $this->data );
	}

	public function resetpassword(){
		$email = $this->input->post('email') ;
		if( $this->pmbuser_model->email_registered( $email ) ){
			// ganti password
			$new = $this->pmbuser_model->resetPassword( $email );

			// kirim ke email
			$this->email->from('layanan_pmb@kharisma.ac.id', 'PMB - STMIK KHARISMA');
			$this->email->to($email);
			$this->email->set_mailtype('html');
			$this->email->subject('Password Baru Anda');

			$data['tampilkanvisi'] = FALSE;

			$data['pesan'] = "<p>Anda atau seseorang telah mengajukan permintaan Reset Password (Lupa Password) pada layanan
								 PMB STMIK KHARISMA Makassar.
							  </p>
							  <p>Berikut ini adalah password baru Anda:</p>

							  <p><strong>$new</strong></p>

							  <p>Segera lakukan login ke <a href='".base_url()."pmb'>layanan PMB STMIK KHARISMA</a> dengan
							     menggunakan password baru Anda, dan segera lakukan penggantian password yang lebih aman.
							  </p>
							  <p>Jika Anda tidak merasa pernah mengajukan permintaan Reset Password, segera laporkan
							     kepada kami (untuk sementara melalui email: sofyan.thayf@kharisma.ac.id), dan kami akan
								 melakukan pemblokiran sementara akun Anda, untuk alasan keamanan.
							  </p>
							  <p>Terima kasih atas kerjasamanya.</p>
	 						 ";
			$this->email->message( $this->load->view('emails/mail_template', $data, true) );

			$this->email->send();
			redirect("/pmb/signin/7");
		} else {
			redirect("/pmb/signin/1");
		}
	}

	public function gantinomorhp( $status = '0'){
		$this->data['judul2'] = "Ganti Nomor HP";
		$this->data['status'] = $status;

		$this->load->view('pmb/pmbheader', $this->data );
		$this->load->view('pmb/pmbgantinomorhp', $this->data );
		$this->load->view('pmb/pmbfooter', $this->data );
	}

	public function resetnomorhp(){
		if($this->pmbuser_model->resetNomorHP( $_SESSION['id'], $this->input->post('nomorhp'), $this->input->post('password')  )){
			$this->send_sms_verifikasi();
			if($_SESSION['klas'] == 'agen') redirect("/pmb/a/profil");
			elseif($_SESSION['klas'] == 'maba') redirect("/pmb/m/profil");
		} else {
			redirect("/pmb/gantinomorhp/1");
		}
	}

	public function gantialamatemail( $status = '0' ){
		$this->data['judul2'] = "Ganti Alamat E-mail";
		$this->data['status'] = $status;

		$this->load->view('pmb/pmbheader', $this->data );
		$this->load->view('pmb/pmbgantialamatemail', $this->data );
		$this->load->view('pmb/pmbfooter', $this->data );
	}

	public function resetalamatemail(){
		if($this->pmbuser_model->resetAlamatEmail( $_SESSION['id'], $this->input->post('email'), $this->input->post('password')  )){
			// $this->send_sms_verifikasi();
			if($_SESSION['klas'] == 'agen') redirect("/pmb/a/profil");
			elseif($_SESSION['klas'] == 'maba') redirect("/pmb/m/profil");
		} else {
			redirect("/pmb/gantiemail/1");
		}
	}

	public function dokumen( $status=0 ){
		$this->data['judul2'] = "Dokumen";
		$this->data['status'] = $status;

		$this->load->view('pmb/pmbheader', $this->data );
		$this->load->view('pmb/pmbuploaddokumen', $this->data );
		$this->load->view('pmb/pmbfooter', $this->data );
	}

	public function upload(){
		$exts = array("pdf", "jpg", "png");

		$ext = pathinfo($_FILES['filedokumen']['name'], PATHINFO_EXTENSION);
		if ( !in_array($ext, $exts) )	redirect("/pmb/dokumen/4");

		$file = $_SESSION['id']."_".$this->input->post('jenisdokumen').".".$ext;
		if( move_uploaded_file($_FILES['filedokumen']['tmp_name'], './assets/dokumen/maba/2017/'.$file) ){
			$stat = $this->pmbuser_model->updateDokumen( $this->input->post('jenisdokumen'), $ext );
			redirect("/pmb/dokumen/".$stat);
		} else {
			redirect("/pmb/dokumen/3");
		}
	}

/***  sending email  funcions ***/

	private function send_email_verifikasi( $who, $email, $link )
	{
		$this->email->from('layanan_pmb@kharisma.ac.id', 'PMB - STMIK KHARISMA');
		$this->email->to($email);
		$this->email->set_mailtype('html');
		$this->email->subject('Verifikasi Email');

		$data['tampilkanvisi'] = FALSE;

		$sbg = "";
		if( $who == 'maba' ) {
			$sbg = " sebagai <strong>Calon Mahasiswa Baru</strong>";
		} elseif ( $who == 'agen' ) {
			$sbg = " sebagai Agen dalam <strong>Program Reward Point</strong>";
		}

		$data['pesan'] = "<p>Anda atau seseorang telah mendaftarkan E-mail Anda ($email) $sbg pada
		  					 layanan Penerimaan Mahasiswa Baru STMIK KHARISMA Makassar.
						  </p>
						  <p>Jika benar Anda melakukan pendaftaran, kami perlu melakukan verifikasi apakah alamat
						  E-mail Anda valid. Klik link berikut ini untuk verifikasi:</p>

						  <p><a href='$link'><button>Verifikasi Alamat E-mail Saya</button></a></p>

						  <p>Jika link tidak berfungsi, <i>copy</i> dan <i>paste</i> link berikut ke browser Anda:<br />$link
						  </p>
						  <p>Jika Anda tidak merasa pernah melakukan pendaftaran pada program kami, maka abaikan
						     email ini.
						  </p>
						  <p>Terima kasih atas kerjasamanya.</p>
 						 ";
		$this->email->message( $this->load->view('emails/mail_template', $data, true) );

		$this->email->send();

	}

	private function send_email_welcomemaba(){
		$this->email->from('layanan_pmb@kharisma.ac.id', 'PMB - STMIK KHARISMA');
		$this->email->to( $_SESSION['email'] );
		$this->email->set_mailtype('html');
		$this->email->subject('Selamat Datang Calon Mahasiswa Baru STMIK KHARISMA');

		$this->pmbuser_model->maba( $_SESSION['email'] );

		$data['tampilkanvisi'] = FALSE;
		$data['tahun'] = $this->tahun;
		$data['nama_lengkap'] = $this->pmbuser_model->data['nama_lengkap'];
		$data['link'] = base_url().'pmb/login';

		$mailmessage = $this->load->view('emails/mail_header', $data, true);
		$mailmessage .= $this->load->view('emails/pmb_selamatdatangcalonmaba', $data, true);
		$mailmessage .= $this->load->view('emails/mail_footer', $data, true);
		$this->email->message( $mailmessage );
// echo $mailmessage;
		$this->email->send();
	}

}
