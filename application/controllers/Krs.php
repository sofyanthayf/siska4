<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Krs extends CI_Controller
{

	public $mhs;

	public function __construct()
	{
		parent::__construct();
		$_SESSION['controller'] = $this->router->fetch_class();

		$this->load->model('mahasiswa_model');
		$this->load->model('matakuliah_model');
		$this->load->model('prodi_model');
		$this->load->model('krs_model');
	}

	public function index()
	{
		redirect('/krs/' . $this->session->uid);
	}

	public function overview($nim)
	{
		if (!$this->siska->uidMahasiswaValid($nim)) redirect('/krs/' . $this->session->uid);

		if ($this->mhs = $this->mahasiswa_model->setNim($nim)) {
			$data['mhs'] = $this->mhs;
			$this->session->page = 'krs';

			$data['judul1'] = "Rencana Studi ";
			$data['judul2'] = $this->mhs['nimhs'] . " " . $this->mhs['namamhs'];
			$data['krs'] = $this->krs_model->listkrs($nim);
			$data['krs_extend'] = $this->krs_model->krsExtend($nim);

			$data['krs_baru'] = $this->krs_model->krsSekarang() ? $this->krs_model->krsSekarang() : $this->krs_model->krsExtend($nim);
			// var_dump( $data['krs_baru'] );
			$this->load->template('krs/krs', $data);
		}
	}

	public function edit($nim, $kodesmt, $status)
	{

		if ($this->mhs = $this->mahasiswa_model->setNim($nim)) {

			$aksesOK = false;
			if (($this->siska->uidMahasiswaValid($nim)) ||
				($this->mhs['dosen_pa']['nidn'] == $this->session->uid)
			) {
				$aksesOK = true;
			}

			if (!$aksesOK) redirect('/krs/' . $this->session->uid);

			$data['mhs'] = $this->mhs;
			$this->session->page = 'krs';

			$data['judul1'] = "Rencana Studi ";
			$data['judul2'] = $this->mhs['nimhs'] . " " . $this->mhs['namamhs'];

			$data['kodesmt'] = $kodesmt;
			$data['krs_status'] = $status;

			$data['krs_in_progress'] = $this->krs_model->krssemester_byMhs($nim, $kodesmt);
			
			if (count($this->mhs['ips']) > 0) {
				foreach ($this->mhs['ips'] as $ips => $value) {
					$refsmt = $value['kodesmt'];
					$lastips = $value['ip'];
				}
				$data['sksmax'] = $this->siska->sksMax($lastips);
				$data['refsmt'] = $refsmt;
				$data['refips'] = $lastips;
			} else {
				// Mahasiswa Baru
				$data['refsmt'] = 'MABA';
				$data['refips'] = '0.0';
				$data['sksmax'] = 21;
			}

			$data['matakuliah'] = $this->matakuliah_model->matakuliahKrsMahasiswa($this->mhs['tahunkur'], $this->mhs['prodi'], $this->mhs['kompetensi'], $this->mhs['nimhs']);
			$this->load->template('krs/editkrs', $data);
		}
	}

	public function krsprodi()
	{
		if ($this->session->who != 'dsn') redirect("/");

		$data['judul1'] = "Rencana Studi ";
		$data['judul2'] = "Mahasiswa Program Studi";

		$this->load->template('krs/krsprodi', $data);
	}

	public function ext_request($nim, $kodesmt)
	{
		if (!$this->siska->uidMahasiswaValid($nim)) redirect('/krs/' . $this->session->uid);

		if ($this->mhs = $this->mahasiswa_model->setNim($nim)) {
			$data['mhs'] = $this->mhs;
			$data['kodesmt'] = $kodesmt;
			$data['judul1'] = "Rencana Studi ";
			$data['judul2'] = "Permohonan Tambahan Waktu Pengisian KRS";
			$this->load->template('krs/extend_req', $data);
		}
	}

	public function ext_validate($nim, $kodesmt)
	{
		// if( !$this->siska->uidMahasiswaValid($nim) ) redirect( '/krs/'.$this->session->uid );

		if ($this->mhs = $this->mahasiswa_model->setNim($nim)) {
			$data['mhs'] = $this->mhs;
			$data['kodesmt'] = $kodesmt;
			$data['judul1'] = "Rencana Studi ";
			$data['judul2'] = "Validasi Permohonan Tambahan Waktu Pengisian KRS";
			$data['krs_ext'] = $this->krs_model->getExtendRequest($kodesmt, $nim);
			$this->load->template('krs/extend_validate', $data);
		}
	}

	public function ext_accept()
	{
		$data['id'] = $this->input->post('ext_id');
		$data['batas_akhir'] = $this->input->post('tglext');
		$this->krs_model->updateExtendRequest($data);

		redirect('/krs/prodi');
	}

	public function konfirmasi_bayar($idkrs, $status = 0)
	{
		$data['krs'] = $this->krs_model->krssemester_byId($idkrs);
		$data['status'] = $status;

		if (!$this->siska->uidMahasiswaValid($data['krs']['nimhs'])) redirect('/krs/' . $this->session->uid);

		if ($this->mhs = $this->mahasiswa_model->setNim($data['krs']['nimhs'])) {
			$data['mhs'] = $this->mhs;
			$data['judul1'] = "Rencana Studi ";
			$data['judul2'] = "Konfirmasi Pembayaran";

			$this->load->template('krs/krskonfirmasibayar', $data);
		}
	}

	public function konfirm_upload()
	{
		$idkrs = $this->input->post('idkrs');
		$notlp = $this->input->post('nomortlp');
		$krs = $this->krs_model->krssemester_byId($idkrs);

		$exts = array("jpg", "png");

		$ext = pathinfo($_FILES['buktitrf']['name'], PATHINFO_EXTENSION);
		if (!in_array($ext, $exts))	redirect("/krs/konfirmasi/" . $idkrs . "/3");

		$file = $krs['nimhs'] . "." . $ext;
		if (move_uploaded_file($_FILES['buktitrf']['tmp_name'], './assets/dokumen/trkrs/' . $krs['kodesmt'] . '/' . $file)) {
			$datakrs['tanggal_konfirmasi'] = date('Y-m-d H:i:s');
			$this->krs_model->updateValidasiKrs($idkrs, $datakrs);
			$this->mahasiswa_model->api_profile_update($krs['nimhs'], array('telepon' => $notlp));
			redirect("/krs/konfirmasi/" . $idkrs . "/1");
		} else {
			redirect("/krs/konfirmasi/" . $idkrs . "/2");
		}
	}

	public function requestnew( $kodesmt, $status = 0 )
	{
		// if (!$this->siska->uidMahasiswaValid($data['krs']['nimhs'])) redirect('/krs/' . $this->session->uid);

		if ($this->mhs = $this->mahasiswa_model->setNim( $this->session->uid )) {
			$data['mhs'] = $this->mhs;
			$data['kodesmt'] = $kodesmt;
			$data['status'] = $status;

			$data['judul1'] = "Rencana Studi ";
			$data['judul2'] = "Request Slip Pembayaran";

			$this->load->template('krs/requestnew', $data);
		}
	}

	public function konfirm_slip_request()
	{
		$nimhs = $this->input->post('nimhs');
		$kodesmt = $this->input->post('kodesmt');
		$opsibayar = $this->input->post('opsibayar');

		$dmhs = $this->mahasiswa_model->setNim( $nimhs);
		$dmhs['telepon'] = $this->input->post('nomortlp');

		$dkrs = array( 'kodesmt' => $kodesmt, 'opsibayar' => $opsibayar);
		
		$this->krs_model->newKrsId($dmhs, $dkrs);

		redirect("/krs/new/" . $kodesmt . "/1");
	}
}
