<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Publik extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$_SESSION['controller'] = $this->router->fetch_class();

		$this->load->model('user_model');
		$this->load->model('prodi_model');
		$this->load->model('region_model');
	}

	public function index($id)
	{
		$_SESSION['page'] = 'pub';
		if (!empty($_SESSION['uid']) && !empty($_SESSION['pin'])) {

			if ($this->user_model->isMahasiswa($id)) redirect(base_url() . 'mhs/' . $id);
			if ($this->user_model->isDosen($id)) redirect(base_url() . 'dsn/' . $id);
		} else {

			if ($this->user_model->isMahasiswa($id)) {

				$this->load->model('mahasiswa_model');

				if ($this->mhs = $this->mahasiswa_model->setNim($id)) {

					$data['judul1'] = "Mahasiswa";
					$data['judul2'] = $id;
					// var_dump($this->mhs);
					if (!empty($this->mhs['distrik_rumah']))
						$this->mhs['region'] = $this->region_model->getRegion($this->mhs['distrik_rumah']);
					if (!empty($this->mhs['sekolah_asal']))
						$this->mhs['asalsekolah'] = $this->region_model->getRegionSekolah($this->mhs['sekolah_asal']);

					/* info status */
					$data['status']['last_update'] = $this->mhs['modified_date'];
					$data['status']['last_login'] = $this->mhs['last_login'];
					$data['status']['ip_address'] = $this->mhs['ip'];

					$this->load->view('publik/pheader', $data);
					$this->load->view('mhs/mahasiswa', $data);
					// $this->load->view('publik/infomhs', $data );

				} else {
					redirect('/');
				}
			} elseif ($this->user_model->isDosen($id)) {
				$data['judul1'] = "Dosen";
				$data['judul2'] = $id;

				$this->load->view('publik/pheader', $data);
				$this->load->view('publik/infodsn', $data);
			}
		}

		$this->load->view('layout/footer');
	}


	public function fresults($id = '')
	{
		$_SESSION['page'] = 'pub';
		$this->load->model('feedback_model');

		$data['judul1'] = "Feedback Results";

		if ($id == '') {
			$data['angket'] = $this->feedback_model->angket_list();
			$data['judul2'] = "All Surveys";
		} else {
			$data['id'] = $id;
			$data['angket'] = $this->feedback_model->angket($id);
			$data['judul2'] = $data['angket']->title;
		}

		$this->load->view('publik/pheader', $data);
		$this->load->view('publik/fresults', $data);
		$this->load->view('layout/footer');
	}

	public function kurikulum($prodi = '57201')
	{
		$_SESSION['page'] = 'pub';
		$this->load->model('matakuliah_model');
		$this->load->model('prodi_model');

		$data['judul1'] = "Kurikulum";
		$data['judul2'] = "";
		$data['prodi'] = $this->prodi_model->getProdi($prodi);
		$data['matakuliah'] = $this->matakuliah_model->daftarMatakuliah('2023', $prodi);

		if( $this->session->uid ){
			$this->load->template('publik/kurikulum', $data);
		} else {
			$this->load->view('publik/pheader', $data);
			$this->load->view('publik/kurikulum', $data);
			$this->load->view('layout/footer');
		}

	}
}
