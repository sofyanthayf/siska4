<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

// class Mysiska_api extends CI_Controller {
class Dosen_api extends REST_Controller {

  public function __construct($config = 'rest') {

    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    parent::__construct();

    $this->load->model('api_model');
    $this->load->model('mahasiswa_model');
    $this->load->model('matakuliah_model');
    $this->load->model('dosen_model');
    $this->load->model('krs_model');
  }

  public function listmhswali_get()
  {
    $nidn = $this->get('nidn');
    $mhswali = $this->dosen_model->mahasiswaWali($nidn);
    if( !$mhswali ) {
      $this->response( 'No Data', 204  );
    } else {
      $this->response( [ "mhswali" => $mhswali ] , 200  );
    }
  }

}


?>
