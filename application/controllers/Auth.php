<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');

		$_SESSION['controller'] = $this->router->fetch_class();
	}

	public function index($kode = 0)
	{
		$data['kode'] = $kode;
		$this->load->view('auth/loginform', $data);
	}

	public function signin()
	{

		if ($this->user_model->setId($this->input->post('uid'))) {

			if ($this->user_model->data['password'] == md5($this->input->post('pass'))) {

				$_SESSION['last_login'] = date('Y-m-d H:i:s');
				$_SESSION['last_ip'] = $_SERVER['REMOTE_ADDR'];

				$this->user_model->setLastLogin();


				if (isset($this->user_model->data['kodeprogramstudi'])) {
					$_SESSION['kodeprodi'] = $this->user_model->data['kodeprogramstudi'];
				}

				if ($_SESSION['uid'] == '0929067001') {
					$_SESSION['admin'] = '1';
				} else {
					$_SESSION['admin'] = '0';
				}


				$_SESSION['akhir'] = time() + 1800;  // session expire 30 menit (1800 detik);

				$this->siska_log->setLog('L', '0', 'Logged in');
				if ($_SESSION['who'] == 'mhs') {
					redirect('/mhs/' . $_SESSION['uid']);
				} else {
					redirect(base_url());
				}
			} else {

				$this->siska_log->setLog('L', '1', 'Login gagal, password tidak cocok');
				session_destroy();
				redirect(base_url() . 'login/2');
			}
		} else {

			$this->siska_log->setLog('L', '1', 'Login gagal, user dengan ID:' . $this->input->post('uid') . ' tidak terdaftar');
			session_destroy();
			redirect(base_url() . 'login/1');
		}
	}

	public function signout()
	{
		$this->siska_log->setLog('L', '0', 'Logged out');
		session_destroy();
		redirect(base_url() . 'login/9');
	}

	public function lupapassword($kode = '0')
	{
		$this->data['public'] = true;
		$this->data['kode'] = $kode;
		$this->data['judul1'] = " ";
		$this->data['judul2'] = "Reset Password";

		$this->load->view('publik/pheader', $this->data);
		$this->load->view('auth/lupapassword', $this->data);
	}

	public function resetpassword()
	{
		$uid = $this->input->post('nid');
		$email = $this->input->post('email');

		if ($this->user_model->setId($uid)) {
			if ($this->user_model->data['email'] == $email) {

				$new = $this->user_model->resetPassword();
				$this->siska_log->setLog('U', '0', 'Reset password, user ID:' . $uid . ', email: ' . $email);

				// kirim ke email
				$this->email->from('siska@kharisma.ac.id', 'SISKA - STMIK KHARISMA');
				$this->email->to($email);
				$this->email->set_mailtype('html');
				$this->email->subject('Password Baru Anda pada SISKA');

				$data['tampilkanvisi'] = TRUE;

				$data['pesan'] = "<p>Anda atau seseorang telah mengajukan permintaan Reset Password (Lupa Password) pada layanan
									 Sistem Informasi Akademik STMIK KHARISMA Makassar (SISKA).
								  </p>
								  <p>Berikut ini adalah password baru Anda:</p>

								  <p><strong>$new</strong></p>

								  <p>Segeralah melakukan <a href='" . base_url() . "login'>login ke SISKA</a> (<a href='" . base_url() .
					"login'>" . base_url() . "</a>) dengan menggunakan password baru Anda, dan segera lakukan penggantian
										 password yang lebih aman.
								  </p>
								  <p>Jika Anda tidak merasa pernah mengajukan permintaan Reset Password, segera laporkan
								     kepada kami dengan mem-forward email ini (untuk sementara melalui email: sofyan.thayf@kharisma.ac.id),
										 dan kami akan melakukan penelusuran dan pemblokiran sementara akun Anda, untuk alasan keamanan.
								  </p>
								  <p>Terima kasih atas kerjasamanya.</p>
		 						 ";

				$this->email->message($this->load->view('emails/mail_template', $data, true));

				// $this->email->send();
				if (!$this->email->send()) {

					$mail_err = '';
					foreach ($this->email->get_debugger_messages() as $debugger_message) $mail_err .= $debugger_message;

					$this->siska_log->setLog('M', '3', 'Password baru untuk ID: ' . $uid . ' GAGAL dikirim ke alamat email: ' . $email . ': ' . $mail_err);
					$this->email->clear_debugger_messages();

					redirect('/lupapassword/3');
				} else {
					$this->siska_log->setLog('M', '0', 'Password baru untuk ID: ' . $uid . ' dikirim ke alamat email: ' . $email);
					redirect('/lupapassword/2');
				}
			}
		}

		$this->siska_log->setLog('U', '0', 'Reset password gagal, user ID:' . $uid . ', email: ' . $email . ' tidak sesuai');
		redirect('/lupapassword/1');
	}


	public function gantipassword()
	{
		if ($this->user_model->setId($this->session->uid)) {
			if ($this->user_model->data['password'] == md5($this->input->post('oldpassword'))) {

				if ($this->user_model->resetPassword($this->input->post('password'))) {
					$this->siska_log->setLog('U', '0', 'Change password, user ID:' . $this->session->uid . ' Success');
					echo "0";
				} else {
					echo "2";
				}
			} else {
				echo "1";
			}
		}
	}
}
