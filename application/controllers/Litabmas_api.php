<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

// class Mysiska_api extends CI_Controller {
class Litabmas_api extends REST_Controller {

  public function __construct($config = 'rest') {

    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    parent::__construct();

    $this->load->model('litabmas_model');
  }

  public function publikasi_get()
  {
    $ndin = $this->get('ndin');
    $publikasi = $this->litabmas_model->daftarpublikasi($ndin);
    if( !$publikasi ) {
      $this->response( "no data", 204  );
    } else {
      $n = 0;
      foreach ($publikasi as $key => $value) {
        $publikasi[$n]['authors'] = $this->litabmas_model->publikasiauthor($value['id_publikasi']);
        $n++;
      }
      $this->response( [ "publikasi" => $publikasi ] , 200  );
    }
  }

  public function daftarpenelitian_get()
  {
    $ndin = $this->get('nidn');

    $penelitian = $this->litabmas_model->daftarpenelitian($ndin);
    if( !$penelitian ) {
      $this->response( "no data", 204  );
    } else {
      $n = 0;
      foreach ($penelitian as $key => $value) {
        $penelitian[$n]['peneliti'] = $this->litabmas_model->peneliti($value['id_penelitian']);
        $n++;
      }
      $this->response( [ "penelitian" => $penelitian ] , 200  );
    }
  }


}
