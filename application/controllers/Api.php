<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

// class Mysiska_api extends CI_Controller {
class Api extends REST_Controller {

  public function __construct($config = 'rest') {

    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
  	parent::__construct();

  	$this->load->model('api_model');
  	$this->load->model('mahasiswa_model');
  	$this->load->model('dosen_model');
  	$this->load->model('prodi_model');
  	$this->load->model('kelas_model');
  }

  // http://siska4.kharisma.local/api/mhs/angkatan/2015/prodi/57201
  // function mhs_get(){
  //   $prodi = $this->get('prodi');
  //   $angkatan = $this->get('angkatan');
  //   $mhs = $this->mahasiswa_model->getListMahasiswa( $prodi, $angkatan );
  //   if( !$mhs || count($mhs)==0 ){
  //     $this->response( 'No Data', 204  );
  //   } else {
  //     $data = [ 'jumlah' => count($mhs),
  //               'list_mahasiswa' => $mhs ];
  //     $this->response( $data , 200  );
  //   }
  // }

  // http://siska4.kharisma.local/api/mhs/angkatan/2015/prodi/57201
  function mhs_get(){
    $prodi = $this->get('prodi');
    $angkatan = $this->get('angkatan');
    $mhs = $this->mahasiswa_model->getListMahasiswa( $prodi, $angkatan );
    if( !$mhs || count($mhs)==0 ){
      $this->response( 'No Data', 204  );
    } else {
      $m = 0;
      foreach( $mhs as $mh){
        $pa = $this->mahasiswa_model->getDosenPA( $mh['nim'] );
        $mhs[$m]['dosen_pa'] = $pa;
        $m++;
      }

      $data = [ 'jumlah' => count($mhs),
                'list_mahasiswa' => $mhs ];
      $this->response( $data , 200  );
    }
  }

    
  // http://siska4.kharisma.local/api/detailmhs/nim/51015013
  function detailmhs_get() {
    $mhs = $this->mahasiswa_model->api_detailmhs( $this->get('nim') );
    if( !$mhs ){
      $this->response( 'No Data', 204  );
    } else {
      $this->response( $mhs , 200  );
    }
  }

  // http://siska4.kharisma.ac.id/api/picmhs/nim/51016002
  function picmhs_get(){
    $pic = $this->mahasiswa_model->api_fotomhs( $this->get('nim') );
    if( !$pic ){
      $this->response( 'No Data', 204  );
    } else {
      $this->response( [ "foto" => $pic ] , 200  );
    }
  }

  // http://siska4.kharisma.local/api/prodi
  function prodi_get(){
    $prodi = $this->prodi_model->getProdiTahun();
    $this->response( ['program_studi' => $prodi] , 200  );
  }

  // http://siska4.kharisma.local/api/detailprodi/kode/57201
  function detailprodi_get(){
    $prodi = $this->prodi_model->getProdi($this->get('kode'));
    $this->response( ['program_studi' => $prodi] , 200  );
  }

  public function daftardosentetap_get()
  {
    $dosen = $this->dosen_model->daftarDosenTetap();
    $i = 0;
    foreach ($dosen as $key => $value) {
      $dosen[$i]['jabatan_fungsional'] = $this->siska->jabatanfungsional[ $value['kode_jabatan_fungsional'] ];
      $dosen[$i]['pendidikan_terakhir'] = $this->siska->jenjang[ $value['kode_pendidikan_terakhir'] ]['kode'];
      $i++;
    }
    $this->response( [ "dosen" => $dosen ] , 200  );
  }

  // http://siska.kharisma.local/api/kelasdosen/nidn/0929067001/smt/20181
  public function kelasdosen_get()
  {
    $semester = $this->get('smt');
    $dosen = $this->get('nidn');
    $kelas = $this->kelas_model->kelasDosen($dosen, $semester);

    if( !$kelas ){
      $this->response( 'No Data', 204  );
    } else {
      $i = 0;
      foreach ($kelas as $key => $value) {
        $mhs = $this->kelas_model->mahasiswaKelas( $kelas[$i]['kode_kelas'] );
        $kelas[$i]['jml_mahasiswa'] = count($mhs);
        $kelas[$i]['mahasiswa'] = $mhs;
        $i++;
      }

      $klsdosen = array( 'kode_semester' => $semester,
                         'nidn_dosen' => $dosen,
                         'jml_kelas' => count($kelas),
                         'kelas' => $kelas );

      $this->response( [ "kelas_dosen" => $klsdosen ] , 200  );
    }
  }

  // http://siska.kharisma.local/api/kelasmahasiswa/nim/51016002/smt/20171
  public function kelasmahasiswa_get()
  {
    $semester = $this->get('smt');
    $mahasiswa = $this->get('nim');
    $kelas = $this->kelas_model->kelasMahasiswa($mahasiswa, $semester);

    if( !$kelas ){
      $this->response( 'No Data', 204  );
    } else {
      $mhs = $this->mahasiswa_model->setNim( $mahasiswa );

      $klsmhs = array( 'kode_semester' => $semester,
                       'nimhs' => $mahasiswa,
                       'namamhs' => $mhs['namamhs'],
                       'jml_kelas' => count($kelas),
                       'kelas' => $kelas );

      $this->response( [ "kelas_mahasiswa" => $klsmhs ] , 200  );
    }

  }

}

?>
