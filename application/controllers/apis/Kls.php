<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once APPPATH . '/libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

class Kls extends REST_Controller
{

  public function __construct($config = 'rest')
  {
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    parent::__construct();

    $this->load->model('kelas_model');
  }

  public function list_get()
  {
    $prodi = $this->get('prd');
    $kodesmt = $this->get('smt');

    $listkls = $this->kelas_model->listKelas($prodi, $kodesmt);

    if (!$listkls) {
      $this->response('No Data', 204);
    } else {
      $this->response(["list_kelas" => $listkls], 200);
    }
  }

  public function listmhs_get()
  {
    $kodekelas = $this->get('kode');

    $listmhs = $this->kelas_model->mahasiswaKelas($kodekelas);

    if (!$listmhs) {
      $this->response('No Data', 204);
    } else {
      $this->response(["list_mhs" => $listmhs], 200);
    }
  }

  public function klsdsn_get()
  {
    $nidn = $this->get('nidn');
    $kodesmt = $this->get('smt');

    $listkls = $this->kelas_model->kelasDosen($nidn, $kodesmt);

    if (!$listkls) {
      $this->response('No Data', 204);
    } else {
      $this->response(["list_kelas" => $listkls], 200);
    }
  }
}
