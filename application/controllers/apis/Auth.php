<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Auth extends REST_Controller {

	public function __construct($config = 'rest')
	{
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		parent::__construct();

    $this->load->model('user_model');
	}


  public function changepass_post()
  {
    if( $this->user_model->setId( $this->session->uid ) ){
			if( $this->user_model->data['password'] == md5( $this->post('oldpassword') ) ) {

				if( $this->user_model->resetPassword( $this->post('password') ) ){
					$this->siska_log->setLog('U','0','Change password, user ID:'.$this->session->uid.' Success' );

					$this->response( ['status'=>'Password Changed', 'kode'=>0 ], 200);  // HTTP_OK
				} else {
					$this->response( ['status'=>'Failed', 'kode'=>2 ], 400);;  // HTTP_BAD_REQUEST
				}
			} else {
        $this->response( ['status'=>'Failed', 'kode'=>1 ], 400);;  // HTTP_BAD_REQUEST
			}
		} else {
      $this->response( ['status'=>'Unauthorized'], 401);;  // HTTP_UNAUTHORIZED
    }
  }



}
