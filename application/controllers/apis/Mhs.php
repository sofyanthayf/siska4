<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Mhs extends REST_Controller {

	public function __construct($config = 'rest')
	{
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		parent::__construct();

    $this->load->model('mahasiswa_model');
	}

  public function profile_update_post()
  {
    var_dump($this->post());
    $nimhs = $this->post('nimhs');
    $data = [
          'gol_darah' => $this->post('goldarah'),
          'agama' => $this->post('agama'),
          'no_ktp' => $this->post('noktp'),
          'tplahir' => $this->post('tplahir'),
          'tglahir' => $this->post('tglahir'),
          'alamat_rumah' => $this->post('alamat'),
          'telepon' => $this->post('telepon'),
          'telepon2' => $this->post('telepon2'),
          'email2' => $this->post('email2'),
          'facebook' => $this->post('facebook'),
          'linkedin' => $this->post('linkedin'),
          'gplus' => $this->post('gplus')
        ];

    if( $this->session->uid == $nimhs ) {
      $this->mahasiswa_model->api_profile_update( $nimhs, $data );
      $this->response( ['status'=>'updated'] , REST_Controller::HTTP_OK );  //200
    } else {
      $this->response( ['status'=>'FAILED'], REST_Controller::HTTP_UNAUTHORIZED );    //401
    }
  }


  public function ips_get()
  {
    $nimhs = $this->get('nim');
    $ips = $this->mahasiswa_model->getIPS( $nimhs );

    if( !$ips ){
      $this->response( 'No Data', 204  );
    } else {
      $this->response( [ "ip_semester" => $ips ] , 200  );
    }
  }


}


?>
