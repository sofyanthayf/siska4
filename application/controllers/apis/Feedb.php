<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once APPPATH . '/libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

class Feedb extends REST_Controller
{

  public function __construct($config = 'rest')
  {
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

    parent::__construct();

    $this->load->model('feedback_model');
    $this->load->model('mahasiswa_model');
    $this->load->model('dosen_model');
    $this->load->model('kelas_model');
    $this->load->model('matakuliah_model');
  }

  public function results_get()
  {
    $id_angket = $this->get('id');

    $results = $this->feedback_model->responses($id_angket);
    if (!$results) {
      $this->response('No Data', 204);
    } else {
      $this->response(["results" => $results], 200);
    }
  }


  public function likertdata_get()
  {
    $id = $this->get('id');
    $key = $this->get('key');
    $val = $this->get('val');
    $likert =  (object) array('id_angket' => $id, $key => $val);
    $likert->data = $this->feedback_model->likert_data($id, $key, $val);
    $likert->kategori = $this->feedback_model->question_categories($id, $val);

    if (!$likert->data) {
      $this->response('No Data', 204);
    } else {
      $this->response($likert, 200);
    }
  }

  public function responsesemester_get()
  {
    $id_angket = $this->get('id');
    $results = $this->feedback_model->response_semester($id_angket);

    if (!$results) {
      $this->response('No Data', 204);
    } else {
      $this->response(["semester" => $results], 200);
    }

  }


}
