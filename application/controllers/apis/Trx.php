<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Trx extends REST_Controller {

	public function __construct($config = 'rest')
	{
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        
        parent::__construct();

        $this->load->model('keuangan_model');
	}

    public function list_get()
    {
      $kodesmt = $this->get('smt');

      $listtrx = $this->keuangan_model->transaksi_krs( $kodesmt );

      if( !$listtrx ){
        $this->response( 'No Data', 204  );
      } else {
        $this->response( [ "list_trx" => $listtrx ] , 200  );
      }
    }


}
