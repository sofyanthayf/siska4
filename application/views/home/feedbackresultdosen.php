<div class="col-sm-12 col-md-10">

    <h3 id="titlekelas">Umpan Balik Mahasiswa</h3>
    <div class="panel panel-info">
        <div class="panel-body">

            <?php
            $id = "00300-0001";
            $listsmt = $this->feedback_model->response_semester($id);
            $listdosen = $this->feedback_model->response_dosen($id);
            ?>

            <h4><strong>Penilaian Mahasiswa Terhadap Proses Pembelajaran</strong></h4>

            <table style="width:85%;padding:20px;">
                <tr>
                    <td style="width:70%; vertical-align: top;">
                        <div id="figure" style="margin-top:30px;margin-bottom:30px;"></div>
                    </td>
                    <td style="vertical-align:top; padding-left:20px; padding-top:30px;">
                    </td>
                </tr>
            </table>

            <a href="/feedback/results"><strong>Lihat Semua Umpan Balik</strong></a>

            <script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.4.13/d3.min.js"></script>
            <script src="/assets/js/LikertChart.js"></script>

            <script>
                var url = "<?= base_url() ?>apis/feedb/likertdata/id/<?= $id ?>";
                var kodesmt = '<?= $listsmt[0]->kodesmt ?>';
                var nidn = '<?= $this->session->uid ?>'
                getLikertChart(url, 'nidn', nidn);
            </script>


        </div>
    </div>
</div>