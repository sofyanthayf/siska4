    <div class="col-sm-7 col-md-7">
        <h3 id="titlekelas">Kelas Mata Kuliah</h3>
        <div class="row">
            <div class="panel panel-info">

                <div class="panel-body">

                    <table class="display" id="klssmt" width="100%">
                        <thead>
                            <tr>
                                <th>Kode MK</th>
                                <th>Mata Kuliah</th>
                                <th>Prodi</th>
                                <th>sks</th>
                                <th>semester</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>


    <script>
        var smtdefault;
        var tablekls;


        $.get("/krs_api/semesterkrs", function(data) {
            smtdefault = data.semesterkrs[0].kodesmt;
            var title = $("#titlekelas").html();
            $("#titlekelas").html(title + " semester " + smtdefault);
            getListKelasDosen('<?= $_SESSION['uid'] ?>', smtdefault);
        });



        function getListKelasDosen(nidn, smt) {
            tablekls = $("#klssmt").DataTable({
                ajax: {
                    url: "/apis/kls/klsdsn/nidn/" + nidn + "/smt/" + smt,
                    type: "GET"
                },
                sAjaxDataProp: "list_kelas",
                columns: [{
                        "data": "kodemk",
                        "className": "dt-center"
                    },
                    {
                        "data": "namamk"
                    },
                    {
                        "data": "prodi"
                    },
                    {
                        "data": "sks",
                        "className": "dt-center"
                    },
                    {
                        "data": "smt",
                        "className": "dt-center"
                    }
                ],
                filter: false,
                info: false,
                paging: false,
                destroy: true

            });
        }
    </script>