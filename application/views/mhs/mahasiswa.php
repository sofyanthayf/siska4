<div class="row text-right">
  <div class="col-sm-12 col-md-12">
    <a href="/mhs"><i class="fa fa-fw fa-users"></i>Daftar Mahasiswa</a>
    <br>&nbsp;
  </div>
</div>

<?php
$this->load->view('mhs/overview');

// echo '<pre>' . var_export($this->mhs['pembimbing_ta'], true) . '</pre>';

if ($this->session->uid == $this->mhs['nimhs'] || $this->session->who == 'dsn') {
?>

  <div class="row" style="min-height:300px;">
    <div class="col-sm-2 col-md-2">
      <!-- Nav tabs -->
      <ul class="nav nav-tabs tabs-left">

        <li class="active">
          <a href="#akadm" data-toggle="tab">
            <i class="fa fa-fw fa-graduation-cap"></i>
            Akademik
          </a>
        </li>

        <?php   // khusus alumni
        if ($this->mhs['status'] == 'L') {
        ?>
          <li>
            <a href="#alumni" data-toggle="tab">
              <i class="glyphicon glyphicon-certificate"></i>
              Status Alumni
            </a>
          </li>

        <?php
        }
        ?>

        <li>
          <a href="#profile" data-toggle="tab">
            <i class="fa fa-fw fa-user"></i>
            Profil
          </a>
        </li>

        <?php   // bagian ini bukan untuk umum
        if (!empty($this->session->uid) && !empty($this->session->pin) && $this->session->uid == $this->mhs['nimhs']) {
        ?>

          <li>
            <a href="#konsul" data-toggle="tab">
              <i class="fa fa-fw fa-plus-circle"></i>
              Bimbingan
            </a>
          </li>
          <li>
            <a href="#account" data-toggle="tab">
              <i class="fa fa-fw fa-pencil-square"></i>
              Account
            </a>
          </li>

        <?php
        }

        if ($this->session->who == 'dsn') {
        ?>
          <li>
            <a href="#update" data-toggle="tab">
              <i class="fa fa-fw fa-user"></i>
              Update Data
            </a>
          </li>

        <?php
        }
        ?>


      </ul>
    </div>

    <div class="col-sm-7 col-md-7">
      <!-- Tab panes -->
      <div class="tab-content">

        <div class="tab-pane active" id="akadm">
          <?php $this->load->view('mhs/akademik'); ?>
        </div>

        <?php   // khusus alumni
        if ($this->mhs['status'] == 'L') {
        ?>
          <div class="tab-pane" id="alumni">
            <?php $this->load->view('mhs/alumni'); ?>
          </div>
        <?php
        }
        ?>

        <div class="tab-pane" id="profile">
          <?php $this->load->view('mhs/profil'); ?>
        </div>


        <?php   // bagian ini bukan untuk umum
        if (!empty($_SESSION['uid']) && !empty($_SESSION['pin'])) {
        ?>

          <div class="tab-pane" id="konsul">
            <?php $this->load->view('mhs/konsul'); ?>
          </div>

          <div class="tab-pane" id="account">
            <?php $this->load->view('auth/gantipassword'); ?>
          </div>

        <?php
        }

        if ($this->session->who == 'dsn') {
        ?>

          <div class="tab-pane" id="update">
            <?php $this->load->view('mhs/update'); ?>
          </div>


        <?php
        }
        ?>


      </div>

    </div>

    <?php
    // SISKA status bukan untuk umum
    if (!empty($_SESSION['uid']) && !empty($_SESSION['pin'])) {
    ?>
      <div class="col-sm-3 col-md-3">
        <?php $this->load->view('statususer'); ?>
      </div>
    <?php
    }

    if (empty($this->mhs['dtkompetensi'])) $kodekompt = substr($this->mhs['nimhs'], 0, 3);
    else $kodekompt = $this->mhs['dtkompetensi']['kode'];

    ?>

  </div>


  <script type="text/javascript">
    var nimhs = '<?= $this->mhs['nimhs'] ?>';
    var who = '<?= $this->session->who ?>'
    var prodi = '<?= $this->mhs['prodi'] ?>';
    var kompetensi = '<?= $kodekompt ?>';
    var tahunkur = '<?= $this->mhs['tahunkur'] ?>';
    var umpanbalik = '00300-0001';
    var umpanbalikskripsi = '00300-0002';

    var ipsmhs = {
      <?php
      $ipsmh = $this->mhs['ips'];
      foreach ($ipsmh as $ips) {
        echo "'" . $ips['kodesmt'] . "':'" . number_format($ips['ip'], 2, ",", ".") . "',\n";
      }
      ?>
    };
    refreshNilaiMK();
  </script>

<?php
}
?>


<script type="text/javascript">
  $.get("/mahasiswa_api/ips/nim/" + nimhs, function(data) {
    if (data && data.ip_semester.length > 1) {
      var labels = data.ip_semester.map(function(e) {
        return e.kodesmt;
      });
      var data = data.ip_semester.map(function(e) {
        return e.ip;
      });
      var ctx = document.getElementById("ipChart").getContext('2d');

      var ipChart = new Chart(ctx, {
        type: 'line',
        data: {
          labels: labels,
          datasets: [{
            label: 'Indeks Prestasi Semester',
            data: data,
            borderWidth: 3,
            backgroundColor: 'rgba(204, 0, 0, 0.2)'
          }]
        },
        options: {
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: true
              }
            }]
          },
          elements: {
            line: {
              tension: 0, // disables bezier curves
            }
          }
        }
      });
    } else {
      $("#ipChart").hide();
    }
  });

  $("#pilihansemester").change(function() {
    smt = $("#pilihansemester").val()
    if ($(this).val() == 'all') {
      $("#ips").html("");
      $("#ipsrow").hide();
    } else {
      $("#ips").html(ipsmhs[$(this).val()]);
      $("#ipsrow").show();
    }
    $('#daftarmkmhs').DataTable().ajax.reload(null, false)
  });

  <?php if (isset($this->mhs['pembimbing_ta'])) { ?>
    var pb = <?= json_encode($this->mhs['pembimbing_ta']) ?>;
  <?php  } ?>
  $('#daftarmkmhs').DataTable({
    "ajax": {
      "url": "/ajax/listmkmahasiswa",
      "type": "POST",
      "data": function(d) {
        d.nim = nimhs,
          d.prodi = prodi,
          d.kompetensi = kompetensi,
          d.tahunkur = tahunkur,
          d.smt = $("#pilihansemester").val(),
          d.dataroot = "listmk"
      }
    },
    "sAjaxDataProp": "listmk",
    "columns": [{
        "data": "kodemk",
        "className": "dt-center"
      },
      {
        "data": "namamk"
      },
      {
        "data": "sks",
        "className": "dt-center"
      },
      //  { "data" : "nilai", "className" : "dt-center" },
      {
        "render": function(data, type, full, meta) {
          if (full.feedback == null && who == 'mhs' &&
            full.kodesmt >= '20192' &&
            full.nilai != 'P'
          ) {
            if (full.dosen != null) {
              return "<a href='/feedback/" + umpanbalik + "/" +
                nimhs + "-" +
                full.kodesmt + "-" +
                full.kodekelas + "-" +
                full.dosen + "-" +
                full.kodemk +
                "' title='beri umpan balik pembelajaran di kelas mata kuliah ini'><img src='/assets/img/edit16.png'></a>";

            } else if (full.namamk == 'SKRIPSI') {
              var plink = "";
              pb.forEach((pn, i, arr) => {
                if (pn.feedback == null) {
                  plink += "<a href='/feedback/" + umpanbalikskripsi + "/" +
                    nimhs + "-" + pn.nidn + "-" + full.kodesmt + "-" + full.kodekelas +
                    "' title='beri umpan balik proses pembimbingan skripsi Anda oleh Pembimbing " + (i + 1) + "'> \
                  <img src='/assets/img/edit16.png'> P" + (i + 1) + "</a><br>"
                }
              });
              return plink;
            } else {
              return full.nilai;
            }
          } else {
            return full.nilai;
          }
        },
        className: "dt-center"
      },
      {
        "data": "kodesmt",
        "className": "dt-center"
      },
      {
        "data": "smt",
        "visible": false
      }
    ],
    "searching": false,
    "paging": false,
    "destroy": true,
    "order": [
      [5, 'asc']
    ],
    "drawCallback": function(settings) {
      var api = this.api();
      var rows = api.rows({
        page: 'current'
      }).nodes();
      var last = null;

      api.column(5, {
        page: 'current'
      }).data().each(function(smt, i) {
        if (last !== smt) {
          if (smt != 9) {
            str = "Semester " + smt;
          } else {
            str = "Mata Kuliah Pilihan";
          }

          $(rows).eq(i).before(
            '<tr class="group"><td colspan="5"> \
                                    <strong>' + str + '</strong></td></tr>'
          );

          last = smt;
        }
      });
    },
    "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
      if (aData['nilai'] == "P") {
        $('td', nRow).css('color', 'blue');
      } else if (aData['nilai'] == "E") {
        $('td', nRow).css('color', 'red');
      } else if (aData['nilai'] == "D") {
        $('td', nRow).css('color', 'orange');
      }
    }
  });
</script>