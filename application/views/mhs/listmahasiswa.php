<div class="col-sm-3 col-md-3">
    <br />
    <h4>Filter:</h4>

    <strong>Angkatan:</strong>
    <select class="form-control" id="angkatan">
        <?php
            foreach ( $this->mahasiswa_model->getListAngkatan() as $angkatan) {
                echo "<option value='".$angkatan['tahunmasuk']."'>".$angkatan['tahunmasuk']."</option>\n";
            }
        ?>
    </select>
    <br />

    <strong>Program Studi:</strong>
    <div class="list-group" id="listprodi">
        <!-- daftar prodi dikerjakan oleh AJaX -->
    </div>

    <h4>Search:</h4>
    <div class="input-group">
        <span class="input-group-addon" id="searchtext"><i class="fa fa-fw fa-search"></i></span>
        <input type="text" class="form-control" placeholder="NIM / Nama Mahasiswa" aria-describedby="searchtext" id="searchtextkey">
    </div>
</div>
<br>

<div class="col-sm-6 col-md-6">
        <div class="panel panel-info">
            <div class="panel-heading">
                <strong>
                    <span id="disp_prodi"></span> - <span id="disp_tahun"></span>
                </strong>
            </div>
            <div class="panel-body">
                <table class="display" id="listmahasiswa" data-order='[[ 0, "asc" ]]'
                                                          data-page-length='50'
                                                          cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th width="40px">N I M</th>
                            <th>Nama Mahasiswa</th>
                            <th width="20px">status</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
</div>
