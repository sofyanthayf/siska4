<div class="row">
    <!-- Foto -->

    <div class="col-sm-3 col-md-3">

    <?php   if( $this->mhs['foto'] ) {   ?>

            <div class="imgprofile">
                <img id='imgprofile' src="<?=$this->mhs['foto']?>">
            </div>

    <?php   }   ?>

    </div>

    <!-- Identitas -->
    <div class="col-sm-6 col-md-6">

        <div class="panel panel-default">
            <table class='table'>
                <tr>
                    <td class="datafields" width='150px'>Nama Lengkap:</td>
                    <td><?= $this->mhs['namamhs'] ?></td>
                </tr>
                <tr>
                    <td class="datafields">N I M:</td>
                    <td><?= $this->mhs['nimhs'] ?></td>
                </tr>
                <tr>
                    <td class="datafields">Jenis Kelamin:</td>
                    <td><?php if( !empty($this->mhs['sex']) ) echo $this->siska->jenis_kelamin[ $this->mhs['sex'] ] ?></td>
                </tr>

<?php
    $prodimhs = $this->prodi_model->getProdi( $this->mhs['prodi'] );
?>

                <tr>
                    <td class="datafields">Program Studi:</td>
                    <td><?= $prodimhs['jenjang']['namaprogram'] . " - " . $prodimhs['nama']  ?></td>
                </tr>
                <tr>
                    <td class="datafields">Kompetensi:</td>
                    <td>
                        <?php
                            if( !empty($this->mhs['dtkompetensi']) )
                                echo $this->mhs['dtkompetensi']['kompetensi']." (".$this->mhs['dtkompetensi']['singkatan'].")"
                        ?></td>
                </tr>
                <tr>
                    <td class="datafields">Pembimbing Akademik:</td>
                    <td>
                        <?php if ( !empty( $this->mhs['dosen_pa'] ) ) {
                                echo $this->mhs['dosen_pa']['gelar_depan']." "
                                    .$this->mhs['dosen_pa']['nama'].", "
                                    .$this->mhs['dosen_pa']['gelar_belakang'];
                            } ?>
                    </td>
                </tr>
                <tr>
                    <td class="datafields">Tanggal Masuk:</td>
                    <td>
                        <?php
                            if(!empty($this->mhs['tanggal_masuk']))
                                echo date( "d-m-Y", strtotime( $this->mhs['tanggal_masuk'] ) );
                        ?>
                    </td>
                </tr>
            </table>
        </div>
        <div class="social-media text-right">
            <?php
                if( !empty($this->mhs['facebook']) ){
                    echo '<a href="'.$this->mhs['facebook'].'" target="_blank"><i class="fa fa-fw fa-facebook-square fa-2x"></i></a>';
                }
                if( !empty($this->mhs['linkedin']) ){
                    echo '<a href="'.$this->mhs['linkedin'].'" target="_blank"><i class="fa fa-fw fa-linkedin-square fa-2x"></i></a>';
                }
                if( !empty($this->mhs['gplus']) ){
                    echo '<a href="'.$this->mhs['gplus'].'" target="_blank"><i class="fa fa-fw fa-google-plus-square fa-2x"></i></a>';
                }
            ?>
            <br>
        </div>

    </div>

    <!-- QR Code -->
    <div class="col-sm-3 col-md-3">
        <div class="imgqr">
        <?php
            $path = 'assets/img/qr/mhs/';
            $params['data'] = base_url() . $this->mhs['nimhs'];
            $params['level'] = 'M';
            $params['size'] = 6;
            // $params['savename'] = FCPATH . str_replace('/','\\', $path) . $this->mhs['nimhs'] . '.png';
            $params['savename'] = FCPATH . $path . $this->mhs['nimhs'] . '.png';
            $this->ciqrcode->generate($params);

            echo '<img id="imgqr" src="' . base_url() . $path.$this->mhs['nimhs'] . '.png" />';
        ?>
        </div>
    </div>

</div>
