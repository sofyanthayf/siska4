<h3>Update Data</h3><hr>

<h4>Perpanjangan Waktu KRS / Modifikasi KRS</h4>
<div class="panel panel-default">
    <table class=table>
        <tr>
          <td class="datafields" width='150px'>Layanan:</td>
          <td>
            <select class="form-control" name="">
              <option>-- Pilih Layanan --</option>
              <option value="0">Pengisian Rencana Studi</option>
              <option value="1">Modifikasi Rencana Studi (Pembatalan/PenggantianMata Kuliah)</option>
            </select>
          </td>
        </tr>
        <tr>
            <td class="datafields" width='150px'>Tgl Batas Perpanjangan:</td>
            <td><input type="text" class="form-control" name="" value=""></td>
        </tr>
        <tr>
            <td class="datafields">Alasan Keterlambatan:</td>
            <td>
              <textarea name="name" class="form-control" rows="5" cols="65"></textarea>
            </td>
        </tr>
        <tr>
            <td class="datafields">Pertimbangan Kebijakan:</td>
            <td>
              <textarea name="name" class="form-control" rows="5" cols="65"></textarea>
            </td>
        </tr>
        <tr>
          <td></td>
          <td class="text-right">
            <input type="submit" class="btn btn-primary" value="Aktifkan Perpanjangan" DISABLED>
          </td>
        </tr>
    </table>
</div>

<h4>Penasehat Akademik</h4>
<div class="panel panel-default">
    <table class=table>
        <tr>
            <td class="datafields" width='150px'>Penasehat Akademik:</td>
            <td>
              <select class="form-control" name="">
                <option>-- Pilih Dosen PA --</option>
              </select>
            </td>
        </tr>
        <tr>
            <td class="datafields">Nomor SK PA:</td>
            <td><input type="text" class="form-control" name="" value=""></td>
        </tr>
        <tr>
            <td class="datafields">Tanggal SK PA:</td>
            <td><input type="text" class="form-control" name="" value=""></td>
        </tr>
        <tr>
          <td></td>
          <td class="text-right">
            <input type="submit" class="btn btn-primary" value="Update Data" DISABLED>
          </td>
        </tr>
    </table>
</div>

<h4>Status</h4>
<div class="panel panel-default">
    <table class=table>
        <tr>
            <td class="datafields" width='150px'>Status:</td>
            <td>
              <select class="form-control" name="">
                <option>-- Pilih Status --</option>
                <option value="A">Aktif</option>
                <option value="L">Lulus</option>
                <option value="K">Keluar / Mengundurkan Diri</option>
                <option value="P">Pindah</option>
                <option value="D">Drop Out</option>
              </select>
            </td>
        </tr>

        <tr id="nrskyudisium" style="display:none;">
          <td class="datafields">Nomor SK Yudisium</td>
          <td></td>
        </tr>
        <tr id="tgskyudisium" style="display:none;">
          <td class="datafields">Tanggal SK Yudisium</td>
          <td></td>
        </tr>

        <tr id="nrsuratpindah" style="display:none;">
          <td class="datafields">Nomor Surat Pindah</td>
          <td></td>
        </tr>
        <tr id="tgsuratpindah" style="display:none;">
          <td class="datafields">Tanggal Surat Pindah</td>
          <td></td>
        </tr>

        <tr id="nrskdropout" style="display:none;">
          <td class="datafields">Nomor SK Drop Out</td>
          <td></td>
        </tr>
        <tr id="tgskdropout" style="display:none;">
          <td class="datafields">Tanggal SK Drop Out</td>
          <td></td>
        </tr>

        <tr>
          <td></td>
          <td class="text-right">
            <input type="submit" class="btn btn-primary" value="Update Status" DISABLED>
          </td>
        </tr>

    </table>
</div>
