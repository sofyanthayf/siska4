<h3>Status Alumni</h3><hr>

<div class="panel panel-default">
    <table class=table>
        <tr>
            <td class="datafields" width='150px'>Nomor Ijazah</td>
            <td></td>
        </tr>
        <tr>
            <td class="datafields">Tanggal Ijazah</td>
            <td></td>
        </tr>
        <tr>
            <td class="datafields">Tanggal Wisuda</td>
            <td></td>
        </tr>
    </table>
</div>

<h4>Yudisium</h4>
<div class="panel panel-default">
    <table class=table>
        <tr>
            <td class="datafields" width='150px'>Index Prestasi</td>
            <td><?= number_format( $this->mhs['ipk'] , 2 , "," , "." ) ?></td>
        </tr>
        <tr>
            <td class="datafields">Nomor SK Yudisium</td>
            <td>
              <?php
                  if( $this->mhs['nomor_sk_yudisium'] != 0 ){
                    echo $this->mhs['nomor_sk_yudisium'];
                  }
                ?>
            </td>
        </tr>
        <tr>
          <td class="datafields">Tanggal Yudisium</td>
          <td>
            <?php
                if( $this->mhs['tanggal_sk_yudisium'] !== NULL ){
                  echo date( "d-m-Y", strtotime( $this->mhs['tanggal_sk_yudisium'] ) );
                }
              ?>
          </td>
        </tr>
        <tr>
            <td class="datafields">Lama Studi</td>
            <td>
              <?php
                  if(isset($this->mhs['masa_studi'])){
                    echo "<strong>".$this->mhs['masa_studi']['tahun']."</strong> tahun ".
                         "<strong>".$this->mhs['masa_studi']['bulan']."</strong> bulan";
                  }
              ?>
            </td>
        </tr>
    </table>
</div>

<h4>Tugas Akhir</h4>
<div class="panel panel-default">
    <table class=table>
        <tr>
            <td class="datafields" width='150px'>Judul Tugas Akhir</td>
            <td><?= $this->mhs['judul_tugas_akhir'] ?></td>
        </tr>
        <tr>
            <td class="datafields">Tanggal Ujian</td>
            <td></td>
        </tr>
        <tr>
            <td class="datafields">Pembimbing (1)</td>
            <td>
                <?php if(isset($this->mhs['pembimbing_ta'])){ ?>
                <?= $this->mhs['pembimbing_ta'][0]['gelar_depan'].
                    $this->mhs['pembimbing_ta'][0]['nama'].", ".
                    $this->mhs['pembimbing_ta'][0]['gelar_belakang'] ?>
                <?php } ?>
            </td>
        </tr>
        <tr>
            <td class="datafields">Pembimbing (2)</td>
            <td>
            <?php if(isset($this->mhs['pembimbing_ta'])){ ?>
                <?= $this->mhs['pembimbing_ta'][1]['gelar_depan'].
                    $this->mhs['pembimbing_ta'][1]['nama'].", ".
                    $this->mhs['pembimbing_ta'][1]['gelar_belakang'] ?>
                <?php } ?>
            </td>
        </tr>
    </table>
</div>
