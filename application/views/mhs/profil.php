<h3>Profil</h3>

<div class="text-right">
  <a href="#profile" id="clickedit" title="edit">
    <img src='/assets/img/edit16.png'> Edit Profil
  </a>
  <a href="#profile" id="clicksave" title="update" style="display:none">
    <img src='/assets/img/disc16.png'> Update
  </a>
</div>
<br>
<div class="panel panel-default">
    <table class=table>
        <tr>
            <td class="datafields" width='120px'>Nomor KTP:</td>
            <td>
              <input class="editfield" type="text" value="<?= $this->mhs['no_ktp'] ?>" id="noktp" style="border:0px;" readonly>
            </td>
        </tr>
        <tr>
            <td class="datafields" width='120px'>Tempat Lahir:</td>
            <td>
              <input class="editfield" type="text" value="<?= $this->mhs['tplahir'] ?>" id="tplahir" style="border:0px;" readonly>
            </td>
        </tr>
        <tr>
            <td class="datafields">Tanggal Lahir:</td>
            <td>
              <?php
                if(!empty($this->mhs['tglahir'])) {
                  $tglh =  date( "d-m-Y", strtotime( $this->mhs['tglahir'] ) );
                } else {
                  $tglh = '';
                }
              ?>
              <input class="editfield" type="text" value="<?= $tglh ?>" id="tglahir" style="border:0px;" readonly>
            </td>
        </tr>
        <tr>
            <td class="datafields">Golongan Darah:</td>
            <td>
              <div id="tampilgd"><?= $this->mhs['gol_darah'] ?></div>
              <div id="pilihgd" style="display:none;">
                <select class="editfield" id="goldarah">
                  <option value="">-- pilih golongan darah --</option>
                  <option value="A" <?= ($this->mhs['gol_darah']=='A')?'SELECTED':''; ?>>A</option>
                  <option value="B" <?= ($this->mhs['gol_darah']=='B')?'SELECTED':''; ?>>B</option>
                  <option value="AB" <?= ($this->mhs['gol_darah']=='AB')?'SELECTED':''; ?>>AB</option>
                  <option value="O" <?= ($this->mhs['gol_darah']=='O')?'SELECTED':''; ?>>O</option>
                </select>
              </div>
            </td>
        </tr>
        <tr>
            <td class="datafields">Agama:</td>
            <td>
              <div id="tampilagama">
                <?php  if( !empty($this->mhs['agama']) ) echo $this->siska->agama[ $this->mhs['agama'] ]; ?>
              </div>
              <div id="pilihagama" style="display:none;">
                <select class="editfield" id="agama">
                  <option value="">-- pilih agama --</option>
                  <?php
                    foreach ($this->siska->agama as $key => $value) {
                  ?>
                  <option value="<?=$key?>" <?= ($key==$this->mhs['agama'])?'SELECTED':''; ?>><?=$value?></option>
                  <?php
                    }
                  ?>
                </select>

              </div>
            </td>
        </tr>
    </table>
</div>

<h4>Sekolah Asal</h4>
<div class="panel panel-default">
    <table class='table'>
        <tr>
            <td class="datafields" width='120px'>Sekolah:</td>
            <td><?php if(isset($this->mhs['asalsekolah'])) echo $this->mhs['asalsekolah']['sekolah'] ?></td>
        </tr>
        <tr>
            <td class="datafields">Jurusan</td>
            <td></td>
        </tr>
        <tr>
            <td class="datafields">Propinsi:</td>
            <td><?php if(isset($this->mhs['asalsekolah'])) echo $this->mhs['asalsekolah']['nama_propinsi'] ?></td>
        </tr>
        <tr>
            <td class="datafields">Kabupaten/Kota:</td>
            <td><?php if(isset($this->mhs['asalsekolah'])) echo $this->mhs['asalsekolah']['nama_kota'] ?></td>
        </tr>
        <tr>
            <td class="datafields">Kecamatan:</td>
            <td><?php if(isset($this->mhs['asalsekolah'])) echo $this->mhs['asalsekolah']['nama_kecamatan'] ?></td>
        </tr>
    </table>
</div>

<h4>Kontak</h4>

<div class="panel panel-default">
    <table class='table'>
        <tr>
            <td class="datafields" width='120px'>Alamat Rumah:</td>
            <td>
              <input class="editfield" type="text" value="<?= $this->mhs['alamat_rumah'] ?>" id="alamat" style="border:0px;" readonly>
            </td>
        </tr>
        <tr>
            <td class="datafields">Kode Pos:</td>
            <td>
              <input class="editfield" type="text" value="<?= (isset($this->mhs['region']))?$this->mhs['region']['kodepos']:'' ?>" id="kodepos" style="border:0px;" readonly>
            </td>
        </tr>
        <tr>
            <td class="datafields">Kabupaten/Kota:</td>
            <td><?php if(isset($this->mhs['region'])) echo $this->mhs['region']['nama_kota'] ?></td>
        </tr>
        <tr>
            <td class="datafields">Kecamatan:</td>
            <td><?php if(isset($this->mhs['region'])) echo $this->mhs['region']['nama_kecamatan'] ?></td>
        </tr>
        <tr>
            <td class="datafields">Desa/Kelurahan:</td>
            <td><?php if(isset($this->mhs['region'])) echo $this->mhs['region']['nama_desa'] ?></td>
        </tr>

<?php   // bagian ini bukan untuk umum
        if ( !empty($_SESSION['uid']) && !empty($_SESSION['pin']) ) {
?>

        <tr>
            <td class="datafields">Telepon Rumah:</td>
            <td>
              <input class="editfield" type="text" value="<?= $this->mhs['telepon2'] ?>" id="telepon2" style="border:0px;" readonly>
            </td>
        </tr>
        <tr>
            <td class="datafields">Handphone:</td>
            <td>
              <input class="editfield" type="text" value="<?= $this->mhs['telepon'] ?>" id="telepon" style="border:0px;" readonly>
            </td>
        </tr>
        <tr>
            <td class="datafields">Email (1):</td>
            <td><?= $this->mhs['email'] ?></td>
        </tr>
        <tr>
            <td class="datafields">Email (2):</td>
            <td>
              <input class="editfield" type="email" value="<?= $this->mhs['email2'] ?>" id="email" placeholder="Email cadangan selain email kharisma.ac.id" readonly>
            </td>
        </tr>

<?php
        }
?>

    </table>
</div>


<?php   // bagian ini bukan untuk umum
        if ( !empty($_SESSION['uid']) && !empty($_SESSION['pin']) ) {
?>

<div id="medsos">
  <h4>Media Sosial</h4>

  <div class="panel panel-default">
    <table class='table'>
      <tr>
        <td class="datafields" width='120px'>LinkedIn:</td>
        <td>
          <input class="editfield" type="text" value="<?= $this->mhs['linkedin'] ?>" placeholder="http://linkedin.com/UserIDAnda"
                 id="linkedin" style="border:0px;" readonly>
        </td>
      </tr>
      <tr>
        <td class="datafields" width='120px'>Facebook:</td>
        <td>
          <input class="editfield" type="text" value="<?= $this->mhs['facebook'] ?>" placeholder="http://facebook.com/UserIDAnda"
          id="facebook" style="border:0px;" readonly>
        </td>
      </tr>
      <tr>
        <td class="datafields" width='120px'>Google PLus:</td>
        <td>
          <input class="editfield" type="text" value="<?= $this->mhs['gplus'] ?>" placeholder="http://plus.google.com/+UserIDAnda"
                 id="gplus" style="border:0px;" readonly>
        </td>
      </tr>
    </table>
  </div>
</div>

<?php
        }
?>


<script type="text/javascript">

$("#clickedit").click(function(){
  enableEdit();
});

$("#clicksave").click(function(){
  updatedata();
  disableEdit();
});

$("#agama").change(function(){
  $("#tampilagama").html( $("#agama option:selected").text() );
});

$("#goldarah").change(function(){
  $("#tampilgd").html( $("#goldarah option:selected").text() );
});

function enableEdit(){
  $("#clickedit").hide();
  $("#clicksave").show();

  $("#pilihagama").show();
  $("#tampilagama").hide();
  $("#pilihgd").show();
  $("#tampilgd").hide();
  $("#medsos").show();

  $("input.editfield").each(function () {
    $(this).prop("readonly", false);
    $(this).css('color', 'blue');
  })

}

function disableEdit(){
  $("#clicksave").hide();
  $("#clickedit").show();

  $("#pilihagama").hide();
  $("#tampilagama").show();
  $("#pilihgd").hide();
  $("#tampilgd").show();
  $("#medsos").hide();

  $("input.editfield").each(function () {
    $(this).prop("readonly", true);
    $(this).css('color', 'black');
  })
}

function updatedata() {
  data = {
   'nimhs': '<?= $this->mhs['nimhs'] ?>',
   'goldarah': $("#goldarah").val(),
   'agama': $("#agama").val(),
   'noktp': $("#noktp").val(),
   'tplahir': $("#tplahir").val(),
   'tglahir': $("#tglahir").val(),
   'alamat': $("#alamat").val(),
   'telepon': $("#telepon").val(),
   'telepon2': $("#telepon2").val(),
   'email2': $("#email2").val(),
   'linkedin': $("#linkedin").val(),
   'facebook': $("#facebook").val(),
   'gplus': $("#gplus").val()
  };
  $.post('/apis/mhs/profile_update', data);
}
</script>
