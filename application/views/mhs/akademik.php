<h3>Akademik</h3>


<div class="panel panel-default">
    <table class=table>
        <tr>
            <td class="datafields" width='180px'>Status:</td>
            <td><?= $this->siska->status_mahasiswa[ $this->mhs['status'] ] ?></td>
        </tr>
        <tr>
            <td class="datafields" width='180px'>Indeks Prestasi Kumulatif:</td>
            <td><?= number_format( $this->mhs['ipk'] , 2 , "," , "." ) ?></td>
        </tr>
        <tr>
            <td class="datafields">Jumlah sks terkumpul:</td>
            <td><?= $this->mhs['jmlsks'] ?></td>
        </tr>
    </table>
</div>

<div class="panel panel-default">
  <canvas id="ipChart" width="400" height="100"></canvas>
</div>

<h4>Daftar Nilai Mata Kuliah</h4>
<div class="panel panel-default">
    <table class=table>
        <tr>
            <td class="datafields" width="180px" style="vertical-align: middle;">Semester:</td>
            <td>
                <select class="form-control" id="pilihansemester">
                    <<option value="all">Semua Semester (DNS)</option>
                    <?php
                        foreach ($this->mahasiswa_model->listSemester( $this->mhs['nimhs'] ) as $smt) {
                    ?>
                        <option value="<?= $smt['kodesmt'] ?>">
                            <?php
                                echo "Semester ".$this->siska->stringSemester( $smt['kodesmt'] ) .
                                     " (" . $smt['kodesmt'] . ")" ;
                            ?>
                        </option>
                    <?php
                        }
                     ?>
                </select>
            </td>
        </tr>
        <tr id="ipsrow" style="display:none;">
            <td class="datafields">Indeks Prestasi Semester:</td>
            <td><div id="ips"></div></td>
        </tr>
        <tr>
            <td colspan="2">
                <table class="display" id="daftarmkmhs" width="100%">
                    <thead>
                        <th width="50px">Kode MK</th>
                        <th>Nama Mata Kuliah</th>
                        <th width="20px">sks</th>
                        <th width="20px">Nilai</th>
                        <th width="10px">kodesmt</th>
                    </thead>
                </table>
            </td>
        </tr>
    </table>
</div>
