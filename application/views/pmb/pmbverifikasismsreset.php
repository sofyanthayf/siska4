<div class="col-sm-8 col-md-8">
    <i class="fa fa-fw fa-envelope-o fa-4x"></i><i class="fa fa-fw fa-times fa-4x"></i><br />
    <h3>PIN baru telah dikirimkan ke nomor HP Anda</h3>
    <h4>Silahkan masukkan PIN yang Anda terima melalui SMS (<i>case-sensitive</i>)</h4>

    <form role="form" action="/pmb/verify/sms" method="post">
        <div class="form-group row">
            <div class="col-sm-3 col-md-3">
                <input type="password" class="form-control input-lg" id="inputpin" name="inputpin" required autofocus>
                <input type="submit" class="form-control btn-success" value="VERIFIKSI">
            </div>
        </div>
    </form>

</div>
