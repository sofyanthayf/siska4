<div class="col-sm-9 col-md-9">
<div class="panel panel-info">

    <div class="panel-heading">
        <h3>I. Persyaratan Calon Mahasiswa</h3>
    </div>

    <div class="panel-body">
        <p align='justify'>
            <ol>
                <li align='justify'>
                    Telah lulus dan berijasah SMA/SMK atau yang sederajat
                </li>
                <li align='justify'>
                    Untuk Program Studi Teknik Informatika (S1 &amp; D-III), lebih disarankan bagi
                    lulusan SMA dari jurusan IPA.
                </li>
                <li align='justify'>
                    Berkelakuan baik yang dibuktikan dengan Surat Keterangan Berkelakuan Baik
                    dari sekolah asal atau dari Kepolisian RI dan dilampirkan pada saat
                    Pendaftaran Ulang (setelah lulus).
                </li>
                <li align='justify'>
                    Bersedia mengikuti ujian seleksi dalam bentuk Ujian Tulis, Ujian
                    Wawancara, dan Psikotest.
                </li>
            </ol>
        </p>
    </div>

    <div class="panel-heading">
        <h3>II. Ujian Seleksi</h3>
    </div>
    <div class="panel-body">
        <ol>
            <li align='justify'>
                Setiap calon Mahasiswa yang telah terdaftar berhak dan wajib mengikuti Ujian Seleksi.
            </li>
            <li align='justify'>
                Materi yang diujikan pada semua jenjang pendidikan adalah Matematika dan Bahasa Inggris.
            </li>
            <li align='justify'>
                Pelaksanaan ujian (penyajian dan penyelesaian soal) dilakukan dengan menggunakan bantuan perangkat lunak komputer.
            </li>
            <li align='justify'>
                Setiap calon mahasiswa wajib mengikuti ujian psikotes
            </li>
            <li align='justify'>
                Setiap calon mahasiswa yang dinyatakan lulus pada Ujian Masuk tahap pertama berhak dan wajib mengikuti Ujian Wawancara.
            </li>
            <li align='justify'>
                Calon mahasiswa yang tidak mengikuti Ujian Wawancara dinyatakan tidak lulus ujian Seleksi
            </li>
        </ol>
    </div>

    <div class="panel-heading">
        <h3>III. Peraturan Umum Ujian Masuk</h3>
    </div>
    <div class="panel-body">
        <ol>
            <li align='justify'>
                Peserta Ujian Masuk wajib hadir 10 menit sebelum ujian dimulai. Peserta yang
                datang terlambat karena suatu alasan yang dapat diterima, diperbolehkan
                masuk ujian dengan tidak mendapatkan waktu tambahan.
            </li>
            <li align='justify'>
                Peserta Ujian Masuk yang tidak hadir sampai ujian berakhir, dinyatakan
                mengundurkan diri dan Kartu Tanda Peserta yang dimilikinya dinyatakan batal,
                serta tidak dapat digunakan untuk Ujian Seleksi tahap berikutnya.
            </li>
            <li align='justify'>
                Peserta Ujian Masuk wajib berpakaian yang sopan dan rapi, serta bersepatu.
            </li>
            <li align='justify'>
                Peserta Ujian Masuk harus dapat menunjukkan Kartu Tanda Peserta yang sah
                kepada Pengawas Ujian dan menandatangani bukti kehadiran.
            </li>
            <li align='justify'>
                Peserta Ujian tidak diperkenankan meninggalkan ruang ujian selain atas izin
                Pengawas Ujian atau setelah menyelesaikan soal ujian.
            </li>
        </ol>
    </div>

    <div class="panel-heading">
        <h3>IV.	Dokumen</h3>
    </div>
    <div class="panel-body">
        <p align='justify'>
            Calon Mahasiswa wajib melampirkan dokumen:
        </p>
        <ol>
            <li>Kartu Tanda Penduduk (KTP) atau Keterangan Domisili</li>
            <li>Akte Kelahiran</li>
            <li>Raport SMA/Sederajat</li>
            <li>Ijazah SMA/SMK/Sederajat</li>
            <li>Surat Keterangan Hasil Ujian (SKHU) </li>
            <li>Pas Foto Berwarna</li>
            <li>Sertifikat Prestasi (untuk peserta jalur prestasi)</li>
        </ol>

    </div>

</div>

</div>
<div class="col-sm-3 col-md-3">
    <div class="list-group">
        <a href="<?=base_url()?>pmb/m/syarat" class="list-group-item disabled">
            <strong><i class="fa fa-fw fa-check-square-o"></i> Persyaratan</strong>
        </a>
        <a href="<?=base_url()?>pmb/m/biaya" class="list-group-item">
            <strong><i class="fa fa-fw fa-check-square-o"></i> Pembiayaan</strong>
        </a>
        <a href="<?=base_url()?>pmb/m/jadwal" class="list-group-item">
            <strong><i class="fa fa-fw fa-check-square-o"></i> Jadwal Pendaftaran</strong>
        </a>

<?php
    if( isset( $_SESSION['klas'] ) && $_SESSION['klas'] == 'maba'  ){
?>

        <a href="<?=base_url()?>pmb/m/profil" class="list-group-item">
            <strong><i class="fa fa-fw fa-address-card"></i> Update Profil</strong>
        </a>
        <a href="<?=base_url()?>pmb/dokumen" class="list-group-item">
            <strong><i class="fa fa-fw fa-upload"></i> Upload Dokumen</strong>
        </a>
<?php
    }
?>

    </div>
</div>
