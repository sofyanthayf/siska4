<div class="col-sm-8 col-md-8">
    <i class="fa fa-fw fa-envelope-o fa-4x"></i><i class="fa fa-fw fa-check fa-4x"></i><br />
    <h3>Alamat email Anda telah berhasil di-verifikasi, <br />
        akun Anda kini sudah aktif dan Anda dapat melakukan <a href="/pmb">Sign-in</a></h3>

<?php
    // if( nomor HP belum di verifikasi ) {

?>

    <h4>Selanjutnya kami perlu melakukan verifikasi nomor handphone Anda, <br />
        sebuah kode verifikasi telah dikirimkan melalui SMS ke nomor handphone Anda</h4>
    <hr />

    <h2>Verifikasi Nomor Handphone</h2>
    <h3>Masukkan 6-digit kode dari SMS yang Anda terima (<i>case-sensitive</i>)</h3>
    <form role="form" action="/pmb/verify/sms" method="post">
        <div class="form-group row">
            <div class="col-sm-3 col-md-3">
                <input type="password" class="form-control input-lg" id="inputpin" name="inputpin" required autofocus>
                <input type="submit" class="form-control btn-success" value="VERIFIKSI">
            </div>
        </div>
    </form>

<?php
    // }
?>


</div>
