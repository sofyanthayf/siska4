<div class="col-sm-5 col-md-5">
    <div class="account-wall">
    <?php
        if( $status == '1' ) {
     ?>
        <div class='alert alert-danger text-center' role='alert'>
                <span class='fa fa-exclamation-triangle fa-2x' aria-hidden='true'></span><br />
                Password salah, gagal mengganti nomor HP
        </div>
    <?php
        }
     ?>
        <p class="text-center">Masukkan Nomor Handphone Anda yang baru:</p>
        <form class="form-signin" action="<?=base_url()?>pmb/resetnomorhp" method="post">
        <div class="form-group">
            <label>Nomor HP Anda*</label>
            <input type="text" id="nomorhp" name="nomorhp" class="form-control" placeholder="08xxxxxxxxxx" required autofocus>
            <p class="help-block" id="warninghp"></p>
        </div>
        <div class="form-group">
            <label>Password Anda*</label>
            <input type="password" id="passw" name="password" class="form-control" required>
        </div>
            <p class="help-block">Nomor HP Anda akan diverifikasi kembali</p>
            <input type="submit" class="btn btn-lg btn-success btn-block" name="submit" value="Simpan Nomor Baru">
        </form>
    </div>
    <br />
</div>
