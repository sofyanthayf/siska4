<form role="form" id="formregmaba" action="/pmb/m/register" method="post">
    <div class="col-sm-6 col-md-6">

        <div class="form-group">
            <label>Nama Lengkap Anda*</label>
            <input class="form-control" id="namalengkap" name="namalengkap" required>
            <p class="help-block"></p>
        </div>

        <div class="form-group">
            <label>Jenis Kelamin*: </label>
            <div id ="jkel" data-toggle="buttons">
                <label class="btn btn-primary btn-outline">
                    <input type="radio" name="jkel" value="L" required>
                    <i class="fa fa-male"></i> Laki-laki
                </label>
                <label class="btn btn-primary btn-outline">
                    <input type="radio" name="jkel" value="P">
                    <i class="fa fa-female"></i> Perempuan
                </label>
            </div>
            <p class="help-block"></p>
        </div>

        <div class="form-group">
            <label>Tempat Lahir* </label>
            <input class="form-control" id="tempatlahir" name="tempatlahir" required>
            <p class="help-block"></p>
        </div>

        <div class="form-group">
            <label>Tanggal Lahir*</label>
            <div class="input-group date form_date" data-date="" data-date-format="dd MM yyyy" data-link-field="tglahir" data-link-format="yyyy-mm-dd">
                <input class="form-control" size="16" type="text" value="" required>
                <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
            </div>
            <input type="hidden" id="tglahir" name="tanggallahir"value="" /><br/>
        </div>
        <hr />

        <div id="sekolah">
            <div class="form-group">
                <label>Sekolah Asal (SMA/SMK/MA)*:</label><br />
                <label>Propinsi Sekolah</label>
                <select class="form-control" id="propinsisekolah">
                    <option value="">-- Pilih Propinsi Sekolah--</option>
<?php
    foreach ($this->region_model->getListPropinsi() as $propinsi) {
        echo "<option value='".$propinsi['id']."'>".$propinsi['nama']."</option>";
    }
?>
                </select>
            </div>

            <div class="form-group">
                <label>Kabupaten / Kota Sekolah</label>
                <select class="form-control" id="kotasekolah">
                    <option value="">-- Pilih Kabupaten/Kota Sekolah --</option>
                </select>
            </div>

            <div class="form-group">
                <label>Nama Sekolah*</label>
                <select class="form-control" id="listsekolah" name="idsekolah" required>
                    <option value="">-- Pilih Sekolah --</option>
                </select>
            </div>

            <div class="form-group">
                <label>Tahun Lulus*</label>
                <select class="form-control" id="tahunlulus" name="tahunlulus" required>
<?php
    $th = $tahun;
    for($t=$th; $t>=$th-30; $t--){
        echo "<option value='$t'>$t</option>";
    }
?>
                </select>
            </div>

            <div class="form-group">
                <label>Jurusan*</label>
                <select class="form-control" id="listjurusan" name="jurusan" required>
                    <option value="">-- Pilih Jurusan --</option>
                    <option value="000001">IPA</option>
                    <option value="000002">IPS</option>
                    <option value="000009">Tidak Ada Jurusan</option>
                </select>
            </div>

        </div>

    </div>
    <div class="col-sm-5 col-md-5">

        <div class="form-group">
            <label>E-mail Anda*</label>
            <input type="email" class="form-control" id="email" name="email" required="required">
            <p class="help-block" id="warningemail"></p>
        </div>

        <div class="form-group">
            <label>Nomor HP Anda* </label> 
            <input class="form-control" placeholder="08xxxxxxxxx" id="nomorhp" name="nomorhp" required="required">
            <p class="help-block" id="warninghp"></p>
        </div>

        <div class="form-group">
            <label>Password*:</label>
            <input type="password" class="form-control" id="password1" name="password" required>
            <p class="help-block"></p>
        </div>
        <div class="form-group">
            <label>Verifikasi Password*:</label>
            <input type="password" class="form-control" id="password2" required>
            <p class="help-block" id="warningpassword"></p>
        </div>

        <p class="help-block">Password hanya akan digunakan untuk layanan PMB saja.</p>
        <hr />

        <div class="form-group">
            <label class="checkbox-inline">
                <input type="checkbox" id="jprestasi" name="jprestasi" value="1">
                <p>Saya memiliki prestasi di bidang akademik / keterampilan / olahraga / seni budaya / hobi,
                   mohon untuk diikutkan dalam proses seleksi <strong>Jalur Prestasi</strong></p>
                <p class="help-block">Dibuktikan dengan dokumen pendukung (piagam/sertifikat)</p>
            </label>
        </div>
        <hr />

        <br />
        <a href="<?=base_url()?>pmb/m/syarat" target="_blank"><strong>Silahkan membaca Syarat dan Ketentuan</strong></a>
        <br />
        <br />

        <div class="form-group">
            <label class="checkbox-inline">
                <input type="checkbox" id="setujusk" name="setujusk" required>
                <p>Saya telah membaca dan menyetujui semua persyaratan dan ketentuan yang berlaku
                   sebagai calon Mahasiswa Baru STMIK KHARISMA</p>
            </label>
        </div>
        <div class="form-group">
            <label class="checkbox-inline">
                <input type="checkbox" id="databenar" name="databenar" required>
                <p>Semua data yang saya berikan dalam formulir isian ini adalah benar sesuai dengan keadaan yang sebenarnya</p>
            </label>
        </div>
        <input type="hidden" name="tahun" value="<?= $tahun ?>">
        <input type="submit" class="btn btn-lg btn-success btn-block" name="submit" id="mregsubmit" value="Daftarkan">
        <br />
        <p class="help-block">* Harus diisi</p>
    </div>
</form>
