<div class="col-sm-5 col-md-5">
    <div class="account-wall">

<?php
switch ($kode) {
    case 1:
        echo "<div class='alert alert-danger text-center' role='alert'>
                <span class='fa fa-exclamation-triangle fa-2x' aria-hidden='true'></span><br />
                E-mail tidak dikenali atau salah tulis
            </div>";
        break;

    case 2:
        echo "<div class='alert alert-success text-center' role='alert'>
                <span class='fa fa-key fa-2x' aria-hidden='true'></span><br />
                Password baru telah dikirimkan ke elamat e-mail Anda<br />

                <a href='/pmb'><strong>Sign-in</strong></a>
            </div>";
        break;

}

if( $kode != 2 ) {
?>
        <p class="text-center">Masukkan alamat E-mail yang pernah Anda daftarkan</p>
        <form class="form-signin" action="<?=base_url()?>pmb/resetpassword" method="post">
            <input type="text" name="email" class="form-control" placeholder="Email Anda" required autofocus>
            <input type="submit" class="btn btn-lg btn-success btn-block" name="submit" value="Reset Password">
        </form>
    </div>
    <br />
<?php
}
?>
</div>
