<div class="row">
    <div class="col-sm-7 col-md-7">
        <h2>Update Profil</h2><br />
    </div>
    <div class="col-sm-4 col-md-4 text-right">
        <div id="statusupdate" class="help-block">
            <i class="fa fa-fw fa-info-circle"></i>
            Perubahan akan tersimpan otomatis<br />
        </div>
        <a href="/pmb/m"><button><i class="fa fa-fw fa-check-square-o"></i> Lihat Status  </button></a>
    </div>
</div>
<div class="row">
    <div class="col-sm-6 col-md-6">

        <div class="form-group">
            <label>Nama Lengkap Anda</label>
            <input class="form-control" id="pnamalengkap" name="namalengkap" value="<?=$maba['nama_lengkap']?>">
            <p class="help-block"></p>
        </div>

<?php
    if( $maba['sex'] == 'L' ){
        $clk = " checked='checked'";
        $vlk = " active";
        $cpr = "";
        $vpr = "";
    } else {
        $clk = "";
        $vlk = "";
        $cpr = " checked='checked'";
        $vpr = " active";
    }
?>
        <div class="form-group">
            <label>Jenis Kelamin: </label>
            <div id ="jkel" data-toggle="buttons">
                <label class="btn btn-primary btn-outline<?= $vlk ?>">
                    <input type="radio" name="jkel" value="L"<?= $clk ?>>
                    <i class="fa fa-male"></i> Laki-laki
                </label>
                <label class="btn btn-primary btn-outline<?= $vpr ?>">
                    <input type="radio" name="jkel" value="P"<?= $cpr ?>>
                    <i class="fa fa-female"></i> Perempuan
                </label>
            </div>
            <p class="help-block"></p>
        </div>

        <div class="form-group">
            <label>Tempat Lahir </label>
            <input class="form-control" id="ptempatlahir" name="ptempatlahir" value="<?=$maba['tempat_lahir']?>">
            <p class="help-block"></p>
        </div>

        <div class="form-group">
            <label>Tanggal Lahir</label>
            <div class="input-group date form_date" data-date="" data-date-format="dd MM yyyy" data-link-field="ptglahir" data-link-format="yyyy-mm-dd">
                <input class="form-control" size="16" type="text" value="<?=$maba['tanggal_lahir']?>" id="itglahir">
                <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
            </div>
            <input type="hidden" id="ptglahir" value="<?=$maba['tanggal_lahir']?>" /><br/>
        </div>

        <div class="form-group">
            <label>Agama</label>
            <select class="form-control" id="agama" name="agama">
                <option value="">-- Pilih Agama --</option>
                <?php
                    $a=1;
                    foreach ($this->siska->agama as $agama) {
                        $asel = "";
                        if( !empty($maba['agama']) && $maba['agama']==$a ) $asel = " selected";
                        echo "<option value='$a'$asel>".$agama."</option>";
                        $a++;
                    }
                ?>
            </select>
        </div>
        <hr />

        <div class="form-group">
            <label>Pilihan Program Studi: (Pilihan 1)</label>
            <div class="funkyradio">
                <?php
                $r1 = 1;
                foreach ($this->prodi_model->getListProdiByTahunKurikulum() as $prodi) {
                    if( $prodi['kode'] == $maba['prodi_pilihan1'] ) $psel1 = " checked='checked'";
                    else $psel1 = "";

                    ?>
                    <div class="funkyradio-primary">
                        <input type="radio" name="rprodi1" id="rprodi1<?=$r1?>" value="<?=$prodi['kode']?>"<?= $psel1 ?> />
                        <label for="rprodi1<?=$r1?>"><?=$prodi['nama']." - ". $prodi['jenjang']?></label>
                    </div>

                    <?php
                    $r1++;
                }
                ?>
            </div>
            <p class="help-block"></p>
        </div>
        <div class="form-group">
            <label>Pilihan Program Studi: (Pilihan 2)</label>
            <div class="funkyradio">
                <?php
                $r2 = 1;
                foreach ($this->prodi_model->getListProdiByTahunKurikulum() as $prodi) {
                    if( $prodi['kode'] == $maba['prodi_pilihan2'] ) $psel2 = " checked='checked'";
                    else $psel2 = "";

                    ?>
                    <div class="funkyradio-primary">
                        <input type="radio" name="rprodi2" id="rprodi2<?=$r2?>" value="<?=$prodi['kode']?>"<?= $psel2 ?> />
                        <label for="rprodi2<?=$r2?>"><?=$prodi['nama']." - ". $prodi['jenjang']?></label>
                    </div>

                    <?php
                    $r2++;
                }
                ?>
            </div>
            <p class="help-block"></p>
        </div>
        <hr />

        <div class="form-group">
            <label>Alamat Tempat Tinggal</label>
            <input class="form-control" id="alamat" name="alamat" value="<?=$maba['alamat']?>">
            <p class="help-block"></p>
        </div>

        <div class="form-group">
            <label>Propinsi Tempat Tinggal</label>
            <select class="form-control" id="propinsi" name="propinsi">
                <option value="">-- Pilih Propinsi --</option>
                <?php
                    // $mabaregion = "";
                    if( !empty( $maba['kelurahan'] ) ){
                        $mabaregion = $this->region_model->getRegion( $maba['kelurahan'] );
                    }
                    foreach ($this->region_model->getListPropinsi() as $propinsi) {
                        $selp = "";
                        if( !empty( $maba['kelurahan'] ) && $propinsi['id'] == $mabaregion['id_propinsi'] ){
                            $selp = " selected";
                        }
                        echo "<option value='".$propinsi['id']."'$selp>".$propinsi['nama']."</option>";
                    }
                ?>
            </select>
        </div>

        <div class="form-group">
            <label>Kabupaten / Kota</label>
            <select class="form-control" id="kabkota" name="kabkota">
                <option value="">-- Pilih Kabupaten/Kota --</option>
                <option value="">KOTA MAKASSAR</option>
                <?php
                    if( !empty( $maba['kelurahan'] ) ){
                        foreach ($this->region_model->getListKotaKabupaten( $mabaregion['id_propinsi'] ) as $kota) {
                            if( $kota['id'] == $mabaregion['id_kota'] ){
                                $selk = " selected";
                            } else {
                                $selk = "";
                            }
                            echo "<option value='".$kota['id']."'$selk>".$kota['name']."</option>";
                        }
                    }
                ?>
            </select>
        </div>

        <div class="form-group">
            <label>Kecamatan</label>
            <select class="form-control" id="kecamatan">
                <option value="">-- Pilih Kecamatan --</option>
                <?php
                    if( !empty( $maba['kelurahan'] ) ){
                        foreach ($this->region_model->getListKecamatan( $mabaregion['id_kota'] ) as $kecamatan) {
                            if( $kecamatan['id'] == $mabaregion['id_kecamatan'] ){
                                $selk = " selected";
                            } else {
                                $selk = "";
                            }
                            echo "<option value='".$kecamatan['id']."'$selk>".$kecamatan['name']."</option>";
                        }
                    }
                ?>
            </select>
        </div>

        <div class="form-group">
            <label>Desa/Kelurahan</label>
            <select class="form-control" id="desakelurahan" name="desakelurahan">
                <option value="">-- Pilih Desa/Kelurahan --</option>
                <?php
                    if( !empty( $maba['kelurahan'] ) ){
                        foreach ($this->region_model->getListDesaKelurahan( $mabaregion['id_kecamatan'] ) as $desa) {
                            if( $desa['id'] == $mabaregion['id_desa'] ){
                                $selk = " selected";
                            } else {
                                $selk = "";
                            }
                            echo "<option value='".$desa['id']."'$selk>".$desa['name']."</option>";
                        }
                    }
                ?>
            </select>
        </div>

        <hr />

        <div class="form-group">
            <label>Nama Ayah</label>
            <input class="form-control" id="namaayah" name="namaayah" value="<?= $maba['nama_ayah']?>">
            <p class="help-block"></p>
        </div>

        <div class="form-group">
            <label>Nomor HP Ayah</label>
            <input class="form-control" id="hpayah" name="hpayah" value="<?= $maba['telepon_ayah']?>">
            <p class="help-block"></p>
        </div>

        <div class="form-group">
            <label>Nama Ibu</label>
            <input class="form-control" id="namaibu" name="namaibu" value="<?= $maba['nama_ibu']?>">
            <p class="help-block"></p>
        </div>

        <div class="form-group">
            <label>Nomor HP Ibu</label>
            <input class="form-control" id="hpibu" name="hpibu" value="<?= $maba['telepon_ibu']?>">
            <p class="help-block"></p>
        </div>

        <div class="form-group">
            <label>Pekerjaan Ayah</label>
            <select class="form-control" id="pekerjaanayah" name="pekerjaanayah">
                <option value="">-- Pilih Jenis Pekerjaan --</option>

                <?php
                    foreach ( $this->siska->pekerjaan as $pekerjaan ) {
                        if( $pekerjaan['kode'] == $maba['pekerjaan_ayah'] ){
                            $defk = ' selected';
                        } else {
                            $defk ='';
                        }
                        echo "<option value='".$pekerjaan['kode']."'$defk>".$pekerjaan['pekerjaan']."</option>";
                    }

                 ?>

            </select>
        </div>
        <div class="form-group">
            <label>Pekerjaan Ibu</label>
            <select class="form-control" id="pekerjaanibu" name="pekerjaanibu">
                <option value="">-- Pilih Jenis Pekerjaan --</option>
                <?php
                    foreach ( $this->siska->pekerjaan as $pekerjaan ) {
                        if( $pekerjaan['kode'] == $maba['pekerjaan_ibu'] ){
                            $defk = ' selected';
                        } else {
                            $defk ='';
                        }
                        echo "<option value='".$pekerjaan['kode']."'$defk>".$pekerjaan['pekerjaan']."</option>";
                    }

                 ?>

            </select>
        </div>
        <div class="form-group">
            <label>Penghasilan Orang Tua: </label>
            <div class="funkyradio">
                <?php
                    foreach ( $this->siska->penghasilan as $penghasilan ) {
                        if( $penghasilan['kode'] == $maba['penghasilan_ortu'] ){
                            $defh = ' checked="checked"';
                        } else {
                            $defh ='';
                        }
                ?>
                <div class="funkyradio-primary">
                    <input type="radio" name="rpenghasilan"
                                        id="rpenghasilan<?= $penghasilan['kode'] ?>"
                                        value="<?= $penghasilan['kode'] ?>"<?= $defh ?>/>
                    <label for="rpenghasilan<?= $penghasilan['kode'] ?>"><?= $penghasilan['range'] ?></label>
                </div>
                <?php
                    }
                 ?>
            </div>
            <p class="help-block"></p>
        </div>
    </div>

    <div class="col-sm-5 col-md-5">
        <div id="sekolah">
            <div class="form-group">
                <label>Sekolah Asal (SMA/SMK/MA)</label>
                <select class="form-control" id="propinsisekolah">
                    <option value="">-- Pilih Propinsi Sekolah--</option>
                    <?php
                    if( !empty( $maba['sekolah_asal'] ) ){
                        $sekolah_asal = $this->region_model->getRegionSekolah( $maba['sekolah_asal'] );
                        $region_sekolah = $this->region_model->getRegion( $sekolah_asal['id_kabkota']);
                        $propsekolah = $region_sekolah['id_propinsi'];
                    }

                    foreach ( $this->region_model->getListPropinsi() as $propinsi ) {
                        if( !empty( $maba['sekolah_asal'] ) && $propinsi['id'] == $propsekolah ){
                            $defp = ' selected';
                        } else {
                            $defp ='';
                        }
                        echo "<option value='".$propinsi['id']."'$defp>".$propinsi['nama']."</option>";
                    }
                    ?>
                </select>
            </div>

            <div class="form-group">
                <label>Kabupaten / Kota Sekolah</label>
                <select class="form-control" id="kotasekolah">
                    <option value="">-- Pilih Kabupaten/Kota Sekolah --</option>
                    <?php
                    foreach ( $this->region_model->getListKotaKabupaten( $region_sekolah['id_propinsi'] ) as $kota ) {
                        if( $kota['id'] == $region_sekolah['id_kota'] ){
                            $defk = ' selected';
                        } else {
                            $defk ='';
                        }
                        echo "<option value='".$kota['id']."'$defk>".$kota['name']."</option>";
                    }

                    ?>
                </select>
            </div>

            <div class="form-group">
                <label>Nama Sekolah</label>
                <select class="form-control" id="listsekolah" name="idsekolah">
                    <option value="">-- Pilih Sekolah --</option>
                    <?php
                    foreach ( $this->region_model->getListSekolahKota( $region_sekolah['id_kota'] ) as $sekolah ) {
                        if( $sekolah['id'] == $maba['sekolah_asal'] ){
                            $defs = ' selected';
                        } else {
                            $defs ='';
                        }
                        echo "<option value='".$sekolah['id']."'$defs>".$sekolah['sekolah']."</option>";
                    }

                    ?>
                </select>
            </div>

            <div class="form-group">
                <label>Tahun Lulus</label>
                <select class="form-control" id="tahunlulus" name="tahunlulus">
                    <?php
                    $th = date('Y');
                    for($t=$th; $t>=$th-30; $t--){
                        if( $t == $maba['sekolah_lulus'] ){
                            $deft = ' selected';
                        } else {
                            $deft ='';
                        }
                        echo "<option value='$t'$deft>$t</option>";
                    }
                    ?>
                </select>
            </div>

            <div class="form-group">
                <label>Jurusan</label>
                <select class="form-control" id="listjurusan" name="jurusan">
                    <option value="">-- Pilih Jurusan --</option>

                    <?php
                        if( substr( $sekolah_asal['sekolah'],0,3 ) == 'SMK' ) {
                            $listjurusan = $this->region_model->getListJurusanSMK( $maba['sekolah_lulus'] );
                        } else {
                            $listjurusan = $this->region_model->getListJurusanSMA();
                        }
                        foreach (  $listjurusan as $jurusan ) {
                            if( $jurusan['kode'] == $maba['sekolah_jurusan'] ){
                                $defs = ' selected';
                            } else {
                                $defs ='';
                            }
                            echo "<option value='".$jurusan['kode']."'$defs>".$jurusan['jurusan']."</option>";
                        }

                    ?>

                </select>
            </div>

        </div>
        <hr />

        <div class="form-group">
            <label>Alamat E-mail</label>
            <input class="form-control" value="<?= $maba['email'] ?>" disabled>
            <p class="help-block"></p>
        </div>
        <button class="btn btn-success" id="reqgantiemail">Ganti Alamat E-mail</button>
        <br />
        <hr />

        <div class="form-group">
            <label>Nomor HP</label>
            <input class="form-control" value="<?= $maba['telepon'] ?>" disabled>
        </div>

        <button class="btn btn-success" id="reqgantihp">Ganti Nomor HP</button>
        <br />
        <hr />

        <div class="panel panel-info">
            <div class="panel-heading"><strong>Informasi</strong></div>
            <div class="panel-body">
                <p>Dari mana Anda mengetahui tentang STMIK KHARISMA Makassar</p>
                    <?php
                        foreach ( $this->siska->sumberinfo as $info ) {
                            $cinfo = "";
                            if( substr( $maba['info'], $info['kode']-1, 1 ) == '1' ){
                                $cinfo = " checked";
                            }
                    ?>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="info[]" value="<?= $info['kode'] ?>" onchange="info_changed();"<?= $cinfo ?>>
                                    <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                    <?= $info['src'] ?>
                                </label>
                            </div>
                    <?php
                        }
                    ?>
                <p class="help-block">*Boleh menandai lebih dari satu</p>
            </div>
        </div>

        <div class="panel panel-info">
            <div class="panel-heading"><strong>Ganti Password</strong></div>
            <div class="panel-body">
                <div id="passwordrespons"></div>

                <div class="form-group">
                    <label>Password Anda Sekarang:</label>
                    <input type="password" class="form-control" id="oldpassword" name="oldpassword" required>
                    <p class="help-block" id="warningoldpassword"></p>
                </div>
                <div class="form-group">
                    <label>Password Baru:</label>
                    <input type="password" class="form-control" id="password1" name="password" required>
                    <p class="help-block" id="warningnewpassword"></p>
                </div>
                <div class="form-group">
                    <label>Verifikasi Password Baru:</label>
                    <input type="password" class="form-control" id="password2" required>
                    <p class="help-block" id="warningpassword"></p>
                </div>

                <p class="help-block">Pastikan bahwa Anda benar-benar ingin mengganti password</p>
                <button class="btn btn-success" id="submitgantipass">Ganti Password</button>

            </div>
        </div>

    </div>
</div>
<div class="row">
    <div class="col-sm-7 col-md-7">
    </div>
    <div class="col-sm-4 col-md-4 text-right">
        <div id="statusupdatebot" class="help-block">
            <i class="fa fa-fw fa-info-circle"></i>
            Perubahan akan tersimpan otomatis
        </div>
        <a href="/pmb/m"><button><i class="fa fa-fw fa-check-square-o"></i> Lihat Status  </button></a>
    </div>
</div>
