<div class="row">
    <div class="col-sm-8 col-md-8">

<?php
    switch ($status) {
        case 1:
            echo "<div class='alert alert-info text-center' role='alert'>
                  <span class='fa fa-check fa-2x' aria-hidden='true'></span><br />
                  Dokumen Anda telah berhasil di-upload dan menunggu validasi oleh Panitia
                  </div>";
            break;
        case 2:
            echo "<div class='alert alert-success text-center' role='alert'>
                  <span class='fa fa-check fa-2x' aria-hidden='true'></span><br />
                  Dokumen Anda telah diganti dengan yang baru dan menunggu validasi ulang oleh Panitia
                  </div>";
            break;
        case 3:
            echo "<div class='alert alert-danger text-center' role='alert'>
                    <span class='fa fa-exclamation-triangle fa-2x' aria-hidden='true'></span><br />
                    Maaf, upload dokumen belum berhasil, ulangi beberapa saat lagi, <br />
                    jika masih gagal, silahkan menghubungi Panitia
                </div>";
            break;
        case 4:
            echo "<div class='alert alert-danger text-center' role='alert'>
                    <span class='fa fa-exclamation-triangle fa-2x' aria-hidden='true'></span><br />
                    Maaf, upload dokumen tidak berhasil, format file tidak dapat diterima, <br />
                    pasikan hanya meng-upload dokumen berformat PDF, JPG atau PNG 
                </div>";
            break;
    }
?>
        <div class="panel panel-info">
            <div class="panel-heading"><strong>Dokumen</strong></div>
            <div class="panel-body">
                <table class="table">
                    <thead>
                        <th width="220px">Dokumen</th>
                        <th class="text-center">Tanggal Upload</th>
                        <th class="text-center">File</th>
                        <th class="text-center">Validasi</th>
                    </thead>
                    <tbody>
                    <?php
                        foreach ($this->siska->dok_maba as $dok) {
                            if( $dok['status'] == '0') $nb = " <strong>**</strong>";
                            elseif( $dok['status'] == '1') $nb = " <strong>*</strong>";
                            elseif( $dok['status'] == '2') $nb = " <strong>***</strong>";

                            $uploaddok = $this->pmbuser_model->checkDokumen( $dok['kode'], $_SESSION['id']);

                            $file = $uploaddok['id_owner']."_".$uploaddok['tipe'].".".$uploaddok['tipefile'];
                            $link = "/assets/dokumen/maba/$tahun/$file";
                            $icon = "<i class='fa fa-file-photo-o text-danger' aria-hidden='true'></i>";
                            if( $uploaddok['tipefile'] == 'pdf') $icon = "<i class='fa fa-file-pdf-o text-danger' aria-hidden='true'></i>";
                    ?>
                            <tr>
                                <td><?= $dok['dokumen'].$nb ?></td>
                                <td class="text-center"><?= $uploaddok['tg_upload'] ?></td>
                                <td class="text-center">
                                    <a href="<?= $link ?>" target="_new">
                                        <?php
                                            if( !empty( $uploaddok['id'] ) ) echo $icon." ".$file;
                                        ?>
                                    </a>
                                </td>
                                <td class="text-center">
                                    <?php
                                        if( !empty( $uploaddok['validasi'] ) ){
                                            echo "<i class='fa fa-check text-success' aria-hidden='true'></i>";
                                        }
                                    ?>
                                </td>
                            </tr>
                    <?php
                        }
                     ?>
                    </tbody>
                </table>
                <p>
                    <strong>Keterangan:</strong><br />
                    <strong>*</strong> dokumen wajib untuk pendaftaran awal<br />
                    <strong>**</strong> dokumen wajib untuk mendapatkan Nomor Induk Mahasiswa (setelah dinyatakan lulus)<br />
                    <strong>***</strong> dokumen wajib untuk Seleksi Jalur Prestasi, tanpa dokumen ini calon tidak akan
                    diikutsertakan dalam seleksi Jalur Prestasi<br /><br />
                    Seluruh dokumen yang di-upload akan divalidasi oleh Panitia
                </p>
            </div>
        </div>
    </div>
    <div class="col-sm-4 col-md-4">
        <div class="panel panel-info">
            <div class="panel-heading"><strong>Upload</strong></div>
            <div class="panel-body">

                <form class="upload-form" action="/pmb/upload" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label>Dokumen*</label>
                        <select class="form-control" id="jenisdokumen" name="jenisdokumen" required>
                            <option value="">-- Pilih Jenis Dokumen --</option>
                            <?php
                                foreach ($this->siska->dok_maba as $dok) {
                                    echo "<option value='".$dok['kode']."'>".$dok['dokumen']."</option>";
                                }
                            ?>

                        </select>
                    </div>

                    <div class="form-group">
                        <label>File* </label>
                        <input type="file" class="form-control upload-file" id="filedokumen" name="filedokumen"
                               data-max-size="512000" accept="application/pdf, image/png, image/jpeg" required>
                        <p class="help-block">.PDF/.JPG/.PNG, max.500KB</p>
                    </div>

                    <input type="submit" class="btn btn-lg btn-success btn-block" id="submit" value="Upload">

                </form>
            </div>
        </div>
        <div class="list-group">
            <a href="<?=base_url()?>pmb/m/syarat" class="list-group-item">
                <strong><i class="fa fa-fw fa-check-square-o"></i> Syarat dan Ketentuan</strong>
            </a>
            <a href="<?=base_url()?>pmb/m" class="list-group-item">
                <strong><i class="fa fa-fw fa-check-square-o"></i> Lihat Status</strong>
            </a>
            <a href="<?=base_url()?>pmb/m/profil" class="list-group-item">
                <strong><i class="fa fa-fw fa-address-card"></i> Update Profil</strong>
            </a>
        </div>
    </div>

</div>
