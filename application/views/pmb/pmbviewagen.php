<div class="row">
    <div class="col-sm-9 col-md-9">
        <div class="panel panel-info">
            <div class="panel-heading"><strong>Link Referrer</strong></div>
            <div class="panel-body">

                <p align='justify'>Berikut adalah link referrer Anda:</p>
                <p class="alert alert-success text-center" id='myref'><?= $mylink ?></p>
                <p align='right'>
                    <button onclick="copyToClipboard('#myref')"><i class="fa fa-fw fa-copy"></i> Copy</button>
                </p>

                <p align='justify'>Atau:</p>
                <p class="alert alert-warning text-center" id='mybitlyref'><?= $mybitly ?></p>
                <p align='right'>
                    <button onclick="copyToClipboard('#mybitlyref')"><i class="fa fa-fw fa-copy"></i> Copy</button>
                </p>
                <p>Sebarkan link referral Anda seluas mungkin pada saluran-saluran komunikasi
                    dan informasi yang Anda miliki untuk membuka peluang mendapatkan reward point
                    yang lebih besar
                </p>
            </div>
        </div>
    </div>
    <div class="col-sm-3 col-md-3">
        <div class="imgqr">
            <img id="imgqr" src="<?= $myqrimg ?>" />
        </div>
        <br />
        <input type="hidden" id="linkref" value="<?= $mylink ?>" />
        <input type="hidden" id="qrref" value="<?= $myqrimg ?>" />
        <input type="hidden" id="bitly" value="<?= $mybitly ?>" />
        <button class="btn btn-success btn-block" id="mailmemyref">
            <i class="fa fa-fw fa-envelope-o"></i> Kirim ke E-mail saya
        </button>
        <br />
        <a href="/pdf/mylinkreferrer" target="_blank">
            <button class="btn btn-success btn-block" id="printmyref">
                <i class="fa fa-fw fa-print"></i> Cetak (<i class="fa fa-fw fa-file-pdf-o text-danger"></i>PDF)
            </button>
        </a>
    </div>

</div>

<div class="row">
    <div class="col-sm-9 col-md-9">
        <div class="panel panel-info">
            <div class="panel-heading"><strong>Referral</strong></div>
            <div class="panel-body">
                <?php
                    $referral = $this->pmbuser_model->get_referral();
                    if( $referral ){
                 ?>
                        <table class='table'>
                            <thead>
                                <tr>
                                    <th class="text-center">No.</th>
                                    <th>Nama Calon Mahasiswa</th>
                                    <th class="text-center">Tgl Reg.</th>
                                    <th class="text-center">Data</th>
                                    <th class="text-center">Dokumen</th>
                                    <th class="text-center">Ujian</th>
                                    <th class="text-center">Register</th>
                                    <th class="text-center">Status</th>
                                </tr>
                            </thead>
                            <tbody>
                <?php
                        $n = 1;
                        foreach ($referral as $ref) {
                            $val = md5($ref['idr'].$ref['ref_id'].$ref['tanggal'].$_SESSION['token']);
                ?>
                                <tr>
                                    <td><?= $n ?>.</td>
                                    <td>
                                        <a href="/pmb/pan/maba/<?= $ref['id'] ?>">
                                            <?= $ref['nama_lengkap'] ?>
                                        </a>
                                    </td>
                                    <td class="text-center"><?= $ref['tgregistrasi'] ?></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center">
                                        <?php
                                            // if( $ref['valid'] == $val ){
                                            //     echo "Pending";
                                            // } else {
                                            //     echo "<span class='text-danger'><strong>INVALID!</strong></span>";
                                            // }
                                        ?>
                                    </td>
                                </tr>
                <?php
                                $n++;
                        }
                 ?>
                            </tbody>
                        </table>
                <?php
                    } else {
                 ?>
                    <h3>Belum ada Referral</h3>
                <?php
                    }
                ?>
            </div>
        </div>
    </div>
    <div class="col-sm-3 col-md-3">
<?php
    $statusreferal = [
                        'klik' => $this->pmbuser_model->get_referral_link(),
                        'regs' => $this->pmbuser_model->get_referral_registers(),
                        'lulus' => $this->pmbuser_model->get_referral_lulus(),
                        'masuk' => $this->pmbuser_model->get_referral_regmasuk()
                    ];

    if( $statusreferal['klik'] > 0 ){
?>
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-2">
                        <i class="fa fa-mouse-pointer fa-3x"></i>
                    </div>
                    <div class="col-xs-9">
                        <span class="big"><?= $statusreferal['klik'] ?></span>
                        <span><strong>Klik</strong></span>
                    </div>
                </div>
            </div>
            <a href="#">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
<?php
    }

    if( $statusreferal['regs'] > 0 ){
 ?>
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-2">
                        <i class="fa fa-pencil-square-o fa-3x"></i>
                    </div>
                    <div class="col-xs-9">
                        <span class="big"><?= $statusreferal['regs'] ?></span>
                        <span><strong>Registrasi</strong></span>
                    </div>
                </div>
            </div>
            <a href="#">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
<?php
    }

    if( $statusreferal['lulus'] > 0 ){
?>
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-2">
                        <i class="fa fa-check-square-o fa-3x"></i>
                    </div>
                    <div class="col-xs-9">
                        <span class="big"><?= $statusreferal['lulus'] ?></span>
                        <span><strong>Lulus</strong></span>
                    </div>
                </div>
            </div>
            <a href="#">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
<?php
    }

    if( $statusreferal['masuk'] > 0 ){
 ?>

        <div class="panel panel-green">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-2">
                        <i class="fa fa-unlock fa-3x"></i>
                    </div>
                    <div class="col-xs-9">
                        <span class="big"><?= $statusreferal['masuk'] ?></span>
                        <span><strong>Unlock</strong></span>
                    </div>
                </div>
            </div>
            <a href="#">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
<?php
    }
 ?>
    </div>

</div>
