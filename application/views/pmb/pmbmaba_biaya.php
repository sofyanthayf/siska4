<div class="col-sm-9 col-md-9">
    <div class="panel panel-info">

        <div class="panel-heading">
            <h3>Biaya Pendidikan Tahun Akademik <?php echo $tahun."-".($tahun+1); ?></h3>
        </div>

        <div class="panel-body">
            <table class="table" width="100%" cellspacing="5px" cellpadding="15px"
                    style="padding:20px;
                           text-align:center;
                           font-size:18px;">
                <tr>
                    <td width="250px"><strong>Program Pendidikan</strong></td>
                    <td><strong>Program Sarjana Sistem Informasi<br>
                                Program Sarjana Teknik Informatika
                    </strong></td>
                    <td><strong>Program Sarjana Bisnis Digital</strong></td>
                </tr>
                <tr>
                    <td>
                        Biaya SPP
                        <p class="help-block" style="font-size:12px">
                            (Sumbangan Pengembangan Pendidikan)<br />
                            Dibayar sekali, setelah lulus seleksi
                        </p>
                    </td>
                    <td><p class="text-danger"><strong>Free</strong></p></td>
                    <td><p class="text-danger"><strong>Free</strong></p></td>
                </tr>
                <tr>
                    <td>
                        Biaya Kuliah
                        <p class="help-block" style="font-size:12px">
                            Dibayar per-semester
                        </p>
                    </td>
                    <td>Rp 4.500.000,-</td>
                    <td>Rp 2.500.000,-</td>
                </tr>
                <tr>
                    <td>Biaya Kelengkapan dan Kegiatan Mahasiswa</td>
                    <td>Rp 1.000.000,-</td>
                    <td>Rp 1.000.000,-</td>
                </tr>
                <tr>
                    <td colspan="3" align="center">
                        <p class="help-block">
                            Belum termasuk biaya KKLP, Proposal, Ujian Akhir, dan Wisuda
                        </p>
                    </td>
                </tr>

            </table>
        </div>

    </div>
    <p class="help-block">
        <strong>Biaya yang sudah dibayarkan, tidak dapat ditarik kembali</strong>
    </p>
</div>

<div class="col-sm-3 col-md-3">
    <div class="list-group">
        <a href="<?=base_url()?>pmb/m/syarat" class="list-group-item">
            <strong><i class="fa fa-fw fa-check-square-o"></i> Persyaratan</strong>
        </a>
        <a href="<?=base_url()?>pmb/m/biaya" class="list-group-item disabled">
            <strong><i class="fa fa-fw fa-check-square-o"></i> Pembiayaan</strong>
        </a>
        <a href="<?=base_url()?>pmb/m/jadwal" class="list-group-item">
            <strong><i class="fa fa-fw fa-check-square-o"></i> Jadwal Pendaftaran</strong>
        </a>

<?php
    if( isset( $_SESSION['klas'] ) && $_SESSION['klas'] == 'maba'  ){
?>

        <a href="<?=base_url()?>pmb/m/profil" class="list-group-item">
            <strong><i class="fa fa-fw fa-address-card"></i> Update Profil</strong>
        </a>
        <a href="<?=base_url()?>pmb/dokumen" class="list-group-item">
            <strong><i class="fa fa-fw fa-upload"></i> Upload Dokumen</strong>
        </a>
<?php
    }
?>

    </div>
</div>
