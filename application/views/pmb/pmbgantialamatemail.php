<div class="col-sm-5 col-md-5">
    <div class="account-wall">
    <?php
        if( $status == '1' ) {
     ?>
        <div class='alert alert-danger text-center' role='alert'>
                <span class='fa fa-exclamation-triangle fa-2x' aria-hidden='true'></span><br />
                Password salah, gagal mengganti alamat E-Mail
        </div>
    <?php
        }
     ?>
        <p class="text-center">Masukkan Alamat E-Mail Anda yang baru:</p>
        <form class="form-signin" action="<?=base_url()?>pmb/resetalamatemail" method="post">
        <div class="form-group">
            <label>Alamat E-Mail Anda*</label>
            <input type="email" id="email" name="email" class="form-control" required autofocus>
            <p class="help-block" id="warningemail"></p>
        </div>
        <div class="form-group">
            <label>Password Anda*</label>
            <input type="password" id="passw" name="password" class="form-control" required>
        </div>
            <p class="help-block">Alamat E-mail Anda akan diverifikasi kembali</p>
            <input type="submit" class="btn btn-lg btn-success btn-block" name="submit" value="Simpan Alamat Email">
        </form>
    </div>
    <br />
</div>
