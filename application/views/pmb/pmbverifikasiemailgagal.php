<div class="col-sm-8 col-md-8">
    <i class="fa fa-fw fa-envelope-o fa-4x"></i><i class="fa fa-fw fa-times fa-4x"></i><br />
    <h3>Maaf, verifikasi alamat E-mail Anda tidak berhasil, atau link verifikasi telah <i>expired</i> </h3>
    <h4>Klik link di bawah ini untuk meminta pengiriman kode verifikasi yang baru</h4><hr />

    <a href="/pmb/verify/request"><button class="btn btn-lg btn-success btn-block">Kirim Ulang Kode Verifikasi</button></a>
    <hr />

</div>
