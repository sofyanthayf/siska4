<form role="form" action="/pmb/a/register" method="post">
    <div class="col-sm-6 col-md-6">
        <div class="form-group">
            <label>Status Afiliasi</label>
            <select class="form-control" id="opsiafiliasi" name="tipeafiliasi" required>
                <option value="">-- Pilih --</option>
                <option value="A">Alumni STMIK KHARISMA</option>
                <option value="S">Dosen/Staf Pengelola STMIK KHARISMA</option>
                <option value="G">Guru SMA/SMK</option>
                <option value="M">Mahasiswa STMIK KHARISMA</option>
                <option value="U">Umum</option>
            </select>
        </div>

        <!-- isian untuk Mahasiswa -->
        <div id="opsimhs">
            <div class="form-group">
                <label>Nomor Induk Mahasiswa</label>
                <input class="form-control" placeholder="NIM Anda" id="nimmhs" name="nimhs">
                <p class="help-block"></p>
            </div>
        </div>

        <!-- isian untuk Alumni -->
        <div id="opsialumni">
            <div class="form-group">
                <label>Angkatan</label>
                <select class="form-control" id="thnalumni">
                    <option value="">-- Pilih Angkatan Anda --</option>
                </select>
            </div>
            <div class="form-group">
                <label>Program Studi Anda</label>
                <select class="form-control" id="prdalumni">
                    <option value="">-- Pilih Program Studi --</option>
                </select>
            </div>
            <div class="form-group">
                <label>Tunjukkan Nama Anda</label>
                <select class="form-control" id="namaalumni" name="nimalumni">
                    <option value="">-- Pilih Nama Anda --</option>
                </select>
            </div>
        </div>

        <!-- isian untuk Dosen dan Staf -->
        <div id="opsistaf">
            <div class="form-group">
                <label>NIDN (dosen) atau NIK (staf)</label>
                <input class="form-control" placeholder="NIDN/NIK" id="nidnnik" name="nidnnik">
                <p class="help-block"></p>
            </div>
        </div>


        <!-- isian untuk Guru -->
        <div id="opsiguru">
            <div class="form-group">
                <label>Propinsi Sekolah</label>
                <select class="form-control" id="propinsisekolah">
                    <option value="">-- Pilih Propinsi Sekolah--</option>
<?php
    foreach ($this->region_model->getListPropinsi() as $propinsi) {
        echo "<option value='".$propinsi['id']."'>".$propinsi['nama']."</option>";
    }
?>
                </select>
            </div>

            <div class="form-group">
                <label>Kabupaten / Kota Sekolah</label>
                <select class="form-control" id="kotasekolah">
                    <option value="">-- Pilih Kabupaten/Kota Sekolah --</option>
                    <option value="">KOTA MAKASSAR</option>
                </select>
            </div>

            <div class="form-group">
                <label>Nama Sekolah</label>
                <select class="form-control" id="listsekolah" name="idsekolah">
                    <option value="">-- Pilih Sekolah --</option>
                    <option value="">KOTA MAKASSAR</option>
                </select>
            </div>

        </div>
        <hr />
        <div class="form-group">
            <label>E-mail Anda*</label>
            <input class="form-control" id="email" name="email" required>
            <p class="help-block" id="warningemail"></p>
        </div>
        <div class="form-group">
            <label>Nomor HP Anda*</label>
            <input class="form-control" placeholder="08xxxxxxxxx" id="nomorhp" name="nomorhp" required>
            <p class="help-block" id="warninghp"></p>
        </div>
        <div class="form-group">
            <label>Nama Lengkap Anda*</label>
            <input class="form-control" id="namalengkap" name="namalengkap" required>
            <p class="help-block"></p>
        </div>
        <div class="form-group">
            <label>Alamat</label>
            <input class="form-control" id="alamat" name="alamat">
            <p class="help-block"></p>
        </div>

        <div class="form-group">
            <label>Propinsi Tempat Tinggal</label>
            <select class="form-control" id="propinsi" name="propinsi">
                <option value="">-- Pilih Propinsi --</option>
<?php
    foreach ($this->region_model->getListPropinsi() as $propinsi) {
        echo "<option value='".$propinsi['id']."'>".$propinsi['nama']."</option>";
    }
?>
            </select>
        </div>

        <div class="form-group">
            <label>Kabupaten / Kota</label>
            <select class="form-control" id="kabkota" name="kabkota">
                <option value="">-- Pilih Kabupaten/Kota --</option>
                <option value="">KOTA MAKASSAR</option>
            </select>
        </div>

        <div class="form-group">
            <label>Kecamatan</label>
            <select class="form-control" id="kecamatan">
                <option value="">-- Pilih Kecamatan --</option>
            </select>
        </div>

        <div class="form-group">
            <label>Desa/Kelurahan</label>
            <select class="form-control" id="desakelurahan" name="desakelurahan">
                <option value="">-- Pilih Desa/Kelurahan --</option>
            </select>
        </div>


    </div>
    <div class="col-sm-5 col-md-5">

        <div class="form-group">
            <label>Password*:</label>
            <input type="password" class="form-control" id="password1" name="password" required>
            <p class="help-block"></p>
        </div>
        <div class="form-group">
            <label>Verifikasi Password*:</label>
            <input type="password" class="form-control" id="password2" required>
            <p class="help-block" id="warningpassword"></p>
        </div>

        <p class="help-block">Password hanya akan digunakan untuk layanan PMB saja.</p>
        <hr />
        <br />
        <a href="<?=base_url()?>pmb/a/syarat" target="_blank"><strong>Silahkan membaca Syarat dan Ketentuan</strong></a>
        <br />
        <br />

        <div class="form-group">
            <label class="checkbox-inline">
                <input type="checkbox" id="setujusk" name="setujusk">
                <p>Saya telah membaca dan menyetujui semua ketentuan yang berlaku dalam Reward Program 2017 ini</p>
            </label>
        </div>
        <div class="form-group">
            <label class="checkbox-inline">
                <input type="checkbox" id="databenar" name="databenar">
                <p>Semua data yang saya berikan dalam formulir isian ini adalah benar sesuai dengan keadaan yang sebenarnya</p>
            </label>
        </div>

        <input type="submit" class="btn btn-lg btn-success btn-block" name="submit" id="submit" value="Daftarkan">
        <br />
        <p class="help-block">* Harus diisi</p>

    </div>
</form>
