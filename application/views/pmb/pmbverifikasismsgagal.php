<div class="col-sm-8 col-md-8">
    <i class="fa fa-fw fa-mobile fa-4x"></i><i class="fa fa-fw fa-times fa-4x"></i><br />
    <h3>Maaf, verifikasi nomor HP Anda tidak berhasil, PIN yang anda masukkan tidak benar atau telah <i>expired</i> </h3>
    <h4>Silahkan masukkan ulang PIN yang Anda terima melalui SMS (<i>case-sensitive</i>)</h4>
    <form role="form" action="/pmb/verify/sms" method="post">
        <div class="form-group row">
            <div class="col-sm-3 col-md-3">
                <input type="password" class="form-control input-lg" id="inputpin" name="inputpin" required autofocus>
                <input type="submit" class="form-control btn-success" value="VERIFIKSI">
            </div>
        </div>
    </form>

    <h4>atau, klik untuk <a href="/pmb/verify/smsrequest">Minta Pengiriman Ulang Kode Verifikasi</a></h4>
    <hr />

</div>
