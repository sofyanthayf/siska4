<div class="col-sm-8 col-md-8">
    <i class="fa fa-fw fa-envelope fa-4x"></i></i><br />
    <h3>Kode verifikasi telah dikirimkan ke alamat e-mail Anda,
        silahkan cek mailbox Anda untuk langkah registrasi berikutnya</h3>

    <p class="help-block">Ada kemungkinan email dari sistem kami masuk ke kotak spam mailbox Anda</p>

</div>
