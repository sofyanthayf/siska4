<?php
    if( !isset( $_SESSION['id'] ) && !$public  ){
        redirect('/pmb');
    }

    if (empty($_SESSION['csrf_token'])) {
        // if (function_exists('mcrypt_create_iv')) {
        //     $_SESSION['csrf_token'] = bin2hex(mcrypt_create_iv(32, MCRYPT_DEV_URANDOM));
        // } else {
            $_SESSION['csrf_token'] = bin2hex(openssl_random_pseudo_bytes(32));
        // }
    }

    if( isset($_SESSION['klas']) ){
        if( $_SESSION['klas'] == 'maba' ) {
            $linkprofil = '/pmb/m/profil';
        } elseif( $_SESSION['klas'] == 'agen' ) {
            $linkprofil = '/pmb/a/profil';
        }
    }
?>
<!DOCTYPE html>
<html lang="id">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="SISKA PMB - Layanan Penerimaan Mahasiswa Baru, pada Sistem Informasi Akademik STMIK KHARISMA Makassar">
    <meta name="author" content="Sofyan Thayf - KHARISMA Innovation Lab">
    <meta name="token-a" content="<?= $_SESSION['csrf_token'] ?>">

    <title>SISKA 4.0</title>

    <link href="<?=base_url()?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/css/sb-admin.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/css/plugins/morris.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <link href="<?=base_url()?>assets/css/bootstrap-datetimepicker.css" rel="stylesheet">

    <link href="<?=base_url()?>assets/css/jquery.dataTables.css" rel="stylesheet" type="text/css">

    <link href="<?=base_url()?>assets/css/siskapmbstyle.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/css/tabs.css" rel="stylesheet">

    <link rel='shortcut icon' href='<?=base_url()?>assets/img/favicon.ico' type='image/x-icon'/ >

    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '268155040281066'); // Insert your pixel ID here.
    fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=268155040281066&ev=PageView&noscript=1"
    /></noscript>
    <!-- DO NOT MODIFY -->
    <!-- End Facebook Pixel Code -->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/"><span class="fa fa-fw fa-graduation-cap" aria-hidden="true"></span> SISKA 4.0</a>
            </div>

            <ul class="nav navbar-right top-nav">

<?php
    if( isset( $_SESSION['id'] ) ) {
 ?>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?=$_SESSION['nama']?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">

        <?php
            if( isset( $_SESSION['klas'] ) && $_SESSION['klas'] == 'agen'  ){
        ?>
                        <li>
                            <a href="/pmb/a"><i class="fa fa-fw fa-group"></i> Referral</a>
                        </li>
        <?php
            }
        ?>

        <?php
            if( isset( $_SESSION['klas'] ) && $_SESSION['klas'] == 'maba'  ){
        ?>
                        <li>
                            <a href="/pmb/m"><i class="fa fa-fw fa-check-square"></i> Status</a>
                        </li>
        <?php
            }
        ?>

                        <li>
                            <a href="<?= $linkprofil ?>"><i class="fa fa-fw fa-user"></i> Profil</a>
                        </li>

                        <li class="divider"></li>
                        <li>
                            <a href="/pmb/signout"><i class="fa fa-fw fa-sign-out"></i> Sign Out</a>
                        </li>
                    </ul>
                </li>
<?php
    } else {
?>

                <li><a href="/pmb"><i class="fa fa-sign-in"></i> SIGN IN </a></li>
<?php
    }
?>

            </ul>



            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">

                    <li>
                        <a href="http://www.kharisma.ac.id" target="_blank"><i class="fa fa-fw fa-university"></i> STMIK KHARISMA Makassar</a>
                    </li>

                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#maba">
                            <i class="fa fa-fw fa-graduation-cap"></i> Info Mahasiswa Baru
                            <i class="fa fa-fw fa-caret-down text"></i>
                        </a>
                        <ul id="maba" class="collapse">
                             <li>
                                <a href="/pmb/m/syarat"><i class="fa fa-fw fa-check-square-o"></i> Persyaratan</a>
                            </li>
                            <li>
                                <a href="/pmb/m/biaya"><i class="fa fa-fw fa-calendar"></i> Pembiayaan</a>
                            </li>
                            <li>
                                <a href="/pmb/m/jadwal"><i class="fa fa-fw fa-calendar"></i> Jadwal</a>
                            </li>
<?php
    if( !isset( $_SESSION['id'] ) ){
?>
                            <li>
                                <a href="/pmb/m/reg"><i class="fa fa-fw fa-pencil-square-o"></i> Formulir Pendaftaran</a>
                            </li>
<?php
    }
?>
                        </ul>
                    </li>


                    <li>
                        <a href="/prodi"><i class="fa fa-fw fa-book"></i> Program Studi</a>
                    </li>
                    <li>
                        <a href="/kurikulum"><i class="fa fa-fw fa-book"></i> Kurikulum</a>
                    </li>

    <?php

        if( isset( $_SESSION['id'] ) && $_SESSION['klas'] == 'agen' ){
            if( $this->pmb_lib->panitia( $_SESSION['token'], $_SESSION['pan'] ) ) {
    ?>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#panitia">
                            <i class="fa fa-fw fa-user-secret"></i> Panitia PMB
                            <i class="fa fa-fw fa-caret-down text"></i>
                        </a>
                        <ul id="panitia" class="collapse">
                            <li>
                                <a href="/pmb/pan/maba"><i class="fa fa-fw fa-users"></i> Daftar Calon Maba</a>
                            </li>
                            <li>
                                <a href="/pmb/pan/validasi"><i class="fa fa-fw fa-check-square-o"></i> Dokumen Calon Maba</a>
                            </li>
                            <li>
                                <a href="/pmb/pan/jadwalseleksi"><i class="fa fa-fw fa-check-square-o"></i> Penjadwalan Seleksi</a>
                            </li>
                            <li>
                                <a href="/pmb/pan/agen"><i class="fa fa-fw fa-handshake-o"></i> Agen Reward Point</a>
                            </li>
                        </ul>
                    </li>
    <?php
            }
        }
    ?>


                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            <?= $judul1 ?> <small><?= $judul2 ?></small>
                        </h1>
                    </div>
                </div>
                <!-- /.row -->
