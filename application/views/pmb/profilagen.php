<div class="row">
    <div class="col-sm-7 col-md-7">
        <h2>Update Profil</h2><br />
    </div>
    <div class="col-sm-4 col-md-4 text-right">
        <div id="statusupdate" class="help-block">
            <i class="fa fa-fw fa-info-circle"></i>
            Perubahan akan tersimpan otomatis<br />
        </div>
        <a href="/pmb/a"><button><i class="fa fa-fw fa-check-square-o"></i> Lihat Status Agen</button></a>
    </div>
</div>
<div class="row">
    <div class="col-sm-6 col-md-6">

        <div class="form-group">
            <label>Nama Lengkap Anda</label>
            <input class="form-control" id="pnamalengkap" name="namalengkap" value="<?=$agen['nama_lengkap']?>">
            <p class="help-block"></p>
        </div>

<?php
    if( $agen['sex'] == 'L' ){
        $clk = " checked='checked'";
        $vlk = " active";
        $cpr = "";
        $vpr = "";
    } else {
        $clk = "";
        $vlk = "";
        $cpr = " checked='checked'";
        $vpr = " active";
    }
?>
        <div class="form-group">
            <label>Jenis Kelamin: </label>
            <div id ="jkel" data-toggle="buttons">
                <label class="btn btn-primary btn-outline<?= $vlk ?>">
                    <input type="radio" name="jkel" value="L"<?= $clk ?>>
                    <i class="fa fa-male"></i> Laki-laki
                </label>
                <label class="btn btn-primary btn-outline<?= $vpr ?>">
                    <input type="radio" name="jkel" value="P"<?= $cpr ?>>
                    <i class="fa fa-female"></i> Perempuan
                </label>
            </div>
            <p class="help-block"></p>
        </div>

        <div class="form-group">
            <label>Pekerjaan</label>
            <select class="form-control" id="pekerjaan" name="pekerjaan">
                <option value="">-- Pilih Jenis Pekerjaan --</option>

                <?php
                    foreach ( $this->siska->pekerjaan as $pekerjaan ) {
                        if( $pekerjaan['kode'] == $agen['pekerjaan'] ){
                            $defk = ' selected';
                        } else {
                            $defk ='';
                        }
                        echo "<option value='".$pekerjaan['kode']."'$defk>".$pekerjaan['pekerjaan']."</option>";
                    }

                 ?>
            </select>
        </div>

        <div class="form-group">
            <label>Alamat Tempat Tinggal</label>
            <input class="form-control" id="alamat" name="alamat" value="<?=$agen['alamat']?>">
            <p class="help-block"></p>
        </div>

        <div class="form-group">
            <label>Propinsi Tempat Tinggal</label>
            <select class="form-control" id="propinsi" name="propinsi">
                <option value="">-- Pilih Propinsi --</option>
                <?php
                    // $mabaregion = "";
                    if( !empty( $agen['kelurahan'] ) ){
                        $mabaregion = $this->region_model->getRegion( $agen['kelurahan'] );
                    }
                    foreach ($this->region_model->getListPropinsi() as $propinsi) {
                        $selp = "";
                        if( !empty( $agen['kelurahan'] ) && $propinsi['id'] == $mabaregion['id_propinsi'] ){
                            $selp = " selected";
                        }
                        echo "<option value='".$propinsi['id']."'$selp>".$propinsi['nama']."</option>";
                    }
                ?>
            </select>
        </div>

        <div class="form-group">
            <label>Kabupaten / Kota</label>
            <select class="form-control" id="kabkota" name="kabkota">
                <option value="">-- Pilih Kabupaten/Kota --</option>
                <option value="">KOTA MAKASSAR</option>
                <?php
                    if( !empty( $agen['kelurahan'] ) ){
                        foreach ($this->region_model->getListKotaKabupaten( $mabaregion['id_propinsi'] ) as $kota) {
                            if( $kota['id'] == $mabaregion['id_kota'] ){
                                $selk = " selected";
                            } else {
                                $selk = "";
                            }
                            echo "<option value='".$kota['id']."'$selk>".$kota['name']."</option>";
                        }
                    }
                ?>
            </select>
        </div>

        <div class="form-group">
            <label>Kecamatan</label>
            <select class="form-control" id="kecamatan">
                <option value="">-- Pilih Kecamatan --</option>
                <?php
                    if( !empty( $agen['kelurahan'] ) ){
                        foreach ($this->region_model->getListKecamatan( $mabaregion['id_kota'] ) as $kecamatan) {
                            if( $kecamatan['id'] == $mabaregion['id_kecamatan'] ){
                                $selk = " selected";
                            } else {
                                $selk = "";
                            }
                            echo "<option value='".$kecamatan['id']."'$selk>".$kecamatan['name']."</option>";
                        }
                    }
                ?>
            </select>
        </div>

        <div class="form-group">
            <label>Desa/Kelurahan</label>
            <select class="form-control" id="desakelurahan" name="desakelurahan">
                <option value="">-- Pilih Desa/Kelurahan --</option>
                <?php
                    if( !empty( $agen['kelurahan'] ) ){
                        foreach ($this->region_model->getListDesaKelurahan( $mabaregion['id_kecamatan'] ) as $desa) {
                            if( $desa['id'] == $mabaregion['id_desa'] ){
                                $selk = " selected";
                            } else {
                                $selk = "";
                            }
                            echo "<option value='".$desa['id']."'$selk>".$desa['name']."</option>";
                        }
                    }
                ?>
            </select>
        </div>
        <hr />

        <div class="form-group">
            <label>Rekening Bank</label><br />

            <div class="form-group">
                <label>Nomor Rekening:</label>
                <input class="form-control" id="rekening" name="rekening" value="<?=$agen['nomor_rekening']?>">
                <p class="help-block"></p>
            </div>

            <label>Bank:</label>
            <select class="form-control" id="bank" name="bank">
                <option value="">-- Pilih Nama Bank --</option>

                <?php
                    foreach ( $this->region_model->getListBank() as $bank ) {
                        if( $bank['kode'] == $agen['bank_rekening'] ){
                            $defk = ' selected';
                        } else {
                            $defk ='';
                        }
                        echo "<option value='".$bank['kode']."'$defk>".$bank['nama']."</option>";
                    }

                 ?>
            </select>
        </div>

        <div class="form-group">
            <label>Atas Nama:</label>
            <input class="form-control" id="namarekening" name="namarekening" value="<?=$agen['nama_rekening']?>">
            <p class="help-block"></p>
        </div>

        <div class="form-group">
            <label class="checkbox-inline">
                <?php
                    $chkb = "";
                    if( $agen['setuju_biaya'] == 1 ) $chkb=" checked"
                ?>
                <input type="checkbox" id="setujubiaya" name="setujubiaya" value="1"<?=$chkb?>>
                <p>Dengan memberikan nomor rekening saya maka saya menyetujui transaksi claim reward melalui transfer bank
                   dan menyetujui pemotongan biaya transfer sesuai ketentuan</p>
            </label>
        </div>

        <hr />

        <div class="panel panel-info">
            <div class="panel-heading"><strong>Informasi</strong></div>
            <div class="panel-body">
                <p>Jika Anda bukan Mahasiswa/Alumni/Dosen/Staf STMIK KHARISMA Makassar,<br />
                   dari mana Anda mengetahui tentang STMIK KHARISMA Makassar</p>
                    <?php
                        foreach ( $this->siska->sumberinfo as $info ) {
                            $cinfo = "";
                            if( substr( $agen['info'], $info['kode']-1, 1 ) == '1' ){
                                $cinfo = " checked";
                            }
                    ?>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="info[]" value="<?= $info['kode'] ?>" onchange="info_changed();"<?= $cinfo ?>>
                                    <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                    <?= $info['src'] ?>
                                </label>
                            </div>
                    <?php
                        }
                    ?>
                <p class="help-block">*Boleh menandai lebih dari satu</p>
            </div>
        </div>


    </div>

    <div class="col-sm-5 col-md-5">
        <div class="panel panel-info">
            <div class="panel-heading"><strong>Link Referrer</strong></div>
            <div class="panel-body">
                <div class="form-group">
                    <label>Bit.ly Custom URL:</label>
                    <input class="form-control" id="bitly" value="<?= $agen['bitly_url'] ?>">
                    <p class="help-block">
                        Sebelum merubah, pastikan Anda telah memiliki Custom URL Bit.ly yang valid
                        untuk Link Referrer Anda
                        (bisa dibuat di <a href="http://bitly.com" target="_blank">bitly.com</a>)
                    </p>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label>Alamat E-mail</label>
            <input class="form-control" value="<?= $agen['email'] ?>" disabled>
            <p class="help-block"></p>
        </div>
        <button class="btn btn-success" id="reqgantiemail">Ganti Alamat E-mail</button>
        <span class="text-primary"> (<i>akan diverifikasi ulang</i>)</span>
        <br />
        <hr />

        <div class="form-group">
            <label>Nomor HP</label>
            <input class="form-control" value="<?= $agen['telepon'] ?>" disabled>
        </div>

<?php
    if( $agen['telepon_valid'] == '0000-00-00 00:00:00'){
?>
    <div class='alert alert-danger' role='alert'>
        <form action="/pmb/verify/sms" method="post">
            <div class="form-group">
                <label>Nomor HP Anda belum terverifikasi:</label>
                <p class="help-block">Masukkan kode verifikasi yang Anda terima melalui SMS (case-sensitive)</p>
                <input type="password" name="inputpin" required="required">
                <input type="hidden" name="page" value="profil">
                <input type="submit" class="btn btn-success" value="Verifikasi">
            </div>
        </form>

        <a><button id="requestsms"><i class="fa fa-fw fa-refresh"></i> Request Pengiriman Ulang SMS Verifikasi</button></a>
    </div>
<?php
    }
?>

        <button class="btn btn-success" id="reqgantihp">Ganti Nomor HP</button>
        <span class="text-primary"> (<i>akan diverifikasi ulang</i>)</span>
        <br />
        <hr />

        <div class="panel panel-info">
            <div class="panel-heading"><strong>Ganti Password</strong></div>
            <div class="panel-body">
                <div id="passwordrespons"></div>

                <div class="form-group">
                    <label>Password Anda Sekarang:</label>
                    <input type="password" class="form-control" id="oldpassword" name="oldpassword" required>
                    <p class="help-block" id="warningoldpassword"></p>
                </div>
                <div class="form-group">
                    <label>Password Baru:</label>
                    <input type="password" class="form-control" id="password1" name="password" required>
                    <p class="help-block" id="warningnewpassword"></p>
                </div>
                <div class="form-group">
                    <label>Verifikasi Password Baru:</label>
                    <input type="password" class="form-control" id="password2" required>
                    <p class="help-block" id="warningpassword"></p>
                </div>

                <p class="help-block">Pastikan bahwa Anda benar-benar ingin mengganti password</p>
                <button class="btn btn-success" id="submitgantipass">Ganti Password</button>

            </div>
        </div>

    </div>
</div>
<div class="row">
    <div class="col-sm-7 col-md-7">
    </div>
    <div class="col-sm-4 col-md-4 text-right">
        <div id="statusupdatebot" class="help-block">
            <i class="fa fa-fw fa-info-circle"></i>
            Perubahan akan tersimpan otomatis
        </div>
        <a href="/pmb/a"><button><i class="fa fa-fw fa-check-square-o"></i> Lihat Status Agen</button></a>
    </div>
</div>
