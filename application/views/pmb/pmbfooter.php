

            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

       <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>
                        &copy; STMIK KHARISMA Makassar, 2017<br>
                        Developed by <a href='http://k-innovation.org' target="_blank">KHARISMA Innovation Lab</a><br>
                        Powered by <a href='https://m.do.co/c/6cc56cdf090e' target="_blank">Digital Ocean</a>
                    </p>
                </div>
            </div>
        </footer>


    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="<?=base_url()?>assets/js/jquery.js"></script>
    <script src="<?=base_url()?>assets/js/jquery.validate.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript
    <script src="<?=base_url()?>assets/js/plugins/morris/raphael.min.js"></script>
    <script src="<?=base_url()?>assets/js/plugins/morris/morris.min.js"></script>
    <script src="<?=base_url()?>assets/js/plugins/morris/morris-data.js"></script>
-->
    <script type="text/javascript" src="<?=base_url()?>assets/js/jquery.dataTables.js" charset="utf8"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/js/bootstrap-datetimepicker.id.js" charset="UTF-8"></script>

    <script src="<?=base_url()?>assets/js/siska_form.js"></script>
    <script src="<?=base_url()?>assets/js/siska_pmb.js"></script>

<?php
    if( $page == 'profilmaba' ) {
?>
    <script src="<?=base_url()?>assets/js/siska_profilmaba.js"></script>
<?php
    }
?>

<?php
    if( $page == 'profilagen' ) {
?>
    <script src="<?=base_url()?>assets/js/siska_profilagen.js"></script>
<?php
    }
?>

<?php
    if( $page == 'panpmb' ) {
?>
    <script src="<?=base_url()?>assets/js/siska_panpmb.js"></script>
<?php
    }
?>

</body>

</html>
