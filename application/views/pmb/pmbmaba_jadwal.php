<div class="col-sm-9 col-md-9">
    <div class="panel panel-info">

        <div class="panel-heading">
            <h3>Jadwal Penerimaan Maba Tahun Akademik <?php echo $tahun."-".($tahun+1); ?></h3>
        </div>

        <div class="panel-body">
            <table class="table" width="100%" cellpadding="10px"
                style="padding:20px;
                       font-size:16px;">
                <thead>
                    <th>Periode</th>
                    <th>Mulai</th>
                    <th>Sampai</th>
                </thead>
                <tbody>
                    <tr class="success">
                        <td>
                            <strong>Gelombang Promo/Expo</strong>
                            <p class="help-block" style="font-size:12px">
                                (dengan penawaran khusus)
                            </p>
                        </td>
                        <td>16 Oktober 2024</td>
                        <td>31 Januari 2025</td>
                    </tr>
                    <tr>
                        <td><strong>Gelombang I</strong></td>
                        <td>01 Februari 2025</td>
                        <td>31 Mei 2025</td>
                    </tr>
                    <tr>
                        <td><strong>Gelombang II</strong></td>
                        <td>01 Juni 2025</td>
                        <td>31 Agustus 2025</td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Gelombang III</strong>
                            <p class="help-block" style="font-size:12px">
                                (jika kapasitas masih memungkinkan)
                            </p>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                </tbody>

            </table>
            <p class="help-block" style="font-size:12px">
                Tahapan dan proses seleksi (ujian, wawancara, psikotest) akan dijadwalkan tersendiri untuk<br />
                masing-masing individu atau kelompok calon mahasiswa, dalam setiap periode penerimaan
            </p>
        </div>

    </div>
</div>

<div class="col-sm-3 col-md-3">
    <div class="list-group">
        <a href="<?=base_url()?>pmb/m/syarat" class="list-group-item">
            <strong><i class="fa fa-fw fa-check-square-o"></i> Persyaratan</strong>
        </a>
        <a href="<?=base_url()?>pmb/m/biaya" class="list-group-item">
            <strong><i class="fa fa-fw fa-check-square-o"></i> Pembiayaan</strong>
        </a>
        <a href="<?=base_url()?>pmb/m/jadwal" class="list-group-item disabled">
            <strong><i class="fa fa-fw fa-check-square-o"></i> Jadwal Pendaftaran</strong>
        </a>

<?php
    if( isset( $_SESSION['klas'] ) && $_SESSION['klas'] == 'maba'  ){
?>

        <a href="<?=base_url()?>pmb/m/profil" class="list-group-item">
            <strong><i class="fa fa-fw fa-address-card"></i> Update Profil</strong>
        </a>
        <a href="<?=base_url()?>pmb/dokumen" class="list-group-item">
            <strong><i class="fa fa-fw fa-upload"></i> Upload Dokumen</strong>
        </a>
<?php
    }
?>

    </div>
</div>
