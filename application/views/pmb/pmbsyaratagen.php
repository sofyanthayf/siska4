<div class="col-sm-9 col-md-9">
<div class="panel panel-info">

    <div class="panel-heading">
        <h3>I. Nama Program</h3>
    </div>

    <div class="panel-body">
        <p align='justify'>Program Reward Point STMIK KHARISMA Makassar</p>
    </div>

    <div class="panel-heading">
        <h3>II. Maksud dan Tujuan</h3>
    </div>
    <div class="panel-body">
        <p align='justify'>
            Program Reward Point merupakan suatu program apresiasi yang diselenggarakan oleh STMIK KHARISMA Makassar
            dengan tujuan memberikan kesempatan seluas-luasnya kepada seluruh civitas akademika (mahasiswa, dosen,
            dan staf) dan stake holder (guru/staf sekolah dan alumni) untuk terlibat dalam kegiatan memperkenalkan
            STMIK KHARISMA Makassar dan berupaya semaksimal mungkin mempengaruhi calon mahasiswa supaya mendaftar dan
            menjadi mahasiswa di STMIK KHARISMA Makassar.
        </p>
        <p align='justify'>
            Sebagai bentuk penghargaan STMIK KHARISMA Makassar atas partisipasi tersebut, maka panitia akan memberikan
            apresiasi dalam bentuk dana tunai kepada setiap civitas academica yang berhasil mengajak calon mahasiswa
            untuk bergabung menjadi bagian dari keluarga besar STMIK KHARISMA Makassar sebagai mahasiswa baru STMIK KHARISMA
            Makassar Tahun Akademik 2017/2018.
        </p>

    </div>

    <div class="panel-heading">
        <h3>III.	Peserta Program</h3>
    </div>
    <div class="panel-body">
        <p align='justify'>
            Peserta program reward dapat diikuti oleh:
        </p>
        <ul>
            <li align='justify'>Mahasiswa STMIK KHARISMA Makassar</li>
            <li align='justify'>Dosen STMIK KHARISMA Makassar</li>
            <li align='justify'>Staf STMIK KHARISMA Makassar</li>
        </ul>

    </div>

    <div class="panel-heading">
        <h3>IV.	Periode Program</h3>
    </div>
    <div class="panel-body">
        <p align='justify'>
            Program ini berlaku dalam periode tanggal <strong>19 Oktober 2016</strong> s.d. tanggal <strong>16 September 2017</strong>
        </p>

    </div>

    <div class="panel-heading">
        <h3>V.	Syarat dan Ketentuan</h3>
    </div>
    <div class="panel-body">
        <ol>
            <li align='justify'>
                <strong>Pendaftaran</strong>
                <ol type="a">
                    <li align='justify'>
                        Untuk mengikuti program ini, setiap peserta WAJIB mendaftarkan diri sebagai peserta program Reward Point
                        dengan mengisi formulir pendaftaran dan TELAH MENYETUJUI seluruh SYARAT DAN KETENTUAN program Reward Point ini;
                    </li>

                    <li align='justify'>
                        Setiap peserta yang telah mendaftar akan mendapatkan satu nomor Referensi ID yang berlaku selama periode program;
                    </li>
                    <li align='justify'>
                        Setiap orang hanya berhak mendaftar untuk satu nomor Referensi ID;
                    </li>
                </ol>
            </li>
            <li align='justify'>
                <strong>Kewajiban Peserta</strong>
                <ol type="a">
                    <li align='justify'>
                        Setiap peserta WAJIB mempromosikan dan mereferensikan STMIK KHARISMA Makassar kepada calon mahasiswa sesuai arahan
                        dan informasi yang diberikan oleh panitia PMB STMIK KHARISMA Makassar Tahun Akademik 2017/2018 dengan cara-cara yang
                        BENAR dan DAPAT DIPERTANGGUNGJAWABKAN;
                    </li>
                    <li align='justify'>
                        Setiap peserta WAJIB menjaga nama baik STMIK KHARISMA Makassar dalam kegiatan yang berkaitan dengan program Reward
                        Point ini;
                    </li>
                </ol>
            </li>
            <li align='justify'>
                <strong>Hak Peserta</strong>
                <ol type="a">
                    <li align='justify'>
                        Setiap peserta yang berhasil mereferensikan seorang calon mahasiswa sehingga mejadi mahasiswa STMIK KHARISMA Makassar
                        akan mendapatkan apresiasi (reward) dalam bentuk dana tunai sebesar Rp 300.000 (tiga ratus ribu rupiah) dengan proses
                        pengambilan apresiasi mengikuti ketentuan pada poin V.5;
                    </li>
                    <li align='justify'>
                        Untuk setiap calon mahasiswa yang akan direferensikan, peserta berhak mendapatkan 1 (satu) voucher referensi;
                    </li>
                    <li align='justify'>
                        Setiap peserta boleh mereferensikan lebih dari 1 (satu) orang calon mahasiswa; 
                    </li>
                </ol>
            </li>
            <li align='justify'>
                <strong>Proses Referensi</strong>
                <ol type="a">
                    <li align='justify'>
                        Untuk melakukan proses referensi, setiap peserta akan mendapatkan 1 (satu) nomor Referensi ID sebagai tanda pengenal
                        peserta dan sejumlah voucher referensi sesuai calon mahasiswa yang akan direferensikan;
                    </li>
                    <li align='justify'>
                        Setiap peserta yang telah mendapatkan calon mahasiswa yang akan direferensikan WAJIB memberikan informasi nomor
                        Referensi ID dan nomor Voucher Referensi kepada calon mahasiswa tersebut untuk kemudian WAJIB dimasukkan pada formulir
                        pendaftaran calon mahasiswa bersangkutan;
                    </li>
                    <li align='justify'>
                        Setiap peserta WAJIB memastikan pengisian nomor Referensi ID dan nomor Voucher Referensi-nya telah dilakukan oleh
                        calon mahasiswa dalam formulir pedaftaran calon mahasiswa bersangkutan;
                    </li>
                    <li align='justify'>
                        STMIK KHARISMA Makassar tidak bertanggung jawab jika terjadi kelalaian dari peserta dalam bentuk kealpaan memberikan
                        informasi Referensi ID-nya kepada calon mahasiswa yang bersangkutan, sehingga Referensi ID peserta tidak tercatat
                        dalam sistem Reward Point sebagai pemberi referensi mahasiswa bersangkutan;
                    </li>
                    <li align='justify'>
                        STMIK KHARISMA Makassar BERHAK melakukan verifikasi dan konfirmasi kepada calon mahasiswa dan pihak lain yang
                        diperlukan untuk memastikan SATU calon mahasiswa TERCATAT HANYA untuk SATU nomor Referensi ID dan nomor Voucher
                        Referensi;
                    </li>
                    <li align='justify'>
                        Setelah memastikan segala sesuatunya telah sesuai, STMIK KHARISMA Makassar akan mencatat proses referensi yang
                        terjadi ke dalam sistem Reward Point dan perubahan data setelahnya TIDAK DIBENARKAN dan TIDAK DAPAT DILAKUKAN;
                    </li>
                </ol>
            </li>
            <li align='justify'>
                <strong>Pengambilan Reward Point</strong>
                <ol type="a">
                    <li align='justify'>
                        1 (satu) voucher referensi dianggap berhak mendapatkan apresiasi dana tunai JIKA DAN HANYA JIKA mahasiswa yang
                        direferensikan telah melunasi biaya kuliah semester awal minimal sebesar Rp 5.000.000 (Lima Juta Rupiah);
                        Pengambilan reward dapat dilakukan selama masa periode program sepanjang telah memenuhi ketentuan poin V.5.b;
                    </li>
                    <li align='justify'>
                        Peserta yang melakukan pengambilan apresiasi dana tunainya, WAJIB menyampaikan kepada STMIK KHARISMA Makassar
                        paling lambat 3 (tiga) hari sebelumnya dengan membawa bukti otentifikasi dan otorisasi yang dimiliki;
                    </li>
                    <li align='justify'>
                        Pengambilan reward dilakukan di bagian keuangan dengan menandatangani bukti pengambilan reward;
                    </li>
                </ol>
            </li>
            <li align='justify'>
                <strong>Ketentuan Tambahan</strong>
                <ol type="a">
                    <li align='justify'>
                        STMIK KHARISMA Makassar hanya mengakui satu voucher referensi untuk satu orang mahasiswa yang berhasil direferensi,
                        dan STMIK KHARISMA Makassar tidak terlibat dan tidak bertanggung jawab terhadap perselisihan di antara peserta
                        akibat terjadinya referensi ganda;
                    </li>
                    <li align='justify'>
                        Syarat dan Ketentuan ini disepakati secara bersama dan mengikat, apabila ada sengketa yang timbul sehubungan dengan
                        Syarat dan Ketentuan ini akan diselesaikan secara musyawarah dan mufakat bersama.
                    </li>
                </ol>
            </li>
        </ol>

    </div>
</div>
</div>
