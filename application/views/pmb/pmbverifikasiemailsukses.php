<div class="col-sm-8 col-md-8">
    <i class="fa fa-fw fa-envelope-o fa-4x"></i><i class="fa fa-fw fa-check fa-4x"></i><br />
    <h3>Alamat email Anda telah berhasil di-verifikasi, <br />
        akun Anda kini sudah aktif dan Anda dapat melakukan <a href="/pmb">Sign-in</a></h3>

    <h4>Selanjutnya kami perlu melakukan verifikasi nomor handphone Anda, <br />
        sebuah kode verifikasi telah dikirimkan melalui SMS ke nomor handphone Anda</h4>

    <p class="help-block">Lakukan <a href="/pmb">Sign-in</a> dan masukkan kode verifikasi di halaman Profil Anda</p>


</div>
