<div class="col-sm-5 col-md-5">
    <div class="account-wall">

<?php
switch ($kode) {
    case 1:
        echo "<div class='alert alert-danger text-center' role='alert'>
                <span class='fa fa-exclamation-triangle fa-2x' aria-hidden='true'></span><br />
                Sign-in Gagal:<br />
                Email/username tidak dikenali atau salah tulis
            </div>";
        break;
    case 2:
        echo "<div class='alert alert-danger text-center' role='alert'>
                <span class='fa fa-exclamation-triangle fa-2x' aria-hidden='true'></span><br />
                Sign-in Gagal: Password keliru
            </div>";
        break;
    case 3:
        echo "<div class='alert alert-warning text-center' role='alert'>
                <span class='fa fa-clock-o fa-2x' aria-hidden='true'></span><br />
                Session expire, silahkan login kembali
            </div>";
        break;
    case 4:
        echo "<div class='alert alert-warning text-center' role='alert'>
                <span class='fa fa-exclamation-triangle fa-2x' aria-hidden='true'></span><br />
                Akun Anda belum aktif karena masih memerlukan verifikasi, silahkan periksa email Anda<br />
            </div>";
        break;
    case 7:
        echo "<div class='alert alert-success text-center' role='alert'>
                <span class='fa fa-key fa-2x' aria-hidden='true'></span><br />
                Password baru telah dikirimkan ke elamat e-mail Anda
            </div>";
        break;
    case 9:
        echo "<div class='alert alert-warning text-center' role='alert'>
                <span class='fa fa-sign-out fa-2x' aria-hidden='true'></span>
                Anda sudah Sign-out, Terima kasih
            </div>";
        break;
}
?>

        <h1 class="text-center login-title">Sign In to <br />Sistem Informasi STMIK KHARISMA<br />(SISKA)</h1>
        <form class="form-signin" action="<?=base_url()?>pmb/signin" method="post">
            <input type="text" name="uid" class="form-control" placeholder="Email Anda" required autofocus>
            <input type="password" name="pass" class="form-control" placeholder="Password" required>
            <input type="submit" class="btn btn-lg btn-success btn-block" name="submit" value="Sign in">
            <a href="/pmb/lupapassword" class="pull-right need-help">Lupa password? </a><span class="clearfix"></span>
        </form>
    </div>
    <br />
    <a href="<?=base_url()?>pmb/m/reg">
        <button class="btn btn-lg btn-primary btn-block" name="btregister">
            Registrasi Calon Mahasiswa Baru
        </button>
    </a>
</div>

<div class="col-sm-6 col-md-6">
    <img src="<?=base_url()?>assets/img/pmb/2025/maba.jpg" class="img-responsive">
</div>
