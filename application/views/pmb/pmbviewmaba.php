<div class="row">
    <div class="col-sm-9 col-md-9">
        <div class="panel panel-info">
            <div class="panel-heading"><strong>Prosedur PMB</strong></div>
            <div class="panel-body">
                <table class="table">
                    <thead>
                        <th width="220px">PROSEDUR</th>
                        <th>STATUS</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Registrasi Awal</td>
                            <td>
                                <i class="fa fa-fw fa-envelope"></i>
                                <?php
                                    if( $maba['email_valid'] != "0000-00-00 00:00:00" ){
                                ?>
                                        <strong>E-mail terverifikasi:</strong> <?= $maba['email_valid'] ?> <br />
                                <?php
                                    } else {
                                ?>
                                        <strong>E-mail belum diverifikasi</strong><br />
                                <?php
                                    }
                                 ?>


                                <i class="fa fa-fw fa-mobile"></i>
                                <?php
                                    if( $maba['telepon_valid'] != "0000-00-00 00:00:00" ){
                                ?>
                                        <strong>Nomor HP terverifikasi:</strong> <?= $maba['telepon_valid'] ?> <br />
                                <?php
                                    } else {
                                ?>
                                        <strong>Nomor HP belum diverifikasi</strong>
                                        (<a href='/pmb/m/profil'>Update Profil</a>)<br />
                                <?php
                                    }
                                 ?>
                            </td>
                        </tr>
                        <tr>
            <?php
                $pil1 = $this->prodi_model->getProdi( $maba['prodi_pilihan1'] );
                $pil2 = $this->prodi_model->getProdi( $maba['prodi_pilihan2'] );
            ?>
                            <td>Pilihan Program Studi</td>
                            <td>
                                <strong>Pilihan 1:</strong>
                                <?= $maba['prodi_pilihan1']." - ".strtoupper($pil1['nama']).' '.$pil1['jenjang']['kode'] ?>
                                <br />
                                <strong>Pilihan 2:</strong>
                                <?= $maba['prodi_pilihan2']." - ".strtoupper($pil2['nama']).' '.$pil2['jenjang']['kode'] ?>
                            </td>
                        </tr>
                        <tr>
                            <td>Update Profil</td>
                            <td>
                            <?php
                                if ( $this->pmbuser_model->statusprofilmaba( $_SESSION['id'] ) ) {
                                    echo "<strong>Data lengkap</strong> (" . $maba['last_update'] . ")";
                                } else {
                                    echo "<strong>Data Profil belum lengkap</strong> (".
                                         "<a href='/pmb/m/profil'>Update Profil</a>".")";
                                }
                            ?>
                            </td>
                        </tr>
                        <tr>
                            <td>Upload Dokumen</td>
                            <td>
                                <?php
                                foreach ($this->siska->dok_maba as $dok) {
                                    $uploaddok = $this->pmbuser_model->checkDokumen( $dok['kode'], $_SESSION['id']);

                                    if( !empty($uploaddok) ) {
                                        $file = $uploaddok['id_owner']."_".$uploaddok['tipe'].".".$uploaddok['tipefile'];
                                        $link = "/assets/dokumen/maba/$tahun/$file";
                                        $icon = "<i class='fa fa-file-photo-o text-danger' aria-hidden='true'></i>";
                                        if( $uploaddok['tipefile'] == 'pdf') $icon = "<i class='fa fa-file-pdf-o text-danger' aria-hidden='true'></i>";

                                        $valid="";
                                        if( !empty( $uploaddok['validasi'] ) ){
                                            $valid = "(<i class='fa fa-check text-success' aria-hidden='true'> valid</i>)";
                                        }

                                        echo $dok['dokumen'].' &nbsp;'.
                                             '<a href="'.$link.'" target="_new" title="Lihat file: '.$file.'">'.$icon.'</a> '.
                                             $valid.'<br />';
                                    }
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td>Kartu Tanda Peserta Seleksi</td>
                            <td>
                            <?php
                                $uploaddok = $this->pmbuser_model->checkDokumen( '11', $_SESSION['id']);
                                if( !empty($uploaddok) ){
                                    $filedok = $_SESSION['id']."_11.".$uploaddok['tipefile'];
                                    echo "<a href='".base_url()."assets/dokumen/maba/2017/".$filedok.
                                    "' target='_blank'>".$filedok." <i class='fa fa-file-pdf-o text-danger' aria-hidden='true'></i></a>";
                                }
                            ?>
                            </td>
                        </tr>
                        <tr>
                            <td>Ujian Seleksi</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Penentuan Kelulusan</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Pendaftaran Ulang</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Kuliah Umum</td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-sm-3 col-md-3">
        <div class="list-group">
            <a href="<?=base_url()?>pmb/m/syarat" class="list-group-item">
                <strong><i class="fa fa-fw fa-check-square-o"></i> Syarat dan Ketentuan</strong>
            </a>
            <a href="<?=base_url()?>pmb/m/profil" class="list-group-item">
                <strong><i class="fa fa-fw fa-address-card"></i> Update Profil</strong>
            </a>
            <a href="<?=base_url()?>pmb/dokumen" class="list-group-item">
                <strong><i class="fa fa-fw fa-upload"></i> Upload Dokumen</strong>
            </a>
        </div>
    </div>

</div>
