<div class="col-lg-6 col-sm-12">

    <div class="panel">
        <div class="panel-body">
            <table class='table table-bordered'>
                <tr>
                    <td class="datafields" width='130px'>Mahasiswa:</td>
                    <td id="namamhs"><?= $mhs['namamhs'] ?></td>
                </tr>
                <tr>
                    <td class="datafields" width='130px'>N I M:</td>
                    <td id="nimhs"><?= $mhs['nimhs'] ?></td>
                </tr>
                <tr>
                    <td class="datafields" width='130px'>Biaya Semester</td>
                    <td id="biaya">
                        <strong>
                            Rp <?= number_format($krs['biaya_sks'] + $krs['biaya_adm'] + $krs['biaya_unik'], 0, ",", ".") ?>
                        </strong>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <h5>
        Silahkan melakukan konfirmasi dengan mengupload file <i>scan</i> / <i>foto</i> / <i>screenshot</i>
        bukti pembayaran atau bukti transfer, agar Anda terdaftar sebagai mahasiswa aktif
        dan peserta mata kuliah pada <strong>Semester <?= $this->siska->stringSemester($krs['kodesmt']) ?></strong>
    </h5>
    <h5 style="color: #f00; font-weight: bold;">
        Pastikan jumlah pembayaran sesuai dengan invoice/slip pembayaran yang telah diterbitkan oleh Bagian
        Keuangan, termasuk tiga angka terakhir sebagai kode unik (<u>jangan dibulatkan</u>) yang akan memudahkan verifikasi.<br>
        Jika jumlah pembayaran tidak sesuai maka Anda tidak akan didaftarkan sebagai
        peserta mata kuliah pada Semester <?= $this->siska->stringSemester($krs['kodesmt']) ?>
    </h5>

</div>
<div class="col-lg-6  col-sm-12">
    <div class="panel panel-info">
        <div class="panel-heading"><strong>Upload Bukti Transfer</strong></div>
        <div class="panel-body">

            <form class="form" action="/krs/konfirm_upload" method="post" enctype="multipart/form-data">
                <input type="hidden" name="idkrs" id="idkrs" value="<?= $krs['id_krs'] ?>">

                <div class="form-group">
                    <label>Tanggal Bayar/Transfer * </label>
                    <div style="position:relatives;">
                        <input type="text" class="form-control" id="tglbayar" data-provide='datetimepicker' required>
                    </div>
                </div>

                <div class="form-group">
                    <label>Jumlah Transfer * </label>
                    <div style="position:relatives;">
                        <input type="number" class="form-control" id="jmltransfer" name="jmltransfer" placeholder="0" required>
                    </div>
                </div>

                <div class="form-group">
                    <label>File scan bukti pembayaran/bukti transfer * </label>
                    <input type="file" class="form-control upload-file" id="buktitrf" name="buktitrf" data-max-size="512000" accept="image/png, image/jpg, image/jpeg" required>
                    Format file JPG atau PNG dengan ukuran file max. 512 KB
                </div>

                <div class="form-group">
                    <label>Nomor handphone yang dapat dihubungi * </label>
                    <div style="position:relatives;">
                        <input type="number" class="form-control" id="nomortlp" name="nomortlp" placeholder="08xxxxxxxxxx" required>
                    </div>
                </div>

                <div>
                    <input type="submit" class="btn btn-block btn-primary" id="btsend" value="Kirim" disabled>
                </div>
            </form>

            <br>

            <?php
            switch ($status) {
                case 1:
                    echo "<div class='alert alert-info text-center' role='alert'>
                            <span class='fa fa-check fa-2x' aria-hidden='true'></span><br />
                            File Anda telah berhasil di-upload dan menunggu validasi oleh Bagian Keuangan <br><br>
                            <a href='/krs/" . $mhs['nimhs'] . "'>&lt;&lt; kembali ke layanan KRS</a>
                            </div>";
                    break;
                case 2:
                    echo "<div class='alert alert-danger text-center' role='alert'>
                                <span class='fa fa-exclamation-triangle fa-2x' aria-hidden='true'></span><br />
                                Maaf, upload dokumen belum berhasil, ulangi beberapa saat lagi, <br />
                                jika masih gagal, silahkan menghubungi Admin SISKA
                            </div>";
                    break;
                case 3:
                    echo "<div class='alert alert-danger text-center' role='alert'>
                                <span class='fa fa-exclamation-triangle fa-2x' aria-hidden='true'></span><br />
                                Maaf, upload dokumen tidak berhasil, format file tidak dapat diterima, <br />
                                pasikan hanya meng-upload dokumen berformat JPG atau PNG 
                            </div>";
                    break;
            }
            ?>

        </div>
    </div>
</div>

<br>

</div>


<script type="text/javascript">
    const jmltrf = <?= $krs['biaya_sks'] + $krs['biaya_adm'] + $krs['biaya_unik'] ?>

    $("#tglbayar").datetimepicker({
        defaultDate: "<?= date('Y-m-d H:i:s') ?>",
        autoclose: true
    });

    $("#jmltransfer").keyup(function() {
        if (this.value == jmltrf) {
            $("#btsend").attr("disabled", false);
        } else {
            $("#btsend").attr("disabled", true);
        }
    });
</script>