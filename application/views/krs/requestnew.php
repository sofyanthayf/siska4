<div class="col-lg-6 col-sm-12">

    <div class="panel">
        <div class="panel-body">
            <table class='table table-bordered'>
                <tr>
                    <td class="datafields" width='130px'>Mahasiswa:</td>
                    <td id="namamhs"><?= $mhs['namamhs'] ?></td>
                </tr>
                <tr>
                    <td class="datafields" width='130px'>N I M:</td>
                    <td id="nimhs"><?= $mhs['nimhs'] ?></td>
                </tr>
                <tr>
                    <td class="datafields" width='130px'>KRS Semester:</td>
                    <td id="nimhs"><?= $this->siska->stringSemester($kodesmt) ?></td>
                </tr>
            </table>
        </div>
    </div>

    <div class="panel panel-info">
        <div class="panel-heading"><strong>Request Slip Pembayaran</strong></div>
        <div class="panel-body">
            <form class="form" action="/krs/konfirm_slip_request" method="post">
                <input type="hidden" name="kodesmt" id="kodesmt" value="<?= $kodesmt ?>">
                <input type="hidden" name="nimhs" id="nimhs" value="<?=$mhs['nimhs']?>">
                
                <label>Opsi Pembayaran:</label>

                <div class="form-group">
                    <input type="radio" id="opsibayar" name="opsibayar" value=0 required> Pembayaran Penuh <br>
                    <input type="radio" id="opsibayar" name="opsibayar" value=1 required> Pembayaran dengan Angsuran
                </div>

                <div class="form-group">
                    <label>Nomor handphone yang dapat dihubungi * </label>
                    <div style="position:relatives;">
                        <input type="number" class="form-control" id="nomortlp" name="nomortlp" placeholder="08xxxxxxxxxx" required>
                    </div>
                </div>

                <div>
                    <input type="submit" class="btn btn-block btn-primary" id="btsend" value="Ajukan"
                        <?= $status==1?"disabled":""?>>
                </div>

            </form>
        </div>
    </div>




</div>
<div class="col-lg-6  col-sm-12">
    <h5>
        <strong>PERHATIAN</strong><br><br>
        <ul>
            <li>Silahkan mengajukan penerbitan slip/invoice pembayaran biaya perkuliahan semester
                 <?= $this->siska->stringSemester($kodesmt) ?> sebelum melakukan pengisian Rencana Studi </li>
            <li>Pengajuan/request penerbitan slip/invoice dan konfirmasi pembayaran hanya dapat dilakukan di <b>dalam batas waktu 
                yang telah ditentukan</b></li>
            <li>Pengisian Rencana Studi hanya dapat dilakukan setelah mahasiswa melakukan pembayaran dan melakukan konfirmasi 
                dengan <i>upload</i> bukti pembayaran </li>
            <li>Mahasiswa yang tidak melakukan pembayaran biaya perkuliahan dan tidak mengisi Rencana Studi, tidak akan terdaftar 
                dan tidak diperkenankan mengikuti perkuliahan pada semester <?= $this->siska->stringSemester($kodesmt) ?>
            </li>
            <li>Bagi mahasiswa yang memilih opsi Pembayaran dengan Angsuran, setelah melakukan pengajuan, <b>wajib mengkonfirmasikan secara 
                langsung</b> dengan Bagian Keuangan sebelum slip diterbitkan
            </li>

        </ul>
    </h5>
  

    <br>

    <?php
    switch ($status) {
        case 1:
            echo "<div class='alert alert-info text-center' role='alert'>
                    <span class='fa fa-check fa-2x' aria-hidden='true'></span><br />
                    Permintaan penerbitan slip pembayaran telah diajukan dan menunggu diproses oleh Bagian Keuangan<br>
                    Silahkan cek email Anda beberapa waktu ke depan, untuk melihat slip pembayaran Anda <br><br>
                    Saat melakukan pembayaran, pastikan jumlah pembayaran sesuai dengan invoice/slip pembayaran yang telah diterbitkan oleh Bagian
                    Keuangan, termasuk tiga angka terakhir sebagai kode unik (<u>jangan dibulatkan</u>) yang akan memudahkan verifikasi.
                    <br><br>
                    <a href='/krs/" . $mhs['nimhs'] . "'><strong>&lt;&lt; kembali ke layanan KRS</strong></a>
                    </div>";
            break;
    }
    ?>

</div>

<br>

</div>

