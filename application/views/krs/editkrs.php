<?php
  $this->load->view('mhs/overview');
?>
<div class="col-sm-6 col-md-6">

  <div class="row">
    <div class="panel panel-info">

      <div class="panel-heading">
        <h4>Rencana Studi Semester <?= $this->siska->stringSemester( $kodesmt, '' ) ?></h4>
      </div>

      <div class="panel-body">
        <table class="table table-bordered table-hover" id="krssmt" width="100%">
          <thead>
            <tr class="bg-primary">
              <th width='30px'class="text-center">Batal</th>
              <th width='60px'class="text-center">Kode MK</th>
              <th class="text-center">Mata Kuliah</th>
              <th width='30px'class="text-center">sks</th>
              <th width='30px'class="text-center">smt</th>
            </tr>
          </thead>
          <tbody>
            <?php
              $i = 1;
              if( $krs_in_progress['status'] >= 2 ) {
                $dis = "DISABLED";
              } else {
                $dis = "";
              }
              foreach ($matakuliah as $mk) {
                ?>
                <tr id="belanja<?= $i ?>" style="display:none">
                  <td class="text-center">
                    <input type="checkbox" id="" checked
                           onclick="krsBatalItem(<?= $i ?>);return false;" <?= $dis ?>>
                  </td>
                  <td class="text-center" id="mkpilih<?= $i ?>"><?= $mk['kodemk'] ?></td>
                  <td id="namamkpilih<?= $i ?>"><?= $mk['namamk'] ?></td>
                  <td class="text-center" id="skspilih<?= $i ?>"><strong><?= $mk['sks'] ?></strong></td>
                  <td class="text-center" id="smtpilih<?= $i ?>" text-muted><?= $mk['smt'] ?></td>
                </tr>

                <?php

                $i++;  // id counter

              }
              ?>
          </tbody>
        </table>

        <table class='table table-bordered'>
          <tr>
            <td class="datafields" width='130px'>Jumlah sks:</td>
            <td id="jmlsks"></td>
          </tr>
          <tr>
            <td class="datafields" width='130px'>Jumlah Mata kuliah:</td>
            <td id="jmlmk"></td>
          </tr>
          <tr>
            <td class="datafields" width='130px'>Maksimal sks:</td>
            <td id="maxsks"><?= $sksmax ?></td>
          </tr>
          <tr>
            <td class="datafields" width='130px'>IPS Rujukan:</td>
            <td id="refips">
              <strong><?= ($refsmt=='MABA')?'Mahasiswa Baru':number_format((float)$refips, 2, '.', ''); ?></strong>
              <i><?= ($refsmt=='MABA')?'':'(Semester '.$this->siska->stringSemester($refsmt,'')." / ".$refsmt.')'; ?></i>
            </td>
          </tr>
        </table>

    <?php
      if( isset($krs_in_progress) ){
      ?>

        <div class="block" id="catatanpa" style="display:block;background-color:#eeffdd;padding:5px;">
          <?= str_replace( "\r\n", "<br>", $krs_in_progress['catatan'] ) ?>
        </div>

      <?php
      }
     ?>

    <?php
      if( $this->session->who == 'mhs' ){
     ?>
       <div class="block" id="opsipa" style="display:none;">
         <table class="table">
           <tr>
             <td><input type="checkbox" id="submitpa"></td>
             <td><b>Ajukan ke Penasehat Akademik (PA)</b><br>
               <i>(setelah opsi ini dipilih dan disimpan maka untuk sementara tidak memungkinkan
                 untuk melakukan peng-edit-an KRS lagi, kecuali atas konfirmasi PA)</i>
             </td>
           </tr>
         </table><br>
       </div>
       <button id="tbsave" class="btn btn-block btn-primary" style="display:none;"> Simpan KRS </button>
    <?php } ?>

    <?php
      if( $this->session->who == 'dsn' ){

        if($krs_in_progress['status'] < 2){
     ?>
        <hr>
         <div class="block" id="saranbatal" style="display:none;background-color:#ffeedd;padding:5px;">
           <h4>Saran Penasehat Akademik</h4>
           <h5><strong>Batalkan Mata kuliah:</strong></h5>
           <table class="table">
             <thead>
               <tr>
                 <td>Kode MK</td>
                 <td>Mata Kuliah</td>
                 <td>sks</td>
                 <td>smt</td>
               </tr>
             </thead>
             <tbody id="mksaranbatal"></tbody>
           </table>
           <textarea class="form-control" id="alasanbatal" rows="5" cols="70" placeholder="Berikan pertimbangan/alasan pembatalan"></textarea>
           <br>
         </div>

         <div class="block" id="sarantambah" style="display:none;background-color:#eeffdd;padding:5px;">
           <h5><strong>Tambahkan Mata kuliah:</strong></h5>
           <table class="table">
             <thead>
               <tr>
                 <td>Kode MK</td>
                 <td>Mata Kuliah</td>
                 <td>sks</td>
                 <td>smt</td>
               </tr>
             </thead>
             <tbody id="mksarantambah"></tbody>
           </table>
           <textarea class="form-control" id="alasantambah" rows="5" cols="70" placeholder="Berikan pertimbangan/alasan penambahan"></textarea>
           <br>
         </div>

         <div class="block" id="acceptpa" style="display:block;">
           <table class="table">
             <tr>
               <td><input type="checkbox" id="acceptkrs"></td>
               <td><b>Setujui KRS</b><br>
                 <i>(setelah opsi ini dipilih dan disimpan maka tidak memungkinkan
                   untuk melakukan peng-edit-an KRS lagi)</i>
               </td>
             </tr>
           </table><br>
         </div>
         <br>
         <button id="tbsavepa" class="btn btn-block btn-primary" style="display:block;"> Simpan KRS </button>

         <?php
          }   // ---> end status < 2

          if( $krs_in_progress['tanggal_cetak_slip'] == null ){

          ?>
         <div class="block" id="hapuskrs" style="display:block;">
           &nbsp;<br>
           <table class="table">
             <tr>
               <td><input type="image" src="/assets/img/delete16.png" id="delkrs"></td>
               <td><b>Hapus KRS</b><br>
                 <i>(setelah disetujui PA, KRS tidak dapat lagi di-edit, tetapi masih dapat
                   dibatalkan/dihapus atas <strong>persetujuan PA dengan mahasiswa</strong>, sepanjang belum
                   diproses oleh Bagian Keuangan)</i>
               </td>
             </tr>
           </table><br>
         </div>


    <?php
          }   // --> end if tanggal_cetak_slip == null
        }     // --> end if who == 'dsn'
        ?>


      </div>
    </div>
  </div>
</div>

<div class="col-sm-6 col-md-6">
  <div class="row">

    <div class="panel panel-info">

      <div class="panel-heading">
        <h4>Dafar Mata Kuliah</h4>
      </div>

      
      <table class=table>
        <tr>
            <td class="datafields" width='180px'>Indeks Prestasi Kumulatif:</td>
            <td><?= number_format( $this->mhs['ipk'] , 2 , "," , "." ) ?></td>
        </tr>
        <tr>
            <td class="datafields">Jumlah sks terkumpul:</td>
            <td><?= $this->mhs['jmlsks'] ?></td>
        </tr>
      </table>

      <div class="panel-body">
        <table class="table table-bordered table-hover" id="krssmt" width="100%">
          <thead>
            <tr class="bg-primary">
              <th class="text-center">Kode MK</th>
              <th class="text-center">Mata Kuliah</th>
              <th class="text-center">sks</th>
              <th class="text-center">Nilai</th>
              <th class="text-center">Pilih</th>
            </tr>
          </thead>
          <tbody>
            <?php
              $smt = 0;
              $i = 1;
              foreach ($matakuliah as $mk) {
                if($mk['smt'] != $smt){
                  $smt = $mk['smt'];

                  if($smt > 1) echo "</div>\n";
                  echo "<tr onclick='showHideSemester($smt)'>\n
                        <td style='background-color:#eec' colspan=5><strong>";
                  if( $mk['smt'] == 9 ){
                    echo "Pilihan Bebas";
                  } elseif ( $mk['smt'] == 0 ){
                    echo "NILAI KONVERSI";
                  } else {
                    echo "Semester $mk[smt]";
                  }
                  echo "</strong></td></tr>\n";

                  echo "<div id='semester$smt' style='display:block;'>\n";
                }

                $ketsmt = $mk['kodesmt'];
                if( $mk['nilai'] == 'D' ) {
                  $txtcol = 'color:#ff9800;';
                } elseif( $mk['nilai'] == 'E' ) {
                  $txtcol = 'color:#f55e4B;';
                } elseif( $mk['nilai'] == 'P' ) {	 // matakuliah sedang diprogram, belum ada nilai
                  $txtcol = 'color:#5D76C8;';
                  $ketsmt = 'sedang diprogramkan '.$mk['kodesmt'];
                } else {
                  $txtcol = 'color:#000;';
                }

                ?>
                <tr id='matakuliah<?= $i ?>'>
                  <td class="text-center"><?= $mk['kodemk'] ?></td>
                  <td><?= $mk['namamk'] ?></td>
                  <td class="text-center"><?= $mk['sks'] ?></td>
                  <td class="text-center">
                    <a href='' title='<?= $ketsmt ?>' style='text-decoration:none; <?= $txtcol ?>'>
                      <?= $mk['nilai'] ?></a>
                  </td>
                  <td class="text-center">
                    <input type="checkbox" id="chkmatakuliah<?= $i ?>"
                           onclick="krsPilihItem(<?= $i ?>);return false;" <?= $dis ?>>
                  </td>
                </tr>

          <?php
                $i++;  // id counter
              }
          ?>
            </div>
          </tbody>
        </table>
      </div>

    </div>

  </div>

</div>

<script type="text/javascript">

var dkrs = {};
var listmk = <?= $i-1 ?> ;
var enupdate = false;
var idk="";
dkrs['mk'] = [];

<?php
  if($this->session->who == 'dsn'){
    ?>
    var kstart = true;
    dkrs['mkadd'] = [];
    dkrs['mkrem'] = [];

    <?php
  }
 ?>

dkrs['nimhs'] = <?= $mhs['nimhs'] ?> ;
dkrs['kodesmt'] = <?= $kodesmt ?>;
dkrs['jmlmk']=0;
dkrs['jmlsks']=0;
dkrs['status']=0;
var sksmax=<?= $sksmax ?>;

// setInterval(saveKRS,20000);
$(document).ready(function(){
  getKRS();
})

$("#tbsave").click(function(){
  if( $("#submitpa").is(":checked") ) { dkrs['status'] = 1; }
  else { dkrs['status'] = 0; }
  saveKRS();
  window.location= "/krs";
});

function krsPilihItem( id ) {
  var sksadd = parseInt($("#skspilih"+id).text());
  if( dkrs['jmlsks'] + sksadd <= sksmax ){
    $("#belanja"+id).show();
    $("#matakuliah"+id).hide();
    dkrs['jmlmk']++;
    dkrs['jmlsks'] += sksadd;
    dkrs['mk'].push( { kodemk:$("#mkpilih"+id).text(), sks:$("#skspilih"+id).text() } );

<?php
  if($this->session->who == 'dsn'){
    ?>

    if(!kstart){
      var mkb = inArrayAssoc( $("#mkpilih"+id).text(), dkrs['mkrem'], 'kodemk');
      if( mkb > -1 ){
        dkrs['mkrem'].splice( mkb , 1 );
      } else {
        dkrs['mkadd'].push( { kodemk:$("#mkpilih"+id).text(),
        namamk:$("#namamkpilih"+id).text(),
        sks:$("#skspilih"+id).text(),
        smt:$("#smtpilih"+id).text() } );
      }
    }

  <?php
}
?>
    refreshStatus();
  } else {
    alert("Jumlah maksimal " + sksmax + " sks telah tercapai, tidak dapat menambah sks lagi");
  }
}

function krsBatalItem( id ) {
  $("#belanja"+id).hide();
  $("#matakuliah"+id).show();
  if(dkrs['jmlmk']>0) dkrs['jmlmk']--;
  dkrs['jmlsks'] -= parseInt($("#skspilih"+id).text());
  dkrs['mk'].splice( $.inArray( $("#mkpilih"+id).text(), dkrs['mk'] ), 1 );

<?php
  if($this->session->who == 'dsn'){
    ?>

  var mkp = inArrayAssoc( $("#mkpilih"+id).text(), dkrs['mkadd'], 'kodemk' );
  if( mkp > -1 ){
    dkrs['mkadd'].splice( mkp, 1 );
  } else {
    dkrs['mkrem'].push( { kodemk:$("#mkpilih"+id).text(),
                          namamk:$("#namamkpilih"+id).text(),
                          sks:$("#skspilih"+id).text(),
                          smt:$("#smtpilih"+id).text() } );
  }

  <?php
}
?>

  refreshStatus();
}

function refreshStatus() {
  $("#jmlsks").html("<strong>" + dkrs['jmlsks'] + "</strong> <i>sks</i>");
  $("#jmlmk").html("<strong>" + dkrs['jmlmk'] + "</strong> <i>mata kuliah</i>");

<?php
  if( $this->session->who == 'mhs' ){
 ?>
  if(dkrs['jmlmk'] > 0){
    $("#opsipa").show();
    $("#tbsave").show();
  } else {
    $("#opsipa").hide();
    $("#tbsave").hide();
  }

<?php } ?>

<?php
  if( $this->session->who == 'dsn' ){
 ?>
  enupdate = false;

  $("#mksarantambah").empty();
  $.each(dkrs['mkadd'], function (key, val) {
      $("#mksarantambah:last-child").append(
        "<tr>\
          <td>"+ val.kodemk +"</td>\
          <td>"+ val.namamk +"</td>\
          <td>"+ val.sks +"</td>\
          <td>"+ val.smt +"</td>\
        </tr>"
      );
  });

  $("#mksaranbatal").empty();
  $.each(dkrs['mkrem'], function (key, val) {
      $("#mksaranbatal:last-child").append(
        "<tr>\
          <td>"+ val.kodemk +"</td>\
          <td>"+ val.namamk +"</td>\
          <td>"+ val.sks +"</td>\
          <td>"+ val.smt +"</td>\
        </tr>"
      );
  });

  if(dkrs['mkadd'].length > 0 ){
    $("#sarantambah").show();
  } else {
    $("#sarantambah").hide();
  }

  if(dkrs['mkrem'].length > 0 ){
    $("#saranbatal").show();
  } else {
    $("#saranbatal").hide();
  }

  if( dkrs['mkadd'].length > 0 || dkrs['mkrem'].length > 0 ){
    $("#acceptpa").hide();
    $("#tbsavepa").prop("disabled", true);
    $("#acceptkrs").prop("checked", false);
  } else {
    $("#acceptpa").show();
    $("#tbsavepa").prop("disabled", false);
  }

<?php } ?>

  if( enupdate ) saveKRS();
}

function showHideSemester( smt ) {
  if( $("#semester"+smt).is(":visible") ) {
    $("#semester"+smt).hide();
  } else {
    $("#semester"+smt).show();
  }
}

function getKRS() {
  $.get("/krs_api/krsedit/nim/"+dkrs['nimhs']+"/smt/"+dkrs['kodesmt']+"/tipe/0", function(data){
    var kmk = data.krs_semester.map(function (c) { return c.kodemk; });
    for (var i = 0; i <= listmk; i++) {
      if( $.inArray( $("#mkpilih"+i).text(), kmk ) !== -1 ) { krsPilihItem(i); }
    }
    enupdate = true;
    kstart = false;
    idk = data.krs_semester[0].id_krs;
  });
}

function inArrayAssoc( needle, haystack, skey ){
  var pos = -1;
  for(var n = 0; n < haystack.length; n++){
    if( haystack[n][skey] == needle ){
      pos = n;
      break;
    }
  }
  return pos;
}

<?php
// ========================================= DOSEN PA ========================================================

  if( $this->session->who == 'dsn' ){
 ?>

 $("#alasanbatal").keyup( function(){
   tbSaveEnableByTextArea( $(this) );
 });

 $("#alasantambah").keyup( function(){
   tbSaveEnableByTextArea( $(this) );
 });

 $("#tbsavepa").click(function(){
   if( $("#acceptkrs").is(":checked") ) {
     dkrs['status'] = 2;
     alert("KRS disetujui dan telah disimpan");
   } else {
     dkrs['status'] = 0;
     alert("KRS dan catatan Anda telah disimpan untuk dikembalikan ke mahasiswa ");
   }
   saveKRS();
   window.location= "/dsn/perwalian";
 });

 $("#delkrs").click(function(){
   if(confirm("Anda yakin, KRS mahasiswa akan dihapus?\nPastikan atas persetujuan bersama dengan mahasiswa")) {
     delKRS();
     window.location= "/dsn/perwalian";
   }
 });

 function tbSaveEnableByTextArea( el ) {
   if( el.val().length > 5 ){
     $("#tbsavepa").prop("disabled", false);
     dkrs['alasanbatal'] = $("#alasanbatal").val();
     dkrs['alasantambah'] = $("#alasantambah").val();
   } else {
     $("#tbsavepa").prop("disabled", true);
   }
 }

 function delKRS() {
   $.post("/krs_api/delkrs", {id:idk} );
   alert("KRS telah dihapus");
 }

<?php
  }  // =========================================================================================================
?>

function saveKRS(){
  $.post("/krs_api/krssemester", {data:JSON.stringify(dkrs)} );
}





</script>
