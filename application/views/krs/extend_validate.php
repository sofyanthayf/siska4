 <?php
   $this->load->view('mhs/overview');
 ?>

 <div class="row">
   <div class="col-sm-3 col-md-3">
   </div>

   <div class="col-sm-6 col-md-6">
     <div class="panel panel-default">
       <form role="form" action="/krs/ext_accepted" method="post">
         <input type="hidden" name="ext_id" value="<?= $krs_ext->id ?>">

         <table class='table'>
           <tr>
             <td class="datafields" width='150px'>Semester KRS:</td>
             <td><?= $this->siska->stringSemester($kodesmt) ?></td>
           </tr>
           <tr>
             <td class="datafields" width='150px'>Tanggal Request:</td>
             <td><?= $krs_ext->tanggal_request ?></td>
           </tr>
           <tr>
             <td class="datafields" width='150px'>Alasan Keterlambatan:</td>
             <td><i><?= $krs_ext->alasan_extend ?></i></td>
           </tr>
           <tr>
             <td class="datafields" width='150px'>Surat Pernyataan:</td>
             <td>
               <input type="checkbox" onchange="doc_changed()" required> Tanda tangan mahasiswa bermaterai<br>
               <input type="checkbox" onchange="doc_changed()" required> Tanda tangan Penasehat Akademik<br>
             </td>
           </tr>
           <tr>
             <td class="datafields" width='150px'>Tanggal Batas Perpanjangan:</td>
             <td>
               <div class="form-group">
                 <div class="input-group date form_date" data-date="" data-date-format="dd MM yyyy" data-link-field="tglext" data-link-format="yyyy-mm-dd">
                   <input class="form-control" size="16" type="text" value="" onchange="changed()" required>
                   <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                   <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                 </div>
                 <input type="hidden" id="tglext" name="tglext" value="" /><br/>
               </div>
             </td>
           </tr>
           <tr>
             <td style="text-align:center;">
             </td>
             <td style="text-align:right; vertical-align:bottom;">
               <button type='submit' class="btn btn-primary" id='submitext' disabled>Setujui Permohonan</button>
             </td>
           </tr>

         </table>

       </form>
     </div>
   </div>
 </div>


<script type="text/javascript">
var doc_req = "";

function doc_changed(){
  doc_req = "";
  $("input:checkbox").each( function() {
      if( $(this).is(":checked") ) doc_req += '1';
      else doc_req += '0';
  });
  changed();
};

function changed() {
  if( doc_req == "11" && $("#tglext").val().length > 0 ) {
    $("#submitext").prop('disabled', false);
  } else {
    $("#submitext").prop('disabled', true);
  }
};

$('.form_datetime').datetimepicker({
    //language:  'fr',
    weekStart: 1,
    todayBtn:  1,
    autoclose: 1,
    todayHighlight: 1,
    startView: 2,
    forceParse: 0,
    showMeridian: 1
});
$('.form_date').datetimepicker({
    language:  'id',
    weekStart: 1,
    todayBtn:  1,
    autoclose: 1,
    todayHighlight: 1,
    startView: 2,
    minView: 2,
    forceParse: 0
});
$('.form_time').datetimepicker({
    language:  'id',
    weekStart: 1,
    todayBtn:  1,
    autoclose: 1,
    todayHighlight: 1,
    startView: 1,
    minView: 0,
    maxView: 1,
    forceParse: 0
});
</script>
