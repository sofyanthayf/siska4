
<?php
  $this->load->view('mhs/overview');
?>

<div class="col-sm-3 col-md-3">
  <h4>KRS Semester:</h4>

  <?php
  $last = "";
  $next = "";
  $lastidk = "";

  $next = $krs_baru['kodesmt'];
  $nexturl = "/krs/" . $this->session->uid . "/$next/0";

  // var_dump( $krs);

  foreach ($krs as $dk) {
    ?>
    <button type="button" class="list-group-item" onclick="getKrsSemester('<?= $dk["id_krs"] ?>')">
      <?= $this->siska->stringSemester( $dk['kodesmt'], '' ); ?>
    </button>

  <?php
    // $last = $dk['kodesmt'];
    $last = $dk;
    $lastidk = $dk['id_krs'];
  }

   // var_dump( 'LAST:', $last);
   // var_dump( $krs_baru['kodesmt']);
   // var_dump( 'KRS BARU:',$krs_baru);
   // var_dump( 'KRS EXT:',$krs_extend);

  // if( $krs_baru && $last != $krs_baru['kodesmt'] ){

/*  if( (empty($last) || $last['kodesmt'] != $krs_baru['kodesmt']) &&
      date('Y-m-d') > $krs_baru['akhir'] && !$krs_extend )
      {
      //belum mengisi KRS  &&   masih tanggal sudah lewat && tidak ada krs extend
      if( empty($last) ){
        $next = $krs_baru['kodesmt'];
      } else {
        $next = $this->siska->nextSemester( $last['kodesmt'] );
      }
      // $next = $this->siska->nextSemester( $last['kodesmt'] );

  ?>

  <a href="/krs/extend/<?= $this->session->uid ?>/<?= $next ?>">
    <button type='button' class='list-group-item btn-primary' id='"+i+"'>
      <strong>Ajukan Perpanjangan Pengisian KRS Semester <?= $this->siska->stringSemester( $next, '' ) ?></strong>
    </button>
  </a>

  <?php
  } else {

    if( (empty($last) || $last['kodesmt'] != $krs_baru['kodesmt']) ) {
      $next = $krs_baru['kodesmt'];
      $nexturl = "/krs/" . $this->session->uid . "/$next/0";

      ?>

      <a href="<?= $nexturl ?>">
        <button type='button' class='list-group-item btn-primary' id='"+i+"'>
          <strong>Registrasi KRS <br>Semester <?= $this->siska->stringSemester( $next, '' ); ?></strong>
        </button>
      </a>

<?php
    }
  } */
  ?>

</div>
<div class="col-sm-6 col-md-6">

  <div class="panel panel-default">
    <canvas id="ipChart" width="400" height="100"></canvas>
  </div>

  <div class="panel-body">

    <?php
      //if( $krs_baru && $last['kodesmt'] != $krs_baru['kodesmt'] ){
      if( (empty($last) || $last['kodesmt'] != $krs_baru['kodesmt']) &&
          date('Y-m-d') > $krs_baru['akhir'] && !$krs_extend ){
          //belum mengisi KRS  &&  masih ada waktu && tidak ada krs extend
    ?>
        <div class="row">
          <a href="/krs/extend/<?= $this->session->uid ?>/<?= $next ?>">
            <button type='button' class="btn btn-block btn-danger" id='rgkrs'>
              Ajukan Perpanjangan Waktu Registrasi KRS Semester <?= $this->siska->stringSemester( $next, '' ) ?>
            </button>
          </a>
          <br>&nbsp;
        </div>

    <?php
      } else {
    ?>

        <h4 class="text-center">
          Registrasi KRS Semester <?= $this->siska->stringSemester( $next ); ?>
        </h4>
        <div class="row">
          <div class="col-sm-4 col-md-4">
            <a href="/krs/new/<?=$next?>/0">
              <button type='button' class="btn btn-block <?= ($last['tanggal_request_slip'] === null)? "btn-primary":"btn-success"?>" id='reqslip'
              <?= ((!empty($last) && $last['kodesmt'] == $krs_baru['kodesmt']) && $last['tanggal_request_slip'] !== null)?"disabled":"" ?>>
                Request<br>Slip Pembayaran
              </button>
            </a>

            <div class="text-center"><i>
            <?php
              if( $last['kodesmt'] == $krs_baru['kodesmt'] && $last['tanggal_request_slip'] !== null && $last['tanggal_cetak_slip'] === null) {
                echo "menunggu penerbitan slip,<br>cek email Anda atau hubungi bagian keuangan";
              } elseif( $last['kodesmt'] == $krs_baru['kodesmt'] &&  $last['tanggal_cetak_slip'] !== null ) {
                echo "slip: " . $last['nomorslip'] . "<br>" . date( 'd-m-Y', strtotime( $last['tanggal_cetak_slip'] ) );
              }
            ?>
            </i></div>
          </div>


          <div class="col-sm-4 col-md-4">
            <a href="/krs/konfirmasi/<?=$last['id_krs']?>">
              <button type='button' class="btn btn-block <?=($last['kodesmt'] == $krs_baru['kodesmt'] && $last['tanggal_konfirmasi'] !== null)? "btn-success":"btn-primary"?>" id='confirmpay' 
                <?= ((!empty($last) && $last['kodesmt'] == $krs_baru['kodesmt']) && $last['tanggal_konfirmasi'] === null && $last['nomorslip'] != "")?"":"disabled" ?>>
                Konfirmasi<br>Pembayaran
              </button>
            </a>

            <div class="text-center"><i>
            <?php
              if($last['kodesmt'] == $krs_baru['kodesmt'] && $last['tanggal_konfirmasi'] !== null) {
                if($last['tanggal_validasi_keu'] === null) {
                  echo "menunggu validasi<br>Bagian Keuangan";
                } else {
                  echo "validasi keuangan<br>".date( 'd-m-Y', strtotime( $last['tanggal_validasi_keu'] ) );
                }
              } else {
                echo "sampai dengan tanggal<br>".date( 'd-m-Y', strtotime( ($krs_extend!==false)?$krs_extend['akhir']:$krs_baru['akhir'] ) );
              }
            ?>
            </i></div>

          </div>
          <div class="col-sm-4 col-md-4">
            <a href="<?= $nexturl ?>">
              <button type='button' class="btn btn-block <?=($last['kodesmt'] == $krs_baru['kodesmt'] && $last['tanggal_validasi_pa'] !== null)? "btn-success":"btn-primary"?>" id='regkrs' 
                <?= ((!empty($last) && $last['kodesmt'] == $krs_baru['kodesmt']) && $last['tanggal_validasi_keu'] !== null )?"":"disabled" ?>>
                Registrasi<br>KRS Semester
              </button>
            </a>
          </div>
        </div>
        <br>&nbsp;

    <?php
        }
    ?>


    <div class="row">
      <div class="panel panel-info">
        <div class="panel-heading">
          <strong>
            Rencana Studi Semester <span id="disp_smt"></span>
          </strong>
        </div>

        <div class="panel-body">

          <table class="display" id="krssmt" width="100%">
            <thead>
              <tr>
                <th>Kode MK</th>
                <th>Mata Kuliah</th>
                <th>sks</th>
                <th>status</th>
              </tr>
            </thead>
          </table>

          <table class='table table-bordered'>
            <tr>
              <td class="datafields" width='130px'>Jumlah sks:</td>
              <td id="jmlsks"></td>
            </tr>
            <tr>
              <td class="datafields" width='130px'>Jumlah Mata kuliah:</td>
              <td id="jmlmk"></td>
            </tr>
          </table>

          <?php if (!empty($last) && $last['tanggal_validasi_keu'] !== null): ?>
            <div id="editkrs" style="display:none;">
              <br>
              <a href="<?= "/krs/" . $this->session->uid . "/".$last['kodesmt']."/0"?> ">
                <button type='button' class="btn btn-block btn-primary">
                  Edit KRS
                </button>
              </a>
            </div>
          <?php endif; ?>

        </div>

        <table class='table'>
          <tr>
            <td class="datafields">Cetak Slip:</td>
            <td id="tgslip"></td>
          </tr>
          <tr>
            <td class="datafields">Pembayaran:</td>
            <td id="tgbayar"></td>
          </tr>
          <tr>
            <td class="datafields" width='150px'>Validasi Keuangan:</td>
            <td id="tgvalkeu"></td>
          </tr>
          <tr>
            <td class="datafields" width='150px'>Registrasi:</td>
            <td id="tgreg"></td>
          </tr>
          <tr>
            <td class="datafields" width='150px'>Validasi PA:</td>
            <td id="tgvalpa"></td>
          </tr>
          <tr>
            <td class="datafields" width='150px'>Validasi Pengelola Prodi:</td>
            <td id="tgvalprodi"></td>
          </tr>
        </table>
      </div>

    </div>

  </div>

</div>


<script type="text/javascript">
  var nimhs = <?= $this->session->uid ?>;
  getKrsSemester('<?= $lastidk ?>');

  function getKrsSemester( idk ) {
    var tablekrs = $("#krssmt").DataTable({
      ajax: { url  : "/krs_api/krssemester/idk/" + idk,
              type : "GET"
              },
      sAjaxDataProp: "krs_semester.belanja_krs",
      columns : [ {"data" : "kodemk", "className": "dt-center"},
                  {"data" : "matakuliah.namamk"},
                  {"data" : "sks", "className": "dt-center"},
                  {"data" : "status", "className": "dt-center"}
                ],
      filter: false,
      info: false,
      paging: false,
      destroy: true
    })

    tablekrs.on( 'xhr', function () {
      var dkrs = tablekrs.ajax.json();
      $("#jmlsks").html(dkrs.krs_semester.jumlah_sks);
      $("#jmlmk").html(dkrs.krs_semester.belanja_krs.length);

      $("#tgreg").html(dkrs.krs_semester.tanggal_registrasi_mhs);
      $("#tgvalpa").html(dkrs.krs_semester.tanggal_validasi_pa);
      $("#tgslip").html(dkrs.krs_semester.tanggal_cetak_slip);
      $("#tgbayar").html(dkrs.krs_semester.tanggal_bayar);
      $("#tgvalkeu").html(dkrs.krs_semester.tanggal_validasi_keu);
      $("#tgvalprodi").html(dkrs.krs_semester.tanggal_validasi_prodi);
      $("#disp_smt").html(dkrs.krs_semester.kodesmt);

      if(dkrs.krs_semester.status == 0){
        $("#editkrs").show();
      } else {
        $("#editkrs").hide();
      }
    });
  }

  $.get("/mahasiswa_api/ips/nim/"+nimhs, function(data){
    if( data && data.ip_semester.length > 1 ){
      var labels = data.ip_semester.map(function(e) { return e.kodesmt; });
      var data = data.ip_semester.map(function(e) { return e.ip; });
      var ctx = document.getElementById("ipChart").getContext('2d');

      var ipChart = new Chart(ctx, {
        type: 'line',
        data: {
          labels: labels,
          datasets: [{
            label: 'Indeks Prestasi Semester',
            data: data,
            borderWidth: 3,
            backgroundColor: 'rgba(204, 0, 0, 0.2)'
          }]
        },
        options: {
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero:true
              }
            }]
          },
          elements: {
            line: {
                tension: 0, // disables bezier curves
            }
          }
        }
      });
    } else {
      $("#ipChart").hide();
    }
  });

</script>
