<div class="row">
  <div class="col-sm-5 col-md-5">
    <br />

    <strong>Semester:</strong>
    <select class="form-control" id="semesterkrs">
      <?php
      $nextsmt = $this->siska->nextSemester( $this->siska->semesterNow() );
      ?>
      <option value="<?= $nextsmt ?>">Semester <?= $this->siska->stringSemester( $nextsmt ) ?></option>
      <?php
      $listsemester = $this->mahasiswa_model->listSemester( $this->session->uid );
      foreach ($listsemester as $smt) {
        ?>
        <option value="<?= $smt['kodesmt'] ?>">
          <?php
          echo "Semester " . $this->siska->stringSemester( $smt['kodesmt'] ) .
          " (" . $smt['kodesmt'] . ")" ;
          ?>
        </option>
        <?php
      }
      ?>
    </select>

    <br />

  </div>

</div>

<div class="row">
  <div class="col-sm-5 col-md-5">
    <div class="panel panel-info">
      <div class="panel-heading">
        <strong>
          Rencana Mata Kuliah
        </strong>
      </div>
      <div class="panel-body">
        <table class="display" id="mkrencana" width="100%">
        <thead>
          <tr>
            <th>Kode MK</th>
            <th>Nama Mata Kuliah</th>
            <th>sks</th>
            <th>batal</th>
          </tr>
        </thead>
      </table>
    </div>
  </div>
</div>
  <div class="col-sm-5 col-md-5">
    <div class="panel panel-info">
      <div class="panel-heading">
        <strong>
          Daftar Mata Kuliah
        </strong>
      </div>
      <div class="panel-body">
        <table class="display" id="daftarmkpilihan" width="100%">
        <thead>
          <tr>
            <th>Kode MK</th>
            <th>Nama Mata Kuliah</th>
            <th>sks</th>
            <th>nilai</th>
            <th>pilih</th>
          </tr>
        </thead>
      </table>
    </div>
  </div>
</div>

</div>
