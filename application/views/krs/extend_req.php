 <?php
   $this->load->view('mhs/overview');
 ?>

 <div class="row">
   <div class="col-sm-3 col-md-3">
   </div>

   <div class="col-sm-6 col-md-6">
     <div class="panel panel-default">
       <table class='table'>
         <tr>
           <td class="datafields" width='150px'>Semester KRS:</td>
           <td><?= $this->siska->stringSemester($kodesmt) ?></td>
         </tr>
         <tr>
           <td class="datafields" width='150px'>Tipe KRS:</td>
           <td>
             <select class="form-control" name="tipekrs" id="tipekrs">
               <option value="0">KRS Baru</option>
               <option value="1">Perubahan KRS</option>
             </select>
           </td>
         </tr>
         <tr>
           <td class="datafields" width='150px'>Alasan Keterlambatan:</td>
           <td>
             <textarea id="alasanextend" name="alasanextend" class="form-control" rows="10"
                       placeholder="-- wajib menjelaskan alasan keterlambatan (minimal dalam 15 kata) --"></textarea>
           </td>
         </tr>
         <tr>
           <td style="text-align:center;">
             <div id="pdfpernyataan" style="display:none;">
               Download dan Cetak<br>Surat Pernyataan<br>
               <input type="image" src="/assets/img/pdf32.gif" onclick="requestpdf();">
             </div>
           </td>
           <td style="text-align:right; vertical-align:bottom;">
             <button type='button' class="btn btn-primary" id='submitext' disabled>Ajukan Permohonan</button>
           </td>
         </tr>

       </table>
     </div>
   </div>
 </div>


<script type="text/javascript">
  $("#alasanextend").keyup(function(){
    var words = $.trim($("#alasanextend").val()).split(" ");
    if( words.length >= 15 ){
      $("#pdfpernyataan").show();
    } else {
      $("#pdfpernyataan").hide();
    }
  });

  function requestpdf() {
    dkrs = {
         'mhs': '<?= $this->session->uid ?>',
        'type': $("#tipekrs").val(),
         'smt': '<?= $kodesmt ?>',
      'alasan': $("#alasanextend").val()
    };
    $.post('/krs_api/krsextend', dkrs);

    var win = window.open('/pdf/krsextend/<?= $this->session->uid ?>/<?= $kodesmt ?>', '_blank');
    if (!win) {
        alert('Please allow popups for this website');
    }
    $("#submitext").prop('disabled', false);
  }
</script>
