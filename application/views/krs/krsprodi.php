<div class="col-sm-7 col-md-7">
  <div class="row">
    <div class="panel panel-info">
      <div class="panel-body">

        <h4>Rencana Studi Mahasiswa</h4>

        <div class="form-group">
          <label for="pilihsmtkrs">Semester:</label>
          <select class="form-control" id="pilihsmtkrs">
          </select>
        </div>

        <div id="pilihpr" class="form-group">
          <label for="pilihprodikrs">Program Studi:</label>
          <select class="form-control" id="pilihprodikrs">
          </select>
        </div>

      </div>
    </div>
  </div>
</div>

<div class="col-sm-5 col-md-5">
  <div class="row">
    <div class="panel panel-info" id="krsmhsrekap">
      <div class="panel-body">

        <table class='table table-bordered'>
          <tr class="bg-primary">
            <td colspan='3'>Rekapitulasi</td>
          </tr>
          <tr>
            <td class="datafields">Mahasiswa Aktif:</td>
            <td class="text-center" id="ijmlmhsaktif"></td>
            <td class="text-center" id="ipermhsaktif"></td>
          </tr>
          <tr>
            <td class="datafields">Registrasi KRS:</td>
            <td class="text-center" id="ijmlmhskrs"></td>
            <td class="text-center" id="ipermhskrs"></td>
          </tr>
          <tr>
            <td class="datafields">Validasi PA:</td>
            <td class="text-center" id="ijmlmhsvalpa"></td>
            <td class="text-center" id="ipermhsvalpa"></td>
          </tr>
          <tr>
            <td class="datafields">Validasi Keuangan:</td>
            <td class="text-center" id="ijmlmhsvalkeu"></td>
            <td class="text-center" id="ipermhsvalkeu"></td>
          </tr>
        </table>

      </div>
    </div>
  </div>
</div>

<div class="col-sm-7 col-md-7" id="waitinglistkrs">
  <div class="row">
    <div class="panel panel-info">
      <div class="panel-body">

        <div class="row">
          <div class="col-sm-6 col-md-6">
            <input type="checkbox" id="chkautorefresh"> Auto Refresh
          </div>
          <div class="col-sm-6 col-md-6 text-right">
            <button title="refresh list" id="btrefresh">
              <img src="/assets/img/reload16.png"> Refresh
            </button><br>&nbsp;
          </div>
        </div>

        <table class="table table-sm table-bordered table-striped table-hover" id="mhskrs" width="100%">
          <thead>
          <tr class="bg-primary">
              <th width='40px' class="text-center">NIM</th>
              <th class="text-center">Mahasiswa</th>
              <th width='50px' class="text-center">Reg</th>
              <th width='5px' class="text-center">Inv</th>
              <th width='5px' class="text-center">Konf</th>
              <th width='5px' class="text-center">Byr</th>
              <th width='5px' class="text-center">Keu</th>
              <th width='50px' class="text-center">KRS</th>
              <th width='5px' class="text-center">PA</th>
              <th width='5px' class="text-center">Prd</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>

      </div>
    </div>
  </div>
</div>

<div class="col-sm-5 col-md-5">
  <div class="row">
    <div class="panel panel-info" id="krsmhsdetail" style="display:none">

      <div class="panel-heading">
        <h5>KRS Mahasiswa </h5>
      </div>

      <div class="panel-body">
        <table class='table table-bordered'>
          <tr>
            <td class="datafields" width='130px'>Semester:</td>
            <td id="smtkrs"></td>
          </tr>
          <tr>
            <td class="datafields" width='130px'>NIM:</td>
            <td id="nimhs"></td>
          </tr>
          <tr>
            <td class="datafields" width='130px'>Nama Mahasiswa:</td>
            <td id="namamhs"></td>
          </tr>
          <tr>
            <td class="datafields" width='130px'>Dosen PA:</td>
            <td id="dosenpa"></td>
          </tr>
        </table>

        <table class="table table-bordered table-striped" id="krssmt" width="100%">
          <thead>
            <tr class="bg-primary">
              <td width='5px' class="text-center">No.</td>
              <td width='35px' class="text-center">Kode MK</td>
              <td class="text-center">Nama Mata Kuliah</td>
              <td width='5px' class="text-center">sks</td>
            </tr>
          </thead>
        </table><br>

        <table class='table table-bordered'>
          <tr>
            <td class="datafields" width='130px'>Jumlah sks:</td>
            <td id="jmlsks"></td>
          </tr>
          <tr>
            <td class="datafields" width='130px'>Jumlah Mata Kuliah:</td>
            <td id="jmlmk"></td>
          </tr>
          <tr>
            <td class="datafields" width='130px'>Tanggal Registrasi:</td>
            <td id="tanggalreg"></td>
          </tr>
          <tr id="rowtgvalidasipa" style="display:none;">
            <td class="datafields" width='130px'>Validasi PA:</td>
            <td id="tanggalvalidasipa"></td>
          </tr>
          <tr id="rowtgcetakslip" style="display:none;">
            <td class="datafields" width='130px'>Tgl Cetak Slip:</td>
            <td id="incetakslip"></td>
          </tr>
          <tr id="rowtgbayar" style="display:none;">
            <td class="datafields" width='130px'>Tgl Bayar:</td>
            <td id="intglbayar"></td>
          </tr>
          <tr id="rowtgvalidasikeu" style="display:none;">
            <td class="datafields" width='130px'>Tgl Validasi Keu.:</td>
            <td id="intglvalidasikeu"></td>
          </tr>
          <tr id="rowtgvalidasiprodi" style="display:none;">
            <td class="datafields" width='130px'>Tgl Validasi Prodi:</td>
            <td id="intglvalidasiprodi"></td>
          </tr>
        </table>

        <button id="tbkonfirmprodi" class="btn btn-block btn-primary" style="display:block;">Registrasi sebagai Peserta Mata Kuliah</button>
        <br>

      </div>

    </div>

    <div class="panel panel-danger" id="krsmhsrekap">

      <div class="panel-heading">
        <h5>Permintaan Perpanjangan</h5>
      </div>

      <div class="panel-body">
        <table class="table table-bordered table-striped" id="krsext">
          <thead>
            <tr class="bg-primary">
              <td width='5px' class="text-center">NIM</td>
              <td width='35px' class="text-center">Mahasiswa</td>
              <td width='5px' class="text-center">Requested</td>
              <td width='5px' class="text-center">Extended</td>
            </tr>
          </thead>
        </table><br>

      </div>
    </div>

  </div>
</div>


<script type="text/javascript" src="/assets/js/siska_krs.js"></script>
<script type="text/javascript">
  var roles = {
    'prd': true,
    "keu": false
  };
  var prodi = <?= $this->session->prodi ?>;
  var dttable = "#mhskrs";
  var smtkrs = "#pilihsmtkrs";
  var dttableres;
  var idkrs;


  getSemesterKRS(smtkrs);
  
  setTimeout(function() {
    listProdiKRS(smtdefault);
    // console.log(smtdefault);

    dttableres.on('xhr', function() {
      var lkrsmhs = dttableres.ajax.json();
      var nregm = lkrsmhs.listkrsmhs.length;
      var nvalp = 0;
      var nvalk = 0;
      $.each(lkrsmhs.listkrsmhs, function(i) {
        if (lkrsmhs.listkrsmhs[i].tanggal_validasi_pa != null) nvalp++;
        if (lkrsmhs.listkrsmhs[i].tanggal_validasi_keu != null) nvalk++;
      });

      $("#ijmlmhskrs").html(nregm);
      $("#ijmlmhsvalpa").html(nvalp);
      $("#ipermhsvalpa").html(((nvalp / nregm) * 100).toFixed(2) + ' %');
      $("#ijmlmhsvalkeu").html(nvalk);
      $("#ipermhsvalkeu").html(((nvalk / nregm) * 100).toFixed(2) + ' %');

    });

    $("#pilihprodikrs option[value=" + prodi + "]").prop("selected", "selected");


    $("#krsext").DataTable({
      ajax: {
        url: "/krs_api/krsextendlist/smt/" + smtdefault,
        type: "GET"
      },
      sAjaxDataProp: "krs_extend",
      columns: [{
          "data": "nimhs",
          "className": "dt-center"
        },
        {
          "data": "namamhs"
        },
        {
          "data": "tanggal_request",
          "className": "dt-center"
        },
        {
          "render": function(data, type, full, meta) {
            if (full.batas_akhir != null) {
              return full.batas_akhir;
            } else {
              return "<a href='/krs/ext_validate/" + full.nimhs + "/" + smtdefault + "' title='validasi'><img src='/assets/img/edit16.png'></a>";
            }
          },
          className: "dt-center"
        },
      ],
      order: [[ 2, 'desc' ]],
      filter: false,
      info: false,
      paging: false,
      destroy: true
    })

  }, 1500);

  $(dttable + " tbody").on('click', 'tr', function() {
    var data = dttableres.row(this).data();

    $("#krssmt").DataTable({
      ajax: {
        url: "/krs_api/krssemester/idk/" + data.id_krs,
        type: "GET"
      },
      sAjaxDataProp: "krs_semester.belanja_krs",
      columns: [{
          render: function(data, type, full, meta) {
            return meta.row + 1;
          },
          className: "dt-center"
        },
        {
          "data": "kodemk",
          "className": "dt-center"
        },
        {
          "data": "matakuliah.namamk"
        },
        {
          "data": "sks",
          "className": "dt-center"
        }
      ],
      filter: false,
      info: false,
      paging: false,
      destroy: true
    })

    idkrs = data.id_krs;
    showDetailMahasiswa(data);
    $("#krsmhsdetail").show();
  });




  $(smtkrs).change(function() {
    smtdefault = $(this).val();
    listProdiKRS($(this).val());
    getKRSMahasiswa($(this).val());
  });

  setInterval(function() {
    if ($("#chkautorefresh").is(":checked")) {
      getKRSMahasiswa(smtdefault);
    }
  }, 60000);

  $("#btrefresh").click(function() {
    getKRSMahasiswa(smtdefault);
  })


  function listProdiKRS(smt) {
    $("#pilihprodikrs").empty();
    $.get("/krs_api/prodikrsaktif/smt/" + smt, function(data) {
      $.each(data.listprodi, function(i) {
        var kdpr = data.listprodi[i].kode;
        var nmpr = data.listprodi[i].jenjang.namaprogram + " " + data.listprodi[i].nama + " (" + data.listprodi[i].jenjang.kode + ")";
        var opt = new Option(nmpr, kdpr);
        $(opt).html(nmpr);
        $("#pilihprodikrs").append(opt);
      });
    });
  }

  $("#pilihprodikrs").change(function() {
    prodi = $(this).val();
    getKRSMahasiswa(smtdefault);
  });

  $("#tbkonfirmprodi").click(function() {
    $.post("/krs_api/validasiprodi", {
      id: idkrs
    });
    getKRSMahasiswa(smtdefault);
    $(this).hide();
  });
</script>
