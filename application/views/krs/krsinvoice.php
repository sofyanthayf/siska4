<div class="col-lg-5">
    <h5>
        Invoice Biaya Pendidikan untuk <strong>Semester <?= $this->siska->stringSemester($krs['kodesmt']) ?></strong>
        akan dikirimkan ke email mahasiswa atas nama:

    </h5>

    <div class="panel">
        <div class="panel-body">
            <table class='table table-bordered'>
                <tr>
                    <td class="datafields" width='130px'>Mahasiswa:</td>
                    <td id="namamhs"><?= $mhs['namamhs'] ?></td>
                </tr>
                <tr>
                    <td class="datafields" width='130px'>N I M:</td>
                    <td id="nimhs"><?= $mhs['nimhs'] ?></td>
                </tr>
                <tr>
                    <td class="datafields" width='130px'>email:</td>
                    <td id="email"><?= $mhs['email'] ?></td>
                </tr>
                <tr>
                    <td class="datafields" width='130px'>Biaya Semester</td>
                    <td id="biaya">
                        <strong>
                            Rp <?= number_format($krs['biaya_sks'] + $krs['biaya_adm'] + $krs['biaya_unik'], 0, ",", ".") ?>
                        </strong>
                    </td>
                </tr>
                <tr>
                    <td class="datafields" width='130px'>Kode Transfer</td>
                    <td id="kodetransfer"><?= $krs['kodetransfer'] ?></td>
                </tr>
            </table>
        </div>
    </div>

    <button id="tbsendemail" class="btn btn-block btn-primary" style="display:block;"> Kirim Email Invoice </button>
    <button id="tbcetakslip" class="btn btn-block btn-primary" style="display:block;" disabled> Cetak Slip </button>

    <br><br>
    <?php if ($stat > 0) { ?>

        <?php
        switch ($stat) {
            case 1:
                echo "<div class='alert alert-success' role='alert'>
                          Email invoice berhasil dikirimkan
                          </div>";
                break;
            case 2:
                echo "<div class='alert alert-danger' role='alert'>
                          <span class='fa fa-exclamation-triangle fa-2x' aria-hidden='true'></span><br>
                          Maaf, email invoice gagal dikirimkan, gangguan teknis, jika berulang laporkan ke admin SISKA
                          </div>";
                break;
        }
        ?>
    <?php } ?>

</div>
<div class="col-lg-7">
    <h5>Email Preview</h5>
    <iframe src="/keu/krs_invoice/<?= $krs['id_krs'] ?>" title="Invoice Preview" width="100%" height="460" style="background: #FFFFFF;"></iframe>
</div>


<script type="text/javascript" src="/assets/js/siska_krs.js"></script>

<script type="text/javascript">
    $("#tbsendemail").click(function() {
        window.open("/keu/krs_invoice_mail/<?= $krs['id_krs'] ?>", "_top");
    });
</script>