<!DOCTYPE html>
<html lang="id">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="SISKA - Sistem Informasi Akademik STMIK KHARISMA Makassar">
    <meta name="author" content="Sofyan Thayf">

    <title>SISKA 4.0</title>

    <link href="<?= base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= base_url();?>assets/css/sb-admin.css" rel="stylesheet">
    <link href="<?= base_url();?>assets/css/plugins/morris.css" rel="stylesheet">
    <link href="<?= base_url();?>assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="<?= base_url();?>assets/css/siskastyle.css" rel="stylesheet">

    <link rel='shortcut icon' href='<?= base_url();?>assets/img/favicon.ico' type='image/x-icon'/ >

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-6">
                <img src="<?= base_url();?>assets/img/STMIKKHARISMA01b.gif" class="img-responsive">
                <h2>Sistem Informasi Akademik</h2>
                <h3>v.4.0 2017</h3>


            </div>

            <div class="col-sm-5 col-md-5 col-md-offset-1">
                <div class="account-wall">

<?php
    switch ($kode) {
        case 1:
            echo "<div class='alert alert-danger' role='alert'>
                    <span class='fa fa-exclamation-triangle fa-2x' aria-hidden='true'></span>
                    Sign-in Gagal: ID salah atau tidak dikenali
                </div>";
            break;
        case 2:
            echo "<div class='alert alert-danger' role='alert'>
                    <span class='fa fa-exclamation-triangle fa-2x' aria-hidden='true'></span>
                    Sign-in Gagal: Password keliru
                </div>";
            break;
        case 3:
            echo "<div class='alert alert-warning' role='alert'>
                    <span class='fa fa-clock-o fa-2x' aria-hidden='true'></span>
                    Session expire, silahkan login kembali
                </div>";
            break;
        case 7:
            echo "<div class='alert alert-success' role='alert'>
                    <span class='fa fa-key fa-2x' aria-hidden='true'></span>
                    Password baru telah dikirimkan ke elamat e-mail Anda
                </div>";
            break;
        case 9:
            echo "<div class='alert alert-warning' role='alert'>
                    <span class='fa fa-sign-out fa-2x' aria-hidden='true'></span>
                    Anda sudah Sign-out, Terima kasih
                </div>";
            break;

    }
?>

                    <h1 class="text-center login-title">Sign In to SISKA</h1>
                    <form class="form-signin" action="<?=base_url()?>signin" method="post">
                    <!-- <form class="form-signin" action="" method="post"> -->
                      <input type="text" name="uid" class="form-control" placeholder="NIM / NIDN / NIK" required autofocus><br>
                      <input type="password" name="pass" class="form-control" placeholder="Password" required>
                      <input type="submit" class="btn btn-lg btn-primary btn-block" name="submit" value="Sign in">
                      <a href="/lupapassword" class="pull-right need-help">Lupa password? </a><span class="clearfix"></span>
                    </form>
                </div>
                <a href="https://alumni.kharisma.ac.id" class="text-center new-account">Alumni, lewat sini </a>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12 col-md-12">
                <h2> <i class='fa fa-info-circle' aria-hidden='true'></i> Info Kampus</h2>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6 col-md-4">
                <div class="infobox">
                    <div class="caption">
                        <h3>Mahasiswa Baru 2022</h3>
                        <p>
                            Selamat datang, mahasiswa baru angkatan 23 STMIK KHARISMA Makassar,
                            tahun akademik 2022-2023. Selamat bergabung di kampusnya Digital Entrepreneur
                            <br><br>
                            Cek e-mail Anda untuk dapat melakukan akses ke layanan Sistem Informasi Akademik 
                            STMIK KHARISMA (SISKA)
                          </p>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-md-4">
                <div class="infobox" style="height: 200px;">
                    <div class="caption">
                        <h3></h3>
                        <p>
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-md-4">
              <div class="infobox" style="height: 200px;">
                <div class="caption">
                  <h3></h3>
                  <p>
                  </p>
                </div>
              </div>
            </div>
        </div>


       <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                  &copy; STMIK KHARISMA Makassar, 2017<br>
                  Powered by <a href='https://m.do.co/c/6cc56cdf090e' target="_blank">Digital Ocean</a>
                </div>
            </div>
        </footer>

    </div> <!-- /container -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="<?= base_url();?>assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="<?= base_url();?>assets/js/bootstrap.min.js"></script>
  </body>

</html>
