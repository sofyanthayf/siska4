<h3>Pengaturan Account</h3>
<div class="panel panel-info">
    <div class="panel-heading"><strong>Ganti Password</strong></div>
    <div class="panel-body">
      <form class="form">
        <div id="passwordrespons"></div>

        <div class="form-group">
          <label>Password Anda Sekarang:</label>
          <input type="password" class="form-control" id="oldpassword" autocomplete="current-password" name="oldpassword" placeholder="current password" required>
          <p class="help-block" id="warningoldpassword"></p>
        </div>

        <div class="form-group">
          <label>Password Baru:</label>
          <input type="password" class="form-control" id="password1" autocomplete="new-password" name="password" placeholder="new password" required>
          <p class="help-block" id="warningnewpassword"></p>
        </div>

        <div class="form-group">
          <label>Verifikasi Password Baru:</label>
          <input type="password" class="form-control" id="password2" autocomplete="new-password" placeholder="re-type new password" required>
          <p class="help-block" id="warningpassword"></p>
        </div>

        <button class="btn btn-success" id="submitgantipass">Ganti Password</button>
      </form>
    </div>
</div>

<script type="text/javascript">
$("#password2").keyup(function(){
    if( $(this).val() != $("#password1").val() ){
        $("#warningpassword").html("Verifikasi password tidak sama");
        $( this ).focus();
        // $(":submit").attr("disabled", true);
        $("#submitgantipass").attr("disabled", true);
    } else {
        $("#warningpassword").html("");
        // $(":submit").removeAttr("disabled");
        $("#submitgantipass").removeAttr("disabled");
    }
});

$("#oldpassword").keyup(function(){ $("#warningoldpassword").html(""); });
$("#password1").keyup(function(){ $("#warningnewpassword").html(""); });
$("#submitgantipass").focusout(function(){ $("#passwordrespons").html(""); });

$("#submitgantipass").click(function(){
    if( !$("#oldpassword").val() ){
        $("#warningoldpassword").html("Password Lama harus diisi")
        $("#oldpassword").focus();
    } else if( !$("#password1").val() ){
        $("#warningnewpassword").html("Password Baru belum diisi")
        $("#password1").focus();
    } else {
        var data = {
          'oldpassword' : $("#oldpassword").val(),
          'password' : $("#password1").val()
        };

        var style2 = "</div>";
        $.post('/apis/auth/changepass', data, function(result){
          var style1 = "<div class='alert alert-success text-center' role='alert'>";
          $("#passwordrespons").html(style1+"Password Anda telah berhasil diganti"+style2);
        }).fail(function(xhr){
          var style1 = "<div class='alert alert-warning text-center' role='alert'>";
          $("#passwordrespons").html(style1+"Gagal mengganti password, silahkan dicoba kembali"+style2);
        });

        $("#oldpassword").val("");
        $("#password1").val("");
        $("#password2").val("");
    }
});

</script>
