<div class="col-sm-5 col-md-5">
    <div class="account-wall">

<?php
switch ($kode) {
    case 1:
        echo "<div class='alert alert-danger text-center' role='alert'>
                <span class='fa fa-exclamation-triangle fa-2x' aria-hidden='true'></span><br />
                Nomor ID dan Alamat E-mail tidak sesuai atau salah tulis
            </div>";
        break;

    case 2:
        echo "<div class='alert alert-success text-center' role='alert'>
                <span class='fa fa-key fa-2x' aria-hidden='true'></span><br />
                Password baru telah dikirimkan ke elamat e-mail Anda<br />
                <br />
                <a href='/login'><strong>Sign-in</strong></a>
            </div>";
        break;
    case 3:
        echo "<div class='alert alert-danger text-center' role='alert'>
                <span class='fa fa-exclamation-triangle fa-2x' aria-hidden='true'></span><br />
                Maaf, ada masalah di proses pengiriman password melalui email.<br />
                Cobalah ulangi beberapa saat lagi, jika masalah berulang mohon bantuan untuk
                melaporkan ke administrator SISKA
              </div>";
        break;


}

if( $kode != 2 ) {
?>
        <form class="form-signin" action="/resetpassword" method="post">
            <p>Nomor ID Anda:</p>
            <input type="text" name="nid" pattern="[0-9]{5,10}" title="Numeric only (0-9) for NIM/NIDN/NIK, 8/10/5 digits" class="form-control" placeholder="NIM / NIDN / NIK" required autofocus>
            <br />
            <p>Alamat E-mail Anda:</p>
            <input type="email" name="email" class="form-control" placeholder="Email Anda" required autofocus>
            <br />
            <input type="submit" class="btn btn-lg btn-success btn-block" name="submit" value="Reset Password">
        </form>
    </div>
    <br />
<?php
}
?>

</div>
