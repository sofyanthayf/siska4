<div class="row">
    <div class="col-sm-7 col-md-7">

        <div class="panel panel-info">
            <div class="panel-heading"><strong>Biodata</strong></div>
            <div class="panel-body">
                <table class="table">
                    <tr>
                        <td class="text-right" width="110px">Nama Lengkap:</td>
                        <td>
                            <strong><?= $maba['nama_lengkap'] ?></strong>
                            <input type="hidden" id="mabaid" value="<?= $maba['id'] ?>" />
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="text-right">Alamat Email:</td>
                        <td><strong><?= $maba['email'] ?></strong></td>
                        <td>
                            <?php
                                if( $maba['email_valid'] != '0000-00-00 00:00:00' ){
                                    echo "<i class='fa fa-fw fa-check text-success'></i> <i>terverifikasi</i>";
                                } else {
                                    echo "<i class='fa fa-fw fa-exclamation text-danger'></i> <i>belum terverifikasi</i>";
                                }
                             ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right">Nomor HP:</td>
                        <td><strong><?= $maba['telepon'] ?></strong></td>
                        <td width="140px">
                            <?php
                                if( $maba['telepon_valid'] != '0000-00-00 00:00:00' ){
                                    echo "<i class='fa fa-fw fa-check text-success'></i> <i>terverifikasi</i>";
                                } else {
                                    echo "<i class='fa fa-fw fa-exclamation text-danger'></i> <i>belum verifikasi</i>";
                                }
                             ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right">Tempat Lahir:</td>
                        <td><strong><?= $maba['tempat_lahir']?></strong></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="text-right">Tanggal Lahir:</td>
                        <td><strong>
                            <?php
                                if( !empty($maba['tanggal_lahir']) )
                                    echo date("d-m-Y",strtotime($maba['tanggal_lahir']));
                            ?></strong></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="text-right">Jenis Kelamin:</td>
                        <td><strong><?= $this->siska->jenis_kelamin[$maba['sex']]?></strong></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="text-right">Agama:</td>
                        <td><strong>
                            <?php
                                if( !empty($maba['agama']) ) echo $this->siska->agama[$maba['agama']];
                            ?></strong></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="text-right">Alamat:</td>
                        <td><strong>
                            <?php
                                echo $maba['alamat']."<br />";
                                if( !empty( $maba['kelurahan'] ) ){
                                    $aregion = $this->region_model->getRegion( $maba['kelurahan'] );
                                    echo "Kelurahan: ".$aregion['nama_desa']."<br />".
                                         "Kecamatan: ".$aregion['nama_kecamatan']."<br />".
                                         "Kabupaten/Kota: ".$aregion['nama_kota']."<br />".
                                         "Propinsi: ".$aregion['nama_propinsi']."<br />";
                                }
                            ?>
                        </strong></td>
                        <td></td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="panel panel-info">
            <div class="panel-heading"><strong>Sekolah Asal</strong></div>
            <div class="panel-body">

                <?php
                    if( !empty( $maba['sekolah_asal'] ) ){
                        $sekolah_asal = $this->region_model->getRegionSekolah( $maba['sekolah_asal'] );
                        $region_sekolah = $this->region_model->getRegion( $sekolah_asal['id_kabkota']);
                        $propsekolah = $region_sekolah['id_propinsi'];
                    }
                ?>

                <table class="table">
                    <tr>
                        <td class="text-right" width="150px">Sekolah:</td>
                        <td><strong>
                            <?php
                                if( !empty( $maba['sekolah_asal'] ) ) echo $sekolah_asal['sekolah'];
                            ?>
                        </strong></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="text-right" width="110px">Kota/Kabupaten:</td>
                        <td><strong>
                            <?php
                                if( !empty( $maba['sekolah_asal'] ) ) echo $sekolah_asal['nama_kota'];
                            ?>
                        </strong>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="text-right" width="110px">Propinsi:</td>
                        <td><strong>
                            <?php
                                if( !empty( $maba['sekolah_asal'] ) ) echo $sekolah_asal['nama_propinsi'] ;
                            ?>
                        </strong></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="text-right" width="110px">Jurusan:</td>
                        <td><strong>
                            <?php
                                if(!empty($maba['sekolah_jurusan']))
                                    echo $this->region_model->getJurusanSMA($maba['sekolah_jurusan'])['jurusan'];
                            ?>
                        </strong></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="text-right">Tahun Lulus:</td>
                        <td><strong><?= $maba['sekolah_lulus'] ?></strong></td>
                        <td></td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="panel panel-info">
            <div class="panel-heading"><strong>Data Orang Tua</strong></div>
            <div class="panel-body">
                <table class="table">
                    <tr>
                        <td class="text-right" width="150px">Nama Ayah:</td>
                        <td><strong><?= $maba['nama_ayah'] ?></strong></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="text-right" width="110px">Nama Ibu:</td>
                        <td><strong><?= $maba['nama_ibu'] ?></strong></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="text-right" width="110px">Pekerjaan Ayah:</td>
                        <td><strong>
                            <?php
                                if(!empty($maba['pekerjaan_ayah']))
                                    echo $this->siska->getPekerjaan( $maba['pekerjaan_ayah'] )['pekerjaan'];
                            ?>
                        </strong></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="text-right" width="110px">Pekerjaan Ibu:</td>
                        <td><strong>
                            <?php
                                if(!empty($maba['pekerjaan_ibu']))
                                    echo $this->siska->getPekerjaan( $maba['pekerjaan_ibu'] )['pekerjaan'];
                            ?>
                        </strong></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="text-right" width="110px">Penghasilan Orang Tua:</td>
                        <td><strong>
                            <?php
                                if(!empty($maba['penghasilan_ortu']))
                                    echo $this->siska->getPenghasilan( $maba['penghasilan_ortu'] )['range'];
                            ?>
                        </strong></td>
                        <td></td>
                    </tr>
                </table>
            </div>
        </div>

    </div>

    <div class="col-sm-5 col-md-5">

        <div class="panel panel-info">
            <div class="panel-heading"><strong>Pilihan Program Studi</strong></div>
            <div class="panel-body">
                <table class="table">
                    <tr>
                        <td class="text-right" width="110px">Pilihan 1:</td>
                        <td>
                            <strong>
                                <?php
                                if( !empty($maba['prodi_pilihan1']) ){
                                    $pil1 = $this->prodi_model->getProdi($maba['prodi_pilihan1']);
                                    echo $pil1['nama']." - ".$pil1['jenjang']['kode'];
                                }
                                ?>
                            </strong>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="text-right" width="110px">Pilihan 2:</td>
                        <td>
                            <strong>
                                <?php
                                if( !empty($maba['prodi_pilihan2']) ){
                                    $pil2 = $this->prodi_model->getProdi($maba['prodi_pilihan2']);
                                    echo $pil2['nama']." - ".$pil2['jenjang']['kode'];
                                }
                                ?>
                            </strong>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="text-right" width="110px">Jalur Prestasi:</td>
                        <td>
                            <strong>
                                <?php
                                if( $maba['jalurprestasi']=='1' ) echo "Ya";
                                else echo "Tidak";
                                ?>
                            </strong>
                        </td>
                        <td></td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="panel panel-info">
            <div class="panel-heading"><strong>Status</strong></div>
            <div class="panel-body">
                <table class="table">
                    <tr>
                        <td class="text-right" width="140px">Tanggal Registrasi:</td>
                        <td><strong><?= $maba['tg_registrasi'] ?></strong></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="text-right">Biodata:</td>
                        <td>
                            <strong>
                            </strong>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="text-right">Ujian Seleksi:</td>
                        <td>
                            <strong>
                            </strong>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="text-right">Wawancara:</td>
                        <td>
                            <strong>
                            </strong>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="text-right">Psikotest:</td>
                        <td>
                            <strong>
                            </strong>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="text-right">Status Akhir:</td>
                        <td>
                            <strong>
                            </strong>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="text-right">Registrasi Akhir:</td>
                        <td>
                            <strong>
                            </strong>
                        </td>
                        <td></td>
                    </tr>
                </table>

            </div>
        </div>

        <div class="panel panel-info">
            <div class="panel-heading"><strong>Dokumen</strong></div>
            <div class="panel-body">
                <table class="table">
<?php
    foreach ($this->siska->dok_maba as $dok ) {
        $uploaddok = $this->pmbuser_model->checkDokumen( $dok['kode'], $maba['id']);
?>
                    <tr>
                        <td><?= $dok['dokumen']?></td>
                        <td>
                            <?php
                                if( !empty($uploaddok) ){
                                    $filedok = $maba['id']."_".$dok['kode'].".".$uploaddok['tipefile'];
                                    echo "<a href='".base_url()."assets/dokumen/maba/2017/".$filedok.
                                         "' target='_blank'>".$filedok."</a>";
                                } else {
                                    echo "-";
                                }
                             ?>
                        </td>
                        <td>
                            <?php
                                if( !empty($uploaddok) && !empty($uploaddok['validasi']) )
                                    echo "<i class='fa fa-fw fa-check text-success'></i> valid";
                            ?>
                        </td>
                    </tr>
<?php
    }
?>

<?php
    foreach ($this->siska->dok_maba_internal as $doi ) {
        $uploaddok = $this->pmbuser_model->checkDokumen( $doi['kode'], $maba['id']);
        if( !empty($uploaddok) ){
?>
                    <tr>
                        <td><?= strtoupper( $doi['dokumen'] )?></td>
                        <td><strong>
                            <?php
                                $filedok = $maba['id']."_".$doi['kode'].".".$uploaddok['tipefile'];
                                echo "<a href='".base_url()."assets/dokumen/maba/2017/".$filedok.
                                     "' target='_blank'>".$filedok."</a>";
                             ?>
                             </strong>
                        </td>
                        <td>
                        </td>
                    </tr>
<?php
        }
    }
?>

                </table>
            </div>
        </div>


    </div>
</div>
