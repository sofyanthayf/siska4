<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="panel panel-info">
            <div class="panel-heading"><strong>Validasi Dokumen</strong></div>
            <div class="panel-body">
                <table class="display" id="listdokumen" data-order='[[ 3, "desc" ]]' data-page-length='25' cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>ID Maba</th>
                            <th>Nama Maba</th>
                            <th>Dokumen</th>
                            <th>Tanggal Upload</th>
                            <th>File</th>
                            <th>Validasi</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="panel panel-info">
            <div class="panel-heading"><strong>Kartu Peserta Seleksi</strong></div>
            <div class="panel-body">
                <table class="display" id="listkartupeserta" data-order='[[ 3, "asc" ]]' data-page-length='25' cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>ID Maba</th>
                            <th>Nama Maba</th>
                            <th>Tanggal Ujian</th>
                            <th>Waktu Ujian</th>
                            <th>Kartu Ujian</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var namadok = {
    <?php
        foreach ($this->siska->dok_maba as $dok) {
            echo "'".$dok['kode']."':'".$dok['dokumen']."',\n";
        }
    ?>
                };
    var filepath = "<?= base_url() ?>assets/dokumen/maba/2017/";
</script>
