<div class="row">
    <div class="col-sm-7 col-md-7">

        <div class="panel panel-info">
            <div class="panel-heading"><strong>Biodata</strong></div>
            <div class="panel-body">
                <table class="table">
                    <tr>
                        <td class="text-right" width="110px">Nama Lengkap:</td>
                        <td>
                            <strong><?= $agen['nama_lengkap'] ?></strong>
                            <input type="hidden" id="agenid" value="<?= $agen['id'] ?>" />
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="text-right">Alamat Email:</td>
                        <td><strong><?= $agen['email'] ?></strong></td>
                        <td>
                            <?php
                                if( $agen['email_valid'] != '0000-00-00 00:00:00' ){
                                    echo "<i class='fa fa-fw fa-check text-success'></i> <i>terverifikasi</i>";
                                } else {
                                    echo "<i class='fa fa-fw fa-check text-danger'></i> <i>belum terverifikasi</i>";
                                }
                             ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right">Nomor HP:</td>
                        <td><strong><?= $agen['telepon'] ?></strong></td>
                        <td width="120px">
                            <?php
                                if( $agen['telepon_valid'] != '0000-00-00 00:00:00' ){
                                    echo "<i class='fa fa-fw fa-check text-success'></i> <i>terverifikasi</i>";
                                } else {
                                    echo "<i class='fa fa-fw fa-exclamation text-danger'></i> <i>belum verifikasi</i>";
                                }
                             ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right">Afiliasi:</td>
                        <td><strong>
                            <?php
                                $afskl = "";
                                if( $agen['tipe_afiliasi'] == 'G' ) {
                                    $skl = $this->region_model->getRegionSekolah($agen['id_afiliasi']);
                                    $afskl = " - ".$skl['sekolah'];
                                }
                                echo $this->pmb_lib->afiliasi[ $agen['tipe_afiliasi'] ]."<br />".
                                     $agen['id_afiliasi'].$afskl;
                            ?>
                        </strong></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="text-right">Alamat:</td>
                        <td><strong>
                            <?php
                                echo $agen['alamat']."<br />";
                                if( !empty( $agen['kelurahan'] ) ){
                                    $aregion = $this->region_model->getRegion( $agen['kelurahan'] );
                                    echo "Kelurahan: ".$aregion['nama_desa']."<br />".
                                         "Kecamatan: ".$aregion['nama_kecamatan']."<br />".
                                         "Kabupaten/Kota: ".$aregion['nama_kota']."<br />".
                                         "Propinsi: ".$aregion['nama_propinsi']."<br />";
                                }
                            ?>
                        </strong></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="text-right">Pekerjaan:</td>
                        <td><strong>
                            <?php
                                if( !empty($agen['pekerjaan']) ) {
                                    $kerjaagen = $this->siska->getPekerjaan( $agen['pekerjaan'] ) ;
                                    echo $kerjaagen['pekerjaan'];
                                }
                            ?>
                        </strong></td>
                        <td></td>
                    </tr>

                </table>
            </div>
        </div>

    </div>

    <div class="col-sm-5 col-md-5">

    <?php
        if( $agen['tipe_afiliasi'] == 'S' ){
     ?>
        <div class="panel panel-info">
            <div class="panel-body text-center">
            <?php
                if( $this->pmb_lib->panitia( $agen['token'], $agen['pan'] )) {
            ?>
                    PANITIA ADMINISTRATIF PMB
            <?php
                    if( $this->pmb_lib->panitia( $_SESSION['token'], $_SESSION['pan'] )) {
            ?>
                        <a title="Batalkan Status Panitia" href="" id="notpan"><i class="fa fa-fw fa-times text-danger"></i></a>
            <?php
                    }

                } else {
                    if( $this->pmb_lib->panitia( $_SESSION['token'], $_SESSION['pan'] )) {
            ?>
                    <button id="setpan"><i class="fa fa-fw fa-check text-danger"></i> Set Status Panitia</button>
                    <i>(khusus administratif)</i>
            <?php
                    }
                }
            ?>
            </div>
        </div>
    <?php
        }
     ?>


        <div class="panel panel-info">
            <div class="panel-heading"><strong>Data Bank</strong></div>
            <div class="panel-body">
                <table class="table">
                    <tr>
                        <td class="text-right" width="120px">Nomor Rekening:</td>
                        <td><strong><?= $agen['nomor_rekening'] ?></strong></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="text-right">Atas Nama:</td>
                        <td><strong><?= $agen['nama_rekening'] ?></strong></td>
                        <td></td>
                    </tr>
            <?php
                $bankagen = $this->region_model->getBank( $agen['bank_rekening'] );
            ?>
                    <tr>
                        <td class="text-right">Nama Bank:</td>
                        <td><strong><?= $bankagen['nama'] ?></strong></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="text-right">Kode Bank:</td>
                        <td><strong><?= $bankagen['kode'] ?></strong></td>
                        <td></td>
                    </tr>
                </table>
            </div>
        </div>

    </div>
</div>

<div class="row">
    <div class="col-sm-7 col-md-7">

        <div class="panel panel-info">
            <div class="panel-heading"><strong>Channels</strong></div>
            <div class="panel-body">
                <table class="display" id="listchannel" data-order='[[ 1, "desc" ]]' data-page-length='25'>
                    <thead>
                        <th>Channel</th>
                        <th>Klik</th>
                    </thead>
            <?php
                foreach ( $this->pmbuser_model->listchannel( $agen['id'] ) as $ch ) {
                    $chn = $ch['channel'];
                    if( $ch['channel'] == "" ) $chn = "Direct Link / Undetected URL";
            ?>
                    <tr>
                        <td><?= $chn ?></td>
                        <td><?= $ch['jmlklik'] ?></td>
                    </tr>
            <?php
                }
             ?>

                </table>
            </div>
        </div>

    </div>

    <div class="col-sm-5 col-md-5">
        <div class="panel panel-info">
            <div class="panel-heading"><strong>Earning</strong></div>
            <div class="panel-body">
                <table class="table">
                    <tr>
                        <td class="text-right" width="100px">Registrasi:</td>
                        <td><strong></strong></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="text-right">Masuk:</td>
                        <td><strong></strong></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="text-right">Klaim:</td>
                        <td><strong></strong></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="text-right">Terbayar:</td>
                        <td><strong></strong></td>
                        <td></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
