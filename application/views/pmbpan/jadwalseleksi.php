<div class="row">
    <div class="col-sm-7 col-md-7">
        <div class="panel panel-info">
            <div class="panel-heading"><strong>Jadwal Seleksi</strong></div>
            <div class="panel-body">
                <table class="display" id="listjadwal" data-order='[[ 0, "desc" ]]' data-page-length='25' cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Tanggal</th>
                            <th>Waktu</th>
                            <th>Jenis Seleksi</th>
                            <th>Peserta</th>
                        </tr>
                    </thead>
                </table>

            </div>
        </div>
    </div>

    <div class="col-sm-5 col-md-5">

        <form role="form" action="/pmb/pan/jadwalbaru" method="post">

        <div class="panel panel-info">
            <div class="panel-heading"><strong>Jadwal Baru</strong></div>
            <div class="panel-body">

                <div class="form-group">
                    <label>Jenis Seleksi</label>
                    <select class="form-control" id="jenisseleksi" name="jenisseleksi" required="required">
                        <option value="">-- Pilih Jenis Seleksi --</option>
                        <option value="A">Test Akademik</option>
                        <option value="P">Psikotest</option>
                        <option value="W">Wawancara</option>
                    </select>
                </div>
                <div class="form-group">
                    <label id="labeltgtest">Tanggal dan Waktu</label>
                    <div class="input-group date form_datetime" data-date="" data-date-format="dd MM yyyy HH:ii"
                         data-link-field="ptgujian" data-link-format="yyyy-mm-dd HH:ii:ss">
                        <input class="form-control" size="16" type="text" value="<?= date("d F Y H:i") ?>" id="itgujian" required="required">
                        <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                    </div>
                    <input type="hidden" id="ptgujian" name="ptgujian" value="<?= date("Y-m-d H:i:s") ?>" />
                </div>

            <?php
                if( $kode == '1' ){
                    echo "<div class='alert alert-warning text-center' role='alert'>
                         <span class='fa fa-exclamation-triangle fa-2x' aria-hidden='true'></span><br />
                         Pastikan Anda memilih Calon Peserta Ujian dari Waiting List
                         </div>";
                }
             ?>

            </div>
            <div class="panel-heading" id="labelwaitingtest">Waiting List Peserta</div>
            <div class="panel-body">
                <div id="waitinglist">
                </div>

                <br />
                <div class="form-group">
                    <input type="submit" class="btn btn-lg btn-success btn-block"
                           id="submitjadwal" value="Jadwalkan Ujian">
                </div>
            </div>
        </div>

        </form>

    </div>
</div>
<script type="text/javascript">
    var namaujian = {
    <?php
        foreach ( $this->pmb_lib->ujianseleksi as $ujian ) {
            echo "'".$ujian['kode']."':'".$ujian['ujian']."',\n";
        }
    ?>
                };
</script>
