<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/"><span class="fa fa-fw fa-graduation-cap" aria-hidden="true"></span> SISKA 4.0</a>
    </div>
    <!-- Top Menu Items -->
    <ul class="nav navbar-right top-nav">
        <!-- <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> <b class="caret"></b></a>
            <ul class="dropdown-menu message-dropdown">
                <li class="message-preview">
                    <a href="#">
                        <div class="media">
                            <span class="pull-left">
                                <img class="media-object" src="http://placehold.it/50x50" alt="">
                            </span>
                            <div class="media-body">
                                <h5 class="media-heading"><strong>John Smith</strong>
                                </h5>
                                <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                <p>Lorem ipsum dolor sit amet, consectetur...</p>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="message-preview">
                    <a href="#">
                        <div class="media">
                            <span class="pull-left">
                                <img class="media-object" src="http://placehold.it/50x50" alt="">
                            </span>
                            <div class="media-body">
                                <h5 class="media-heading"><strong>John Smith</strong>
                                </h5>
                                <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                <p>Lorem ipsum dolor sit amet, consectetur...</p>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="message-preview">
                    <a href="#">
                        <div class="media">
                            <span class="pull-left">
                                <img class="media-object" src="http://placehold.it/50x50" alt="">
                            </span>
                            <div class="media-body">
                                <h5 class="media-heading"><strong>John Smith</strong>
                                </h5>
                                <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                <p>Lorem ipsum dolor sit amet, consectetur...</p>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="message-footer">
                    <a href="#">Read All New Messages</a>
                </li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-bell"><span class="badge">3</span></i>
                <b class="caret"></b>
            </a>
            <ul class="dropdown-menu alert-dropdown">
                <li>
                    <a href="#">Alert Name <span class="label label-default">Alert Badge</span></a>
                </li>
                <li>
                    <a href="#">Alert Name <span class="label label-primary">Alert Badge</span></a>
                </li>
                <li>
                    <a href="#">Alert Name <span class="label label-success">Alert Badge</span></a>
                </li>
                <li>
                    <a href="#">Alert Name <span class="label label-info">Alert Badge</span></a>
                </li>
                <li>
                    <a href="#">Alert Name <span class="label label-warning">Alert Badge</span></a>
                </li>
                <li>
                    <a href="#">Alert Name <span class="label label-danger">Alert Badge</span></a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="#">View All</a>
                </li>
            </ul>
        </li> -->
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?= $_SESSION['nama'] ?> <b class="caret"></b></a>
            <ul class="dropdown-menu">
                <li>
                    <a href="<?= "/" . $this->session->who . "/" . $this->session->uid ?>"><i class="fa fa-fw fa-user"></i> Profil</a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-fw fa-envelope"></i> Pesan</a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-fw fa-gear"></i> Setting</a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="<?= base_url() ?>signout"><i class="fa fa-fw fa-sign-out"></i> Sign Out</a>
                </li>
            </ul>
        </li>
    </ul>


    <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav side-nav">

            <li>
                <a href="/"><i class="fa fa-fw fa-home"></i> Home</a>
            </li>
            <li>
                <a href="/kurikulum"><i class="fa fa-fw fa-calendar-check-o"></i> Kalender Akademik</a>
            </li>
            <li>
                <a href="/kurikulum"><i class="fa fa-fw fa-book"></i> Kurikulum</a>
            </li>

            <?php

            /** load Menu Layanan untuk Mahasiswa **/
            if ($this->session->who == 'mhs') $this->load->view('menus/menumhs');

            /** load Menu Layanan untuk Dosen **/
            if ($this->session->who == 'dsn') $this->load->view('menus/menudosen');

            /** load Menu Layanan untuk WK1 dan Prodi **/
            if (
                $this->session->uid == '0929067001' ||
                $this->session->uid == '0804037001' ||
                $this->session->uid == '0911098801' ||
                $this->session->uid == '0928027201' ||
                $this->session->uid == '0920046604' ||
                $this->session->uid == '0916078202' ||
                $this->session->uid == '0909057801' ||
                $this->session->uid == '30108'
            ) {
                $this->load->view('menus/menuprodi');
            }

            /** load Menu Layanan untuk Staf Pengelola**/
            if ($this->session->who == 'stf' && $this->session->uid == '31165') $this->load->view('menus/menukeu');

            /** load Menu Layanan untuk ADMIN **/
            if ($_SESSION['admin'] == '1') $this->load->view('menus/menuadmin');

            ?>



            <!-- Menu untuk WK2, Keuangan -->



            <!-- Menu untuk WK2, Sarana -->



            <!-- Menu untuk WK3  -->



            <li>
                <a href="javascript:;" data-toggle="collapse" data-target="#siska">
                    <i class="fa fa-fw fa-graduation-cap"></i> SISKA
                    <i class="fa fa-fw fa-caret-down text"></i>
                </a>
                <ul id="siska" class="collapse">
                    <li>
                        <a href="#"><i class="fa fa-fw fa-address-book-o"></i> Panduan Penggunaan</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-fw fa-check-square-o"></i> Laporkan Masalah</a>
                    </li>
                    <li>
                        <a href="/mhs"><i class="fa fa-fw fa-users"></i> Tentang SISKA</a>
                    </li>
                </ul>
            </li>

        </ul>
    </div>
    <!-- /.navbar-collapse -->
</nav>

<div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <?= $judul1 ?>
                    <span id="pagetitle2"><small><?= $judul2 ?></small></span>
                </h1>
            </div>
        </div>
        <!-- /.row -->