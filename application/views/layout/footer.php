

            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

       <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>
                        &copy; STMIK KHARISMA Makassar, 2017<br>
                        Powered by <a href='https://m.do.co/c/6cc56cdf090e' target="_blank">Digital Ocean</a>
                    </p>
                </div>
            </div>
        </footer>


    </div>
    <!-- /#wrapper -->

<?php
    // load javascript only for spesific page
    if( $this->session->page == 'mhs' ){
        if( !isset( $_SESSION['View_mhs_prodi'] ) ) $_SESSION['View_mhs_prodi'] = $_SESSION['prodi'];
        if( !isset( $_SESSION['View_mhs_tahun'] ) ) $_SESSION['view_mhs_tahun'] = '2018';
?>
        <script type='text/javascript'>
            var view_mhs_prodi = <?= $_SESSION['View_mhs_prodi'] ?>;
            var view_mhs_tahun = <?= $_SESSION['view_mhs_tahun'] ?>;
        </script>
        <script src="<?=base_url()?>assets/js/siska_mhs.js"></script>
<?php
    }
?>

</body>

</html>
