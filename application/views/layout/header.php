<?php
    if ( empty($_SESSION['uid']) && empty($_SESSION['pin']) ) redirect('/login');

    $now = time();

    if ($now > $_SESSION['akhir']) {
        $this->siska_log->setLog('L','0','Logged out, session expired');
        session_destroy();
        redirect('/login/3');
        //  setcookie("lastpage", "/?".$_SERVER['QUERY_STRING'] );
        // echo "<meta http-equiv='refresh' content='0; url=".base_url()."login/3'>";
    } else {
        $_SESSION['akhir'] = time() + 1800;
    }
?>
<!DOCTYPE html>
<html lang="id">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="SISKA - Sistem Informasi Akademik STMIK KHARISMA Makassars">
    <meta name="author" content="Sofyan Thayf">

    <title>SISKA 4.0</title>

    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/css/sb-admin.css" rel="stylesheet">
    <link href="/assets/css/plugins/morris.css" rel="stylesheet">
    <link href="/assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/academicons.min.css" rel="stylesheet" type="text/css">

    <link href="/assets/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <link href="/assets/css/jquery.dataTables.css" rel="stylesheet" type="text/css">

    <link href="/assets/css/siskastyle.css" rel="stylesheet">
    <link href="/assets/css/tabs.css" rel="stylesheet">

    <link rel='shortcut icon' href='/assets/img/favicon.ico' type='image/x-icon'/ >

    <!-- jQuery -->
    <script src="<?=base_url()?>assets/js/jquery.js"></script>

    <script src="<?=base_url()?>assets/js/moment.min.js"></script>
    <script src="<?=base_url()?>assets/js/locales.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?=base_url()?>assets/js/Chart.min.js"></script>
    <script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>
    <script src="<?=base_url()?>assets/js/siska.js"></script>

    <script type="text/javascript" src="<?=base_url()?>assets/js/jquery.dataTables.js" charset="utf8"></script>

    <script type="text/javascript" src="/assets/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
    <script type="text/javascript" src="/assets/js/bootstrap-datetimepicker.id.js" charset="UTF-8"></script>

    </script>
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">
