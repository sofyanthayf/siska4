<?php
  setlocale(LC_MONETARY, 'id_ID');
  // var_dump($dmhs);
?>
<html>
<head>
  <style media="screen">
    @page {
        size: 21cm 29.7cm;
        margin-top: 2cm !important;
        margin-left: 2.5cm !important;
        margin-right: 2.5cm !important;
        margin-bottom: 2.5cm !important;
        font-family: Helvetica, Arial;
        font-size: 11.5px;
    }

    .qr {
        margin-bottom: 0px;
    }

    table {
        border-collapse: collapse;
        border: 1px solid #2a2a2a;
        width: 100%;
        margin-top: 15px;
    }
    td {
        border: 1px solid #2a2a2a;
        padding: 7px;
        vertical-align: top;
    }

    th {
        border: 1px solid #2a2a2a;
        padding: 3px;
        font-weight: bold;
        background: #efefef;
    }

    .field {
        text-align: right;
        vertical-align: top;
        width: 140px;
        padding-right: 20px;
    }

    .boxfoto {
        width: 150px;
        text-align: center;
        vertical-align: middle;
        overflow: hidden;
        padding: 3px;
    }

    .foto {
        width: 100%;
        display: block;
        margin: auto;
        /*margin-right: auto;*/
    }

    .noborder {
        border: none;
        padding: 2;
    }

    .quote {
        margin-left: .5cm;
        margin-right: .5cm;
        font-style: italic;
        font-weight: bold;
    }

  </style>

</head>
<body>
  <div class="container">
    <img src='assets/img/stmikkharismamks.gif' height='50'>

    <br>&nbsp; <br>
    <h1 style="text-align:center;">Surat Pernyataan</h1>

    <p>
      Saya, yang bertanda tangan di bawah ini:
    </p>

    <table class="noborder">
      <tr>
        <td class="field noborder">Nama Lengkap:</td>
        <td class="noborder"><strong><?= $dmhs['namamhs']?></strong></td>
      </tr>
      <tr>
        <td class="field noborder">N I M:</td>
        <td class="noborder"><strong><?= $dmhs['nimhs']?></strong></td>
      </tr>
      <tr>
        <td class="field noborder">Program Studi:</td>
        <td class="noborder">
          <strong><?= $dmhs['program_studi']['jenjang']['namaprogram']." ".$dmhs['program_studi']['nama']?></strong>
        </td>
      </tr>
    </table>
    &nbsp; <br>
    <p>
      Dengan ini mengajukan permohonan kiranya dapat diberi perpanjangan waktu untuk
      pengisian Rencana Studi untuk <strong>Semester
      <?= $this->siska->stringSemester( $dkrs['kodesmt'], '' )." (".$dkrs['kodesmt'].")"  ?></strong>, karena alasan:
    </p>
    <p class="quote">
      <?= $dkrs['alasan_extend'] ?>
    </p>
    <p>
      dan menyatakan bahwa:
    </p>
    <ol>
      <li>
        Alasan yang saya sebutkan di atas adalah yang sebenar-benarnya, dan saya bersedia menerima sanksi
        jika ternyata dikemudian hari terbukti bahwa saya memberikan alasan yang tidak benar.
      </li>
      <li>
        Keterlambatan pengisian Rencana Studi hanya untuk semester ini saja, dan saya bersedia menerima
        sanksi akademik sesuai peraturan dan ketentuan yang berlaku jika terlambat melakukan pengisian
        Rencana Studi pada semester-semester berikutnya.
      </li>
      <li>
        Permohonan dan pernyataan ini saya buat dengan penuh kesadaran, di atas kertas ber-materai cukup,
        dan siap menerima dan menjalankan segala konsekwensi yang timbul, atas diterima atau ditolaknya Surat
        Pernyataan ini
      </li>
    </ol>
    <p>
      Demikian Surat Pernyataan ini saya buat untuk dipergunakan sesuai keperluannya.
    </p>

    <table class="noborder">
      <tr>
        <td class="noborder">
          Mengetahui, <br>
          Penasehat Akademik
        </td>
        <td style="width:45%;" class="noborder">
          Makassar, <?= date('d-m-Y', strtotime($dkrs['tanggal_request']))?>
          <p><i>
            Materai <br>
            Rp10.000,-</i>
          </p>
        </td>
      </tr>
      <tr>
        <td></td>
        <td class="noborder">
          <strong><?= $dmhs['namamhs']?></strong>
        </td>
      </tr>
    </table>

  </div>

</body>
</html>
