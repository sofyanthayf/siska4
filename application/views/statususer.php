
<div class="panel panel-default">
    <div class=panel-heading>SISKA Status</div>
    <table class=table>
        <tr>
            <th scope=row>Profile Updated</th>
            <td><?= $status['last_update'] ?></td>
        </tr>
        <tr>
            <th scope=row>Last Login</th>
            <td><?= $status['last_login'] ?></td>
        </tr>
        <tr>
            <th scope=row>IP Address</th>
            <td><?= $status['ip_address'] ?></td>
        </tr>
    </table>
</div>
