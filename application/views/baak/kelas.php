<div class="col-sm-7 col-md-7">
  <div class="row">
    <div class="panel panel-info">
      <div class="panel-body">

        <h4>Daftar Kelas Mata Kuliah</h4>

        <div class="form-group">
          <label for="pilihsmtkrs">Semester:</label>
          <select class="form-control" id="pilihsmtkrs">
          </select>
        </div>

        <div id="pilihpr" class="form-group">
          <label for="pilihprodikrs">Program Studi:</label>
          <select class="form-control" id="pilihprodikrs">
          </select>
        </div>

      </div>
    </div>
  </div>
</div>

<div class="col-sm-7 col-md-7">
  <div class="row">
    <div class="panel panel-info">

      <div class="panel-body">
        <div class="text-right" id="exportlink">
        </div>

        <table class="display" id="klssmt" width="100%">
          <thead>
            <tr>
              <th>Kode MK</th>
              <th>Mata Kuliah</th>
              <th>sks</th>
              <th>semester</th>
              <th>peserta</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>

      </div>
    </div>
  </div>
</div>

<div class="col-sm-5 col-md-5">
  <div class="row">
    <div class="panel panel-info" id="kelasdetail" style="display:none">

      <div class="panel-heading">
        <h5>Mata Kuliah </h5>
      </div>

      <div class="panel-body">
        <table class='table table-bordered'>
          <tr>
            <td class="datafields" width='130px'>Mata Kuliah:</td>
            <td id="mknama"></td>
          </tr>
          <tr>
            <td class="datafields" width='130px'>Kode MK:</td>
            <td id="mkkode"></td>
          </tr>
          <tr>
            <td class="datafields" width='130px'>Kode Kelas:</td>
            <td id="mkkodekelas"></td>
          </tr>
          <tr>
            <td class="datafields" width='130px'>sks:</td>
            <td id="mksks"></td>
          </tr>
          <tr>
            <td class="datafields" width='130px'>dosen:</td>
            <td id="mkdosen"></td>
          </tr>
        </table>

        <table class="table table-bordered table-striped" id="mhskls" width="100%">
          <thead>
            <tr class="bg-primary">
              <td width='3px' class="text-center">No.</td>
              <td width='35px' class="text-center">NIM</td>
              <td class="text-center">Mahasiswa</td>
            </tr>
          </thead>
        </table><br>

      </div>

    </div>

  </div>
</div>

<script type="text/javascript">
  var tablekls;
  var smtkrs = "#pilihsmtkrs";
  var smtdefault;
  var prodi = <?= $this->session->prodi ?>;

  getSemesterKRS(smtkrs);

  setTimeout(function() {
    listProdiKRS(smtdefault);
    getListKelas(prodi, smtdefault);

    $("#pilihprodikrs option[value=" + prodi + "]").prop("selected", "selected");

  }, 1500);


  $("#klssmt tbody").on('click', 'tr', function() {
    var data = tablekls.row(this).data();
    console.log(data);

    $("#mknama").html(data.namamk);
    $("#mkkode").html(data.kodemk);
    $("#mkkodekelas").html(data.kodekls);
    $("#mksks").html(data.sks);

    var dosen = data.nama_dosen;
    if (data.gelar_depan !== null) dosen = data.gelar_depan + " " + dosen;
    if (data.gelar_belakang !== null) dosen = dosen + ", " + data.gelar_belakang;
    $("#mkdosen").html(dosen);

    $("#mhskls").DataTable({
      ajax: {
        url: "/apis/kls/listmhs/kode/" + data.kodekls,
        type: "GET"
      },
      sAjaxDataProp: "list_mhs",
      columns: [{
          render: function(data, type, full, meta) {
            return meta.row + 1;
          },
          className: "dt-center"
        },
        {
          "data": "nimhs",
          "className": "dt-center"
        },
        {
          "data": "namamhs"
        }
      ],
      filter: false,
      info: false,
      paging: false,
      destroy: true
    })

    $("#kelasdetail").show();
  });


  function getListKelas(prodi, smt) {
    tablekls = $("#klssmt").DataTable({
      ajax: {
        url: "/apis/kls/list/prd/" + prodi + "/smt/" + smt,
        type: "GET"
      },
      sAjaxDataProp: "list_kelas",
      columns: [{
          "data": "kodemk",
          "className": "dt-center"
        },
        {
          "data": "namamk"
        },
        {
          "data": "sks",
          "className": "dt-center"
        },
        {
          "data": "smt",
          "className": "dt-center"
        },
        {
          "data": "mahasiswa",
          "className": "dt-center"
        }
      ],
      filter: false,
      info: false,
      paging: false,
      destroy: true

    });

    $("#exportlink").html(
      `<a href="/baak/kelasxls/${prodi}/${smt}">
            <img src="/assets/img/excel16.png" alt="Export ke MS Excel"> Export ke MS Excel
         </a>`
    );

  }

  function listProdiKRS(smt) {
    $("#pilihprodikrs").empty();
    $.get("/krs_api/prodikrsaktif/smt/" + smt, function(data) {
      $.each(data.listprodi, function(i) {
        var kdpr = data.listprodi[i].kode;
        var nmpr = data.listprodi[i].jenjang.namaprogram + " " + data.listprodi[i].nama + " (" + data.listprodi[i].jenjang.kode + ")";
        var opt = new Option(nmpr, kdpr);
        $(opt).html(nmpr);
        $("#pilihprodikrs").append(opt);
      });
    });
  }

  function getSemesterKRS(listsemester) {
    $.get("/krs_api/semesterkrs", function(data) {
      smtdefault = data.semesterkrs[0].kodesmt;
      $.each(data.semesterkrs, function(i) {
        if (data.semesterkrs[i].kodesmt != "") {
          var smt = data.semesterkrs[i].kodesmt;
          var ssmt = "Semester " + data.semesterkrs[i].stringsmt + " (" + smt + ")";
          var opt = new Option(ssmt, smt);
          $(opt).html(ssmt);
          $(listsemester).append(opt);
        }
      });
      // getKRSMahasiswa(smtdefault)
    });
  }

  $(smtkrs).change(function() {
    smtdefault = $(this).val();
    listProdiKRS($(this).val());
    getListKelas('57201', smtdefault);

  });

  $("#pilihprodikrs").change(function() {
    prodi = $(this).val();
    getListKelas(prodi, smtdefault);
  });
</script>