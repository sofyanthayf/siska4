<?php
if (!isset($id)) {
?>
  <div class="row">
    <div class="col col-sm-10">
      <div class="panel panel-default" style="padding:10px;">

        <table class="table">

          <?php
          foreach ($angket as $a) {
          ?>
            <tr>
              <td class="text-center"><?= $a->id_angket ?></td>
              <td>
                <a href="/feedback/results/<?= $a->id_angket ?>">
                  <?= $a->title ?>
                </a>
              </td>
              <td class="text-center">
                <i><?= "<strong>" . $a->response . "</strong>" .
                      ($a->response > 1 ? ' responses' : ' response') ?></i>
              </td>
            </tr>

          <?php
          }
          ?>
        </table>
      </div>
    </div>
  </div>

<?php

} else {
  $this->load->view('/feedback/' . $id . '_01.php');
?>

  <div class="panel panel-info">
    <div class="panel-heading">
      <strong>Feedback Questions</strong>
    </div>
    <div class="panel-body">
      <table style="width:80%">
        <?php
        $question = $this->feedback_model->questions($id);
        $this->siska->showquest($question, '00000', TRUE);
        ?>
      </table>

    </div>
  </div>

<?php
}
?>