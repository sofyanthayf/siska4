<div class="panel panel-info">
    <div class="panel-heading">
        <strong>Likert Analytics</strong>
    </div>
    <div class="panel-body">

        <?php
        $listsmt = $this->feedback_model->response_semester($id);
        $listdosen = $this->feedback_model->response_dosen($id);
        ?>

        <h4><strong>Penilaian Mahasiswa Terhadap Proses Pembelajaran</strong></h4>
        <h4 id="range">Semester <?= $listsmt[0]->kodesmt ?></h4>

        <table style="width:85%;padding:20px;">
            <tr>
                <td style="width:70%; vertical-align: top;">
                    <div id="figure" style="margin-top:30px;margin-bottom:30px;"></div>
                </td>
                <td style="vertical-align:top; padding-left:20px; padding-top:30px;">
                    <strong>Semester: </strong>
                    <?php
                    foreach ($listsmt as $key => $value) {
                    ?>
                        <div style="padding:5px;">
                            <button type="button" class="btn btn-info btn-sm" onclick="callChartbySmt(<?= $value->kodesmt ?>)">
                                <?= $value->kodesmt ?>
                            </button>
                        </div>
                    <?php
                    }

                    ?>
                    <br />
                    <strong>Dosen Pengajar:</strong>
                    <select class="form-control" id="dosen" onclick="callChartbyNid(this)">
                        <option value="">- pilih dosen pengajar - </option>
                        <?php
                        foreach ($listdosen as $key => $value) {
                        ?>
                            <option value="<?= $value->nidn ?>">
                                <?=
                                ($value->gelar_depan != '' ? $value->gelar_depan . ' ' : '') .
                                    $value->nama .
                                    ($value->gelar_belakang != '' ? ', ' . $value->gelar_belakang : '')
                                ?>
                            </option>
                        <?php
                        }

                        ?>
                    </select>
                </td>
            </tr>
        </table>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.4.13/d3.min.js"></script>
        <script src="/assets/js/LikertChart.js"></script>

        <script>
            // var param = "id/<?= $id ?>/";
            var url = "<?= base_url() ?>apis/feedb/likertdata/id/<?= $id ?>";
            var kodesmt = '<?= $listsmt[0]->kodesmt ?>';
            getLikertChart(url, 'kodesmt', kodesmt);

            function callChartbySmt(smt) {
                kodesmt = smt;
                getLikertChart(url, 'kodesmt', smt);
                $("#range").html('Semester ' + smt)
            }

            function callChartbyNid() {
                getLikertChart(url, 'nidn', $("#dosen").find('option:selected').val());
                $("#range").html('Dosen: ' + $("#dosen").find('option:selected').text())
            }
        </script>


    </div>
</div>