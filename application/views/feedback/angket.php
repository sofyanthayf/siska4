<div class="row">
  <div class="col-sm-12 col-md-5">
    <div class="panel panel-default" style="padding:10px;">
      <?= nl2br($angket->deskripsi) ?><br />&nbsp;<br />
      <div class="text-danger font-weight-bold">* <i>required</i> </div>
    </div>
  </div>

  <div class="col-sm-12 col-md-5">
    <div class="panel panel-default">

      <table class=table>

        <?php
        // var_dump($angket_data);
        foreach ($angket_data as $dt) {
          $data = "";
          switch ($dt['field']) {
            case 'mahasiswa':
              $data = $dt['data']['nimhs'] . " " . $dt['data']['namamhs'];
              break;
            case 'dosen':
              $data = "<strong>" . $dt['data']['nama'] . "</strong>";
              break;
            case 'matakuliah':
              $data = "<strong>" . $dt['data']['namamk'] . "</strong><br/>kode MK:" . $dt['data']['kodemk'];
              break;

            default:
              $data = $dt['data'][$dt['key']];
              break;
          }

        ?>
          <tr>
            <td class="datafields" width='120px'><?= $dt['label'] ?>:</td>
            <td><?= $data ?></td>
          </tr>

        <?php } ?>
      </table>
    </div>
  </div>

  <div class="row">
    <div class="col-sm-12 col-md-10">
      <div class="panel panel-default" style="padding:10px;">

        <form action="/feedback/respond" method="post">
          <input type="hidden" name="id" value="<?= $angket->id_angket ?>">

          <?php
          foreach ($angket_data as $ad) {
          ?>
            <input type="hidden" name="<?= $ad['key'] ?>" value="<?= $ad['data'][$ad['key']] ?>">
          <?php
          }
          ?>

          <table class="table">

            <?php
            $this->siska->showquest($question, '00000');
            ?>
            <tr>
              <td class="text-danger font-weight-bold">* <i>required</i> </td>
              <td></td>
              <td class="text-right">
                <input type="submit" class="btn btn-primary" value=" Submit Response ">
              </td>
            </tr>
          </table>

        </form>
      </div>
    </div>



  </div>