<div class="col-sm-7 col-md-7">
  <div class="row">
    <div class="panel panel-info">
      <div class="panel-body">

        <h4>Rencana Studi Mahasiswa</h4>

        <div class="form-group">
          <label for="pilihsmtkrs">Semester:</label>
          <select class="form-control" id="pilihsmtkrs">
          </select>
        </div>

      </div>
    </div>
  </div>
</div>

<div class="col-sm-7 col-md-7" id="waitinglistkrs">
  <div class="row">
    <div class="panel panel-info">
      <div class="panel-body">

        <div class="row">
          <div class="col-sm-6 col-md-6">
            <input type="checkbox" id="chkautorefresh"> Auto Refresh
          </div>
          <div class="col-sm-6 col-md-6 text-right">
            <button title="refresh list" id="btrefresh">
              <img src="/assets/img/reload16.png"> Refresh
            </button><br>&nbsp;
          </div>
        </div>

        <table class="table table-sm table-bordered table-striped table-hover" id="mhskrs" width="100%">
          <thead>
            <tr class="bg-primary">
              <th width='40px' class="text-center">NIM</th>
              <th class="text-center">Mahasiswa</th>
              <th width='50px' class="text-center">Reg</th>
              <th width='5px' class="text-center">PA</th>
              <th width='5px' class="text-center">Inv</th>
              <th width='5px' class="text-center">Konf</th>
              <th width='5px' class="text-center">Byr</th>
              <th width='5px' class="text-center">Keu</th>
              <th width='5px' class="text-center">Prd</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>


      </div>
    </div>
  </div>
</div>

<div class="col-sm-5 col-md-5">
  <div class="row">
    <div class="panel panel-info" id="krsmhsdetail" style="display:none">

      <div class="panel-heading">
        <h5>KRS Mahasiswa </h5>
      </div>

      <div class="panel-body">
        <table class='table table-bordered'>
          <tr>
            <td class="datafields" width='130px'>Semester:</td>
            <td id="smtkrs"></td>
          </tr>
          <tr>
            <td class="datafields" width='130px'>NIM:</td>
            <td id="nimhs"></td>
          </tr>
          <tr>
            <td class="datafields" width='130px'>Nama Mahasiswa:</td>
            <td id="namamhs"></td>
          </tr>
          <tr>
            <td class="datafields" width='130px'>Nomor Telepon:</td>
            <td id="telepon"></td>
          </tr>
          <tr>
            <td class="datafields" width='130px'>Dosen PA:</td>
            <td id="dosenpa"></td>
          </tr>
        </table>

        <table class='table table-bordered'>
          <tr>
            <td class="datafields" width='130px'>Jumlah sks:</td>
            <td id="jmlsks"></td>
          </tr>
          <tr>
            <td class="datafields" width='130px'>Jumlah Mata Kuliah:</td>
            <td id="jmlmk"></td>
          </tr>
          <tr>
            <td class="datafields" width='130px'>Tanggal Registrasi:</td>
            <td id="tanggalreg"></td>
          </tr>
          <tr id="rowtgvalidasipa" style="display:none;">
            <td class="datafields" width='130px'>Validasi PA:</td>
            <td id="tanggalvalidasipa"></td>
          </tr>
          <tr id="rowtgcetakslip" style="display:none;">
            <td class="datafields" width='130px'>Tanggal Cetak Slip:</td>
            <td id="incetakslip"></td>
          </tr>
          <tr id="rowtgbayar" style="display:none;">
            <td class="datafields" width='130px'>Tanggal Bayar:</td>
            <td id="intglbayar"></td>
          </tr>
          <tr id="rowtgvalidasikeu" style="display:none;">
            <td class="datafields" width='130px'>Tgl. Validasi Keu.:</td>
            <td id="intglvalidasikeu"></td>
          </tr>
          <tr id="rowjumlahbayar" style="display:none;">
            <td class="datafields" width='130px'>Jumlah Bayar:</td>
            <td id="injumlahbayar"></td>
          </tr>
          <tr id="rowmetodebayar" style="display:none;">
            <td class="datafields" width='130px'>Metode Bayar:</td>
            <td id="inmetodebayar"></td>
          </tr>
        </table>

        <div class="block" id="frcetakslip" style="display:none;">
          <form class="form">

            <div class="form-control">
              <input type="checkbox" class="form-check-input" id="chkbeasiswa">
              <label class="form-check-label" for="chkbeasiswa">Beasiswa</label>
            </div>
            <br>

            <table class='table table-bordered' id="frmbiaya">
              <tr>
                <td class="datafields" width='130px'>Biaya sks:</td>
                <td id="tdbiayasks">
                  <input id="nbiayasks" min="0" data-number-to-fixed="2" data-number-stepfactor="100" class="form-control currency" placeholder="0">
                </td>
              </tr>
              <tr>
                <td class="datafields" width='130px'>Biaya Administrasi:</td>
                <td id="tdbiayaadm">
                  <input id="nbiayaadm" min="0" data-number-to-fixed="2" data-number-stepfactor="100" class="form-control currency" placeholder="0">
                </td>
              </tr>
              <tr>
                <td class="datafields" width='130px'>Kode Unik:</td>
                <td id="tdbiayaunik"></td>
              </tr>
              <tr>
                <td class="datafields" width='130px'>Total Biaya:</td>
                <td id="ntotalbiaya"></td>
              </tr>
            </table>

            <button id="tbcetakslip" class="btn btn-block btn-primary" style="display:block;" disabled> Buat Invoice </button>
            <br>
          </form>

        </div>

        <div class="block" id="frmbayar" style="display:none">
          <table class='table table-bordered' id="infobiaya">
            <tr>
              <td class="datafields" width='130px'>Nomor slip:</td>
              <td id="innomorslip"></td>
            </tr>
            <tr>
              <td class="datafields" width='130px'>Tgl cetak slip:</td>
              <td id="incetakslip"></td>
            </tr>
            <tr>
              <td class="datafields" width='130px'>Biaya sks:</td>
              <td id="inbiayasks" class="text-right"></td>
            </tr>
            <tr>
              <td class="datafields" width='130px'>Biaya Administrasi:</td>
              <td id="inbiayaadm" class="text-right"></td>
            </tr>
            <tr>
              <td class="datafields" width='130px'>Kode Unik:</td>
              <td id="inbiayaunik"></td>
            </tr>
            <tr>
              <td class="datafields" width='130px'>Total Biaya:</td>
              <td id="intotalbiaya" class="text-right"></td>
            </tr>
            <tr>
              <td class="datafields" width='130px'>Metode Pembayaran:</td>
              <td>
                <select id="metodebayar" class="form-control">
                  <option value="">-- pilih metode pembayaran --</option>
                  <option value="1">Tunai</option>
                  <option value="2">Bank</option>
                  <option value="3">Transfer</option>
                  <option value="4">Deposit</option>
                </select>
              </td>
            </tr>
            <tr>
              <td class="datafields" width='130px'>Tanggal Bayar:</td>
              <td>
                <div style="position:relatives;">
                  <input type="text" class="form-control" id="tglbayar" data-provide='datetimepicker'>
                </div>

              </td>
            </tr>
          </table>


          <button id="tbinvoice" class="btn btn-block btn-primary" style="display:block;"> Invoice </button>
          <button id="tbkonfirmbayar" class="btn btn-block btn-primary" style="display:block;" disabled> Konfirmasi Pembayaran </button>
          <br>

        </div>

        <br>
        <div id='imgtransfer' style='width:100%; display: none;'>
          <img id="pictransfer" src="" width="100%">
        </div>

      </div>

    </div>
  </div>
</div>


<script type="text/javascript" src="/assets/js/siska_krs.js"></script>
<script type="text/javascript">
  var roles = {
    'prd': false,
    "keu": true
  };
  var prodi = "";
  var nbsks = 0;
  var nbadm = 0;
  var nunik = 0;
  var ntb = 0;
  var nslp = "";
  var idkrs = "";
  var kodesmt = "";

  var dttable = "#mhskrs";
  var listsmt = "#pilihsmtkrs";
  var dttableres;

  getSemesterKRS(listsmt);

  setTimeout(function() {
    kodesmt = smtdefault;
  }, 1500);

  $(dttable + " tbody").on('click', 'tr', function() {
    var data = dttableres.row(this).data();
    showDetailMahasiswa(data);
    $("#krsmhsdetail").show();
  });

  $(listsmt).change(function() {
    getKRSMahasiswa($(this).val());
    kodesmt = $(this).val();
  });

  $("#nbiayasks").change(function() {
    if ($("#nbiayasks").val() != "") {
      nbsks = parseInt($("#nbiayasks").val());
      $("#tbcetakslip").prop("disabled", false);
    }
    totalBiaya();
  });

  $("#nbiayaadm").change(function() {
    if ($("#nbiayaadm").val() != "") {
      nbadm = parseInt($("#nbiayaadm").val());
    }
    totalBiaya();
  });

  $("#chkbeasiswa").click(function() {
    if ($("#chkbeasiswa").is(":checked")) {
      nslp = "BEASISWA";
      $("#frmbiaya").hide();
      $("#tbcetakslip").html("Konfirmasi");
      $("#tbcetakslip").prop("disabled", false);
    } else {
      nslp = "";
      $("#frmbiaya").show();
      $("#tbcetakslip").html("Cetak Slip");
      $("#tbcetakslip").prop("disabled", true);
    }
  })

  $("#tbcetakslip").click(function() {
    dkrs = {
      id: idkrs,
      bsks: nbsks,
      badm: nbadm,
      slp: nslp,
      smt: kodesmt
    };

    $.post("/krs_api/updateslip", dkrs);

    // window.open( "/pdf/slipkrs/" + idkrs , "_blank" );
    window.open("/keu/krs_invoice_prev/" + idkrs, "_blank");

    $("#krsmhsdetail").hide();
    getKRSMahasiswa(smtdefault);
  });

  $("#tbinvoice").click(function() {
    dkrs = {
      id: idkrs,
      bsks: nbsks,
      badm: nbadm,
      slp: nslp,
      smt: kodesmt
    };
    window.open("/keu/krs_invoice_prev/" + idkrs, "_blank");
  });

  $("#tglbayar").datetimepicker({
    defaultDate: "<?= date('Y-m-d H:i:s') ?>",
    autoclose: true
  });

  $("#tglbayar").change(function() {
    if ($(this).val() != "") $("#tbkonfirmbayar").prop("disabled", false);
  });

  $("#tbkonfirmbayar").click(function() {
    dkrs = {
      id: idkrs,
      mbyr: $("#metodebayar").val(),
      tbyr: $("#tglbayar").val()
    };
    $.post("/krs_api/validasikeu", dkrs);

    $("#krsmhsdetail").hide();
    getKRSMahasiswa(smtdefault);
  });


  function totalBiaya() {
    ntb = nbsks + nbadm + nunik;
    $("#ntotalbiaya").html("<h5>Rp " + ntb + "</h5>");
  }
</script>