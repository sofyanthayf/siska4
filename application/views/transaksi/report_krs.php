<div class="col-sm-7 col-md-7">
  <div class="row">
    <div class="panel panel-info">
      <div class="panel-body">

        <h4>Transaksi KRS Mahasiswa</h4>

        <div class="form-group">
          <label for="pilihsmtkrs">Semester:</label>
          <select class="form-control" id="pilihsmtkrs">
          </select>
        </div>

      </div>
    </div>
  </div>
</div>

<div class="col-sm-12 col-md-12">
  <div class="row">
    <div class="panel panel-info">

      <div class="panel-body">
        <div class="text-right" id="exportlink">
        </div>

        <table class="display" id="trxkrs" width="100%">
          <thead>
            <tr>
              <th>NIM</th>
              <th>Mahasiswa</th>
              <th>Bayar</th>
              <th>Konfirmasi</th>
              <th>Validasi</th>
              <th>No.Slip</th>
              <th>Biaya semester</th>
              <th>Biaya adm</th>
              <th>Jumlah</th>
            </tr>
          </thead>
          <tbody></tbody>
          <tfoot>
            <tr>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td>T O T A L</td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
          </tfoot>
        </table>

      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
  var tablekls;
  var smtkrs = "#pilihsmtkrs";
  var smtdefault;

  getSemesterKRS(smtkrs);

  setTimeout(function () {
    getListTrx(smtdefault);
  }, 1500);

  function getListTrx(smt) {
    tablekls = $("#trxkrs").DataTable({
      ajax: {
        url: "/apis/trx/list/smt/" + smt,
        type: "GET"
      },
      sAjaxDataProp: "list_trx",
      columns: [
        { "data": "nimhs", "className": "dt-center" },
        { "data": "namamhs" },
        { "data": "tanggal_bayar", "className": "dt-center" },
        { "data": "tanggal_konfirmasi", "className": "dt-center" },
        { "data": "tanggal_validasi_keu", "className": "dt-center" },
        { "data": "nomorslip", "className": "dt-center" },
        { "data": "biaya_sks", "className": "dt-right" },
        { "data": "biaya_adm", "className": "dt-right" },
        {
          render: function (data, type, row) {
            var total = (row.biaya_sks * 1) + (row.biaya_adm * 1);
            return total.toLocaleString('id-ID');
          },
          className: "dt-right",
          footer: function (data, type, row) {
            var api = this.api();
            var total = api
              .column(8, { search: "applied" })
              .data()
              .reduce(function (a, b) {
                return (parseFloat(a) || 0) + (parseFloat(b) || 0);
              }, 0);
            return total.toLocaleString('id-ID');
          }
        }
      ],
      footerCallback: function (row, data, start, end, display) {
        var api = this.api();
        api.columns('.dt-right', { search: "applied" }).every(function () {
          var column = this;
          var sum = column
            .data()
            .reduce(function (a, b) {
              return (parseFloat(a) || 0) + (parseFloat(b) || 0);
            }, 0);
          $(column.footer()).html(sum.toLocaleString('id-ID'));
        });
      },
      columnDefs: [
        {
          targets: 6,
          render: $.fn.dataTable.render.number('.', ',', 0, '')
        },
        {
          targets: 7,
          render: $.fn.dataTable.render.number('.', ',', 0, '')
        }
      ],
      order: [[2, "asc"]],
      filter: false,
      info: false,
      paging: false,
      destroy: true
    });
  

  // function getListTrx(smt) {
  //   tablekls = $("#trxkrs").DataTable({
  //     ajax: {
  //       url: "/apis/trx/list/smt/" + smt,
  //       type: "GET"
  //     },
  //     sAjaxDataProp: "list_trx",
  //     columns: [
  //       { "data": "nimhs", "className": "dt-center" },
  //     { "data": "namamhs" },
  //     { "data": "tanggal_bayar", "className": "dt-center" },
  //     { "data": "tanggal_konfirmasi", "className": "dt-center" },
  //     { "data": "tanggal_validasi_keu", "className": "dt-center" },
  //     { "data": "nomorslip", "className": "dt-center" },
  //     { "data": "biaya_sks", "className": "dt-right" },
  //     { "data": "biaya_adm", "className": "dt-right" },
  //     {
  //       render: function (data, type, row) {
  //                   var total = (row.biaya_sks * 1) + (row.biaya_adm * 1);
  //                   return total.toLocaleString('id-ID');
  //                 },
  //                 className: "dt-right",
  //                 footer: function (data, type, row) {
  //                   var api = this.api();
  //                   var total = api
  //                     .column(7, { search: "applied" }) 
  //                     .data()
  //                     .reduce(function (a, b) {
  //                       return (parseFloat(a) || 0) + (parseFloat(b) || 0);
  //                     }, 0);
  //                   return total.toLocaleString('id-ID');
  //                 }
  //     }
  //     ],
  //     footerCallback: function (row, data, start, end, display) {
  //       var api = this.api();
  //       api.columns('.dt-right', { search: "applied" }).every(function () {
  //         var column = this;
  //         var sum = column
  //           .data()
  //           .reduce(function (a, b) {
  //             return (parseFloat(a) || 0) + (parseFloat(b) || 0);
  //           }, 0);
  //         $(column.footer()).html(sum.toLocaleString('id-ID'));
  //       });
  //     },
  //     columnDefs: [
  //       {
  //         targets: 6,
  //         render: $.fn.dataTable.render.number('.', ',', 0, '')
  //       },
  //       {
  //         targets: 7,
  //         render: $.fn.dataTable.render.number('.', ',', 0, '')
  //       }
  //     ],
  //     order: [[2, "asc"]],
  //     filter: false,
  //     info: false,
  //     paging: false,
  //     destroy: true

  //   });

  $("#exportlink").html(
    `<a href="/keu/reportxls_krs/${smt}">
        <img src="/assets/img/excel16.png" alt="Export ke MS Excel"> Export ke MS Excel
        </a>`
  );

  }


  function getSemesterKRS(listsemester) {
    $.get("/krs_api/semesterkrs", function (data) {
      smtdefault = data.semesterkrs[0].kodesmt;
      $.each(data.semesterkrs, function (i) {
        if (data.semesterkrs[i].kodesmt != "") {
          var smt = data.semesterkrs[i].kodesmt;
          var ssmt = "Semester " + data.semesterkrs[i].stringsmt + " (" + smt + ")";
          var opt = new Option(ssmt, smt);
          $(opt).html(ssmt);
          $(listsemester).append(opt);
        }
      });
    });
  }


  $(smtkrs).change(function () {
    smtdefault = $(this).val();
    getListTrx(smtdefault);

  });


</script>