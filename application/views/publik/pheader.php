<!DOCTYPE html>
<html lang="id">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="SISKA - Sistem Informasi Akademik STMIK KHARISMA Makassars">
    <meta name="author" content="Sofyan Thayf">

    <title>SISKA 4.0</title>

    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/css/sb-admin.css" rel="stylesheet">
    <link href="/assets/css/plugins/morris.css" rel="stylesheet">
    <link href="/assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <link href="/assets/css/siskastyle.css" rel="stylesheet">
    <link href="/assets/css/tabs.css" rel="stylesheet">

    <link rel='shortcut icon' href='/assets/img/favicon.ico' type='image/x-icon'/ >

    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <script src="<?=base_url()?>assets/js/jquery.js"></script>

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/"><span class="fa fa-fw fa-graduation-cap" aria-hidden="true"></span> SISKA 4.0</a>
            </div>


            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">

                    <li>
                        <a href="http://www.karisma.ac.id"><i class="fa fa-fw fa-university"></i> STMIK KHARISMA Makassar</a>
                    </li>
                    <li>
                        <a href="/"><i class="fa fa-fw fa-home"></i> SISKA Home</a>
                    </li>


                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            <?= $judul1 ?> <small><?= $judul2 ?></small>
                        </h1>
                    </div>
                </div>
                <!-- /.row -->
