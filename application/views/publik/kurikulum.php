<div class="row">
  <div class="col-sm-12 col-md-6">
    <div class="panel panel-info">
      <div class="panel-body">

        <h4>Program Studi</h4>
        <table class='table'>
            <tr><td>
                <a href="/kurikulum/55201">Program Sarjana Informatika</a>
            </td></tr>
            <tr><td>
                <a href="/kurikulum/57201">Program Sarjana Sistem Informasi</a>
            </td></tr>
            <tr><td>
                <a href="/kurikulum/61209">Program Sarjana Bisnis Digital</a>
            </td></tr>
        </table>

      </div>
    </div>
  </div>
  <div class="col-sm-12 col-md-6">
  </div>
</div>

<div class="row"> 
  <div class="col-sm-12 col-md-6">
    <div class="panel panel-info">

      <div class="panel-heading">
        <h4>Dafar Mata Kuliah <?= "Prodi ".$prodi['nama'] ?></h4>
      </div>

      <div class="panel-body">
        <table class="table table-bordered table-hover" id="krssmt" width="100%">
          <thead>
            <tr class="bg-primary">
              <th class="text-center">Kode MK</th>
              <th class="text-center">Mata Kuliah</th>
              <th class="text-center">sks</th>
            </tr>
          </thead>
          <tbody>
            <?php
              $smt = 0;
              $i = 1;
              foreach ($matakuliah as $mk) {
                if($mk['smt'] != $smt){
                  $smt = $mk['smt'];

                  if($smt > 1) echo "</div>\n";
                  echo "<tr onclick='showHideSemester($smt)'>\n
                        <td style='background-color:#eec' colspan=5><strong>";
                  if( $mk['smt'] == 9 ){
                    echo "Pilihan Bebas";
                  } else {
                    echo "Semester $mk[smt]";
                  }
                  echo "</strong></td></tr>\n";

                  echo "<div id='semester$smt' style='display:block;'>\n";
                }


                ?>
                <tr id='matakuliah<?= $i ?>'>
                  <td class="text-center"><?= $mk['kodemk'] ?></td>
                  <td><?= $mk['namamk'] ?></td>
                  <td class="text-center"><?= $mk['sks'] ?></td>
                </tr>

          <?php
                $i++;  // id counter
              }
          ?>
            </div>
          </tbody>
        </table>
      </div>

    </div>

  </div>
  <div class="col-sm-12 col-md-6">

  </div>
</div>