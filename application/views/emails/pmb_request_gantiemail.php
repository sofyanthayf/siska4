<p>
    Anda, atau seseorang, telah mengajukan permintaan untuk melakukan perubahan alamat E-Mail
    pada layanan Penerimaan Mahasiswa Baru STMIK KHARISMA Makassar. Sebelum melakukan
    perubahan, <u>untuk alasan keamanan</u>, kami perlu memastikan bahwa benar Anda yang
    mengajukan permintaan perubahan
</p>
<p>
    Jika benar Anda mengajukan permintaan perubahan alamat E-Mail, silahkan klik link berikut ini
    untuk melakukan perubahan alamat E-Mail Anda:
</p>

<p><a href='<?= $link ?>'><button>Ganti Alamat E-Mail Saya</button></a></p>

<p>
    Jika link tidak berfungsi, <i>copy</i> dan <i>paste</i> link berikut ke browser Anda:<br /><?= $link ?>
</p>
<p>
    Jika Anda tidak merasa pernah mengajukan permintaan perubahan alamat E-Mail, maka abaikan E-mail ini,
    dan alamat E-Mail Anda yang telah terdaftar di sistem kami tidak mengalami perubahan.
</p>
<p>
    Terima kasih atas kerjasamanya.
</p>
