<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>SISKA</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />

        <style>
        .head
        {
            font-size: 18px;
            font-weight: 700;
            letter-spacing: -0.02em;
            line-height: 32px;
            color: #41637e;
            font-family: sans-serif;
            text-align: center
        }

        .logo {
            border: 0;
            -ms-interpolation-mode: bicubic;
            display: block;
            padding-bottom: 5px;
        }

        .visi {
            text-align: center;
        }

        p, td, th, li {
            Margin-top: 0;
            color: #565656;
            font-family: Helvetica;
            font-size: 14px;
            Margin-bottom: 25px
        }

        .noreply {
            text-align: center;
            font-size: 10px;
            /*font-style: italic;*/
            font-weight: bold;
            border: 1px solid;
            padding: 5px;
        }

        .col-right {
            text-align: right;
        }

        .col-center {
            text-align: center;
        }

        </style>
    </head>
    <body>
        <div>
            <img class="logo" src="<?=base_url()?>/assets/img/STMIKKHARISMA01w.gif" alt="STMIK KHARISMA Makassar">
            <h2 class="head">Sistem Informasi STMIK KHARISMA (SISKA)</h2>
            <hr /><br />
