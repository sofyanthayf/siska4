<p>
    Disampaikan kepada
<table>
    <tr>
        <td class='col-right'>Mahasiswa:</td>
        <td><strong><?= $mhs['namamhs'] ?></strong></td>
    </tr>
    <tr>
        <td class='col-right'>Nomor Induk:</td>
        <td><strong><?= $mhs['nimhs'] ?></strong></td>
    </tr>
    <tr>
        <td class='col-right'>Program Studi:</td>
        <td><strong><?= $mhs['program_studi']['nama'] ?></strong></td>
    </tr>
</table>
</p>

<p>
    bahwa rencana studi Anda untuk <strong>Semester <?= $this->siska->stringSemester($krs['kodesmt']) ?></strong> telah disetujui oleh
    Pembimbing Akademik Anda pada tanggal <?= date('d-m-Y pk.H:i:s T ', strtotime($krs['tanggal_validasi_pa'])) ?>,
    dengan perincian:
</p>
<p>
    Matakuliah:<br>
<table cellpadding="2">
    <thead>
        <tr>
            <th class="col-center">Kode MK</th>
            <th class="col-center">Mata Kuliah</th>
            <th class="col-center">SKS</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($belanja as $mk) { ?>
            <tr>
                <td> <?= $mk['kodemk'] ?> </td>
                <td> <?= $mk['namamk'] ?> </td>
                <td class="col-center"><?= $mk['sks'] ?></td>
            </tr>
        <?php } ?>
        <tr>
            <td></td>
            <td class="col-right">Jumlah:</td>
            <td class="col-center"><strong><?= $krs['jumlah_sks'] ?> sks</strong></td>
        </tr>

    </tbody>
</table>
<br>

<table>
    <tr>
        <td class="col-right">Biaya Semester:</td>
        <td> <strong>Rp</strong> </td>
        <td class="col-right"><strong><?= number_format($krs['biaya_sks'], 0, ",", ".") ?></strong></td>
    </tr>
    <tr>
        <td class="col-right">Biaya Administrasi:</td>
        <td> <strong>Rp</strong> </td>
        <td class="col-right"><strong><?= number_format($krs['biaya_adm'], 0, ",", ".") ?></strong></td>
    </tr>
    <tr>
        <td class="col-right">Kode Unik:</td>
        <td> <strong>Rp</strong> </td>
        <td class="col-right"><strong><?= number_format($krs['biaya_unik'], 0, ",", ".") ?></strong></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td class="col-right">Total Biaya Semester:</td>
        <td> <strong>Rp</strong> </td>
        <td class="col-right"><strong><?= number_format($krs['biaya_sks'] + $krs['biaya_adm'] + $krs['biaya_unik'], 0, ",", ".") ?></strong></td>
    </tr>
</table>
<br>
<table>
    <tr>
        <td class="col-right">Kode Transfer:</td>
        <td colspan=2><strong><?= $krs['kodetransfer'] ?></strong></td>
    </tr>
</table>
</p>

<p>
    Selanjutnya, silahkan melakukan pembayaran Biaya Semester Anda melalui rekening berikut:<br>

<table>
    <tr>
        <td class="col-right">nomor rekening: </td>
        <td><strong>1671671116</strong></td>
    </tr>
    <tr>
        <td class="col-right">bank: </td>
        <td><strong>BNI (kode: 009)</strong></td>
    </tr>
    <tr>
        <td class="col-right">atas nama: </td>
        <td><strong>YAYASAN PENDIDIKAN KHARISMA MAKASSAR</strong></td>
    </tr>
</table>
</p>

<p>
    <strong><u>PENTING:</u></strong> <br>
<ol>
    <li>Wajib melakukan transfer sejumlah <strong>Total Biaya Semester</strong> yang disebutkan di atas, termasuk dengan
        tiga angka kode unik (<u>jangan dibulatkan</u>) untuk memudahkan verifikasi. Jika jumlah yang ditransfer tidak sesuai maka
        mahasiswa tidak akan terdaftar sebagai peserta mata kuliah pada semester
        <?= $this->siska->stringSemester($krs['kodesmt']) ?> </li>
    <li>Cantumkan Kode Transfer (<strong><?= $krs['kodetransfer'] ?></strong>)
        pada Kolom Berita dalam proses pembayaran atau transfer </li>
    <li>Lakukan konfirmasi pembayaran dengan mengupload file hasil <i>scan</i> / <i>foto</i> /
        <i>screenshot</i> bukti transfer melalui layanan Rencana Studi pada SISKA
    </li>
    <li>Batas akhir pembayaran Biaya Semester dan konfirmasi pembayaran adalah sampai dengan
        <strong> tanggal <?= date('d-m-Y', strtotime($krs_baru['akhir'])) ?> </strong>
    </li>
</ol>
</p>

<p>
    <strong>Terima kasih.</strong>
    <br>
    <br>
</p>