<p>
    Terima kasih atas kesediaan Anda menjadi Agen dalam Program Reward Point pada Penerimaan Mahasiswa Baru (PMB)
    STMIK KHARISMA Makassar.
</p>
<p>
    Di bawah ini adalah informasi Link Referensi Anda, sebarkanlah seluas mungkin kepada keluarga, kerabat, sahabat,
    atau kenalan Anda yang mungkin sedang mencari perguruan tinggi untuk Studi Lanjut.
</p>

<p>
    Terima kasih atas kerjasamanya.
</p>

<hr />
<br />

<h3>STMIK KHARISMA Makassar, Pilihan Tepat</h3>
<p>
    Memilih sekolah untuk studi lanjut ke Pendidikan Tinggi merupakan keputusan penting yang diharapkan tidak menjadi
    penyesalan di kemudian hari. Karena itu memilih perguruan tinggi yang mengedepankan kualitas menjadi satu prinsip utama,
    STMIK KHARISMA Makassar, perguruan tinggi bidang informatika dan komputer di kota Makassar yang selalu mengutamakan kualitas,
    bisa menjadi pilihan Anda.
</p>
<p>
    Daftarkan diri Anda sebagai calon mahasiswa baru STMIK KHARISMA Makassar dan raih masa depanmu sebagai ahli di bidang
    Teknologi Informasi, melalui link berikut ini:
</p>
<p>
    <?= $link ?>
</p>
<p>
    Atau:
</p>
<p>
    <?= $bitly ?>
</p>
<p>
    Atau scan QR-Code berikut melalui smartphone Anda:
</p>
<p>
    <img src="<?= $qrimg ?>">
</p>
<br />
