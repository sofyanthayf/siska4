<p>
    Anda, atau seseorang, telah mengajukan permintaan untuk melakukan perubahan nomor HP
    pada layanan Penerimaan Mahasiswa Baru STMIK KHARISMA Makassar. Sebelum melakukan
    perubahan, <u>untuk alasan keamanan</u>, kami perlu memastikan bahwa benar Anda yang
    mengajukan permintaan perubahan
</p>
<p>
    Jika benar Anda mengajukan permintaan perubahan nomor HP, silahkan klik link berikut ini
    untuk perubahan nomor HP Anda:
</p>

<p><a href='<?= $link ?>'><button>Ganti Nomor Handphone Saya</button></a></p>

<p>
    Jika link tidak berfungsi, <i>copy</i> dan <i>paste</i> link berikut ke browser Anda:<br /><?= $link ?>
</p>
<p>
    Jika Anda tidak merasa pernah mengajukan permintaan perubahan nomor HP, maka abaikan E-mail ini,
    dan nomor HP Anda yang telah terdaftar di sistem kami tidak mengalami perubahan
</p>
<p>
    Terima kasih atas kerjasamanya.
</p>
