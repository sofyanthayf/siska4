<p>
    sdr(i). <?= $nama_lengkap ?>
</p>

<p>
    Terima kasih telah mendaftarkan diri sebagai calon Mahasiswa Baru STMIK KHARISMA Makassar
    pada Tahun Akademik <?= $tahun."-".($tahun+1) ?>.   Selamat datang di STMIK KHARISMA Makassar.
</p>
<p>
    Anda telah menyelesaikan pendaftaran awal dari rangkaian proses Penerimaan Mahasiswa Baru (PMB),
    selanjutnya, untuk melengkapi proses pendaftaran, diharapkan Anda dapat segera:<br />
    <ol>
        <li>
            Melakukan/melengkapi pengisian data profil, termasuk menentukan pilihan program studi Anda
        </li>
        <li>
            Meng-upload hasil scan dari sejumlah dokumen yang dibutuhkan (dokumen minimal adalah
            KTP/Keterangan Domisili, Pas Foto Berwarna, dan Buku Raport )
        </li>
    </ol>
</p>

<p>
    Untuk menjalankan proses tersebut, Anda dapat melakukan <a href="<?= base_url() ?>pmb">Sign-In</a>
    menggunakan alamat E-mail dan password yang telah Anda daftarkan melalui link berikut:
</p>

<p><a href='<?= base_url() ?>pmb'><button>Sign-In ke Layanan PMB</button></a></p>

<p>Jika link tidak berfungsi, <i>copy</i> dan <i>paste</i> link berikut ke browser Anda:<br /><?= base_url() ?>pmb
</p>

<p>
    Tahapan proses berikutnya akan kami sampaikan kepada Anda setelah Anda melengkapi dua point di atas
</p>

<p>
    Terima kasih atas kerjasamanya.
</p>

<p>
    PANITIA PMB STMIK KHARISMA Makassar.
</p>
