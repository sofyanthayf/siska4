<div class="col-sm-9 col-md-9">
  <div class="row">

    <div class="panel panel-default">

      <div class="panel-heading">
        <h4>Edit Data <?=$judul1?></h4>
      </div>
      <div class="panel-body">

        <div class="text-right">
          <a href="" id="clicksave" title="update">
            <img src='/assets/img/disc16.png'> Update
          </a>
          &nbsp;&nbsp;
          <a href="" id="clickcancel" title="batalkan edit">
            <img src='/assets/img/back-blue16.png'> Batal
          </a>
          &nbsp;&nbsp;
          <a href="" id="clickhapus" title="hapus data penelitian ini">
            <img src='/assets/img/delete16.png'> Hapus
          </a>
        </div><br>

        <table class=table>
          <tr>
            <td class="datafields" width='130px'>Judul <?=$judul1?>:</td>
            <td>
              <textarea class="editfield" id="judul" name="judul" rows="4" cols="80"><?= $penelitian['judul'] ?></textarea>
            </td>
          </tr>
          <tr>
            <td class="datafields" width='130px'>Skema:</td>
            <td><input class="editfield" type="text" id="judul"value="<?= $penelitian['skema'] ?>"></td>
          </tr>
          <tr>
            <td class="datafields" width='130px'>Abstrak:</td>
            <td>
              <textarea class="editfield" id="judul" name="judul" rows="8" cols="80"><?= $penelitian['abstrak'] ?></textarea>
            </td>
          </tr>
          <tr>
            <td class="datafields" width='130px'>Peneliti:</td>
            <td>

              <table width="50%">

                <?php
                  $n = 1;
                  foreach ($peneliti as $key => $value):
                  ?>

                  <tr>
                    <td><?=$n?></td>
                    <td style="padding:5px;"><?=$value['peneliti']?></td>
                    <td style="padding:5px;"><img src="/assets/img/delete16.png"></td>
                  </tr>

                <?php
                  $n++;
                  endforeach;
                  ?>

              </table>

            </td>
          </tr>
          <tr>
            <td class="datafields" width='130px'>Biaya:</td>
            <td><input class="editfield" type="text" id="judul"value="<?= $penelitian['biaya'] ?>"></td>
          </tr>
          <tr>
            <td class="datafields" width='130px'>Sumber Pembiayaan:</td>
            <td><input class="editfield" type="text" id="judul"value="<?= $penelitian['sumber_dana'] ?>"></td>
          </tr>
          <tr>
            <td class="datafields" width='130px'>Klas Pembiayaan:</td>
            <td><input class="editfield" type="text" id="judul"value="<?= $penelitian['klas_sumber_dana'] ?>"></td>
          </tr>
          <tr>
            <td class="datafields" width='130px'>Tanggal Mulai:</td>
            <td><input class="editfield" type="text" id="judul"value="<?= $penelitian['tanggal_mulai'] ?>"></td>
          </tr>
          <tr>
            <td class="datafields" width='130px'>Tanggal Selesai:</td>
            <td><input class="editfield" type="text" id="judul"value="<?= $penelitian['tanggal_selesai'] ?>"></td>
          </tr>
          <tr>
            <td class="datafields" width='130px'>Status:</td>
            <td><input class="editfield" type="text" id="judul"value="<?= $penelitian['status'] ?>"></td>
          </tr>
        </table>

        <div class="text-right">
          <a href="" id="clicksave" title="update">
            <img src='/assets/img/disc16.png'> Update
          </a>
          &nbsp;&nbsp;
          <a href="" id="clickcancel" title="batalkan edit">
            <img src='/assets/img/back-blue16.png'> Batal
          </a>
        </div><br>

      </div>
    </div>
  </div>
</div>
