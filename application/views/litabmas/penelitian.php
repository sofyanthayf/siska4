<div class="col-sm-9 col-md-9">
  <div class="row">

    <div class="panel panel-info">

      <div class="panel-heading">
        <h4>Daftar Aktivitas Penelitian</h4>
      </div>
      <div class="panel-body">

        <table class="table table-sm table-bordered table-striped table-hover" id="tblit" width="100%">
          <thead>
            <tr class="bg-primary">
              <th class="text-center">Tahun</th>
              <th class="text-center">Judul</th>
              <th class="text-center">Skema</th>
              <th class="text-center" width="100">Tim Peneliti</th>
              <th class="text-center">Biaya</th>
              <th class="text-center">Sumber Pembiayaan</th>
              <th class="text-center"></th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>

      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
var editable = <?= $this->session->uid==$nidn ? 'true' : 'false' ?>;
var tablekrs = $("#tblit").DataTable({
      ajax: { url  : "/litabmas_api/daftarpenelitian/nidn/<?=$nidn?>",
              type : "GET"
              },
      sAjaxDataProp: "penelitian",
      columns : [ {data : "tahun", className: "dt-center"},
                  {data : "judul"},
                  {data : "skema"},
                  {render: function( data, type, row ) {
                              var peneliti = '';
                              $.each( row.peneliti, function(i, item){
                                console.log(item);
                                peneliti += item.peneliti + "<br>";
                              });
                              return peneliti;
                            }},
                  {render : function( data, type, row ){
                              return row.biaya.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");
                            }, className: "dt-right"},
                  {data : "sumber_dana"},
                  { render : function ( data, type, row ) {
                                return  "<a href='/dsn/editpenelitian/" + row.id_penelitian +
                                        "' title='edit'><img src='/assets/img/edit16.png'></a>";
                              }, className : "dt-center" }
                ],
      order: [[ 0 , "desc" ]],
      columnDefs : [ { targets: 6, visible: editable } ]
    })
</script>
