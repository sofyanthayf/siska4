<div class="row">
   <!-- Foto -->

   <div class="col-sm-3 col-md-3">

   <?php   if( $this->dsn['foto'] ) {   ?>
           <div class="imgprofile">
               <img id='imgprofile' src="<?=$this->dsn['foto']?>">
           </div>

   <?php   }   ?>

   </div>

   <!-- Identitas -->
   <div class="col-sm-6 col-md-6">

       <div class="panel panel-default">
           <table class='table'>
               <tr>
                   <td class="datafields" width='150px'>Nama Lengkap:</td>
                   <td>
                     <?php  echo $this->dsn['gelar_depan']." "
                                 .$this->dsn['nama'].", "
                                 .$this->dsn['gelar_belakang'];
                      ?>
</td>
               </tr>
               <tr>
                   <td class="datafields">N I D N:</td>
                   <td><?= $this->dsn['nidn'] ?></td>
               </tr>
               <tr>
                   <td class="datafields">Jenis Kelamin:</td>
                   <td><?php if( !empty($this->dsn['sex']) ) echo $this->siska->jenis_kelamin[ $this->dsn['sex'] ] ?></td>
               </tr>
               <tr>
                   <td class="datafields">Pendidikan:</td>
                   <td><?=$this->siska->jenjang[ $this->dsn['pendidikan'] ]['kode'] ?></td>
               </tr>
               <tr>
                   <td class="datafields">Nomor Sertifikasi Dosen:</td>
                   <td>
                     <?php
                      if( !empty($this->dsn['no_sertifikasi_dosen']) ){
                        echo $this->dsn['no_sertifikasi_dosen']." (sejak: ".$this->dsn['tanggal_keluar_sertifikasi_dosen'].")";
                      }
                     ?>
                   </td>
               </tr>

<?php
   $prodimhs = $this->prodi_model->getProdi( $this->dsn['prodi'] );
?>

               <tr>
                   <td class="datafields">Program Studi:</td>
                   <td><?= $prodimhs['jenjang']['namaprogram'] . " " . $prodimhs['nama']  ?></td>
               </tr>
               <tr>
                   <td class="datafields">Jabatan Fungsional:</td>
                   <td>
                     <?=$this->siska->jabatanfungsional[ $this->dsn['jabatan_akademik'] ]?>
                   </td>
               </tr>
               <tr>
                   <td class="datafields">Jabatan Struktural:</td>
                   <td>

                   </td>
               </tr>
           </table>
       </div>
       <div class="social-media text-right">
           <?php
               if( !empty($this->dsn['scholar']) ){
                   echo '<a href="'.$this->dsn['scholar'].'" title="Google Scholar"
                            target="_blank"><i class="ai ai-fw ai-google-scholar ai-2x"></i></a>';
               }
               if( !empty($this->dsn['linkedin']) ){
                   echo '<a href="'.$this->dsn['linkedin'].'" title="LinkedIn"
                            target="_blank"><i class="fa fa-fw fa-linkedin-square fa-2x"></i></a>';
               }
               if( !empty($this->dsn['facebook']) ){
                   echo '<a href="'.$this->dsn['facebook'].'" title="Facebook"
                            target="_blank"><i class="fa fa-fw fa-facebook-square fa-2x"></i></a>';
               }
           ?>
           <br>
       </div>

   </div>

   <!-- QR Code -->
   <div class="col-sm-3 col-md-3">
       <div class="imgqr">
       <?php
           $path = 'assets/img/qr/mhs/';
           $params['data'] = base_url() . $this->dsn['nidn'];
           $params['level'] = 'M';
           $params['size'] = 6;
           // $params['savename'] = FCPATH . str_replace('/','\\', $path) . $this->dsn['nidn'] . '.png';
           $params['savename'] = FCPATH . $path . $this->dsn['nidn'] . '.png';
           $this->ciqrcode->generate($params);

           echo '<img id="imgqr" src="' . base_url() . $path.$this->dsn['nidn'] . '.png" />';
       ?>
       </div>
   </div>

</div>

<div class="row" style="min-height:300px;">
  <div class="col-sm-2 col-md-2">
      <!-- Nav tabs -->
      <ul class="nav nav-tabs tabs-left">

          <li class="active">
              <a href="#akadm" data-toggle="tab">
                  <i class="fa fa-fw fa-graduation-cap"></i>
                  Akademik
              </a>
          </li>


          <li>
              <a href="#profile" data-toggle="tab">
                  <i class="fa fa-fw fa-user"></i>
                  Profil
              </a>
          </li>

          <?php
            // Bukan untuk umum
            if ( !empty($_SESSION['uid']) && !empty($_SESSION['pin']) ) {
          ?>

          <li>
              <a href="#account" data-toggle="tab">
                  <i class="fa fa-fw fa-user"></i>
                  Account
              </a>
          </li>

          <?php
            }
          ?>

      </ul>
  </div>

  <div class="col-sm-7 col-md-7">
    <!-- Tab panes -->
    <div class="tab-content">

        <div class="tab-pane active" id="akadm">

        </div>

        <div class="tab-pane" id="profile">
          <?php $this->load->view('dsn/profil'); ?>
        </div>

        <?php
          // Bukan untuk umum
          if ( !empty($_SESSION['uid']) && !empty($_SESSION['pin']) ) {
        ?>

          <div class="tab-pane" id="account">
            <?php $this->load->view('auth/gantipassword'); ?>
          </div>

        <?php
          }
        ?>

    </div>

  </div>

<?php
  // SISKA status bukan untuk umum
  if ( !empty($_SESSION['uid']) && !empty($_SESSION['pin']) ) {
?>
      <div class="col-sm-3 col-md-3">
          <?php $this->load->view('statususer'); ?>
      </div>
<?php
  }
?>

</div>
