<div class="col-sm-7 col-md-7" id="waitinglistkrs" style="display:none;">
  <div class="row">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h4>Rencana Studi Mahasiswa</h4>
      </div>
      <div class="panel-body">

        <div class="form-group row">
          <label for="pilihsmtkrs" class="col-sm-3 col-form-label">Semester KRS:</label>
          <div class="col-sm-8">
            <select class="form-control" id="pilihsmtkrs">
            </select>
          </div>
        </div>

        <table class="table table-sm table-bordered table-striped table-hover" id="mhskrs" width="100%">
          <thead>
            <tr class="bg-primary">
              <th width='5px'class="text-center">No.</th>
              <th width='40px'class="text-center">NIM</th>
              <th class="text-center">Mahasiswa</th>
              <th width='50px'class="text-center">Reg</th>
              <th width='5px'class="text-center">PA</th>
              <th width='5px'class="text-center">Keu.</th>
              <th width='5px'class="text-center">Prodi</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>


      </div>
    </div>
  </div>
</div>

<div class="col-sm-5 col-md-5">
  <div class="row">
    <div class="panel panel-info">

      <div class="panel-heading">
        <h4>Daftar Mahasiswa Wali</h4>
      </div>

      <div class="panel-body">
        <table class="table table-bordered table-hover" id="mhswali" width="100%">
          <thead>
            <tr class="bg-primary">
              <th width='8px'class="text-center">No.</th>
              <th width='40px'class="text-center">NIM</th>
              <th class="text-center">Mahasiswa</th>
              <th width='30px'class="text-center">status</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>


      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
  var smtdefault;
  $.get("/krs_api/semesterkrswali/pa/<?= $this->session->uid; ?>", function(data){
    smtdefault = data.semesterkrs[0].kodesmt;
    $.each(data.semesterkrs, function(i){
      var smt = data.semesterkrs[i].kodesmt;
      var ssmt = "Semester " + data.semesterkrs[i].stringsmt + " (" + smt + ")";
      var opt = new Option( ssmt, smt);
      $(opt).html(ssmt);
      $("#pilihsmtkrs").append(opt);
    });
    getKRSMahasiswa(smtdefault);
  });

  $("#pilihsmtkrs").change(function(){
    getKRSMahasiswa( $(this).val() );
  });


  var listmhswali = $("#mhswali").DataTable({
                        "ajax":{ "url"  : "/dosen_api/listmhswali/nidn/<?= $this->session->uid ?>",
                                 "type" : "GET" },
                        "sAjaxDataProp" : "mhswali",
                        "columns"       : [ { render : function ( data, type, full, meta ) {
                                                          return  meta.row+1;
                                                        }, className : "dt-center" } ,
                                            { data :"nimhs", className : "dt-center" },
                                            { data :"namamhs" },
                                            { render : function ( data, type, full, meta ) {
                                                return status_mahasiswa[full.status];
                                            }, className : "dt-center" }
                                          ],
                        'iDisplayLength': 25
                      });

  $("#mhswali tbody").on('click', 'tr', function() {
    var data = listmhswali.row( this ).data();
    window.location = "/mhs/" + data.nimhs ;
  });


  function getKRSMahasiswa(smt){
      var krslist = $("#mhskrs").DataTable({
                      ajax :{ url  : "/krs_api/krsmahasiswawali/nidn/<?= $this->session->uid ?>/smt/"+smt,
                      type : "GET" },
                      sAjaxDataProp : "listkrsmhs",
                      columns       : [
                          { render : function ( data, type, full, meta ) {
                                        return  meta.row+1;
                                      }, className : "dt-center" } ,
                          { data : "nimhs", className : "dt-center" },
                          { data : "mahasiswa.namamhs" },
                          { data : "tanggal_registrasi_mhs", className : "dt-center" },
                          { render : function ( data, type, full, meta ) {
                                        if( full.tanggal_validasi_pa != null ){
                                          return  "<a title='"+full.tanggal_validasi_pa+"'><img src='/assets/img/check16.png'></a>";
                                        } else {
                                          if( full.status != 0 ){
                                            return  "<a href='/krs/" + full.nimhs + "/"+full.kodesmt+"/1'><img src='/assets/img/edit16.png'></a>";
                                          } else {
                                            return "";
                                          }
                                        }
                                      }, className : "dt-center" },
                          { render : function ( data, type, full, meta ) {
                                        if( full.tanggal_validasi_keu != null ){
                                          return  "<a title='"+full.tanggal_validasi_keu+"'><img src='/assets/img/check16.png'></a>";
                                        } else { return  ""; }
                                      }, className : "dt-center" },
                          { render : function ( data, type, full, meta ) {
                                        if( full.tanggal_validasi_prodi != null ){
                                          return  "<a title='"+full.tanggal_validasi_prodi+"'><img src='/assets/img/check16.png'></a>";
                                        } else { return  ""; }
                                      }, className : "dt-center" }
                          ],
                      searching : false,
                      bPaginate : false,
                      destroy : true
                      });

      krslist.on( 'xhr', function (){
        var lkrsmhs = krslist.ajax.json();
        // console.log(lkrsmhs.listkrsmhs.length);
        if( lkrsmhs.listkrsmhs.length > 0 ){
          $("#waitinglistkrs").show();
        } else {
          $("#waitinglistkrs").hide();
        }
      });

      $("#mhskrs tbody").on('click', 'tr', function() {
        var data = krslist.row( this ).data();
        window.location = "/krs/" + data.nimhs + "/"+data.kodesmt+"/1";
      });

  }

</script>
