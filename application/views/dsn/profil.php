<h3>Profil
</h3>
<div class="text-right">
  <a href="#profile" id="clickedit" title="edit">
    <img src='/assets/img/edit16.png'> Edit Profil
  </a>
  <a href="#profile" id="clicksave" title="update" style="display:none">
    <img src='/assets/img/disc16.png'> Update
  </a>
</div>
<br>
<div class="panel panel-default">
    <table class=table>
        <tr>
            <td class="datafields" width='120px'>Nomor KTP:</td>
            <td>
              <input class="editfield" type="text" value="<?= $this->dsn['no_ktp'] ?>" id="noktp" style="border:0px;" readonly>
            </td>
        </tr>
        <tr>
            <td class="datafields" width='120px'>Tempat Lahir:</td>
            <td>
              <input class="editfield" type="text" value="<?= $this->dsn['tplahir'] ?>" id="tplahir" style="border:0px;" readonly>
            </td>
        </tr>
        <tr>
            <td class="datafields">Tanggal Lahir:</td>
            <td>
              <?php
                if(!empty($this->dsn['tglahir'])) {
                  $tglh =  date( "d-m-Y", strtotime( $this->dsn['tglahir'] ) );
                } else {
                  $tglh = '';
                }
              ?>
              <input class="editfield" type="text" value="<?= $tglh ?>" id="tglahir" style="border:0px;" readonly>
            </td>
        </tr>
        <tr>
            <td class="datafields">Agama:</td>
            <td>
                <?php  if( !empty($this->dsn['agama']) ) echo $this->siska->agama[ $this->dsn['agama'] ]; ?>
            </td>
        </tr>
    </table>
</div>

<h4>Kontak</h4>

<div class="panel panel-default">
    <table class='table'>
        <tr>
            <td class="datafields" width='120px'>Alamat Rumah:</td>
            <td>
              <input class="editfield" type="text" value="<?= $this->dsn['alamat'] ?>" id="alamat" style="border:0px;" readonly>
            </td>
        </tr>
        <tr>
            <td class="datafields">Desa/Kelurahan:</td>
            <td><?php if(isset($this->dsn['region'])) echo $this->dsn['region']['nama_desa'] ?></td>
        </tr>
        <tr>
            <td class="datafields">Kecamatan:</td>
            <td><?php if(isset($this->dsn['region'])) echo $this->dsn['region']['nama_kecamatan'] ?></td>
        </tr>
        <tr>
            <td class="datafields">Kabupaten/Kota:</td>
            <td><?php if(isset($this->dsn['region'])) echo $this->dsn['region']['nama_kota'] ?></td>
        </tr>
        <tr>
            <td class="datafields">Kode Pos:</td>
            <td><?php if(isset($this->dsn['region'])) echo $this->dsn['region']['kodepos'] ?></td>
        </tr>

<?php   // bagian ini bukan untuk umum
        if ( !empty($_SESSION['uid']) && !empty($_SESSION['pin']) ) {
?>

        <tr>
            <td class="datafields">Telepon Rumah:</td>
            <td>
              <input class="editfield" type="text" value="<?= $this->dsn['telepon2'] ?>" id="telepon2" style="border:0px;" readonly>
            </td>
        </tr>
        <tr>
            <td class="datafields">Handphone:</td>
            <td>
              <input class="editfield" type="text" value="<?= $this->dsn['telepon'] ?>" id="telepon" style="border:0px;" readonly>
            </td>
        </tr>
        <tr>
            <td class="datafields">Email:</td>
            <td>
              <input class="editfield" type="email" value="<?= $this->dsn['email'] ?>" id="email" readonly>
            </td>
        </tr>

<?php
        }
?>

    </table>
</div>


<?php   // bagian ini bukan untuk umum
        if ( !empty($_SESSION['uid']) && !empty($_SESSION['pin']) ) {
?>

<div id="medsos">
  <h4>Media Sosial</h4>

  <div class="panel panel-default">
    <table class='table'>
      <tr>
        <td class="datafields" width='120px'>Google Scholar:</td>
        <td>
          <input class="editfield" type="text" value="<?= $this->dsn['scholar'] ?>" placeholder="https://scholar.google.co.id/citations?user=XXXXXXXXXXXX"
                 id="scholar" style="border:0px;" readonly>
        </td>
      </tr>
      <tr>
        <td class="datafields" width='120px'>LinkedIn:</td>
        <td>
          <input class="editfield" type="text" value="<?= $this->dsn['linkedin'] ?>" placeholder="http://linkedin.com/UserIDAnda"
                 id="linkedin" style="border:0px;" readonly>
        </td>
      </tr>
      <tr>
        <td class="datafields" width='120px'>Facebook:</td>
        <td>
          <input class="editfield" type="text" value="<?= $this->dsn['facebook'] ?>" placeholder="http://facebook.com/UserIDAnda"
          id="facebook" style="border:0px;" readonly>
        </td>
      </tr>
      <tr>
        <td class="datafields" width='120px'>Google PLus:</td>
        <td>
          <input class="editfield" type="text" value="<?= $this->dsn['gplus'] ?>" placeholder="http://plus.google.com/+UserIDAnda"
                 id="gplus" style="border:0px;" readonly>
        </td>
      </tr>
    </table>
  </div>
</div>

<?php
        }
?>

<script type="text/javascript">

$("#clickedit").click(function(){
  enableEdit();
});

$("#clicksave").click(function(){
  disableEdit();
});

function enableEdit(){
  $("#clickedit").hide();
  $("#clicksave").show();
  $("#medsos").show();

  $("input").each(function () {
    $(this).prop("readonly", false);
    $(this).css('color', 'blue');
  })

}

function disableEdit(){
  $("#clicksave").hide();
  $("#clickedit").show();
  $("#medsos").hide();

  $("input").each(function () {
    $(this).prop("readonly", true);
    $(this).css('color', 'black');
  })
}
</script>
