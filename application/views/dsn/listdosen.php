<div class="col-sm-3 col-md-3">
    <br />
    <h4>Filter:</h4>

    <strong>Program Studi:</strong>
    <div class="list-group" id="listprodi">
        <!-- daftar prodi dikerjakan oleh AJaX -->
    </div>

    <h4>Search:</h4>
    <div class="input-group">
        <span class="input-group-addon" id="searchtext"><i class="fa fa-fw fa-search"></i></span>
        <input type="text" class="form-control" placeholder="NIDN / Nama Dosen" aria-describedby="searchtext" id="searchtextkey">
    </div>
</div>
<br>

<div class="col-sm-6 col-md-6">
        <div class="panel panel-info">
            <div class="panel-heading">
                <strong>
                    <span id="disp_prodi"></span> - <span id="disp_tahun"></span>
                </strong>
            </div>
            <div class="panel-body">
                <table class="display" id="listdosen" data-order='[[ 0, "asc" ]]'
                                                          data-page-length='50'
                                                          cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>N I D N</th>
                            <th>Nama Dosen</th>
                            <th>status</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
</div>

<script type="text/javascript">
    // var statusmhs = {
    // <?php
    //     $astatus = $this->siska->status_mahasiswa;
    //     foreach ( $astatus as $key => $stat) {
    //         echo "'".$key."':'".$stat."',\n";
    //     }
    // ?>
    //             };
</script>
