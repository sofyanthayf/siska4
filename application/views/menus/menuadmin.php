<!-- Menu untuk Dosen -->
<li>
    <a href="javascript:;" data-toggle="collapse" data-target="#madmin">
        <i class="fa fa-fw fa-wrench"></i> Administrator
        <i class="fa fa-fw fa-caret-down text"></i>
    </a>
    <ul id="madmin" class="collapse">
         <li>
            <a href="/mhs"><i class="fa fa-fw fa-bar-chart"></i> Statistik</a>
        </li>
        <li>
            <a href="#"><i class="fa fa-fw fa-pencil-square-o"></i> Log Aktivitas</a>
        </li>
        <li>
            <a href="#"><i class="fa fa-fw fa-group"></i> Administrasi User</a>
        </li>
    </ul>
</li>
