<!-- Menu untuk WK2, Keuangan, Sarpras -->

<li>
    <a href="javascript:;" data-toggle="collapse" data-target="#transaksi">
        <i class="fa fa-fw fa-book"></i> Transaksi
        <i class="fa fa-fw fa-caret-down"></i>
    </a>
    <ul id="transaksi" class="collapse">
        <li>
            <a href="/keu/krs"><i class="fa fa-fw fa-list"></i> Pembayaran KRS</a>
        </li>
    </ul>
</li>
<li>
    <a href="javascript:;" data-toggle="collapse" data-target="#laporan">
        <i class="fa fa-fw fa-book"></i> Laporan
        <i class="fa fa-fw fa-caret-down"></i>
    </a>
    <ul id="laporan" class="collapse">
        <li>
            <a href="/keu/report_krs"><i class="fa fa-fw fa-list"></i> Pembayaran KRS</a>
        </li>
    </ul>
</li>
