<!-- Menu untuk Mahasiswa -->
<li>
    <a href="javascript:;" data-toggle="collapse" data-target="#mhs">
        <i class="fa fa-fw fa-graduation-cap"></i> Administrasi Akademik
        <i class="fa fa-fw fa-caret-down text"></i>
    </a>
    <ul id="mhs" class="collapse">
         <li>
            <a href="/krs/<?= $this->session->uid; ?>"><i class="fa fa-fw fa-users"></i> Rencana Studi</a>
        </li>
        <li>
            <a href="/mhs/kklp"><i class="fa fa-fw fa-check-square-o"></i> Usulan KKLP</a>
        </li>
        <li>
            <a href="/mhs/ta"><i class="fa fa-fw fa-book"></i> Usulan Tugas Akhir</a>
        </li>
    </ul>
</li>
<li>
    <a href="javascript:;" data-toggle="collapse" data-target="#surat">
        <i class="fa fa-fw fa-graduation-cap"></i> Permintaan Surat
        <i class="fa fa-fw fa-caret-down text"></i>
    </a>
    <ul id="surat" class="collapse">
         <li>
            <a href="/mhs/request/01"><i class="fa fa-fw fa-users"></i> Izin Penelitian</a>
        </li>
        <li>
            <a href="/mhs/request/02"><i class="fa fa-fw fa-check-square-o"></i> Keterangan Masih Kuliah</a>
        </li>
        <li>
            <a href="/mhs/request/03"><i class="fa fa-fw fa-check-square-o"></i> Keterangan Lulus</a>
        </li>
    </ul>
</li>
