<!-- Menu untuk Dosen -->
<li>
    <a href="javascript:;" data-toggle="collapse" data-target="#pendidikan">
        <i class="fa fa-fw fa-graduation-cap"></i> Pendidikan & Pengajaran
        <i class="fa fa-fw fa-caret-down text"></i>
    </a>
    <ul id="pendidikan" class="collapse">
        <li>
            <a href="/dsn/perwalian"><i class="fa fa-fw fa-address-book-o"></i> Perwalian Akademik</a>
        </li>
        <li>
            <a href="/dsn/perkuliahan">
                <i class="fa fa-fw fa-address-book-o"></i>
                Perkuliahan
            </a>
        </li>
        <li>
            <a href="/dsn/perkuliahan">
                <i class="fa fa-fw fa-address-book-o"></i>
                Pembimbingan Skripsi
            </a>
        </li>
    </ul>
</li>

<li>
    <a href="javascript:;" data-toggle="collapse" data-target="#penelitian">
        <i class="fa fa-fw fa-briefcase"></i> Penelitian
        <i class="fa fa-fw fa-caret-down text"></i>
    </a>
    <ul id="penelitian" class="collapse">
        <li>
            <a href="/dsn/penelitian">
                <i class="fa fa-fw fa-check-square-o"></i>
                Penelitian
            </a>
        </li>
        <li>
            <a href="/dsn/publikasi">
                <i class="fa fa-fw fa-check-square-o"></i>
                Publikasi Ilmiah
            </a>
        </li>
    </ul>
</li>

<li>
    <a href="javascript:;" data-toggle="collapse" data-target="#pengabdian">
        <i class="fa fa-fw fa-briefcase"></i> Pengabdian Masyarakat
        <i class="fa fa-fw fa-caret-down text"></i>
    </a>
    <ul id="pengabdian" class="collapse">
        <li>
            <a href="/dsn/pkm">
                <i class="fa fa-fw fa-check-square-o"></i>
                Pengabdian Masyarakat
            </a>
        </li>
    </ul>
</li>
