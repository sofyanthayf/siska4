<!-- Menu untuk WK1, Prodi, BAAK -->

<li>
    <a href="javascript:;" data-toggle="collapse" data-target="#akademik">
        <i class="fa fa-fw fa-book"></i> Administrasi Akademik
        <i class="fa fa-fw fa-caret-down"></i>
    </a>
    <ul id="akademik" class="collapse">
        <li>
           <a href="/mhs"><i class="fa fa-fw fa-users"></i> Mahasiswa</a>
        </li>
        <li>
            <a href="/krs/prodi"><i class="fa fa-fw fa-list"></i> Rencana Studi</a>
        </li>
    </ul>
</li>
<li>
    <a href="javascript:;" data-toggle="collapse" data-target="#kuliah">
        <i class="fa fa-fw fa-book"></i> Administrasi Perkuliahan
        <i class="fa fa-fw fa-caret-down"></i>
    </a>
    <ul id="kuliah" class="collapse">
        <li>
            <a href="/baak/kelas"><i class="fa fa-fw fa-list"></i> Matakuliah Semester</a>
        </li>
        <li>
            <a href="#"><i class="fa fa-fw fa-check-square-o"></i> Monitoring Perkuliahan</a>
        </li>
    </ul>
</li>
