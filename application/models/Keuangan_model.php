<?php

class Keuangan_model extends CI_Model {

  public function __construct() {
      parent::__construct();
  }

  
  function transaksi_krs($kodesmt)
  {
    $this->db->select( 't.nimhs, m.namamhs, 
                        t.tanggal_bayar, t.tanggal_konfirmasi, t.tanggal_validasi_keu,  
                        t.nomorslip, (t.biaya_sks + t.biaya_unik) biaya_sks, t.biaya_adm' );
    $this->db->join( 'siska_mahasiswa m', 'nimhs', 'LEFT');
    $this->db->where('kodesmt', $kodesmt);
    $this->db->where('tanggal_bayar IS NOT NULL');
    $this->db->order_by('tanggal_bayar', 'ASC');

    return $this->db->get('siska_krs t')->result();
  }


}