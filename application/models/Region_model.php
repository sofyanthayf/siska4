<?php
class Region_model extends CI_Model {

    public $region;

    public function __construct() {
        parent::__construct();

        $this->region = ['id_desa'=>'', 'nama_desa'=>'', 'id_kecamatan'=>'', 'nama_kecamatan'=>'',
                         'id_kota'=>'', 'nama_kota'=>'', 'id_propinsi'=>'', 'nama_propinsi'=>'', 'kodepos'=>'' ];
    }

    public function getListPropinsi(){
        $sql = "SELECT * FROM regional_propinsi ORDER BY nama";
        $query = $this->db->query($sql);

        if ( !empty( $query->result_array() ) ) {
            return $query->result_array();
        }
        return false;
    }

    public function getListKotaKabupaten( $idpropinsi ){
        $sql = "SELECT * FROM regional_kabkota
                WHERE province_id='$idpropinsi'
                ORDER BY name";
        $query = $this->db->query($sql);

        if ( !empty( $query->result_array() ) ) {
            return $query->result_array();
        }
        return false;
    }

    public function getListKecamatan( $idkota ){
        $sql = "SELECT * FROM regional_kecamatan
                WHERE regency_id='$idkota'
                ORDER BY name";
        $query = $this->db->query($sql);

        if ( !empty( $query->result_array() ) ) {
            return $query->result_array();
        }
        return false;
    }

    public function getListDesaKelurahan( $idkecamatan ){
        $sql = "SELECT * FROM regional_desa
                WHERE district_id='$idkecamatan'
                ORDER BY name";
        $query = $this->db->query($sql);

        if ( !empty( $query->result_array() ) ) {
            return $query->result_array();
        }
        return false;
    }

    public function getListSekolahKota( $idkota ){
        $sql = "SELECT * FROM sekolah_sma
                WHERE id_kabkota='$idkota'
                ORDER BY statusns, sekolah";
        $query = $this->db->query($sql);

        if ( !empty( $query->result_array() ) ) {
            return $query->result_array();
        }
        return false;
    }

    public function getListJurusanSMA(){
        $sql = "SELECT * FROM `sekolah_jurusan`
                WHERE LEFT(kode,3)<='000'";
        $query = $this->db->query($sql);

        if ( !empty( $query->result_array() ) ) {
            return $query->result_array();
        }
        return false;
    }

    public function getListJurusanSMK( $tahun ){
        if($tahun < 2008){
            $th = '08';
        }
        else {
            $th = substr($tahun,-2);
        }

        $sql = "SELECT * FROM `sekolah_jurusan`
                WHERE LEFT(kode,2)<='$th'
                AND LEFT(kode,2)>=(SELECT MAX(LEFT(kode,2))
                                    FROM sekolah_jurusan
                                    WHERE LEFT(kode,2)<='$th' )";
        $query = $this->db->query($sql);

        if ( !empty( $query->result_array() ) ) {
            return $query->result_array();
        }
        return false;
    }

    public function getJurusanSMA( $kode ){
        $sql = "SELECT * FROM `sekolah_jurusan`
                WHERE kode='$kode'";
        $query = $this->db->query($sql);

        if ( !empty( $query->row_array() ) ) {
            return $query->row_array();
        }
        return false;
    }

    public function getRegion( $id ){
        if( $this->isDesa( $id )) {
            $sql = "SELECT  d.id id_desa, d.name nama_desa,
                            c.id id_kecamatan, c.name nama_kecamatan,
                            k.id id_kota, k.name nama_kota,
                            p.id id_propinsi, p.nama nama_propinsi,
                            m.kodepos
                    FROM regional_desa d
                    LEFT JOIN regional_kecamatan c ON (d.district_id=c.id)
                    LEFT JOIN regional_kabkota k ON (c.regency_id=k.id)
                    LEFT JOIN regional_propinsi p ON (k.province_id=p.id)
                    LEFT JOIN regional_kodepos m ON (m.id_desa=d.id)
                    WHERE d.id='$id'";

        }
        elseif ($this->isKecamatan( $id )) {
            $sql = "SELECT  c.id id_kecamatan, c.name nama_kecamatan,
                            k.id id_kota, k.name nama_kota,
                            p.id id_propinsi, p.nama nama_propinsi
                    FROM regional_kecamatan c
                    LEFT JOIN regional_kabkota k ON (c.regency_id=k.id)
                    LEFT JOIN regional_propinsi p ON (k.province_id=p.id)
                    WHERE k.id='$id'";
        }
        elseif ($this->isKota( $id )) {
            $sql = "SELECT  k.id id_kota, k.name nama_kota,
                            p.id id_propinsi, p.nama nama_propinsi
                    FROM regional_kabkota k
                    LEFT JOIN regional_propinsi p ON (k.province_id=p.id)
                    WHERE k.id='$id'";
        }

        $query = $this->db->query($sql);
        if ( !empty( $query->row_array() ) ) {
            $this->region = $query->row_array();
        }

        return $this->region;
    }

    public function getRegionSekolah( $id ){
        $sql = "SELECT  s.sekolah, s.alamat, s.statusns, s.statusak, s.npsn, s.id_kecamatan, s.id_kabkota,
                        c.id id_kecamatan, c.name nama_kecamatan,
                        k.id id_kota, k.name nama_kota,
                        p.id id_propinsi, p.nama nama_propinsi
                FROM sekolah_sma s
                LEFT JOIN regional_kecamatan c ON (s.id_kecamatan=c.id)
                LEFT JOIN regional_kabkota k ON (s.id_kabkota=k.id)
                LEFT JOIN regional_propinsi p ON (k.province_id=p.id)
                WHERE s.id='$id'";

        $query = $this->db->query($sql);
        if ( !empty( $query->row_array() ) ) {
            $this->region = $query->row_array();
        }

        return $this->region;
    }

    public function isDesa( $id ){
        $query = $this->db->get_where('regional_desa', array( 'id' => $id ) );
        if( !empty( $query->row_array() ) ){
            return true;
        } else {
            return FALSE;
        }
    }

    public function isKecamatan( $id ){
        $query = $this->db->get_where('regional_kecamatan', array( 'id' => $id ) );
        if( !empty( $query->row_array() ) ){
            return true;
        } else {
            return FALSE;
        }
    }

    public function isKota( $id ){
        $query = $this->db->get_where('regional_kabkota', array( 'id' => $id ) );
        if( !empty( $query->row_array() ) ){
            return true;
        } else {
            return FALSE;
        }
    }

    public function getListBank(){
        $sql = "SELECT * FROM regional_bank ORDER BY nama";
        $query = $this->db->query($sql);

        if ( !empty( $query->result_array() ) ) {
            return $query->result_array();
        }
        return false;
    }

    public function getBank( $kode ){
        $sql = "SELECT * FROM regional_bank WHERE kode='$kode'";
        $query = $this->db->query($sql);

        if ( !empty( $query->row_array() ) ) {
            return $query->row_array();
        }
        return false;
    }

}
