<?php

class Kuliah_model extends CI_Model {

  public function __construct() {
      parent::__construct();
  }

  public function registerMKMahasiswaSemester( $dkrs ) {
    $mhs = $this->mahasiswa_model->setNim( $dkrs[0]['nimhs'] );
    foreach ( $dkrs as $mkkrs ) {
      $kuliah = [
                  'kodesmt' => $mkkrs['kodesmt'],
                  'prodi' => $mhs['prodi'],
                  'nimhs' => $mkkrs['nimhs'],
                  'kodemk' => $mkkrs['kodemk'],
                  'kodekelas' => $this->kodeKelas( $mhs['prodi'], $mkkrs['kodesmt'], $mkkrs['kodemk'] ),
                  'status' => 'A',
                  'id_krs' => $mkkrs['id_krs'],
                ];
      $this->db->insert( 'siska_kuliah', $kuliah );
    }
  }

  public function kodeKelas( $prodi, $kodesmt, $kodemk )
  {
    // SELECT DISTINCT kodekelas FROM `siska_kuliah` WHERE prodi='57201' AND kodesmt='20181' AND kodemk='13500029'
    $this->db->distinct();
    $this->db->select('kodekelas');
    $this->db->where('prodi', $prodi);
    $this->db->where('kodesmt', $kodesmt);
    $this->db->where('kodemk', $kodemk);
    $query = $this->db->get('siska_kuliah');

    if( $query->num_rows() != 0 ){
      return $query->row_array()['kodekelas'];
    } else {
      return $this->kodeKelasBaru( $prodi, $kodesmt, $kodemk );
    }

  }

  public function kodeKelasBaru( $prodi, $kodesmt, $kodemk )
  {
    $kodekelas = $kodesmt . $this->siska->tambahNol( 4, $this->jumlahKelas( $kodesmt ) + 1 );
    $kelas = [
                'kode_semester' => $kodesmt,
                'kode_program_studi' => $prodi,
                'kode_matakuliah' => $kodemk,
                'kode_kelas' => $kodekelas,
                'paralel' => '01',
                'mode_split' => 0,
                'status' => 1
              ];
    $this->db->insert( 'siska_kelas', $kelas );
    return $kodekelas;
  }

  public function jumlahKelas( $kodesmt )
  {
    // SELECT DISTINCT kodekelas FROM `siska_kuliah` WHERE kodesmt='20181'
    $this->db->distinct();
    $this->db->select('kodekelas');
    $this->db->where('kodesmt', $kodesmt);
    $num = $this->db->count_all_results('siska_kuliah');
    return $num;
  }



}
