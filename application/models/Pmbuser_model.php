<?php

class Pmbuser_model extends CI_Model {

    public $data;
    private $tabel;

    public function __construct() {
        parent::__construct();
        $this->tabel = [ 'agen'=>'siskapmb_agen',
                         'maba'=>'siskapmb_maba' ];
    }

    /*
     * check if apakah email user sudah terdaftar atau belum
     * @param $email alamat email yang akan dicek
     * @param $akun "agen" atau "maba" menentukan tabel untuk query user
     */
    public function email_registered( $email ) {
        $sql = "(SELECT email FROM `siskapmb_agen`
                  WHERE email='$email')
                 UNION
                (SELECT email FROM `siskapmb_maba`
                  WHERE email='$email')";
        $query = $this->db->query($sql);
        if ( !empty( $query->row_array() ) ) {
            return TRUE;
        }
        return FALSE;
    }

    /*
     * check if apakah nomor HP user sudah terdaftar atau belum
     * @param $nomorhp alamat nomorhp yang akan dicek
     * @param $akun "agen" atau "maba" menentukan tabel untuk query user
     */
    public function nomorhp_registered( $nomorhp ) {

        if( substr($nomorhp,0,1) == '0' ) {
            $nomorhp2 = '+62'.substr($nomorhp, 1, strlen($nomorhp)-1 );
        }
        elseif( substr($nomorhp,0,3) == '+62' ) {
            $nomorhp2 = str_replace( '+62', '0', $nomorhp );
        }

        $sql = "(SELECT telepon FROM `siskapmb_agen`
                  WHERE telepon='$nomorhp' OR telepon='$nomorhp2')
                 UNION
                (SELECT telepon FROM `siskapmb_maba`
                  WHERE telepon='$nomorhp' OR telepon='$nomorhp2')";
        $query = $this->db->query($sql);
        if ( !empty( $query->row_array() ) ) {
            return TRUE;
        }
        return FALSE;
    }

    public function agen( $uid ){
        $sql = "SELECT * FROM `siskapmb_agen` WHERE email='$uid' OR id='$uid'";
        $query = $this->db->query($sql);
        if ( !empty( $query->row_array() ) ) {
            $this->data = $query->row_array();
            $this->data['klas'] = 'agen';
            return $this->data;
        }
        return FALSE;
    }

    public function maba( $uid ){
        $sql = "SELECT * FROM `siskapmb_maba` WHERE email='$uid' OR id='$uid'";
        $query = $this->db->query($sql);
        if ( !empty( $query->row_array() ) ) {
          $this->data = $query->row_array();
          $this->data['klas'] = 'maba';
          return $this->data;
        }
        return FALSE;

    }

    public function get_token( $uid, $akun ) {
        $sql = "SELECT token FROM ".$this->tabel[$akun]." WHERE id='$uid'";
        $query = $this->db->query($sql);
        if ( !empty( $query->row_array() ) ) {
            return $query->row_array()['token'];
        }
        return FALSE;
    }

    public function email_valid( $uid, $akun ) {
        $sql = "SELECT email_valid FROM ".$this->tabel[$akun]." WHERE id='$uid' AND email_valid NOT LIKE '0000-00-00%'";
        $query = $this->db->query($sql);
        if ( !empty( $query->row_array() ) ) {
            return $query->row_array()['email_valid'];
        }
        return FALSE;
    }

    public function telepon_valid( $uid, $akun ) {
        $sql = "SELECT telepon_valid FROM ".$this->tabel[$akun]." WHERE id='$uid' AND telepon_valid NOT LIKE '0000-00-00%'";
        $query = $this->db->query($sql);
        if ( !empty( $query->row_array() ) ) {
            return $query->row_array()['telepon_valid'];
        }
        return FALSE;
    }

    public function verifikasi_mail_token($email, $token, $akun)
    {
        $query = $this->db->get_where( $this->tabel[$akun], array('email' => $email, 'email_token' => $token ) );
        if (!empty($query->row_array())) {

            // renew email_token, email token hanya untuk satu kali verifikasi
            $_SESSION['email'] = $email;
            $_SESSION['email_token'] = random_string('alnum', 16);

            $data = array(  'email_valid' => date('Y-m-d H:i:s'),
                            'email_token' => $_SESSION['email_token'],
                            'status_aktif' => 'A' );

            $this->db->where("`email`='".$email."' AND `email_token`='".$token."'", NULL);
            return $this->db->update($this->tabel[$akun], $data);
        }
        return false;
    }

    public function get_email_token( $email ){
        $sql = "(SELECT email_token FROM `siskapmb_agen`
                  WHERE email='$email')
                 UNION
                (SELECT email_token FROM `siskapmb_maba`
                  WHERE email='$email')";
        $query = $this->db->query($sql);
        if ( !empty( $query->row_array() ) ) {
          return $query->row_array()['email_token'];
        }
        return false;
    }

    public function get_hp_data( $email ){
        $sql = "(SELECT telepon, telepon_pin FROM `siskapmb_agen`
                  WHERE email='$email')
                 UNION
                (SELECT telepon, telepon_pin FROM `siskapmb_maba`
                  WHERE email='$email')";
        $query = $this->db->query($sql);
        if ( !empty( $query->row_array() ) ) {
          return $query->row_array();
        }
        return false;
    }

    public function verifikasi_sms_pin($email, $pin, $akun)
    {
        $query = $this->db->get_where( $this->tabel[$akun], array('email' => $email, 'telepon_pin' => $pin ) );
        if (!empty($query->row_array())) {
            $data = array( 'telepon_valid' => date('Y-m-d H:i:s'),
                           'status_aktif' => 'A' );
            $this->db->where("`email`='".$email."' AND `telepon_pin`='".$pin."'", NULL);
            return $this->db->update($this->tabel[$akun], $data);
        }
        return false;
    }

    public function reset_sms_pin( $email )
    {
        $data['telepon_pin'] = strtoupper(random_string('alnum', 6));

        $this->db->where( 'email', $email );
        if( $this->db->update($this->tabel['agen'], $data) ){
            return TRUE;
        } elseif ( $this->db->update($this->tabel['maba'], $data) ) {
            return TRUE;
        }
        return FALSE;
    }

    /*
     * verifikasi password berdasarkan email
     */
    public function checkPassword($email)
    {
        $query = $this->db->get_where( $this->tabel[$akun], array('email' => $email));
        $hash  = $query->row_array()['password'];

        if (password_verify($this->input->post('password'), $hash)) {
            return true;
        }
        return false;
    }

    /*
     * register agen
     */
    public function registerAgen()
    {
        $_SESSION['token'] = random_string('alnum', 16);
        $_SESSION['email'] = $this->input->post('email');
        $_SESSION['email_token'] = random_string('alnum', 16);

        $data = [
                'id'       		=> date('U'),
                'nama_lengkap'  => strtoupper( $this->input->post('namalengkap') ),
             	'email'    		=> $_SESSION['email'],
                'email_token'   => $_SESSION['email_token'],
                'telepon' 		=> $this->input->post('nomorhp'),
                'telepon_pin'   => strtoupper(random_string('alnum', 6)),
                'alamat'        => $this->input->post('alamat'),
                'kelurahan'     => $this->input->post('desakelurahan'),
                'tipe_afiliasi' => $this->input->post('tipeafiliasi'),
                'password' 		=> password_hash($this->input->post('password'), PASSWORD_BCRYPT),
                'token'    		=> $_SESSION['token']
              ];

        switch ($this->input->post('tipeafiliasi')) {
            case 'A':
                $data['id_afiliasi'] = $this->input->post('nimalumni');
                break;

            case 'G':
                $data['id_afiliasi'] = $this->input->post('idsekolah');

                if( $this->statusSekolah( $this->input->post('idsekolah') ) == "N") $data['pekerjaan'] = '002';
                elseif( $this->statusSekolah( $this->input->post('idsekolah') ) == "S") $data['pekerjaan'] = '102';

                break;

            case 'M':
                $data['id_afiliasi'] = $this->input->post('nimhs');
                break;

            case 'S':
                $data['id_afiliasi'] = $this->input->post('nidnnik');

                if( strlen( $this->input->post('nidnnik') ) < 10 ) $data['pekerjaan'] = '101';
                else $data['pekerjaan'] = '103';

                break;
        }

        $this->db->insert('siskapmb_agen', $data);
    }

    /*
     * UPdate data calon maba
     */
    public function updateprofilagen()
    {
        $data = [
                'nama_lengkap'    => strtoupper( $this->input->post('namalengkap') ),
                'sex'             => $this->input->post('jkel'),
                'bitly_url'       => $this->input->post('bitly'),
                'alamat'          => $this->input->post('alamat'),
                'kelurahan'       => $this->input->post('desakelurahan'),
                'pekerjaan'       => $this->input->post('pekerjaan'),
                'nomor_rekening'  => $this->input->post('rekening'),
                'nama_rekening'   => strtoupper( $this->input->post('namarekening') ),
                'bank_rekening'   => $this->input->post('bank'),
                'setuju_biaya'    => $this->input->post('setujubiaya'),
                'info'            => $this->input->post('infosrc')
              ];
        $this->db->where("id", $_SESSION['id'] );
        $this->db->update('siskapmb_agen', $data);
    }

    public function updatelink( $linkref, $bitly ){
        $data = [
                'kode_referrer'   => $linkref,
                'bitly_url'       => $bitly
              ];
        $this->db->where("id", $_SESSION['id'] );
        $this->db->update('siskapmb_agen', $data);

    }

    public function statusSekolah( $id ){
        $sql = "SELECT statusns FROM sekolah_sma WHERE id='$id'";
        $query = $this->db->query($sql);
        if ( !empty( $query->row_array() ) ) {
            return $query->row_array()['statusns'];
        }
        return false;
    }


    /*
     * register calon maba
     */
    public function registerMaba()
    {
        $_SESSION['token'] = random_string('alnum', 16);
        $_SESSION['email'] = $this->input->post('email');
        $_SESSION['email_token'] = random_string('alnum', 16);

        if( empty( $this->input->post('jprestasi') ) || $this->input->post('jprestasi')==NULL ) $jprestasi = 0;
        else $jprestasi = 1;

        $data = [
                'id'       		=> date('U'),
                'nama_lengkap'  => strtoupper( $this->input->post('namalengkap') ),
                'tempat_lahir' 	=> $this->input->post('tempatlahir'),
                'tanggal_lahir' => $this->input->post('tanggallahir'),
                'sex'           => $this->input->post('jkel'),
                'email'    		=> $_SESSION['email'],
                'email_token'   => $_SESSION['email_token'],
                'telepon' 		=> $this->input->post('nomorhp'),
                'telepon_pin'   => strtoupper(random_string('alnum', 6)),
                'sekolah_asal'  => $this->input->post('idsekolah'),
                'sekolah_lulus' => $this->input->post('tahunlulus'),
                'sekolah_jurusan' => $this->input->post('jurusan'),
                'jalurprestasi' => $jprestasi,
                'password' 		=> password_hash($this->input->post('password'), PASSWORD_BCRYPT),
                'token'    		=> $_SESSION['token'],
                'tahun'         => $this->input->post('tahun'),
                'status_aktif' => 'A'
              ];

        if( isset( $_SESSION['ref_voucher'] ) ) {
            $data['referensi'] = $_SESSION['ref_voucher'];
        }

        $this->db->insert('siskapmb_maba', $data);
    }

    /*
     * UPdate data calon maba
     */
    public function updateprofilmaba()
    {
        $data = [
                'nama_lengkap'    => strtoupper( $this->input->post('namalengkap') ),
                'tempat_lahir' 	  => $this->input->post('tempatlahir'),
                'tanggal_lahir'   => $this->input->post('tanggallahir'),
                'sex'             => $this->input->post('jkel'),
                'sekolah_asal'    => $this->input->post('idsekolah'),
                'sekolah_lulus'   => $this->input->post('tahunlulus'),
                'sekolah_jurusan' => $this->input->post('jurusan'),
                'prodi_pilihan1'  => $this->input->post('rprodi1'),
                'prodi_pilihan2'  => $this->input->post('rprodi2'),
                'alamat'          => $this->input->post('alamat'),
                'agama'           => $this->input->post('agama'),
                'kelurahan'       => $this->input->post('desakelurahan'),
                'nama_ayah'       => strtoupper( $this->input->post('namaayah') ),
                'nama_ibu'        => strtoupper( $this->input->post('namaibu') ),
                'telepon_ayah'    => $this->input->post('hpayah'),
                'telepon_ibu'     => $this->input->post('hpibu'),
                'pekerjaan_ayah'  => $this->input->post('pekerjaanayah'),
                'pekerjaan_ibu'   => $this->input->post('pekerjaanibu'),
                'penghasilan_ortu'=> $this->input->post('penghasilan'),
                'info'            => $this->input->post('infosrc')
              ];
        $this->db->where("id", $_SESSION['id'] );
        $this->db->update('siskapmb_maba', $data);
    }

    public function statusprofilmaba( $uid ){
        $sql = "SELECT * FROM `siskapmb_maba` WHERE id='$uid'";
        $query = $this->db->query($sql);
        $data = $query->row_array();
        if( empty($data['nama_lengkap']) ||
            empty($data['tempat_lahir']) ||
            $data['tanggal_lahir'] == '0000-00-00' ||
            empty($data['sex']) ||
            empty($data['sekolah_asal']) ||
            empty($data['sekolah_lulus']) ||
            empty($data['sekolah_jurusan']) ||
            empty($data['prodi_pilihan1']) ||
            empty($data['prodi_pilihan2']) ||
            empty($data['alamat']) ||
            empty($data['agama']) ||
            empty($data['kelurahan']) ||
            empty($data['nama_ayah']) ||
            empty($data['nama_ibu']) ||
            empty($data['pekerjaan_ayah']) ||
            empty($data['pekerjaan_ibu']) ||
            empty($data['penghasilan_ortu']) ||
            empty($data['info']) || $data['info'] == '00000000'
        ) return false;

        return true;
    }

    /*
     * Ganti password
     */
    public function updatePassword( $who, $id, $password ) {
        $this->db->where( 'id', $id );
        if( $this->db->update($this->tabel[$who], array('password' => password_hash( $password, PASSWORD_BCRYPT ))) ) {
            return true;
        }
        return false;
    }

    public function resetPassword( $email ){
        $newp = random_string('alnum', 10);
        $data['password'] = password_hash( $newp, PASSWORD_BCRYPT );
        $this->db->where( 'email', $email );
        if( $this->db->update($this->tabel['agen'], $data) ){
            return $newp;
        } elseif ( $this->db->update($this->tabel['maba'], $data) ) {
            return $newp;
        }
        return FALSE;

    }

    public function referrer_log( $urlr ){
        $_SESSION['ref_voucher'] = date('U');
        $sql = "INSERT INTO siskapmb_referral SET id='".$_SESSION['ref_voucher'].
                    "', url='".$urlr.
                    "', ref_id='".$_SESSION['ref_id'].
                    "', valid=MD5(CONCAT(id,ref_id,NOW(),'".$_SESSION['ref_token']."'))";

        if( !isset( $_SESSION['ref_logged'] ) ){  // cegah double logging
            $this->db->query( $sql );
            $_SESSION['ref_logged'] = '1';
        }
    }

    public function get_referral( $idref='' ){
        if( $idref=='' ) $idref = $_SESSION['id'];

        $sql = "SELECT m.id, m.nama_lengkap, m.sekolah_asal, DATE(m.tg_registrasi) tgregistrasi,
                       m.prodi_pilihan1, m.prodi_pilihan2, r.id idr, r.valid, r.tanggal, r.ref_id
                FROM (SELECT * FROM siskapmb_maba WHERE tahun='2017') m
                LEFT JOIN (SELECT * FROM siskapmb_referral) r
                ON(m.referensi=r.id)
                WHERE r.ref_id='$idref'";

        $query = $this->db->query($sql);
        if ( !empty( $query->result_array() ) ) {
          return $query->result_array();
        }
        return false;

    }

    public function get_referral_link( $idref='' ){
        if( $idref=='' ) $idref = $_SESSION['id'];

        $sql = "SELECT COUNT(*) jmlklik FROM siskapmb_referral WHERE ref_id='$idref'";

        $query = $this->db->query($sql);
        if ( !empty( $query->row_array() ) ) {
          return $query->row_array()['jmlklik'];
        }
        return 0;
    }

    public function get_referral_registers( $idref='' ){
        if( $idref=='' ) $idref = $_SESSION['id'];

        $sql = "SELECT COUNT(*) jmllulus
                FROM (SELECT * FROM siskapmb_maba WHERE tahun='2017') m
                LEFT JOIN (SELECT * FROM siskapmb_referral) r
                ON(m.referensi=r.id)
                WHERE r.ref_id='$idref'";

        $query = $this->db->query($sql);
        if ( !empty( $query->row_array() ) ) {
          return $query->row_array()['jmllulus'];
        }
        return 0;
    }

    public function get_referral_lulus( $idref='' ){
        if( $idref=='' ) $idref = $_SESSION['id'];

        $sql = "SELECT COUNT(*) jmllulus
                FROM (SELECT * FROM siskapmb_maba WHERE tahun='2017') m
                LEFT JOIN (SELECT * FROM siskapmb_referral) r
                ON(m.referensi=r.id)
                WHERE r.ref_id='$idref'
                AND m.ujian_status='L'";

        $query = $this->db->query($sql);
        if ( !empty( $query->row_array() ) ) {
          return $query->row_array()['jmllulus'];
        }
        return 0;
    }

    public function get_referral_regmasuk( $idref='' ){
        if( $idref=='' ) $idref = $_SESSION['id'];

        $sql = "SELECT COUNT(*) jmllulus
                FROM (SELECT * FROM siskapmb_maba WHERE tahun='2017') m
                LEFT JOIN (SELECT * FROM siskapmb_referral) r
                ON(m.referensi=r.id)
                WHERE r.ref_id='$idref'
                AND m.regmasuk_tanggal<>'0000-00-00 00:00:00'
                AND (m.regmasuk_validasi<>'' OR m.regmasuk_validasi IS NOT NULL)";

        $query = $this->db->query($sql);
        if ( !empty( $query->row_array() ) ) {
          return $query->row_array()['jmllulus'];
        }
        return 0;
    }

    public function updateDokumen( $tipe, $tipefile ){
        $dok = $this->checkDokumen( $tipe );
        if( $dok ){
            $data = [ 'tg_upload'   => date('Y-m-d H:i:s'),
                      'validasi'    => '',
                      'validator'   => '',
                      'tg_validasi' => '0000-00-00 00:00:00' ];
            $this->db->where( id, $dok['id'] );
            $this->db->update( "siskapmb_dokumen", $data );
            return 2;  // update
        } else {
            $data = [ 'id' => date('U'),
                      'tipe'     => $tipe,
                      'id_owner' => $_SESSION['id'],
                      'tipefile' => $tipefile ];
            $this->db->insert( "siskapmb_dokumen", $data );
            return 1;  // insert
        }
    }

    public function dokumenKartuPeserta( $idmaba ){
        $tgdok = date("Y-m-d H:i:s");
        $id = date('U');
        $val = md5( $tgdok . $id . $_SESSION['token'] );

        $data = [ 'id'          => $id,
                  'tipe'        => '11',
                  'id_owner'    => $idmaba,
                  'tipefile'    => 'pdf',
                  'tg_upload'   => $tgdok,
                  'validasi'    => $val,
                  'validator'   => $_SESSION['id'],
                  'tg_validasi' => $tgdok
                ];
        $this->db->insert( "siskapmb_dokumen", $data );

    }

    public function verifikasi_token($id, $kode, $akun)
    {
        $salt = substr($kode,-6);
        $enc = substr($kode,0,strlen($kode)-6);
        $query = $this->db->get_where( $this->tabel[$akun], array('id' => $id ) );
        if ( !empty($query->row_array()) && md5($query->row_array()['token'].$salt) == $enc ) {
            $_SESSION['id'] = $query->row_array()['id'];
            $_SESSION['nama'] = $query->row_array()['nama_lengkap'];
            $_SESSION['email'] = $query->row_array()['email'];
            $_SESSION['token'] = $query->row_array()['token'];
            $_SESSION['pan'] = $query->row_array()['pan'];
            $_SESSION['klas'] = $akun;
            return true;
        }
       return false;
    }

    public function resetNomorHP( $id, $nomorhp, $password ){
        $data = [ 'telepon'       => $nomorhp,
                  'telepon_valid' => '0000-00-00 00:00:00',
                  'telepon_pin'   => strtoupper(random_string('alnum', 6)) ];

        if( $this->agen( $id ) && password_verify( $password, $this->data['password'] ) ) {
            $this->db->where( "id", $id  );
            if( $this->db->update($this->tabel['agen'], $data ) ) return true;
        }
        elseif( $this->maba( $id ) && password_verify( $password, $this->data['password'] ) ) {
            $this->db->where( "id", $id  );
            if( $this->db->update($this->tabel['maba'], $data ) ) return true;
        }
        return false;
    }

    public function resetAlamatEmail( $id, $email, $password ){
        $data = [ 'email'       => $email,
                  'email_valid' => '0000-00-00 00:00:00',
                  'email_token' => random_string('alnum', 16) ];

        if( $this->agen( $id ) && password_verify( $password, $this->data['password'] ) ) {
            $this->db->where( "id", $id  );
            if( $this->db->update($this->tabel['agen'], $data ) ) return true;
        }
        elseif( $this->maba( $id ) && password_verify( $password, $this->data['password'] ) ) {
            $this->db->where( "id", $id  );
            if( $this->db->update($this->tabel['maba'], $data ) ) return true;
        }
        return false;
    }

    public function setpanitia( $id ){
        if( $this->get_token( $id, 'agen') ){
            $this->db->where( 'id', $id );
            $this->db->update( 'siskapmb_agen', array( 'pan' => $this->pmb_lib->panitia_hash( $this->get_token( $id, 'agen') )));
        }
    }

    public function notpanitia( $id ){
        if( $this->get_token( $id, 'agen') ){
            $this->db->where( 'id', $id );
            $this->db->update( 'siskapmb_agen', array( 'pan' => '0' ) );
        }
    }

    public function listagen(){
        // pilih salah satu, belum tau mana yang efisien ..
        // ini ..
        $sql = "SELECT a.id, a.nama_lengkap, a.tg_registrasi, a.tipe_afiliasi, a.id_afiliasi, a.pan,
                       a.email_valid, a.telepon_valid, COUNT(r.id) klik
                FROM siskapmb_agen a
                LEFT JOIN siskapmb_referral r
                ON(a.id=r.ref_id)
                GROUP BY a.id
                ORDER BY tg_registrasi";
        // atau ini ..
        $sql = "SELECT a.id, a.nama_lengkap, a.tg_registrasi, a.tipe_afiliasi, a.id_afiliasi, a.pan,
                      (SELECT COUNT(*) FROM siskapmb_referral WHERE ref_id=a.id) klik
                FROM siskapmb_agen a
                ORDER BY tg_registrasi";

        $sql = "SELECT a.id, a.nama_lengkap, a.tg_registrasi, a.tipe_afiliasi, a.id_afiliasi, a.pan,
                      (SELECT COUNT(*) FROM siskapmb_referral WHERE ref_id=a.id) klik,
                      (SELECT COUNT(*) FROM siskapmb_maba m LEFT JOIN siskapmb_referral r ON (m.referensi=r.id)
                       WHERE r.ref_id=a.id) reg
                FROM siskapmb_agen a
                ORDER BY tg_registrasi";

        $query = $this->db->query($sql);
        if ( !empty( $query->result_array() ) ) {
          return $query->result_array();
        }
        return false;
    }

    public function listmaba($tahun){
        // $sql = "SELECT m.id, m.nama_lengkap, m.tg_registrasi,
        //                s.sekolah, j.jurusan, p.nama_program_studi prodi1, p.jenjang_studi jenjang1,
        //                a.nama_lengkap namaagen
        //         FROM siskapmb_maba m
        //         LEFT JOIN siskapmb_referral r ON (m.referensi=r.id)
        //         LEFT JOIN siskapmb_agen a ON (r.ref_id=a.id)
        //         LEFT JOIN sekolah_sma s ON (m.sekolah_asal=s.id)
        //         LEFT JOIN sekolah_jurusan j ON (m.sekolah_jurusan=j.kode)
        //         LEFT JOIN dikti_kodeprodi p ON (m.prodi_pilihan1=p.kode_program_studi)
        //         WHERE m.tahun='".$tahun."'";

        $sql = "SELECT m.id, m.nama_lengkap, m.tg_registrasi,
                       s.sekolah, j.jurusan, p.nama_program_studi prodi1, p.jenjang_studi jenjang1
                FROM siskapmb_maba m
                LEFT JOIN siskapmb_referral r ON (m.referensi=r.id)
                LEFT JOIN sekolah_sma s ON (m.sekolah_asal=s.id)
                LEFT JOIN sekolah_jurusan j ON (m.sekolah_jurusan=j.kode)
                LEFT JOIN dikti_kodeprodi p ON (m.prodi_pilihan1=p.kode_program_studi)
                WHERE m.tahun='".$tahun."'";

        $query = $this->db->query($sql);
        if ( !empty( $query->result_array() ) ) {
          return $query->result_array();
        }
        return false;
    }

    public function listchannel( $id = ''){
        if( $id != '' ) $whr = "WHERE ref_id='$id'";
        else $whr="";

        $sql = "SELECT IF( LEFT(url,7)='android',url, SUBSTRING_INDEX(SUBSTRING_INDEX(url, '://', -1),'/', 1) ) AS channel,
                       url, count(*) jmlklik
                FROM siskapmb_referral ".$whr." GROUP BY channel, url";

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function checkDokumen( $tipe, $id='' ){
        if( $id=='' ) $id = $_SESSION['id'];

        $sql = "SELECT * FROM siskapmb_dokumen
                WHERE id_owner='$id' AND tipe='$tipe'";

        $query = $this->db->query($sql);
        if ( !empty( $query->row_array() ) ) {
          return $query->row_array();
        }
        return false;
    }

    public function listdokumen( $waiting = false ){
        $whr="";
        if( $waiting ) $whr = "WHERE validasi='' ";

        $sql = "SELECT d.*, m.nama_lengkap
                FROM siskapmb_dokumen d
                LEFT JOIN siskapmb_maba m ON (d.id_owner=m.id) $whr
                ORDER BY id_owner, tipe";

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function validasidokumen( $id ){
        $tgv = date("Y-m-d H:i:s");
        $val = md5( $tgv . $id . $_SESSION['token'] );

        $data = [ "validasi" => $val,
                  "validator" => $_SESSION['id'],
                  "tg_validasi" => $tgv ];

        $this->db->where( "id", $id );
        return $this->db->update( "siskapmb_dokumen", $data );
    }

    public function mabawaitinglist( $tipe ){
        $sql = "SELECT * FROM `siskapmb_maba`
                WHERE id NOT IN (SELECT maba
                                 FROM siskapmb_ujianmaba um
                                 LEFT JOIN siskapmb_ujian uj ON (uj.id=um.ujian)
                                 WHERE jenis='$tipe' )
                ORDER BY tg_registrasi";
        $query = $this->db->query($sql);

        if ( !empty( $query->result_array() ) ) {
            $waitinglist = $query->result_array();
            $lst = 0;
            foreach ( $waitinglist as $calon ) {
                // cek kelengkapan profil
                $waitinglist[$lst]['profillengkap'] = '0';
                if( $this->statusprofilmaba( $calon['id'] ) ) $waitinglist[$lst]['profillengkap'] = '1';

                $lst++;
            }

            return $waitinglist;
        }
        return false;
    }

    public function jadwalujianbaru( $id ){
        $data = [ 'id'      => $id,
                  'jenis'   => $this->input->post('jenisseleksi'),
                  'tanggal' => $this->input->post('ptgujian'),
                  'create'  => $_SESSION['id'],
                  'tahun'   => $this->pmb_lib->tahun
                   ];
        $this->db->insert( 'siskapmb_ujian', $data );
    }

    public function jadwalujianmaba( $id, $maba ){
        $data = [ 'item_id' => $id.$maba,
                  'ujian'   => $id,
                  'maba'    => $maba,
                   ];
        $this->db->insert( 'siskapmb_ujianmaba', $data );
    }

    public function listjadwal(){
        $sql = "SELECT u.id, DATE(u.tanggal) tglujian, TIME(u.tanggal) waktuujian, u.jenis, u.status,
                COUNT(m.maba) peserta
                FROM siskapmb_ujian u
                LEFT JOIN siskapmb_ujianmaba m ON (u.id=m.ujian)
                WHERE u.tahun='".$this->pmb_lib->tahun."'
                GROUP BY u.id";

        $query = $this->db->query($sql);
        if ( !empty( $query->result_array() ) ) {
            return $query->result_array();
        }
        return false;
    }

    public function listkartupeserta(){
        $sql = "SELECT m.item_id, c.nama_lengkap, m.maba, m.ujian,
                       u.jenis, DATE(tanggal) tglujian, TIME(tanggal) jamujian
                FROM siskapmb_ujianmaba m
                LEFT JOIN siskapmb_maba c ON(c.id=m.maba)
                LEFT JOIN siskapmb_ujian u ON(m.ujian=u.id)
                WHERE u.jenis='A'
                AND m.maba NOT IN (SELECT id_owner FROM siskapmb_dokumen
                                    WHERE tipe='11')";

        $query = $this->db->query($sql);
        if ( !empty( $query->result_array() ) ) {
            return $query->result_array();
        }
        return false;
    }

}
