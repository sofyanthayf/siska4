<?php

class Dosen_model extends CI_Model {

  public $data;

  public function __construct() {
    parent::__construct();
  }


  /*------------------------------------------------------------
  * Men-set model dan membaca semua data Dosen berdasarkan NIDN
  * @param $nidn = Nomor Induk Dosen Nasional
  *------------------------------------------------------------*/
  public function setNidn( $nidn )
  {
    $query = $this->db->get_where('siska_dosen', array('nidn' => $nidn));
    if (!empty($query->row_array())) {
      $this->data = $query->row_array();

      $this->data['foto'] = $this->getFoto();

      return $this->data;
    }
    return false;
  }

  public function mahasiswaWali($nidn){
    $sql_pa = "SELECT m.nimhs, m.namamhs, m.status FROM
                (SELECT nimhs, MAX(kodesmt)
                  FROM siska_pembimbingakdm
                  WHERE pembimbing=?
                  AND status=1
                  GROUP BY nimhs) AS b
               INNER JOIN
                (SELECT nimhs, namamhs, prodi, status
                  FROM siska_mahasiswa
                  WHERE status='A') AS m
                  USING (nimhs)
                  ORDER BY prodi DESC, MID(nimhs,4,2) DESC, nimhs ASC";
    $query = $this->db->query( $sql_pa, array($nidn) );
    if( !empty( $query->result_array() ) ) return $query->result_array();
    return false;
  }

  public function daftarDosenTetap()
  {
    $sql = "SELECT nidn, nama, gelar_depan, gelar_belakang, prodi, pt_induk, status_aktif,
                   pendidikan kode_pendidikan_terakhir,
                   jabatan_akademik kode_jabatan_fungsional
            FROM siska_dosen
            WHERE pt_induk='093093'
              AND pendidikan < 'C'
              AND (status_aktif='A' OR status_aktif='S')";

    $query = $this->db->query( $sql );

    return $query->result_array();
  }


  public function getFoto( $nidn = "" )
  {
      if( $nidn == "" ) $nidn = $this->data['nidn'];

      $filefoto = 'assets/img/foto/dsn/' . $nidn . ".jpg" ;

      if( file_exists(  FCPATH . $filefoto ) ) {
      // if( file_exists(  FCPATH.str_replace('/','\\',$filefoto ) ) ){
        return base_url().$filefoto;
      }

      return false;
  }

}
