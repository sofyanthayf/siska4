<?php

class Prodi_model extends CI_Model {

    public $data;

	public function __construct() {
        parent::__construct();
    }

    public function getProdi( $kode ){
        $sql = "SELECT * FROM `siska_prodi` WHERE kode='".$kode."'";
        $query = $this->db->query($sql);
        if ( !empty( $query->row_array() ) ) {
            $this->data = $query->row_array();
            $this->data['jenjang'] = $this->siska->jenjang[ $this->data['kodejenjang'] ];
            return $this->data;
        }

        return false;
    }

    public function getProdiTahun( $thn='all' ){
        if( $thn == 'all') {
            $sql = "SELECT p.kode_program_studi kode, p.nama_program_studi nama, p.jenjang_studi jenjang
                    FROM siska_mahasiswa m LEFT JOIN dikti_kodeprodi p ON( m.prodi=p.kode_program_studi )
                    GROUP BY kode, nama, jenjang
                    ORDER BY MID(kode,3,1)";
        } else {
            $sql = "SELECT p.kode_program_studi kode, p.nama_program_studi nama, p.jenjang_studi jenjang
                    FROM siska_mahasiswa m LEFT JOIN dikti_kodeprodi p ON( m.prodi=p.kode_program_studi )
                    WHERE YEAR(m.tanggal_masuk)='$thn'
                    GROUP BY kode, nama, jenjang
                    ORDER BY MID(kode,3,1)";
        }

        $query = $this->db->query($sql);
        if ( !empty( $query->result_array() ) ) {
            return $query->result_array();
        }

        return false;
    }

    public function getListProdiByTahunKurikulum( $thn = 0 ){
        if( $thn == 0 ) $thn = date('Y');

        $sql = "SELECT DISTINCT m.prodi, p.kode_program_studi kode, p.nama_program_studi nama, p.jenjang_studi jenjang, m.tahunkur
                  FROM (SELECT * FROM siska_kompetensi  WHERE tahunkur<='$thn'
                           AND tahunkur>=(SELECT MAX(tahunkur)
                          FROM siska_kompetensi
                         WHERE tahunkur<='$thn' ) ) m
                  LEFT JOIN dikti_kodeprodi p ON( m.prodi=p.kode_program_studi )
                  ORDER BY kode";

        $query = $this->db->query($sql);
        if ( !empty( $query->result_array() ) ) {
            return $query->result_array();
        }

        return false;
    }



}
