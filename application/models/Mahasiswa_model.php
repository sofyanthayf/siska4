<?php

class Mahasiswa_model extends CI_Model
{

  public $data;

  public function __construct()
  {
    parent::__construct();
  }


  /*------------------------------------------------------------
    * Men-set model dan membaca semua data Mahasiswa berdasarkan NIM
    * @param $nim = Nomor Induk Mahasiswa
    *------------------------------------------------------------*/
  public function setNim($nim)
  {
    $query = $this->db->get_where('siska_mahasiswa', array('nimhs' => $nim));
    if (!empty($query->row_array())) {
      $this->data = $query->row_array();

      $this->data['program_studi'] = $this->prodi_model->getProdi($this->data['prodi']);
      $this->data['foto'] = $this->getFoto();
      $this->data['dosen_pa'] = $this->getDosenPA();
      $this->data['ipk'] = $this->getIPK();
      $this->data['ips'] = $this->getIPS();
      $this->data['jmlsks'] = $this->getsks();
      $this->data['dtkompetensi'] = $this->getKompetensi();
      $this->data['listsemester'] = $this->listSemester($nim);

      $pembimbingta = $this->getPembimbingTA();
      if ($pembimbingta) {
        $this->data['pembimbing_ta'] = $pembimbingta;
      }

      $masastudi = $this->getMasaStudi();
      if ($masastudi) {
        $this->data['masa_studi'] = $masastudi;
      }
      // echo '<pre>' . var_export($this->data, true) . '</pre>';

      return $this->data;
    }
    return false;
  }

  public function getFoto($nimhs = "")
  {
    if ($nimhs == "") $nimhs = $this->data['nimhs'];

    $prd = substr($nimhs, 0, 3);
    $thn = substr($nimhs, 3, 2);
    $filefoto = 'assets/img/foto/mhs/' . $thn . "/" . $prd . "/" . $nimhs . ".jpg";

    if (file_exists(FCPATH . $filefoto)) {
      // if( file_exists(  FCPATH.str_replace('/','\\',$filefoto ) ) ){
      return base_url() . $filefoto;
    }

    return false;
  }

  public function getDosenPA($nimhs = "")
  {
    if ($nimhs == "") $nimhs = $this->data['nimhs'];
    $sql = "SELECT p.pembimbing nidn, d.nama nama, d.gelar_depan, d.gelar_belakang, p.kodesmt, noskpa, tgskpa
              FROM siska_pembimbingakdm p LEFT JOIN siska_dosen d ON (d.nidn=p.pembimbing)
              WHERE nimhs='" . $nimhs . "'
              ORDER BY kodesmt DESC
              LIMIT 1";
    $query = $this->db->query($sql);
    if (!empty($query->row_array())) return $query->row_array();
    return false;
  }

  public function getIPK($nimhs = "")
  {
    if ($nimhs == "") $nimhs = $this->data['nimhs'];
    $sql = "SELECT SUM(sks*n)/ SUM(sks) AS ip
    					  FROM (SELECT
                          mk.kodemk, sks,
       										(CASE
         										WHEN MIN(nilai)='A' THEN 4
         										WHEN MIN(nilai)='B' THEN 3
         										WHEN MIN(nilai)='C' THEN 2
         										WHEN MIN(nilai)='D' THEN 1
         										WHEN MIN(nilai)='E' THEN 0
       										END) n
          						FROM siska_matakuliah mk
                      INNER JOIN siska_kuliah kl USING (kodemk),
                                 siska_mahasiswa mh
          						WHERE mh.nimhs='" . $nimhs . "'
            					  AND mk.tahunkur=mh.tahunkur
            					  AND kl.nimhs=mh.nimhs
            					  AND kl.nilai<>''
          						GROUP BY mk.kodemk) nilaimk";
    $query = $this->db->query($sql);
    if (!empty($query->row_array())) {
      $ipk = $query->row_array()['ip'];

      //update IPK to siska_mahasiswa
      $this->db->where('nimhs', $nimhs);
      $this->db->update('siska_mahasiswa',  array('ipk' => $ipk));

      return $ipk;
    }
    return false;
  }

  public function getIPS($nimhs = "")
  {
    if ($nimhs == "") $nimhs = $this->data['nimhs'];
    $sql = "SELECT kodesmt, SUM(sks*n)/SUM(sks) AS ip
					  FROM (SELECT DISTINCT mk.kodemk, namamk, mk.sks, nilai,
      									 (CASE
        									WHEN nilai='A' THEN 4
        									WHEN nilai='B' THEN 3
         									WHEN nilai='C' THEN 2
         									WHEN nilai='D' THEN 1
         									WHEN nilai='E' THEN 0
       									  END) n, kl.kodesmt
       					  	FROM siska_matakuliah mk INNER JOIN siska_kuliah kl USING (kodemk),siska_mahasiswa mh
       					  	WHERE mh.nimhs='" . $nimhs . "'
         					  AND mk.tahunkur=mh.tahunkur
         					  AND kl.nimhs=mh.nimhs
         					  AND kl.nilai<>'') nilaimk
					  GROUP BY kodesmt";

    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function getsks()
  {
    $sql = "SELECT SUM(sks) as jmlsks FROM (
        				  SELECT DISTINCT kodemk,sks, MIN(nilai)
        	 			  FROM (siska_matakuliah INNER JOIN siska_kuliah USING (kodemk))
        				  INNER JOIN siska_mahasiswa USING (nimhs)
        				  WHERE siska_mahasiswa.nimhs='" . $this->data['nimhs'] . "'
        				    AND siska_kuliah.nilai<>''
        				    AND siska_kuliah.nilai<>'E'
        				  GROUP BY kodemk, sks
        				) AS listnilai";
    $query = $this->db->query($sql);
    if (!empty($query->row_array())) return $query->row_array()['jmlsks'];
    return false;
  }

  public function getPembimbingTA($nimhs = "")
  {
    if ($nimhs == "") $nimhs = $this->data['nimhs'];
    $sql = "SELECT DISTINCT b.pembimbing nidn, d.nama, d.gelar_depan, d.gelar_belakang, b.feedback
                FROM `siska_pembimbingta` b
                LEFT JOIN siska_dosen d ON (b.pembimbing=d.nidn)
               WHERE nimhs=? AND b.status=1
               ORDER BY b.urut";
    $query = $this->db->query($sql, $nimhs);
    if (!empty($query->result_array())) return $query->result_array();
    return false;

    // -------------- NUMPANG CATATAN -------------------
    // $no_nilai_ta ="SELECT DISTINCT m.`nimhs`, m.`namamhs`, m.`nomor_sk_yudisium` yudisium, m.`tanggal_sk_yudisium` tanggal, kl.`nilai` SKRIPSI
    // FROM `siska_mahasiswa` m
    // LEFT JOIN `siska_kuliah` kl USING(nimhs)
    // LEFT JOIN `siska_matakuliah` mk USING(kodemk)
    // WHERE m.`tanggal_sk_yudisium` >= '2020-08-01'
    // AND mk.namamk LIKE '%SKRIPSI%'
    // ORDER BY m.`tanggal_sk_yudisium` ASC, kl.nilai ASC ";
  }


  public function getMasaStudi($nimhs = "")
  {
    if ($nimhs == "") $nimhs = $this->data['nimhs'];
    $sql = "SELECT
                TIMESTAMPDIFF(YEAR, tanggal_masuk, tanggal_sk_yudisium) tahun,
                TIMESTAMPDIFF(MONTH, tanggal_masuk, tanggal_sk_yudisium)-(12*TIMESTAMPDIFF(YEAR, tanggal_masuk, tanggal_sk_yudisium)) bulan
              FROM `siska_mahasiswa` WHERE nimhs=? AND tanggal_sk_yudisium IS NOT NULL";
    $query = $this->db->query($sql, $nimhs);
    if (!empty($query->row_array())) return $query->row_array();
    return false;
  }

  /*
     *  return: array dengan keys: prodi, kode, tahunkur, kompetensi, singkatan
     */
  public function getKompetensi()
  {
    $sql = "SELECT * FROM siska_kompetensi
					 WHERE kode='" . $this->data['kompetensi'] . "'
					 AND tahunkur='" . $this->data['tahunkur'] . "'
					 LIMIT 1";
    $query = $this->db->query($sql);
    if (!empty($query->row_array())) return $query->row_array();
    return false;
  }

  public function getMataKuliah($mhs, $prodi, $tahunkur, $kompetensi, $smt = 'all')
  {

    if ($smt != 'all') {
      //specific kodesmt
      $querymk =  "SELECT DISTINCT a.kodemk, a.namamk, a.sks, a.smt, a.kompetensi, b.nilai, b.kodesmt
                         FROM (SELECT * FROM siska_matakuliah
                                WHERE tahunkur='" . $tahunkur . "'
                                AND (prodi='" . $prodi . "'
                                    OR kompetensi='" . $kompetensi . "')) AS a
                         INNER JOIN
                         (SELECT DISTINCT kodemk, (IF(nilai<>'', nilai, 'P')) nilai, kodesmt
                            FROM siska_kuliah
                            WHERE nimhs='" . $mhs . "'
                              AND kodesmt='" . $smt . "'
                              AND status<>'B') AS b USING(kodemk)
                         ORDER BY a.smt, a.kompetensi, a.kodemk";
    } else {
      // DNS all semester
      $querymk =  "SELECT DISTINCT 
                         a.kodemk, a.namamk, a.sks, a.smt, a.kompetensi, b.nilai, b.kodesmt,
                         b.kodekelas, b.dosen, b.feedback
                         FROM (SELECT * FROM siska_matakuliah
                                WHERE tahunkur='" . $tahunkur . "'
                                AND (prodi='" . $prodi . "'
                                    OR kompetensi='" . $kompetensi . "')) AS a
                         LEFT JOIN
                         (SELECT DISTINCT klh.kodemk,
                                 ( IF( MIN(klh.nilai)<>'', MIN(klh.nilai),'P' ) ) AS nilai, klh.kodesmt,
                                 klh.kodekelas, kls.NIDN_dosen_pengampu dosen, klh.feedback
                            FROM siska_kuliah klh
                            LEFT JOIN siska_kelas kls ON (klh.kodekelas=kls.kode_kelas) 
                            WHERE klh.nimhs='" . $mhs . "'
                              AND klh.status<>'B'
                            GROUP BY kodemk) AS b USING(kodemk)
                         ORDER BY a.smt, a.kompetensi, a.kodemk";
    }

    $query = $this->db->query($querymk);
    if (!empty($query->result_array())) return $query->result_array();
    return false;
  }

  public function getListAngkatan($alumni = false)
  {
    if ($alumni) {
      $where_lulus = "AND status='L'";
    } else {
      $where_lulus = "";
    }

    // $sql = "SELECT YEAR(tanggal_masuk) tahunmasuk FROM `siska_mahasiswa`
    //                 WHERE YEAR(tanggal_masuk) IS NOT NULL $where_lulus
    //                 GROUP BY YEAR(tanggal_masuk)
    //                 ORDER BY tahunmasuk DESC";
    $sql = "SELECT LEFT(smtmulai,4) tahunmasuk FROM `siska_mahasiswa`
                    WHERE YEAR(tanggal_masuk) IS NOT NULL $where_lulus
                    GROUP BY LEFT(smtmulai,4)
                    ORDER BY tahunmasuk DESC";

    $query = $this->db->query($sql);
    if (!empty($query->result_array())) return $query->result_array();
    return false;
  }

  public function getListMahasiswa($prodi, $angkatan)
  {
    $sql = "SELECT nimhs nim, namamhs nama, sex lp, tanggal_masuk, email, status 
              FROM `siska_mahasiswa`
              WHERE prodi='$prodi'
              AND LEFT(smtmulai,4)='$angkatan'
              ORDER BY nim";
    // $sql = "SELECT nimhs nim, namamhs nama, sex lp, tanggal_masuk, email, status 
    //           FROM `siska_mahasiswa`
    //           WHERE prodi='$prodi'
    //           AND YEAR(tanggal_masuk)='$angkatan'
    //           ORDER BY nim";

    //   AND status<>'L'

    $query = $this->db->query($sql);
    if (!empty($query->result_array())) {
      $result = $query->result_array();

      // foreach ($result as $mh) {
      //   $ipk = $this->getIPK( $mh['nim'] );
      // }

      return $result;
    }
    return false;
  }

  public function searchMahasiswa($key)
  {
    $sql = "SELECT nimhs nim, namamhs nama, email, status FROM `siska_mahasiswa`
              WHERE nimhs LIKE '$key%'
                 OR namamhs LIKE '%$key%'
              ORDER BY nim";

    $query = $this->db->query($sql);
    if (!empty($query->result_array())) return $query->result_array();
    return false;
  }

  /** daftar semester yang telah dilalui mahasiswa **/
  public function listSemester($nim)
  {
    $sql = "SELECT DISTINCT kodesmt
                FROM siska_kuliah
                WHERE nimhs='$nim'
                ORDER BY kodesmt DESC";

    $query = $this->db->query($sql);
    if (!empty($query->result_array())) {
      // var_dump($query->result_array());
      $this->data['lastsemester'] = $query->row_array()['kodesmt'];
      return $query->result_array();
    }
    return false;
  }


  public function detailmhsminimalis($nim)
  {
    $data = array();

    $sql = "SELECT nimhs, namamhs, telepon, prodi, kompetensi, status, sekolah_asal, email, sex, agama
                FROM siska_mahasiswa
               WHERE nimhs='$nim'";

    $query = $this->db->query($sql);
    //        $query = $this->db->get_where('siska_mahasiswa', array('nimhs' => $nim));
    if (!empty($query->row_array())) {
      $data = $query->row_array();

      $data['ipk'] = $this->getIPK($nim);
      $data['dosen_pa'] = $this->getDosenPA($nim);

      return $data;
    }
    return false;
  }

  /********************** API function *******************************
   * Fungsi-fungsi khusus untuk layanan SISKA API
   ********************************************************************/

  public function api_detailmhs($nim)
  {
    $data = array();

    $sql = "SELECT nimhs, namamhs, prodi, smtmulai, tanggal_masuk, tanggal_lulus, kompetensi, status,
                       sekolah_asal, email, facebook, linkedin, gplus, sex, agama
                FROM siska_mahasiswa
                WHERE nimhs='$nim'";

    $query = $this->db->query($sql);
    //        $query = $this->db->get_where('siska_mahasiswa', array('nimhs' => $nim));
    if (!empty($query->row_array())) {
      $data = $query->row_array();

      $data['ipk'] = $this->getIPK($nim);
      $data['dosen_pa'] = $this->getDosenPA($nim);

      return $data;
    }
    return false;
  }

  public function api_fotomhs($nim)
  {
    return $this->getFoto($nim);
  }


  public function api_profile_update($nim, $data)
  {
    $this->db->where('nimhs', $nim);
    $this->db->update('siska_mahasiswa', $data);
  }
}
