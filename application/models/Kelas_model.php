<?php

class Kelas_model extends CI_Model
{

  public function __construct()
  {
    parent::__construct();
  }

  function isKelas($id)
  {

    $query = $this->db->get_where('siska_kelas', array('kode_kelas' => $id));

    if (!empty($query->row_array())) {
      return true;
    }

    return false;
  }

  function isMatakuliah($id)
  {

    $query = $this->db->get_where('siska_matakuliah', array('kodemk' => $id));

    if (!empty($query->row_array())) {
      return true;
    }

    return false;
  }

  function isKRS($id)
  {

    $query = $this->db->get_where('siska_krs', array('id_krs' => $id));

    if (!empty($query->row_array())) {
      return true;
    }

    return false;
  }

  public function listKelas($prodi, $kodesmt)
  {
    $sql = "SELECT
              kl.kode_kelas kodekls, kl.kode_matakuliah kodemk, kl.NIDN_dosen_pengampu dosen,
              ds.nama nama_dosen, ds.gelar_depan, ds.gelar_belakang,
              kl.kode_jadwal, kl.slot_waktu, kl.ruang, mk.namamk, mk.sks, mk.smt,
              COUNT(ku.nimhs) mahasiswa
            FROM siska_kelas kl
            LEFT JOIN siska_matakuliah mk ON ( kl.kode_matakuliah = mk.kodemk )
            LEFT JOIN siska_dosen ds ON (kl.NIDN_dosen_pengampu=ds.nidn)
            LEFT JOIN
              (SELECT * FROM siska_kuliah WHERE kodesmt=? ) ku ON ( kl.kode_kelas=ku.kodekelas )
            WHERE kode_semester = ?
              AND kl.kode_program_studi = ?
            GROUP BY ku.kodekelas
            ORDER BY kl.kode_matakuliah,  mk.smt;";

    $query = $this->db->query($sql, array($kodesmt, $kodesmt, $prodi));

    if (!empty($query->result_array())) {
      return $query->result_array();
    }

    return false;
  }


  public function kelasDosen($nidn, $kodesmt)
  {
    // $sql = "SELECT DISTINCT
    //           ku.kodekelas kodekls, kl.kode_matakuliah kodemk, kl.NIDN_dosen_pengampu dosen,
    //           kl.kode_jadwal, kl.slot_waktu, kl.ruang, ku.prodi, mk.namamk, mk.sks, mk.smt,
    //           COUNT(ku.nimhs) mahasiswa
    //         FROM siska_kelas kl
    //         LEFT JOIN siska_kuliah ku ON ( kl.kode_kelas=ku.kodekelas )
    //         LEFT JOIN siska_matakuliah mk ON (mk.kodemk=kl.kode_matakuliah)
    //         WHERE kl.NIDN_dosen_pengampu=?
    //           AND kl.kode_semester=?
    //         GROUP BY ku.kodekelas
    //         ORDER BY kl.kode_matakuliah,  mk.smt;";
    // $query = $this->db->query($sql, array($nidn, $kodesmt));

    $this->db->select("ku.kodekelas kodekls, kl.kode_matakuliah kodemk, kl.NIDN_dosen_pengampu dosen");
    $this->db->select("kl.kode_jadwal, kl.slot_waktu, kl.ruang, COUNT(ku.nimhs) mahasiswa");
    $this->db->select(" ku.prodi, mk.namamk, mk.sks, mk.smt,");
    $this->db->join("siska_kuliah ku", "kl.kode_kelas=ku.kodekelas", "LEFT");
    $this->db->join("siska_matakuliah mk", "mk.kodemk=kl.kode_matakuliah", "LEFT");
    $this->db->where("kl.NIDN_dosen_pengampu", $nidn);
    $this->db->where("kl.kode_semester", $kodesmt);
    $this->db->group_by("ku.kodekelas");
    $this->db->order_by("kl.kode_matakuliah", "ASC");
    $query = $this->db->get("siska_kelas kl");

    if (!empty($query->result_array())) {
      return $query->result_array();
    }

    return false;
  }

  public function mahasiswaKelas($kodekelas)
  {
    $sql = "SELECT kl.nimhs, mh.namamhs
            FROM `siska_kuliah` kl LEFT JOIN siska_mahasiswa mh USING(nimhs)
            WHERE kl.kodekelas=?
            ORDER BY kl.nimhs";

    $query = $this->db->query($sql, $kodekelas);

    if (!empty($query->result_array())) {
      return $query->result_array();
    }

    return false;
  }

  public function kelasMahasiswa($nim, $kodesmt)
  {
    $sql = "SELECT ku.kodekelas, kl.kode_matakuliah, mk.namamk, mk.sks, kl.NIDN_dosen_pengampu
            FROM siska_kuliah ku
            LEFT JOIN siska_kelas kl ON(ku.kodekelas=kl.kode_kelas)
            LEFT JOIN siska_matakuliah mk ON(mk.kodemk=kl.kode_matakuliah)
            WHERE nimhs=?
            AND kodesmt=?
            AND kodekelas IS NOT NULL";

    $query = $this->db->query($sql, array($nim, $kodesmt));

    if (!empty($query->result_array())) {
      return $query->result_array();
    }

    return false;
  }

  public function kelasMKMhs($prodi, $kodesmt)
  {
    $this->db->select('ku.prodi Prodi,
                        ku.nimhs NIM,
                        mh.namamhs Mahasiswa,
                        ku.kodemk Kode_MK,
                        mk.namamk Nama_MK,
                        mk.sks sks,
                        mk.smt smt,
                        ku.kodekelas kode_kelas');
    $this->db->join('siska_matakuliah mk', 'kodemk', 'LEFT');
    $this->db->join('siska_mahasiswa mh', 'nimhs', 'LEFT');
    $this->db->where('ku.prodi', $prodi);
    $this->db->where('kodesmt', $kodesmt);
    $this->db->order_by('ku.kodekelas', 'ASC');

    return $this->db->get('siska_kuliah ku')->result();
  }
}
