<?php

class Litabmas_model extends CI_Model {

  public function __construct() {
      parent::__construct();
  }

  public function daftarpublikasi($tahun)
  {
    $this->siskadb->where( 'LEFT(kodesmt,4) <=', $tahun );
    $this->siskadb->where( 'LEFT(kodesmt,4) >=', $tahun-2 );
    $this->siskadb->order_by('tanggal','desc');
    $query = $this->siskadb->get('siska_publikasi');

    if( $query->num_rows() != 0 ) {
      return $query->result_array();
    }
  }

  public function publikasiauthor($id_publikasi)
  {
    $sql = "SELECT pu.id_author,
    if(ds.nama IS NOT null, ds.nama, mh.namamhs) author
    FROM siska_publikasiauthor pu
    LEFT JOIN siska_dosen ds ON (pu.id_author=ds.nidn)
    LEFT JOIN siska_mahasiswa mh ON (pu.id_author=mh.nimhs)
    WHERE id_publikasi=?";

    $query = $this->siskadb->query($sql, $id_publikasi);

    if( $query->num_rows() != 0 ) {
      return $query->result_array();
    }
    return false;
  }

  public function daftarpenelitian($nidn)
  {
    $sql = "SELECT
              id_penelitian, judul, skema, biaya, sumber_dana, tahun,
              tanggal_mulai, tanggal_selesai, abstrak, url, lit.status
            FROM `siska_peneliti` plit
            LEFT JOIN `siska_penelitian` lit USING (id_penelitian)
            WHERE id_peneliti=?";

    $query = $this->db->query( $sql, $nidn );

    if( $query->num_rows() != 0 ) {
      return $query->result_array();
    }
  }

  public function penelitian($id_penelitian)
  {
    $this->db->where('id_penelitian', $id_penelitian);
    $query = $this->db->get( 'siska_penelitian' );

    if( $query->num_rows() != 0 ) {
      return $query->row_array();
    }

    return false;
  }

  public function peneliti($id_penelitian)
  {
    $sql = "SELECT pn.id_peneliti,
    if(ds.nama IS NOT null, ds.nama, mh.namamhs) peneliti
    FROM siska_peneliti pn
    LEFT JOIN siska_dosen ds ON (pn.id_peneliti=ds.nidn)
    LEFT JOIN siska_mahasiswa mh ON (pn.id_peneliti=mh.nimhs)
    WHERE id_penelitian=?";

    $query = $this->db->query($sql, $id_penelitian);

    if( $query->num_rows() != 0 ) {
      return $query->result_array();
    }
    return false;
  }


}
