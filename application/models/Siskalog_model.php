<?php

class Siskalog_model extends CI_Model {

	public function __construct() {
        parent::__construct();
  }


  public function add( $type, $status, $description )
  {
		if( !$this->session->uid ) $uid = $_SERVER['REMOTE_ADDR'];
		else $uid = $this->session->uid;
		$data = [
							'log_id' => $type.time().strtoupper(random_string('alnum', 4)),
							'uid' => $uid,
							'ip_address' => $_SERVER['REMOTE_ADDR'],
	 						'controller' => $_SESSION['controller'],
	 						'status' => $status,
							'description' => $description
						];

		$this->db->insert('siska_zlog', $data);
  }

}
