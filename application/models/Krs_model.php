<?php

class Krs_model extends CI_Model
{

  public function __construct()
  {
    parent::__construct();
  }


  public function listkrs($nim)
  {
    $this->db->where("nimhs", $nim);
    $this->db->order_by("kodesmt");
    $query = $this->db->get("siska_krs");
    return $query->result_array();
  }

  public function krssemester_byMhs($nim, $kodesmt)
  {
    $this->db->where("nimhs", $nim);
    $this->db->where("kodesmt", $kodesmt);
    $query = $this->db->get("siska_krs");
    return $query->row_array();
  }

  public function krssemester_byID($idkrs)
  {
    $this->db->where("id_krs", $idkrs);
    $query = $this->db->get("siska_krs");
    return $query->row_array();
  }

  public function belanja_krssemester($idkrs, $prodi)
  {
    $this->db->join("siska_matakuliah", "kodemk", "LEFT");
    $this->db->where("id_krs", $idkrs);
    $this->db->where("prodi", $prodi);
    $this->db->order_by("kodemk", "asc");
    $query = $this->db->get("siska_belanja");
    return $query->result_array();
  }

  public function saveKrs($datamhs, $datakrs)
  {
    // saveKRS untuk update KRS otomatis saat pengisian
    // - id_krs berubah jika mata kuliah berubah

    $res = false;  // result

    // - hapus dulu krs dengan id_krs lama untuk nimhs dan kodesmt yang sama
    $krs = $this->getPrevKrs($datakrs['nimhs'], $datakrs['kodesmt'], 0);
    $this->deleteKrs($krs['id_krs']);
    
    // pertahankan tanggal registrasi pertama
    // $slip = $prevkrs['tanggal_request_slip'];
    if (!empty($krs['tanggal_registrasi_mhs'])) {
      $now = $krs['tanggal_registrasi_mhs'];
      $cat = $krs['catatan'];
    } else {
      $now = date('Y-m-d h:i:s');
      $cat = "";
    }

    // - simpan data KRS baru dengan id_krs Baru
    $new_id_krs = $this->setKrsId($datakrs, $datamhs, $now);

    $krs['id_krs'] = $new_id_krs;
    $krs['jumlah_sks'] = $datakrs['jmlsks'];
    $krs['jumlah_mk'] = $datakrs['jmlmk'];
    $krs['status'] = $datakrs['status'];
    $krs['tanggal_registrasi_mhs'] = $now;
    $krs['last_update_mhs'] = date('Y-m-d h:i:s');

    if ($datakrs['status'] == 2) {
      $krs['tanggal_validasi_pa'] = date('Y-m-d h:i:s');
      $krs['kode_validasi_pa'] = md5($new_id_krs . $this->session->pin . $krs['tanggal_validasi_pa']);
    }

    if ((isset($datakrs['mkadd']) && count($datakrs['mkadd']) > 0) || (isset($datakrs['mkrem']) && count($datakrs['mkrem']) > 0)) {
      $krs['catatan'] = $this->createCatatan($datakrs) . "\r\n\r\n----------\r\n" . $cat;
    } else {
      $krs['catatan'] = $cat;
    }

    if ($this->db->insert('siska_krs', $krs)) {
      $res = true;

      // simpan data belanja
      $mk = json_decode(json_encode($datakrs['mk']), true);
      $c = 0;
      foreach ($mk as $dmk) {
        $mk[$c]['id_krs'] =  $new_id_krs;
        $mk[$c]['id_belanja'] = md5($new_id_krs . $dmk['kodemk']);
        $mk[$c]['nimhs'] =  $datakrs['nimhs'];
        $mk[$c]['kodesmt'] =  $datakrs['kodesmt'];
        $mk[$c]['status'] = '1';
        $mk[$c]['urut'] = $c + 1;
        $this->db->insert('siska_belanja', $mk[$c]);
        $c++;
      }
    } else {
      $res =  $this->db->_error_message();
      return $res;
    }

    return $res;
  }

  private function biayaUnik($kodesmt)
  {
    $this->db->select('biaya_unik');
    $this->db->where('kodesmt', $kodesmt);
    $ulist = $this->db->get('siska_krs')->result_array();

    $unique = rand(101, 999);
    while (in_array($unique, $ulist)) {
      $unique = rand(101, 999);
    }

    return $unique;
  }

  public function getPrevKrs($nimhs, $kodesmt, $tipe)
  {
    // $this->db->select('id_krs');
    $this->db->where('nimhs', $nimhs);
    $this->db->where('kodesmt', $kodesmt);
    $this->db->where('tipe_krs', $tipe);
    $result = $this->db->get('siska_krs');
    return $result->row_array();
  }

  public function deleteKrs($idkrs)
  {
    $this->db->where('id_krs', $idkrs);
    $this->db->delete('siska_krs');

    $this->db->where('id_krs', $idkrs);
    $this->db->delete('siska_belanja');
  }

  /*---- 
  baru ditambahkan (230511) untuk initial id saat request slip pembayaran 
  belum ada data belanja mata kuliah
  -- */
  public function newKrsId($datamhs, $datakrs)
  {

    $now = date('Y-m-d h:i:s');

    /**************************** The Secret ************************************/
    $krsId =  md5($datamhs['pin'] . $now . 'new');
    /****************************************************************************/

    $krs = [
      'id_krs' => $krsId,
      'nimhs' => $datamhs['nimhs'],
      'prodi' => $datamhs['prodi'],
      'nidn_pa' => $datamhs['dosen_pa']['nidn'],
      'kodesmt' => $datakrs['kodesmt'],
      'jumlah_sks' => 0,
      'jumlah_mk' => 0,
      'status' => 0,
      'opsi_bayar' => $datakrs['opsibayar'],
      'biaya_unik' => $this->biayaUnik($kodesmt),
      'tanggal_request_slip' => $now,
      // 'tanggal_registrasi_mhs' => $now,
      'last_update_mhs' => $now
    ];
    return ($this->db->insert('siska_krs', $krs));
  }
  /** --- end --- */

  private function setKrsId($datakrs, $datamhs, $tglreg)
  {

    $key = '';
    foreach ($datakrs['mk'] as $mk) {
      $key .= $mk->kodemk . $mk->sks;
    }
    /**************************** The Secret ************************************/
    return md5($datakrs['kodesmt'] . $datamhs['pin'] . $tglreg . $key);
    /****************************************************************************/
  }


  public function listKrsMahasiswa($smt, $prodi, $nidn = '')
  {
    if ($nidn != '') $this->db->where('nidn_pa', $nidn);
    if ($prodi != '') $this->db->where('prodi', $prodi);
    $this->db->where('kodesmt', $smt);
    $this->db->order_by("tanggal_registrasi_mhs", "desc");
    $query = $this->db->get('siska_krs');
    return $query->result_array();
  }

  public function listSemesterKrs($nidn = '')
  {
    $this->db->distinct();
    $this->db->select('kodesmt');

    if ($nidn != '') $this->db->where('nidn_pa', $nidn);

    $this->db->order_by("kodesmt", "desc");
    $query = $this->db->get('siska_krs');
    return $query->result_array();
  }

  public function listProdiKrsAktif($smt)
  {
    $this->db->distinct();
    $this->db->select('prodi');
    $this->db->where('kodesmt', $smt);
    $query = $this->db->get('siska_krs');
    return $query->result_array();
  }

  public function krsSekarang()
  {
    // $sql = "SELECT kodesmt, tanggal_selesai akhir FROM siska_kalender
    //         WHERE event_id='2010'
    //         AND NOW() BETWEEN tanggal_mulai AND tanggal_selesai";
    $sql = "SELECT kodesmt, tanggal_selesai akhir FROM siska_kalender
              WHERE event_id='2010'
              ORDER BY tanggal_mulai DESC
              LIMIT 1";

    $query = $this->db->query($sql);

    if ($query->num_rows() > 0) {
      return $query->row_array();
    }
    return false;
  }

  public function krsExtend($nimhs)
  {
    $sql = "SELECT kodesmt, batas_akhir akhir FROM siska_krs_extend
              WHERE nimhs=?
                AND tipe_krs=0
                AND NOW()<=batas_akhir
              ORDER BY id DESC
              LIMIT 1";

    $query = $this->db->query($sql, $nimhs);

    if ($query->num_rows() > 0) {
      return $query->row_array();
    }
    return false;
  }

  public function updateValidasiKrs($idkrs, $datakrs)
  {
    $this->db->where('id_krs', $idkrs);
    $this->db->update('siska_krs', $datakrs);
  }

  public function nomorSlip($kodesmt)
  {
    $nomor = "R" . substr($kodesmt, -3);
    $sql = "SELECT COUNT(nomorslip) jslp FROM siska_krs WHERE nomorslip LIKE '$nomor%'";
    $query = $this->db->query($sql);

    $nomor .= $this->siska->tambahNol(4, $query->row_array()['jslp'] + 1);
    return $nomor;
  }

  private function createCatatan($dkrs)
  {
    $mkadd = json_decode(json_encode($dkrs['mkadd']), true);
    $mkrem = json_decode(json_encode($dkrs['mkrem']), true);

    $cat = "Pada tanggal " . date('d-m-Y') . " jam " . date('H:i:s') . ", " .
      "Penasehat Akademik telah mereview Rencana Studi yang diajukan mahasiswa, dan menyarankan: \r\n";

    if (count($mkrem) > 0) {
      $cat .= "\r\nMenunda pengambilan matakuliah:\r\n";
      $i = 0;
      foreach ($mkrem as $mkr) {
        $i++;
        $cat .= $i . ". " . $mkr['kodemk'] . " " . $mkr['namamk'] . "(" . $mkr['sks'] . " sks)\r\n";
      }
      $cat .= "Dengan pertimbangan: " . $dkrs['alasanbatal'] . "\r\n";
    }

    if (count($mkadd) > 0) {
      $cat .= "\r\nMenambahkan matakuliah:\r\n";
      $j = 0;
      foreach ($mkadd as $mka) {
        $j++;
        $cat .= $j . ". " . $mka['kodemk'] . " " . $mka['namamk'] . "(" . $mka['sks'] . " sks)\r\n";
      }
      $cat .= "Dengan pertimbangan: " . $dkrs['alasantambah'] . "\r\n";
    }

    return $cat;
  }


  public function getExtendRequest($kodesmt, $nimhs)
  {
    $sql = "SELECT * FROM siska_krs_extend
              WHERE nimhs=? AND kodesmt=?
              ORDER BY id DESC
              LIMIT 1";

    $query = $this->db->query($sql, [$nimhs, $kodesmt]);

    if ($query->num_rows() > 0) {
      return $query->result()[0];
    }
    return false;
  }

  public function getExtendList($kodesmt)
  {
    $sql = "SELECT kx.*, mh.namamhs
              FROM siska_krs_extend kx
              LEFT JOIN siska_mahasiswa mh USING (`nimhs`)
              WHERE kodesmt=?
              ORDER BY kx.tanggal_request";

    $query = $this->db->query($sql, [$kodesmt]);
    // var_dump($kodesmt);
    return $query->result();
  }

  public function insertExtendRequest($data)
  {
    $this->db->insert('siska_krs_extend', $data);
  }

  public function updateExtendRequest($data)
  {
    $this->db->where('id', $data['id']);
    $this->db->update('siska_krs_extend', $data);
  }

  public function semesternow()
  {
    $sql = 'SELECT MAX(`kodesmt`) smtnow FROM `siska_krs`';
    $query = $this->db->query($sql);
    return $query->result()[0]->smtnow;
  }
}
