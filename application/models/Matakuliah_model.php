<?php
class Matakuliah_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }


    public function matakuliah($kodemk){
        $this->db->where('kodemk', $kodemk);
        $result = $this->db->get('siska_matakuliah');
        return $result->row_array();
    }

    public function matakuliahKrsMahasiswa($tahunkur, $prodi, $kompetensi, $nim){
      $querymk =	"SELECT
                     a.kodemk,
    								 a.namamk,
    								 a.sks,
    								 IF( b.status='K', 0, a.smt ) as smt,
    								 a.kompetensi,
    								 b.nilai,
    								 b.kodesmt
          				 FROM
                     (SELECT * FROM siska_matakuliah
          						WHERE tahunkur= ?
          						AND (prodi= ? OR kompetensi= ?)) AS a
          				 LEFT JOIN
            				 (SELECT
                         kodemk,
            				 		 ( IF( MIN(nilai)<>'', MIN(nilai),'P' ) ) AS nilai, kodesmt, status
            				 	FROM siska_kuliah
            					WHERE nimhs= ?
            					  AND status<>'B'
            					GROUP BY kodemk) AS b
                    USING(kodemk)
          				 ORDER BY smt, a.kompetensi, a.kodemk" ;

       $query = $this->db->query($querymk, array( $tahunkur, $prodi, $kompetensi, $nim ));
       return $query->result_array();
    }


    public function daftarMatakuliah($tahunkur, $prodi){

      $querymk =	"SELECT *
          				 FROM siska_matakuliah
          				 WHERE tahunkur=?
          					AND prodi=? 
          				 ORDER BY smt, kodemk" ;

       $query = $this->db->query($querymk, array( $tahunkur, $prodi ));
       return $query->result_array();
    }



}
?>
