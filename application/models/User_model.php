<?php

class User_model extends CI_Model {

	public $data;

		public function __construct() {
        parent::__construct();
    }


	/* -----------------------------------------------------------
	* Fungsi : setId()
	* Return : - False jika $uid tidak dikenali sebagai dosen, mahasiswa atau staf
	* 		   - Tru jika $uid terdaftar dan $this->data  berisi data dosen/mahasiswa/staf
	*            sesuai $uid
	* @param  : - $uid 	String Kode ID
	*
	* created by : Sofyan Thayf
	* ------------------------------------------------------------ */
    public function setId( $uid )
    {
		if ( $this->isDosen( $uid ) ) {
			$this->data = $this->getUser('siska_dosen', 'nidn', $uid );
			$this->data['id'] = $uid;

			$_SESSION['uid']  = $this->data['nidn'];
			$_SESSION['nama'] = $this->data['nama'];
			$_SESSION['prodi'] = $this->data['prodi'];
			$_SESSION['acl']  = $this->data['acl'];
			$_SESSION['pin']  = $this->data['pin'];

			$_SESSION['who']  = 'dsn';

			return true;
		}

		elseif( $this->isMahasiswa( $uid ) ) {
			$this->data = $this->getUser('siska_mahasiswa', 'nimhs', $uid );
			$this->data['id'] = $uid;

			$_SESSION['uid']  = $this->data['nimhs'];
			$_SESSION['nama'] = $this->data['namamhs'];
			$_SESSION['prodi'] = $this->data['prodi'];
			$_SESSION['acl']  = $this->data['acl'];
			$_SESSION['pin']  = $this->data['pin'];

			$_SESSION['who']  = 'mhs';

			return true;
		}

		elseif( $this->isStaf( $uid ) ) {
			$this->data = $this->getUser('siska_staf', 'nip', $uid );
			$this->data['id'] = $uid;

			$_SESSION['uid']  = $this->data['nip'];
			$_SESSION['nama'] = $this->data['nama'];
			$_SESSION['acl']  = $this->data['acl'];
			$_SESSION['pin']  = $this->data['pin'];

			$_SESSION['who']  = 'stf';

			return true;
		}

        return false;
    }

    /*------------------------------------------------------------
     * mengambil semua data user
     *------------------------------------------------------------*/
    public function getUser($table, $key, $value)
    {
        $query = $this->db->get_where($table, array($key => $value));
        if (!empty($query->row_array())) {
            return $query->row_array();
        }
        return false;
    }


	/* -----------------------------------------------------------
	* Fungsi : isMahasiswa()
	* Return : True jika $uid ada dalam data mahasiswa
	* @param  : - $uid 	String Kode ID
	*
	* created by : Sofyan Thayf
	* ------------------------------------------------------------ */
	function isMahasiswa($uid){

        $query = $this->db->get_where('siska_mahasiswa', array( 'nimhs' => $uid ));

        if (!empty($query->row_array())) {
            return true;
        }

        return false;
	}


	/* -----------------------------------------------------------
	* Fungsi : isDosen()
	* Return : True jika $uid ada dalam data dosen
	* @param  : - $uid 	String Kode ID
	*
	* created by : Sofyan Thayf
	* ------------------------------------------------------------ */
	function isDosen( $uid ){

        $query = $this->db->get_where('siska_dosen', array( 'nidn' => $uid ));

        if (!empty($query->row_array())) {
            return true;
        }

        return false;
	}


	/* -----------------------------------------------------------
	* Fungsi : isStaf()
	* Return : True jika $uid ada dalam data staf
	* @param  : - $uid 	String Kode ID
	*
	* created by : Sofyan Thayf
	* ------------------------------------------------------------ */
	function isStaf( $uid ){

        $query = $this->db->get_where('siska_staf', array( 'nip' => $uid ));

        if (!empty($query->row_array())) {
            return true;
        }

        return false;

	}

    public function setLastLogin()
    {
        $data = [
                    'last_login'    => $_SESSION['last_login'],
                    'ip' 			=> $_SERVER['REMOTE_ADDR']
                ];

			$this->updateUserData($data);

    }


		public function resetPassword( $newp = '' )
		{
			if( $newp=='' ) $newp = random_string('alnum', 10);

			$data = [ 'password' => md5( $newp ) ];
			if( $this->updateUserData($data) ) {
				return $newp;
			} else {
				return FALSE;
			}
		}


    // public function updatePassword( $password ) {
    //     $this->db->where( 'id', $id );
    //     if( $this->db->update($this->tabel[$who], array('password' => password_hash( $password, PASSWORD_BCRYPT ))) ) {
    //         return true;
    //     }
    //     return false;
    // }


		public function updateUserData( $data ){
			$status = FALSE;

			switch ( $_SESSION['who'] ) {
				case 'dsn':
							$this->db->where('nidn', $_SESSION['uid']);
							if( $this->db->update('siska_dosen', $data) ) $status = TRUE;
							break;

				case 'mhs':
							$this->db->where('nimhs', $_SESSION['uid']);
							if( $this->db->update('siska_mahasiswa', $data) ) $status = TRUE;
							break;

				case 'stf':
							$this->db->where('nip', $_SESSION['uid']);
							if( $this->db->update('siska_staf', $data) ) $status = TRUE;
							break;
			}

			if( !$status ) $this->siska_log->setLog('U','2', $this->db->_error_message() );

			return $status;
		}

}
