<?php

class Api_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function create( $data ){
        $query = $this->db->insert('keys', $data);
        return $query;
    }

    function verifyCreate($id,$key){
        $this->db->where('user_id', $id);
        $this->db->where('key', $key);
        $query = $this->db->get('keys');
        return $query;
    }

    function getByUser($user){
        $this->db->where('user_id', $user);
        $this->db->limit('1');
        $query = $this->db->get('keys');
        return $query;
    }


    function microtimstamp(){
      list($usec, $sec) = explode(" ", microtime());
      return round(($usec * 1000) + $sec);
    }

}

?>
