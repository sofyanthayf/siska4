<?php

class Feedback_model extends CI_Model
{

    public $data;

    public function __construct()
    {
        parent::__construct();
    }


    public function angket($id)
    {
        $this->db->where('id_angket', $id);
        $angket = $this->db->get('siskafeedback_angket')->result()[0];
        $angket->{'quest_num'} = $this->quest_num($id)->quest_num;
        return $angket;
    }

    public function angket_list()
    {
        $this->db->select('a.*, COUNT(r.id_response) response');
        $this->db->join('siskafeedback_response r', 'id_angket', 'LEFT');
        $this->db->order_by('id_angket', 'ASC');
        $this->db->group_by('id_angket');
        return $this->db->get('siskafeedback_angket a')->result();
    }

    public function questions($id)
    {
        $this->db->where('id_angket', $id);
        $this->db->where('valid', 1);
        $this->db->order_by('quest_order', 'ASC');
        return $this->db->get('siskafeedback_question')->result();
    }

    public function save_response($data)
    {
        // var_dump($data, "<br>&nbsp;<br>");
        foreach ($data as $tosave) {
            foreach ($tosave['data'] as $record) {
                $this->db->insert($tosave['table'], $record);
            }
        }
    }

    public function feedback_kelas_done($nim, $kodekelas)
    {
        $this->db->where('nimhs', $nim);
        $this->db->where('kodekelas', $kodekelas);
        $this->db->update('siska_kuliah', array('feedback' => date('U')));
    }

    public function feedback_done($table, $where)
    {
        foreach ($where as $wh) $this->db->where($wh);
        $this->db->update($table, array('feedback' => date('U')));
    }

    public function responses($id, $kriteria = '')
    {
        $sql = "SELECT res.*, 
                    MAX(CASE WHEN dat.data_label = 'kodesmt' THEN dat.data_value ELSE NULL END) kodesmt,
                    MAX(CASE WHEN dat.data_label = 'nidn' THEN dat.data_value ELSE NULL END) nidn,
                    MAX(CASE WHEN dat.data_label = 'kodekelas' THEN dat.data_value ELSE NULL END) AS kodekelas
                FROM `siskafeedback_response` res
                LEFT JOIN `siskafeedback_data` dat USING(`id_response`)
                WHERE res.`id_angket`=?
                GROUP BY res.id_response
                ORDER BY kodesmt";
        $response = $this->db->query($sql, array($id))->result();

        $r = 0;
        foreach ($response as $res) {
            $sql = "SELECT q.`id_quest`, q.`parent`, q.`quest_order`, d.`score`
                    FROM `siskafeedback_question` q
                    LEFT JOIN `siskafeedback_responsedetail` d 
                    ON q.`id_quest`=d.`id_quest` AND d.`id_response`IN (?)
                    WHERE q.`id_angket` =?
                    AND q.`parent`<>'00000'
                    ORDER BY q.`quest_order`";
            $scores = $this->db->query($sql, array($res->id_response, $res->id_angket))->result();

            foreach ($scores as $key => $value) {
                $response[$r]->{$value->parent . '_' . $value->quest_order} = $value->score;
            }
            $r++;
        }
        return $response;
    }


    public function done_after($tabelafter, $where)
    {
        // echo '<pre>after:' . var_export($where, true) . '</pre>';
        if ($tabelafter != '' && $where != '') {
            foreach ($where as $wh) $this->db->where($wh);
            $this->db->where('feedback IS NULL');
            $query = $this->db->get($tabelafter);

            if ($query->num_rows() > 0) return false;
        }
        return true;
    }

    private function quest_num($id)
    {
        $this->db->select('COUNT(id_quest) quest_num');
        $this->db->where('id_angket', $id);
        $this->db->where('klas', '2');
        $this->db->where('valid', '1');
        $this->db->group_by('id_angket');
        return $this->db->get('siskafeedback_question')->result()[0];
    }

    public function likert_data($id_angket, $key, $val)
    {
        $sql = "SELECT 
                    `que`.`parent`,
                    SUM(IF(`det`.`score`=1, 1, 0)) `1`,
                    SUM(IF(`det`.`score`=2, 1, 0)) `2`,
                    SUM(IF(`det`.`score`=3, 1, 0)) `3`,
                    SUM(IF(`det`.`score`=4, 1, 0)) `4`,
                    SUM(IF(`det`.`score`=5, 1, 0)) `5`,
                    COUNT(`det`.`score`) `N`
                FROM `siskafeedback_responsedetail` `det`
                LEFT JOIN `siskafeedback_question` `que` USING (`id_quest`)
                LEFT JOIN 
                    (SELECT 
                    `an`.`id_angket`, 
                    `rs`.`id_response`,
                    MAX(CASE WHEN dt.data_label = 'kodesmt' THEN dt.data_value ELSE NULL END) `kodesmt`,
                    MAX(CASE WHEN dt.data_label = 'nidn' THEN dt.data_value ELSE NULL END) `nidn`
                    FROM `siskafeedback_response` `rs`
                    LEFT JOIN `siskafeedback_angket` `an` USING (`id_angket`)
                    LEFT JOIN `siskafeedback_data` `dt` USING (`id_response`)
                    GROUP BY `rs`.`id_response`
                    ) `res`
                    ON (`det`.`id_response` = `res`.`id_response`)
                WHERE `res`.`id_angket` = ?
                  AND " . $key . "= ?
                GROUP BY `que`.`parent`
                ORDER BY `que`.`parent` ASC;";
        return $this->db->query($sql, array($id_angket, $val))->result();
    }

    public function question_categories($id_angket)
    {
        $sql = "SELECT id_quest,statement
                FROM `siskafeedback_question`
                WHERE id_angket=?
                    AND parent='00000'
                    AND valid=1";

        return $this->db->query($sql, array($id_angket))->result();
    }


    public function response_semester($id_angket)
    {
        $sql = "SELECT DISTINCT `data_value` `kodesmt`
                FROM `siskafeedback_data`
                WHERE `id_angket` = ? 
                AND `data_label` = 'kodesmt'
                ORDER BY `kodesmt` DESC
                ";

        return $this->db->query($sql, array($id_angket))->result();
    }

    public function response_dosen($id_angket)
    {
        $sql = "SELECT DISTINCT `data_value` `nidn`, `ds`.`nama`, `ds`.`gelar_depan`, `ds`.`gelar_belakang`
                FROM `siskafeedback_data` `dt`
                LEFT JOIN `siska_dosen` `ds` ON (`dt`.`data_value`=`ds`.`nidn`)
                WHERE `id_angket` = ? 
                AND `dt`.`data_value` <> ''
                AND `dt`.`data_label` = 'nidn'
                AND `ds`.`nama` IS NOT NULL
                ORDER BY `ds`.`nama` ASC
                ";

        return $this->db->query($sql, array($id_angket))->result();
    }
}
