<?php

/**
 *  Pmb_lib.php
 *  Library untuk penanganan proses PMB dan reward program
 */
class Pmb_lib
{
    public $tahun;
    public $afiliasi;
    public $ujianseleksi;

    function __construct() {
        $this->CI =& get_instance();
        $this->CI->load->model('pmbuser_model', '', TRUE);

        $this->initvars();
    }

    private function initvars(){
        $this->tahun = '2025';

        $this->afiliasi = [ "A" => "Alumni STMIK KHARISMA",
                            "G" => "Guru SMA/SMK",
                            "S" => "Dosen/Staf Pengelola STMIK KHARISMA",
                            "M" => "Mahasiswa STMIK KHARISMA",
                            "U" => "Umum"  ];

        $this->ujianseleksi = [ ["kode" => "A", "ujian" => "Test Akademik" ],
                                ["kode" => "P", "ujian" => "Psikotest" ],
                                ["kode" => "W", "ujian" => "Wawancara" ]
                            ];
    }

    public function kode_referrer( $qr = FALSE ){
        $l = 'L';
        if( $qr ) $l='Q';
        $kode = base64_encode( md5( $_SESSION['id'].$_SESSION['token'] ) . $_SESSION['id'] );
        return $l.$kode;
    }

    public function panitia( $token, $hash ){
        if( $hash == '0' ) return false;

        $salt = substr( $hash, -6 );
        if( $this->panitia_hash( $token, $salt ) == $hash ){
            return true;
        }
        return false;
    }

    public function panitia_hash( $token, $salt = "" ){
        if( $salt == "" ) $salt = random_string('alnum', 6);
        return md5( "PAN".$token.$salt ).$salt;
    }

    public function link_verifikasi_email(){
        // ambil email_token
        $email_token = $this->CI->pmbuser_model->get_email_token( $_SESSION['email'] );
        // kirim email verifikasi
        $link = base_url()."pmb/verify/".base64_encode( "E;".$_SESSION['email'].";".$email_token ) ;
        return $link;
    }

//** ingat! kode Q dan L sudah reserverd
    public function link_request($kode){
        $salt = random_string('alnum', 6);
        $link = base_url()."pmb/verify/".
                base64_encode( $kode.";".$_SESSION['id'].";".md5($_SESSION['token'].$salt).$salt ) ;
        return $link;
    }

    public function link_verifikasi_kartutest( $id, $token ){
        $link = base_url()."pmb/verify/".base64_encode( "T;".$id.";".md5($token) ) ;
        return $link;
    }

}


?>
