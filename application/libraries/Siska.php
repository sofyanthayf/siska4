<?php

class Siska
{

  public $jenjang;
  public $kodejenjang;
  public $jabatanfungsional;

  public $status_mahasiswa;
  public $agama;
  public $jenis_kelamin;
  public $jurusan_sma;
  public $jurusan_smk;

  public $nilai_angka;
  public $nilai_mutu;

  public $defaultregion;

  public $pekerjaan;
  public $penghasilan;
  public $dok_maba;

  public $visi_stmik;

  public $sumberinfo;
  public $tabelwarna;

  function __construct()
  {
    $this->CI = &get_instance();
    $this->CI->load->model('krs_model', '', TRUE);
    // $this->CI->load->model('pmbuser_model', '', TRUE);

    $this->visi_stmik = "MENJADI PERGURUAN TINGGI YANG UNGGUL DALAM BIDANG DIGITAL ENTREPRENEUR'";
    $this->initvars();
  }

  private function initvars()
  {

    $this->jenjang = array(
      'A' => array('kode' => 'S3', 'namajenjang' => 'Strata Tiga', 'namaprogram' => 'Program Doktor', 'jumlah' => '0'),
      'B' => array('kode' => 'S2', 'namajenjang' => 'Strata Dua', 'namaprogram' => 'Program Magister', 'jumlah' => '0'),
      'C' => array('kode' => 'S1', 'namajenjang' => 'Strata Satu', 'namaprogram' => 'Program Sarjana', 'jumlah' => '2'),
      'E' => array('kode' => 'D-3', 'namajenjang' => 'Diploma Tiga', 'namaprogram' => 'Program Diploma Tiga', 'jumlah' => '2'),
      'G' => array('kode' => 'D-1', 'namajenjang' => 'Diploma Satu', 'namaprogram' => 'Program Diploma Satu', 'jumlah' => '0'),
      'N' => array('kode' => '', 'namajenjang' => '', 'namaprogram' => '', 'jumlah' => '')
    );

    $this->kodejenjang = array('S1' => 'C', 'D3' => 'E', 'D1' => 'G');

    $this->jabatanfungsional = array(
      'A' => 'Tenaga Pengajar',
      'B' => 'Asisten Ahli',
      'C' => 'Lektor',
      'D' => 'Lektor Kepala',
      'N' => ''
    );

    $this->status_mahasiswa = array(
      'A' => 'Aktif',
      'C' => 'Cuti Akademik',
      'D' => 'Drop-out',
      'K' => 'Keluar',
      'L' => 'Lulus',
      'N' => 'Non-aktif',
      'P' => 'Pindah'
    );

    $this->agama = ['1' => 'Islam', '2' => 'Kristen', '3' => 'Katholik', '4' => 'Hindu', '5' => 'Buddha'];
    $this->jenis_kelamin = ['L' => 'Laki-laki', 'P' => 'Perempuan'];

    $this->jurusan_sma = ['1' => 'IPA', '2' => 'IPS'];

    $this->nilai_angka = ['E' => 0, 'D' => 1, 'C' => 2, 'B' => 3, 'A' => 4];
    $this->nilai_mutu =  ['E', 'D', 'C', 'B', 'A'];

    $this->defaultregion = ['propinsi' => '73', 'kota' => '7371'];

    $this->pekerjaan = [
      ['kode' => '000', 'pekerjaan' => 'Tidak Ada'],
      ['kode' => '001', 'pekerjaan' => 'Aparatur Sipil Negara (ASN/PNS, Non-Guru/Dosen)'],
      ['kode' => '002', 'pekerjaan' => 'Aparatur Sipil Negara: Guru'],
      ['kode' => '003', 'pekerjaan' => 'Aparatur Sipil Negara: Dosen'],
      ['kode' => '004', 'pekerjaan' => 'Anggota TNI'],
      ['kode' => '005', 'pekerjaan' => 'Anggota POLRI'],
      ['kode' => '006', 'pekerjaan' => 'Karyawan BUMN/BUMD'],
      ['kode' => '101', 'pekerjaan' => 'Karyawan Swasta (Non-Guru/Dosen)'],
      ['kode' => '102', 'pekerjaan' => 'Karyawan Swasta: Guru'],
      ['kode' => '103', 'pekerjaan' => 'Karyawan Swasta: Dosen'],
      ['kode' => '301', 'pekerjaan' => 'Wiraswasta: Perdagangan'],
      ['kode' => '302', 'pekerjaan' => 'Wiraswasta: Pelayanan Jasa'],
      ['kode' => '303', 'pekerjaan' => 'Wiraswasta: Pertanian/Perkebunan'],
      ['kode' => '304', 'pekerjaan' => 'Wiraswasta: Peternakan'],
      ['kode' => '305', 'pekerjaan' => 'Wiraswasta: Kelautan/Perikanan'],
      ['kode' => '306', 'pekerjaan' => 'Wiraswasta: Teknologi'],
      ['kode' => '307', 'pekerjaan' => 'Wiraswasta: Kuliner'],
      ['kode' => '308', 'pekerjaan' => 'Wiraswasta: Seni'],
      ['kode' => '309', 'pekerjaan' => 'Wiraswasta: Travel/Pariwisata'],
      ['kode' => '401', 'pekerjaan' => 'Profesional: Dokter'],
      ['kode' => '402', 'pekerjaan' => 'Profesional: Akuntan'],
      ['kode' => '403', 'pekerjaan' => 'Profesional: Pengacara'],
      ['kode' => '404', 'pekerjaan' => 'Profesional: Notaris'],
      ['kode' => '405', 'pekerjaan' => 'Profesional: Arsitek'],
      ['kode' => '406', 'pekerjaan' => 'Profesional: Desainer'],
      ['kode' => '407', 'pekerjaan' => 'Profesional: Wartawan'],
      ['kode' => '408', 'pekerjaan' => 'Profesional: Pilot'],
      ['kode' => '409', 'pekerjaan' => 'Profesional: Pelaut']
    ];

    $this->penghasilan = [
      ['kode' => 1, 'range' => 'sampai dengan Rp5.000.000 per-bulan'],
      ['kode' => 2, 'range' => 'Rp5.000.000 - Rp15.000.000 per-bulan'],
      ['kode' => 3, 'range' => 'Rp15.000.000 - Rp25.000.000 per-bulan'],
      ['kode' => 4, 'range' => 'lebih dari Rp25.000.000 per-bulan']
    ];

    $this->sumberinfo = [
      ['kode' => 1, 'src' => 'Brosur / Poster / Banner'],
      ['kode' => 2, 'src' => 'Website Resmi STMIK KHARISMA'],
      ['kode' => 3, 'src' => 'Koran'],
      ['kode' => 4, 'src' => 'Facebook'],
      ['kode' => 5, 'src' => 'Pameran/Expo'],
      ['kode' => 6, 'src' => 'Teman'],
      ['kode' => 7, 'src' => 'Keluarga'],
      ['kode' => 8, 'src' => 'Sekolah / Guru']
    ];

    $this->dok_maba = [
      ['kode' => '01', 'status' => '1', 'dokumen' => 'KTP / Keterangan Domisili'],
      ['kode' => '02', 'status' => '0', 'dokumen' => 'Akte Kelahiran'],
      ['kode' => '03', 'status' => '1', 'dokumen' => 'Pas Foto'],
      ['kode' => '04', 'status' => '0', 'dokumen' => 'Ijazah SMA / SMK / sederajat'],
      ['kode' => '05', 'status' => '0', 'dokumen' => 'SKHU'],
      ['kode' => '06', 'status' => '1', 'dokumen' => 'Raport'],
      ['kode' => '07', 'status' => '2', 'dokumen' => 'Sertifikat/Piagam']
    ];

    $this->dok_maba_internal = [
      ['kode' => '11', 'dokumen' => 'Kartu Peserta Seleksi'],
      ['kode' => '12', 'dokumen' => 'Undangan Psikotest'],
      ['kode' => '13', 'dokumen' => 'Undangan Wawancara'],
      ['kode' => '14', 'dokumen' => 'Dokumen Wawancara'],
      ['kode' => '15', 'dokumen' => 'Pernyataan Kelulusan'],
    ];

    $this->tabelwarna = array('#fcfcff', '#ffd7e5', '#d7ffe5', '#e5d7ff', '#ffe5d7', '#f7f7ff');
  }

  public function getPekerjaan($kode)
  {
    foreach ($this->pekerjaan as $kerja) {
      if ($kerja['kode'] == $kode) return $kerja;
    }
    return false;
  }

  public function getPenghasilan($kode)
  {
    foreach ($this->penghasilan as $hasil) {
      if ($hasil['kode'] == $kode) return $hasil;
    }
    return false;
  }

  /* -----------------------------------------------------------
    * Fungsi : stringSemster()
    * Return : String nama semester berdasarkan kode semester
    * param  : - $kodesmt	String kode semester
    *     	   - $label 	String tahun akademik (optional, default '- TA')
    *
    * created by : Sofyan Thayf
    * ------------------------------------------------------------ */
  public function stringSemester($kodesmt, $label = '- TA')
  {
    $str = "";

    $tahun = substr($kodesmt, 0, 4);

    $ks = substr($kodesmt, -1);
    if ($ks == '1') {
      $sms = "Awal";
    } elseif ($ks == '2') {
      $sms = "Akhir";
    } elseif ($ks == '3') {
      $sms = "Pendek";
    }

    $str = "$sms $label $tahun-" . ($tahun + 1);

    return $str;
  }


  /* -----------------------------------------------------------
    * Fungsi : nextSemster()
    * Return : Kode semester berikutnya dari kode semester yang dimasukkan
    *          (untuk pengisian KRS semester berikut)
    * param  : - $kodesmt	String kode semester
    *
    * created by : Sofyan Thayf
    * ------------------------------------------------------------ */
  public function nextSemester($kodesmt)
  {
    if (substr($kodesmt, -1) == '2') {
      $next = (substr($kodesmt, 0, 4) + 1) . "1";
    } else {
      $next = $kodesmt + 1;
    }
    return $next;
  }

  /* -----------------------------------------------------------
    * Fungsi : sksMax()
    * Return : Maksimum jumlah sks yang boleh diprogramkan berdasarkan IP
    *          (untuk pengisian KRS semester berikut)
    * param  : - $ip nilai IP/IPK yang menentukan jumlah sks maksimum
    *
    * created by : Sofyan Thayf
    * ------------------------------------------------------------ */
  public function sksMax($ip)
  {
    if ($ip >= 3.0) {
      $maxsks = 24;
    } elseif ($ip >= 2.0 && $ip < 3.0) {
      $maxsks = 20;
    } elseif ($ip >= 1.0 && $ip < 2.0) {
      $maxsks = 18;
    } else {
      $maxsks = 14;
    }
    return $maxsks;
  }

  public function uidMahasiswaValid($uid)
  {
    if ($this->CI->session->who == 'mhs' && $this->CI->session->uid == $uid) {
      return true;
    } else {
      return false;
    }
  }

  /* -----------------------------------------------------------
    * Fungsi : tambahNol()
    * Return : menambahkan angka nol di depan nilai sesuai jumlah $digit
    		   contoh: tambahNol( 5, 12 ) --> '00012'
    * param  : $digit 	jumlah digit setelah penambahan angka nol
    * 		   $nilai 	bilangan yang akan ditambahkan nol
    *
    * created by : Sofyan Thayf
    * ------------------------------------------------------------ */
  public function tambahNol($digit, $nilai)
  {
    $nol = "";
    for ($n = 1; $n <= $digit - strlen($nilai); $n++) {
      $nol .= '0';
    }
    return $nol . $nilai;
  }

  /* ----------------------------------------------------------------
     * Fungsi : terbilang()
     * https://gist.github.com/cahsowan/d315d54a59e4f14a6bab
     * ----------------------------------------------------------------*/
  public function terbilang($nilai)
  {
    $angka = ["", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas"];
    if ($nilai < 12)
      return " " . $angka[$nilai];
    elseif ($nilai < 20)
      return $this->terbilang($nilai - 10) . " Belas";
    elseif ($nilai < 100)
      return $this->terbilang($nilai / 10) . " Puluh" . $this->terbilang($nilai % 10);
    elseif ($nilai < 200)
      return "Seratus" . $this->terbilang($nilai - 100);
    elseif ($nilai < 1000)
      return $this->terbilang($nilai / 100) . " Ratus" . $this->terbilang($nilai % 100);
    elseif ($nilai < 2000)
      return "Seribu" . $this->terbilang($nilai - 1000);
    elseif ($nilai < 1000000)
      return $this->terbilang($nilai / 1000) . " Ribu" . $this->terbilang($nilai % 1000);
    elseif ($nilai < 1000000000)
      return $this->terbilang($nilai / 1000000) . " Juta" . $this->terbilang($nilai % 1000000);
  }


  /*--------------- FEEDBACK -----------------*/

  public function showquest($question, $parent, $questonly=FALSE)
  {
    foreach ($question as $quest) {
      if ($quest->parent == $parent) {
?>
        <tr>
          <td <?= $quest->klas != '1' ? 'style="width:50%;padding-left:25px;"' : 'colspan="3" style="background-color:#ddeeff;"' ?>>
            <?= $quest->klas == '1' ? "<strong>" . $quest->statement . "</strong>" : $quest->statement ?>
          </td>
          <?php
          if(!$questonly){ ?>
          <?= $quest->required == '1' ? '<td class="text-danger font-weight-bold">*</td>' : '<td></td>' ?>
          <td>
            <?php if ($quest->klas != '1') { ?>
              <table style="width:100%;">
                <tr>
                  <?php
                  $labels = explode(';', $quest->scale_labels);
                  $weight = explode(';', $quest->scale_weight);
                  $descr = explode(';', $quest->deskripsi);
                  $l = 0;
                  foreach ($labels as $lbl) {
                  ?>
                    <td style="vertical-align:top; width:25%;">
                      <input type="radio" name="<?= $quest->id_quest ?>" value="<?= $weight[$l] ?>" <?= $quest->required == '1' ? 'REQUIRED' : '' ?>>
                      <?= $lbl ?>
                      <?= (count($descr) > 1) && ($descr[$l] != '') ? "<br><i>" . $descr[$l] . "</i>" : '' ?>
                    </td>
                  <?php
                    $l++;
                  }
                  ?>

                </tr>
              </table>
            <?php } ?>
          </td>
          <?php } ?>
        </tr>

<?php
        $this->showquest($question, $quest->id_quest, $questonly);
      }
    }
  }



  //-----------------------------keep it at the bottom----------------------------------------------------------//
  private $keykharisma = "BajiAteka20"; // no change please!
  /* item(s)+currdate+key */
  /*
    * UPPER(MD5(CONCAT(NODOSMSDOSS,CURDATE(),'BajiAteka20')))
    * UPPER(MD5(CONCAT(nimhs,CURDATE(),'BajiAteka20'))) 
    * UPPER(MD5(CONCAT(nip,CURDATE(),'BajiAteka20')))
    */
}
