$("#password2").keyup(function(){
    if( $(this).val() != $("#password1").val() ){
        $("#warningpassword").html("Verifikasi password tidak sama");
        $( this ).focus();
        // $(":submit").attr("disabled", true);
        $("#submitgantipass").attr("disabled", true);
    } else {
        $("#warningpassword").html("");
        // $(":submit").removeAttr("disabled");
        $("#submitgantipass").removeAttr("disabled");
    }
});

$("#oldpassword").keyup(function(){ $("#warningoldpassword").html(""); });
$("#password1").keyup(function(){ $("#warningnewpassword").html(""); });
$("#submitgantipass").focusout(function(){ $("#passwordrespons").html(""); });

$("#submitgantipass").click(function(){
    if( !$("#oldpassword").val() ){
        $("#warningoldpassword").html("Password Lama harus diisi")
        $("#oldpassword").focus();
    } else if( !$("#password1").val() ){
        $("#warningnewpassword").html("Password Baru belum diisi")
        $("#password1").focus();
    } else {
        $.ajax({
            url: '/gantipassword',
            data: { 'oldpassword' : $("#oldpassword").val(),
                    'password' : $("#password1").val()
                },
            type: 'POST',
            success: function(result){
                var style2 = "</div>";
                switch (result) {
                    case '0':
                        var style1 = "<div class='alert alert-success text-center' role='alert'>";
                        $("#passwordrespons").html(style1+"Password Anda telah berhasil diganti"+style2);
                        break;
                    case '1':
                        var style1 = "<div class='alert alert-danger text-center' role='alert'>";
                        $("#passwordrespons").html(style1+"Password Lama tidak cocok"+style2);
                        break;
                    case '2':
                        var style1 = "<div class='alert alert-warning text-center' role='alert'>";
                        $("#passwordrespons").html(style1+"Gagal mengganti password, silahkan dicoba kembali"+style2);
                        break;
                }
                $("#oldpassword").val("");
                $("#password1").val("");
                $("#password2").val("");
            }
        });

    }
});
