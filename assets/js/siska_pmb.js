var datamhs;
hideopsi();
$(document).ready(function(){
});

$("#opsiafiliasi").change(function(){
    hideopsi();

    switch( $(this).val() ) {
        case "A":
            daftarangkatan();
            $("#opsialumni").show();
            break;
        case "M":
            $("#nimmhs").val("");
            $("#nimmhs").attr("required", "required");
            $("#opsimhs").show();
            break;
        case "S":
            $("#nidnnik").val("");
            $("#nidnnik").attr("required", "required");
            $("#opsistaf").show();
            break;
        case "G":
            $("#opsiguru").show();
            break;
    }
});

$('#thnalumni').change(function(){
    $.ajax({
        url: '/ajax/listprodimhs',
        data: { 'angkatan' : $(this).val() },
        success: function(result){
            $("#prdalumni").empty();
            $("#prdalumni").append($('<option>').text("-- Pilih Program Studi --").attr('value', "0"));
            pr = $.parseJSON(result);
            $.each( pr , function(i){
                $("#prdalumni").append($('<option>').text(pr[i].nama + " - " + pr[i].jenjang).attr('value', pr[i].kode));
            });
        }
    });
});

$("#prdalumni").click(function(){
    if( $("#thnalumni").val() == "0" ){
        alert("Tentukan Angkatan Anda terlebih dahulu");
        $("#thnalumni").focus();
    }
});

$("#prdalumni").change(function(){
    if( $("#thnalumni").val() != "0" && $("#prdalumni").val() != "0" ){
        $.ajax({
            url: '/ajax/getlistmahasiswa',
            data: { 'prodi' : $(this).val(),
                    'tahun' : $("#thnalumni").val() },
            success: function(result){
                $("#namaalumni").empty();
                $("#namaalumni").append($('<option>').text("-- Pilih Nama Anda --").attr('value', "0"));
                datamhs = [];
                alu = $.parseJSON(result);
                $.each( alu , function(i){
                    $("#namaalumni").append($('<option>').text(alu[i].nim + " - " + alu[i].nama).attr('value', alu[i].nim));
                    datamhs[alu[i].nim] = alu[i];
                });
            }
        });
    }
});

/* alumni */
$("#namaalumni").change(function(){
    $("#email").val( datamhs[ $(this).val() ].email );
    $("#namalengkap").val( datamhs[ $(this).val() ].nama );
    $("#email").change();
});

/* mahasiswa */
$("#nimmhs").change(function(){
    $.ajax({
        url: '/ajax/getmahasiswa',
        data: { 'nim' : $(this).val() },
        success: function(result){
            if( result != "false" ){
                datamhs = $.parseJSON(result);
                $("#email").val( datamhs.email );
                $("#namalengkap").val( datamhs.namamhs );
                $("#email").change();
            } else {
                alert("Nomor Induk Mahasiswa (NIM) yang dimasukkan keliru atau tidak terdaftar");
                $("#opsiafiliasi").change();
            }
        }
    });
});

/* dosen */
$("#nidnnik").change(function(){
    $.ajax({
        url: '/ajax/getstaf',
        data: { 'id' : $(this).val() },
        success: function(result){
            if( result != "false" ){
                datastaf = $.parseJSON(result);
                $("#email").val( datastaf.email );
                $("#namalengkap").val( datastaf.nama );

                $("#email").change();
            } else {
                alert("NIDN / NIK yang dimasukkan keliru atau tidak terdaftar");
                $("#opsiafiliasi").change();
            }
        }
    });
});

$("#email").change(function(){
    $.ajax({
        url: '/pmbajax/emailexist',
        data: { 'email' : $(this).val() },
        success: function(result){
            if(result==1){
                $("#warningemail").html("Alamat E-mail tidak dapat digunakan, karena sudah pernah didaftarkan");
                $( this ).focus();
                $(":submit").attr("disabled", true);
            } else {
                $("#warningemail").html("");
                $(":submit").removeAttr("disabled");
            }
        }
    });
})

$("#nomorhp").change(function(){
    $.ajax({
        url: '/pmbajax/cellularexist',
        data: { 'nomorhp' : $(this).val() },
        success: function(result){
            if(result==1){
                $("#warninghp").html("Nomor HP tidak dapat digunakan, karena sudah pernah didaftarkan");
                $( this ).focus();
                $(":submit").attr("disabled", true);
            } else {
                $("#warninghp").html("");
                $(":submit").removeAttr("disabled");
            }
        }
    });
})

$("#password2").keyup(function(){
    if( $(this).val() != $("#password1").val() ){
        $("#warningpassword").html("Verifikasi password tidak sama");
        $( this ).focus();
        // $(":submit").attr("disabled", true);
        $("#submitgantipass").attr("disabled", true);
    } else {
        $("#warningpassword").html("");
        // $(":submit").removeAttr("disabled");
        $("#submitgantipass").removeAttr("disabled");
    }
});

$("#oldpassword").keyup(function(){ $("#warningoldpassword").html(""); });
$("#password1").keyup(function(){ $("#warningnewpassword").html(""); });
$("#submitgantipass").focusout(function(){ $("#passwordrespons").html(""); });

$("#submitgantipass").click(function(){
    if( !$("#oldpassword").val() ){
        $("#warningoldpassword").html("Password Lama harus diisi")
        $("#oldpassword").focus();
    } else if( !$("#password1").val() ){
        $("#warningnewpassword").html("Password Baru belum diisi")
        $("#password1").focus();
    } else {
        $.ajax({
            url: '/pmb/gantipassword',
            data: { 'oldpassword' : $("#oldpassword").val(),
                    'password' : $("#password1").val()
                },
            success: function(result){
                var style2 = "</div>";
                switch (result) {
                    case '0':
                        var style1 = "<div class='alert alert-success text-center' role='alert'>";
                        $("#passwordrespons").html(style1+"Password Anda telah berhasil diganti"+style2);
                        break;
                    case '1':
                        var style1 = "<div class='alert alert-danger text-center' role='alert'>";
                        $("#passwordrespons").html(style1+"Password Lama keliru"+style2);
                        break;
                    case '2':
                        var style1 = "<div class='alert alert-warning text-center' role='alert'>";
                        $("#passwordrespons").html(style1+"Gagal mengganti password, silahkan dicoba kembali"+style2);
                        break;
                }
                $("#oldpassword").val("");
                $("#password1").val("");
                $("#password2").val("");
            }
        });

    }
});

function hideopsi(){
    $("#opsimhs").hide();
    $("#opsialumni").hide();
    $("#opsistaf").hide();
    $("#opsiguru").hide();

    $("#thnalumni").empty();
    $("#prdalumni").empty();
    $("#namaalumni").empty();

    $("#namalengkap").val("");
    $("#email").val("");
    $("#nomorhp").val("");

    $("#nimmhs").removeAttr("required");
    $("#nidnnik").removeAttr("required");
}

function daftarangkatan(){
    $.get("/ajax/listangkatanalumni", function(result){
        $("#thnalumni").empty();
        $('#thnalumni').append($('<option>').text("-- Pilih Tahun -- ").attr('value', "0"));
        $.each($.parseJSON(result), function(i, value){
            $('#thnalumni').append($('<option>').text(value.tahunmasuk).attr('value', value.tahunmasuk));
        });
    });
}

function copyToClipboard(element) {
    var $temp = $("<input />");
    $("body").append($temp);
    $temp.val($(element).text()).select();

    var result = false;
    try {
        result = document.execCommand("copy");
    } catch (err) {
        console.log("Copy error: " + err);
    }

    $temp.remove();
    return result;
}

// function mailmemyref(){
$("#mailmemyref").click(function(){
    $("#mailmemyref").attr("disabled", true);
    $.ajax({
        url: '/pmb_ajax/mailmemyref',
        data: { 'mylink' : $("#linkref").val(),
                'myqrimg' : $("#qrref").val(),
                'mybitly' : $("#bitly").val()
            },
        success: function(result){
            alert("Link Referensi telah dikirim ke alamat E-Mail Anda, \nsebarkanlah seluas mungkin")
            $("#mailmemyref").removeAttr("disabled");
        }
    });
});

$("#reqgantiemail").click(function(){
    $("#reqgantiemail").attr("disabled", true);
    $.ajax({
        url: '/pmb_ajax/reqgantiemail',
        success: function(result){
            alert("Untuk alasan keamanan, kode request telah dikirim ke E-mail Anda,\nSilahkan cek E-mail Anda")
            $("#reqgantiemail").removeAttr("disabled");
        }
    });
})

$("#reqgantihp").click(function(){
    $("#reqgantihp").attr("disabled", true);
    $.ajax({
        url: '/pmb_ajax/reqgantinomorhp',
        success: function(result){
            alert("Untuk alasan keamanan, kode request telah dikirim ke E-mail Anda,\nSilahkan cek E-mail Anda")
            $("#reqgantihp").removeAttr("disabled");
        }
    });
})

/** facebook event-track sample **/
$("#formregmaba").on("submit", function(){
   fbq('track', 'CompleteRegistration');
 })
