// jQuery plugin to prevent double submission of forms
// http://stackoverflow.com/questions/2830542/prevent-double-submission-of-forms-in-jquery
$.fn.preventDoubleSubmission = function () {
    $(this).on('submit', function (e) {
        var $form = $(this);

        if ($form.data('submitted') === true) {
            // Previously submitted - don't submit again
            alert('Form sedang dalam proses. Harap bersabar.');
            e.preventDefault();
        } else {
            // Mark it so that the next submit can be ignored
            // ADDED requirement that form be valid
            if($form.valid()) {
                $form.data('submitted', true);
            }
        }
    });

    // Keep chainability
    return this;
};

$('form').preventDoubleSubmission();

$.ajaxSetup({
    headers : { 'CsrfToken': $('meta[name="token-a"]').attr('content') },
    method : 'POST'
});

$("#propinsi").change(function(){
    $.ajax({
        url: '/ajax/listkotakab',
        data: { 'idprop' : $(this).val() },
        success: function(result){
            $("#kabkota").empty();
            $('#kabkota').append($('<option>').text("-- Pilih Kabupaten/Kota --").attr('value', "0"));
            $.each($.parseJSON(result), function(i, value){
                $('#kabkota').append($('<option>').text(value.name).attr('value', value.id));
            });
        }
    });
});

$("#kabkota").change(function(){
    $.ajax({
        url: '/ajax/listkecamatan',
        data: { 'idkota' : $(this).val() },
        success: function(result){
            $("#kecamatan").empty();
            $('#kecamatan').append($('<option>').text("-- Pilih Kecamatan --").attr('value', "0"));
            $.each($.parseJSON(result), function(i, value){
                $('#kecamatan').append($('<option>').text(value.name).attr('value', value.id));
            });
        }
    });
});

$("#kecamatan").change(function(){
    $.ajax({
        url: '/ajax/listdesakelurahan',
        data: { 'idkecamatan' : $(this).val() },
        success: function(result){
            $("#desakelurahan").empty();
            $('#desakelurahan').append($('<option>').text("-- Pilih  Desa/Kelurahan --").attr('value', "0"));
            $.each($.parseJSON(result), function(i, value){
                $('#desakelurahan').append($('<option>').text(value.name).attr('value', value.id));
            });
        }
    });
});


$("#propinsisekolah").change(function(){
    $.ajax({
        url: '/ajax/listkotakab',
        data: { 'idprop' : $(this).val() },
        success: function(result){
            $("#kotasekolah").empty();
            $('#kotasekolah').append($('<option>').text("-- Pilih Kabupaten/Kota Sekolah --").attr('value', "0"));
            $.each($.parseJSON(result), function(i, value){
                $('#kotasekolah').append($('<option>').text(value.name).attr('value', value.id));
            });
        }
    });
});

$("#kotasekolah").change(function(){
    $.ajax({
        url: '/ajax/listsekolahkota',
        data: { 'idkota' : $(this).val() },
        success: function(result){
            $("#listsekolah").empty();
            $('#listsekolah').append($('<option>').text("-- Pilih Sekolah --").attr('value', "0"));
            $.each($.parseJSON(result), function(i, value){
                $('#listsekolah').append($('<option>').text(value.sekolah).attr('value', value.id));
            });
        }
    });
});

$("#listsekolah").change(function(){
    if( $(this).children(':selected').text().substr(0,3) == 'SMK' ) updatelistjurusan( $("#tahunlulus").val() );
    else resetjurusansma();
});

$("#tahunlulus").change(function(){
    if( $("#listsekolah").children(':selected').text().substr(0,3) == 'SMK' ) updatelistjurusan( $(this).val() );
});

function updatelistjurusan( tahun ){
    $.ajax({
        url: '/ajax/listjurusansmk',
        data: { 'tahunlulus' : tahun },
        success: function(result){
            $("#listjurusan").empty();
            $('#listjurusan').append($('<option>').text("-- Pilih Jurusan --").attr('value', "0"));
            $.each($.parseJSON(result), function(i, value){
                $('#listjurusan').append($('<option>').text(value.jurusan).attr('value', value.kode));
            });
        }
    });
}

function resetjurusansma(){
    $("#listjurusan").empty();
    $('#listjurusan').append($('<option>').text("-- Pilih Jurusan --").attr('value', "0"));
    $('#listjurusan').append($('<option>').text('IPA').attr('value', '000001'));
    $('#listjurusan').append($('<option>').text('IPS').attr('value', '000002'));
    $('#listjurusan').append($('<option>').text('Tidak Ada Jurusan').attr('value', '000009'));
}

$('.form_datetime').datetimepicker({
    //language:  'fr',
    weekStart: 1,
    todayBtn:  1,
    autoclose: 1,
    todayHighlight: 1,
    startView: 2,
    forceParse: 0,
    showMeridian: 1
});
$('.form_date').datetimepicker({
    language:  'id',
    weekStart: 1,
    todayBtn:  1,
    autoclose: 1,
    todayHighlight: 1,
    startView: 2,
    minView: 2,
    forceParse: 0
});
$('.form_time').datetimepicker({
    language:  'id',
    weekStart: 1,
    todayBtn:  1,
    autoclose: 1,
    todayHighlight: 1,
    startView: 1,
    minView: 0,
    maxView: 1,
    forceParse: 0
});

$(function(){
    var fileInput = $('.upload-file');
    var maxSize = fileInput.data('max-size');
    $('.upload-form').submit(function(e){
        if(fileInput.get(0).files.length){
            var fileSize = fileInput.get(0).files[0].size; // in bytes
            if(fileSize>maxSize){
                alert('File Anda lebih besar dari 500 Kilo Bytes');
                return false;
            }
        }else{
            alert('Silahkan pilih file');
            return false;
        }

    });
});
