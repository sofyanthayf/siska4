var prodi;
var idx = 0;

$.ajaxSetup({
    headers : { 'CsrfToken': $('meta[name="token-a"]').attr('content') },
    method : 'POST'
});

$(document).ready(function(){
    getListProdi( $('#view_mhs_tahun').val() );
    $('#listmahasiswa').DataTable().ajax.reload()
});

/** Tampilkan List Program Studi untuk tahun terakhr saat halaman /mhs selesai di load **/
$(window).load(function () {
    getListProdi( $('#angkatan').val() );
});

/** Tampilkan List Program Studi sesuai tahun yang dipilih **/
$("#angkatan").change( function() {
    view_mhs_tahun = $(this).val();
    getListProdi( $(this).val() );

    $("#searchtextkey").val("");
});

$("#searchtextkey").keyup( function(){
    if( $(this).val().length > 2 ){
        $('#disp_prodi').html( "Hasil Pencarian" );
        $('#disp_tahun').html( "\'" + $(this).val() + "\'" );

        url = "/ajax/search/mhs";
        data = { "key"      : $(this).val() ,
                 "dataroot" : "listmhs" };
        refreshlistmahasiswa( url, data );
    } else {
        getListProdi( $('#angkatan').val() );
    }
});


function getListProdi( tahun ){
    $.ajax({
        url: '/ajax/listprodimhs',
        data: { 'angkatan' : tahun },
        success: function(result){
            prodi = $.parseJSON(result);
            refreshListProdi();
            getMahasiswa( view_mhs_prodi, view_mhs_tahun )
        }
    });
}

/** List Program Studi sesuai tahun angkatan mahasiswa untuk halaman /mhs **/
function refreshListProdi() {
    var list = '';
    $.each(prodi, function(i){
        list += "<button type='button' class='list-group-item' id='"+i+"' onclick='prodiOnClick( this );'>" +
                 prodi[i].nama + " - " + prodi[i].jenjang +
                 "</button>"
    });
    $("#listprodi").html(list);
}

function prodiOnClick( btn ) {
    idx = btn.id;
    view_mhs_prodi = prodi[ btn.id ].kode;

    getMahasiswa( prodi[ btn.id ].kode , $('#angkatan').val() );
    $("#searchtextkey").val("");
}


function getMahasiswa( kdprodi, tahun ){
    $('#disp_prodi').html( prodi[idx].nama +" "+ prodi[idx].jenjang );
    $('#disp_tahun').html( tahun );

    url = "/ajax/getlistmahasiswa";
    data = { "prodi"    : kdprodi ,
             "tahun"    : tahun ,
             "dataroot" : "listmhs" };

    refreshlistmahasiswa( url, data );
}

function refreshlistmahasiswa( url, param ){
    $('#listmahasiswa').DataTable( {
        "ajax": { "url"  : url,
                  "type" : "POST",
                  "data" : param    },
        "sAjaxDataProp": "listmhs",
        "columns": [ { "render":function(data, type, full, meta){
                                   return "<a href='/mhs/"+full.nim+"'>" + full.nim + "</a>";
                               },
                       "className": "dt-center" },
                     { "render":function(data, type, full, meta){
                                    return "<a href='/mhs/"+full.nim+"'>" + full.nama + "</a>";
                                }
                     },
                     { "render": function(data, type, full, meta){
                                    if( full.status != "" ) return status_mahasiswa[ full.status ];
                                    else return "";
                                },
                       "className": "dt-center"}
                 ],
        "searching": false,
        "destroy": true
    } );
// $('#listmahasiswa').DataTable().ajax.reload( null, false );
// $('#listmahasiswa').DataTable().draw();
}
