var status_mahasiswa = { A:"Aktif",
                         C:"Cuti Akademik",
                         D:"Drop-out",
                         K:"Keluar",
                         L:"Lulus",
                         N:"Non-aktif",
                         P:"Pindah"
                       };

var metode_bayar = { 1: "Tunai", 2:"Bank", 3:"Transfer", 4:"Deposit", 5:"Beasiswa" };

function formatCurrency(n, c, d, t) {
 var c = isNaN(c = Math.abs(c)) ? 2 : c,
   d = d == undefined ? "." : d,
   t = t == undefined ? "," : t,
   s = n < 0 ? "-" : "",
   i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
   j = (j = i.length) > 3 ? j % 3 : 0;

 return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};
