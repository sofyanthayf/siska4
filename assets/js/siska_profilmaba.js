var statusupdate = 0;   //0=nochange; 1=changed; 9=saving
var infosrc = "";
setInterval(update_profile,20000);

$( document ).ready(function() {
    $("input[name='info[]']:checkbox").each( function() {
        if( $(this).is(":checked") ) infosrc += '1';
        else infosrc += '0';
    });
});

/* update data sebelum tinggalkan halaman profil  */
$( window ).on('beforeunload',function() {
  update_profile();
});

/* Perubahan isi field  */
$("#pnamalengkap").change(function(){ changed(); });
$("#ptempatlahir").change(function(){ changed(); });
$("#itglahir").change(function(){ changed(); });
$("input[name='jkel']").change(function(){ changed(); });
$("#listsekolah").change(function(){ changed(); });
$("#tahunlulus").change(function(){ changed(); });
$("#listjurusan").change(function(){ changed(); });
$("#alamat").change(function(){ changed(); });
$("#agama").change(function(){ changed(); });
$("#desakelurahan").change(function(){ changed(); });
$("#namaayah").change(function(){ changed(); });
$("#namaibu").change(function(){ changed(); });
$("#hpayah").change(function(){ changed(); });
$("#hpibu").change(function(){ changed(); });
$("#pekerjaanayah").change(function(){ changed(); });
$("#pekerjaanibu").change(function(){ changed(); });
$("input[name='rpenghasilan']").change(function(){ changed(); });
$("input[name='rprodi1']").change(function(){ changed(); });
$("input[name='rprodi2']").change(function(){ changed(); });

/*  Function changed:
 *  menandai adanya perubahan dari satu atau beberapa field
 */
function changed(){
    statusupdate = 1;
    setstatusupdate(1);
}

/*  Function info_changed()
 *  update perubahan status pilihan sumber infromasi
 */
function info_changed(){
    infosrc = "";
    $("input[name='info[]']:checkbox").each( function() {
        if( $(this).is(":checked") ) infosrc += '1';
        else infosrc += '0';
    });
    changed();
}

/*  Function setstatusupdate:
 *  menampilkan status perubahan di bagian atas (#statusupdate)
 *  dan di bagian bawah (#statusupdatebot) halaman
 */
function setstatusupdate( s = 0 ){
    var mess;
    switch ( s ) {
        case 1:
            mess = "<i class='fa fa-fw fa-info-circle'></i> Perubahan akan tersimpan otomatis sesaat lagi";
            break;
        case 2:
            mess = '<i class="fa fa-fw fa-floppy-o"></i> saving ..';
            break;
        case 3:
            mess = '<i class="fa fa-fw fa-check"></i> perubahan data tersimpan';
            break;
    }
    $("#statusupdate").html( mess );
    $("#statusupdatebot").html( mess );
}

/*  Function update_profile:
 *  penyimpanan perubahan data ke database
 *  secara asynchronous ($.ajax)
 */
function update_profile(){
    if( statusupdate == 1 ){
        setstatusupdate(2);
        $.ajax({
            url: '/pmbajax/updateprofilmaba',
            method: 'post',
            data: { 'namalengkap'  : $("#pnamalengkap").val(),
                    'tempatlahir'  : $("#ptempatlahir").val(),
                    'tanggallahir' : $("#ptglahir").val(),
                    'jkel'         : $("input[name='jkel']:checked").val(),
                    'rprodi1'      : $("input[name='rprodi1']:checked").val(),
                    'rprodi2'      : $("input[name='rprodi2']:checked").val(),
                    'idsekolah'    : $("#listsekolah").val(),
                    'tahunlulus'   : $("#tahunlulus").val(),
                    'jurusan'      : $("#listjurusan").val(),
                    'alamat'       : $("#alamat").val(),
                    'agama'        : $("#agama").val(),
                    'desakelurahan': $("#desakelurahan").val(),
                    'namaayah'     : $("#namaayah").val(),
                    'namaibu'      : $("#namaibu").val(),
                    'hpayah'       : $("#hpayah").val(),
                    'hpibu'        : $("#hpibu").val(),
                    'pekerjaanayah': $("#pekerjaanayah").val(),
                    'pekerjaanibu' : $("#pekerjaanibu").val(),
                    'penghasilan'  : $("input[name='rpenghasilan']:checked").val(),
                    'infosrc'      : infosrc
                  },
            success: function(){
                        statusupdate = 0;
                        setstatusupdate(3);
                    }
        });
    }
};
