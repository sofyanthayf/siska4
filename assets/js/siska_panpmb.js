// getListAgen();
// setInterval(getListAgen,30000);
getListAgen();
getListMaba();
getListDokumen();
getListKartuPeserta();
getListJadwal();

setInterval( function () {
    $('#list').DataTable().ajax.reload( null, false ); // user paging is not reset on reload
    $('#list').DataTable().draw();
}, 30000 );

$(document).ready( function() {
});

$('#listchannel').DataTable({
});

$("#setpan").click(function(){
    $.ajax({
        url: '/pmb_ajax/setpanitia',
        data: { 'id' : $("#agenid").val() },
        success: function(result){
            if( result == '1') location.reload();
        }
    });
});

$("#notpan").click(function(){
    $.ajax({
        url: '/pmb_ajax/notpanitia',
        data: { 'id' : $("#agenid").val() },
        success: function(result){
            if( result == '1') location.reload();
        }
    });
});

function getListAgen(){
    $('#listagen').DataTable( {
        "ajax": {
                    "url" : "/pmb_ajax/getlistagen",
                    "type": "POST",
                    "data" : []
                },
        "sAjaxDataProp": "listagen",
        "columns": [ { "data": "nama_lengkap",
                       "render":function(data, type, full, meta){
                             return "<a href='/pmb/pan/agen/"+full.id+"'>" + full.nama_lengkap + "</a>";
                         }
                     },
                     { "data": "tg_registrasi", "className": "dt-center" },
                     { "data": "tipe_afiliasi",
                       "render":function(data, type, full, meta){
                                            return full.tipe_afiliasi + "-" + full.id_afiliasi;
                                        }
                     },
                     { "data": "klik", "className": "dt-center" },
                     { "data": "reg", "className": "dt-center" }
                  ]
    } );
}

function getListMaba(){
    $('#listmaba').DataTable( {
        "ajax": {
                    "url" : "/pmb_ajax/getlistmaba",
                    "type": "POST",
                    "data" : []
                },
        "sAjaxDataProp": "listmaba",
        "columns": [ { "data": "nama_lengkap",
                       "render":function(data, type, full, meta){
                             return "<a href='/pmb/pan/maba/"+full.id+"'>" + full.nama_lengkap + "</a>";
                         }
                     },
                     { "data": "tg_registrasi" },
                     { "data": "sekolah" },
                     { "data": "prodi1" ,
                       "render":function(data, type, full, meta){
                                          if( full.prodi1 != null && full.jenjang1 != null  ) {
                                              return full.prodi1 + " " + full.jenjang1;
                                          }
                                          return "";
                                      }
                     }
                  ]
    } );
}

function getListDokumen(){
    $('#listdokumen').DataTable( {
        "ajax": {
                    "url" : "/pmb_ajax/getlistdokumen",
                    "type": "POST",
                    "data" : []
                },
        "sAjaxDataProp": "listdokumen",
        "columns": [ { "data": "id_owner" },
                     { "data": "nama_lengkap" },
                     { "data": "tipe",
                       "render":function(data,type, full, meta){
                           return namadok[full.tipe];
                       }
                      },
                     { "data"  : "tg_upload" },
                     { "data"  : "tipefile",
                       "render": function(data,type, full, meta){
                           var dfile = full.id_owner + "_" + full.tipe + "." + full.tipefile;
                           var icon;
                           if( full.tipefile == "pdf" ) icon = "file-pdf-o";
                           else icon = "file-image-o";
                           var link = "<i class='fa fa-fw fa-"+icon+"'></i> " +
                                      "<a href='" + filepath + dfile +
                                      "' target='_blank' onclick='enablevalidasi("+full.id+");'>" +
                                      dfile + "</a>";
                           return link;
                       }
                     },
                     { "render" : function(data,type, full, meta){
                           var button = "<button class='btn btn-info btn-sm' id='b_"+full.id+
                                        "' onclick='validateddok("+full.id+")' disabled> \
                                        <i class='fa fa-fw fa-check-square-o'></i> Dokumen Valid</button>";
                           return button;
                       }
                      }
                  ]
    } );
}

function getListKartuPeserta(){
    $('#listkartupeserta').DataTable( {
        "ajax": {
                    "url" : "/pmb_ajax/getwaitinglistkartupeserta",
                    "type": "POST",
                    "data" : []
                },
        "sAjaxDataProp": "listkartupeserta",
        "columns": [ { "data": "maba" },
                     { "data": "nama_lengkap" },
                     { "data": "tglujian" },
                     { "data": "jamujian" },
                     { "render": function(data,type, full, meta){
                                           var button = "<button class='btn btn-info btn-sm' id='btk_"+full.maba+
                                                        "' onclick='terbitkankartu("+full.maba+")'> \
                                                        <i class='fa fa-fw fa-file-pdf-o'></i> Terbitkan Kartu Peserta</button>";
                                           return button;
                                        }
                       }
                  ]
    } );
}

function getListJadwal(){
    $("#listjadwal").DataTable( {
        "ajax": {
                    "url" : "/pmb_ajax/getlistjadwal",
                    "type": "POST",
                    "data" : []
                },
        "sAjaxDataProp": "listjadwal",
        "columns": [ { "data": "tglujian" },
                     { "data": "waktuujian" },
                     { "data": "jenis",
                       "render": function(data,type, full, meta){
                           return namaujian[full.jenis];
                       }
                        },
                     { "data": "peserta" }
                 ]
    });
}

function enablevalidasi( id ){
    $( "#b_"+ id ).removeAttr("disabled");
}

function validateddok( id ){
    $.ajax({
        url: '/pmb_ajax/validasidokumen',
        data: { 'id_dokumen' : id },
        success: function(){
            $('#listdokumen').DataTable().ajax.reload( null, false );
            $('#listdokumen').DataTable().draw();
        }
    });
}

$("#jenisseleksi").change(function(){
    var caption = $(this).find('option:selected').text();
    $("#labeltgtest").html("Tanggal " + caption );
    $("#labelwaitingtest").html("Waiting List Peserta " + caption );

    $.ajax({
        url: '/pmb_ajax/waitinglistmaba',
        data: { 'tipe' : $(this).val() },
        success: function(result){
            var maba = $.parseJSON(result);
            $('#waitinglist').empty();
            $.each(maba, function(i){
                var dis = ' disabled';
                var muted = ' text-muted';
                var kstatus = ' <span class="text-sm text-danger">(<i>belum lengkap</i>)<span>';
                if( maba[i].profillengkap == '1' ) {
                    dis = "";
                    muted = "";
                    kstatus = "";
                }
                $('#waitinglist').append('<div class="checkbox' + muted + '">  \
                                            <label> \
                                            <input type="checkbox" name="maba[]" value="'+ maba[i].id +'" \
                                            onchange=""' + dis + '> \
                                            <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span> \
                                            ' + maba[i].nama_lengkap + kstatus +' \
                                            </label> \
                                          </div>');
            });
        }
    });

});

function terbitkankartu( $id ){
    $.ajax({
        url: '/pmb_panitia/buatkartuujian',
        data: { 'id' : $id },
        success: function(result){
            $('#listkartupeserta').DataTable().ajax.reload( null, false );
            $('#listkartupeserta').DataTable().draw();
            alert( "Kartu Tanda Peserta dengan nomor ID: " + $id +
                   " telah diterbitkan. Peserta akan menerima E-mail Notifikasi.\n" +
                   "Dokumen dapat dilihat pada Detail Profil Peserta" );
        }
    });
}
