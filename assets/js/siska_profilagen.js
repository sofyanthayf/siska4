var statusupdate = 0;   //0=nochange; 1=changed; 9=saving
var infosrc = "";
var setujutransfer = 0;
setInterval(update_profile,20000);

$( document ).ready(function() {
    $("input[name='info[]']:checkbox").each( function() {
        if( $(this).is(":checked") ) infosrc += '1';
        else infosrc += '0';
    });
});

/* update data sebelum tinggalkan halaman profil  */
$( window ).on('beforeunload',function() {
    update_profile();
});

/* Perubahan isi field  */
$("#pnamalengkap").change(function(){ changed(); });
$("#bitly").change(function(){ changed(); });
$("input[name='jkel']").change(function(){ changed(); });
$("#alamat").change(function(){ changed(); });
$("#desakelurahan").change(function(){ changed(); });
$("#pekerjaan").change(function(){ changed(); });
$("#rekening").change(function(){
    $("#setujubiaya").attr('checked', true);
    $("#setujubiaya").change();
    changed();
});
$("#namarekening").change(function(){
    $("#setujubiaya").attr('checked', true);
    $("#setujubiaya").change();
    changed(); }
);
$("#bank").change(function(){
    $("#setujubiaya").attr('checked', true);
    $("#setujubiaya").change();
    changed();
});
$("#setujubiaya").change(function(){
    if( $(this).is(":checked") ) setujutransfer = 1;
    else setujutransfer = 0;
    changed();
});


$("#requestsms").click(function(){
    $(this).attr("disabled", true);
    $(this).hide();
    $.ajax({
        url: '/pmb/send_sms_verifikasi',
        success: function(){
                    alert("Kode verifikasi telah dikirimkan ke nomor handphone Anda");
                }
    });
});

/*  Function changed:
 *  menandai adanya perubahan dari satu atau beberapa field
 */
function changed(){
    statusupdate = 1;
    setstatusupdate(1);
}

/*  Function info_changed()
 *  update perubahan status pilihan sumber infromasi
 */
function info_changed(){
    infosrc = "";
    $("input[name='info[]']:checkbox").each( function() {
        if( $(this).is(":checked") ) infosrc += '1';
        else infosrc += '0';
    });
    changed();
}

/*  Function setstatusupdate:
 *  menampilkan status perubahan di bagian atas (#statusupdate)
 *  dan di bagian bawah (#statusupdatebot) halaman
 */
function setstatusupdate( s = 0 ){
    var mess;
    switch ( s ) {
        case 1:
            mess = "<i class='fa fa-fw fa-info-circle'></i> Perubahan akan tersimpan otomatis sesaat lagi";
            break;
        case 2:
            mess = '<i class="fa fa-fw fa-floppy-o"></i> saving ..';
            break;
        case 3:
            mess = '<i class="fa fa-fw fa-check"></i> perubahan data tersimpan';
            break;
    }
    $("#statusupdate").html( mess );
    $("#statusupdatebot").html( mess );
}

/*  Function update_profile:
 *  penyimpanan perubahan data ke database
 *  secara asynchronous ($.ajax)
 */
function update_profile(){
    if( statusupdate == 1 ){
        setstatusupdate(2);
        $.ajax({
            url: '/pmbajax/updateprofilagen',
            method: 'post',
            data: { 'namalengkap'  : $("#pnamalengkap").val(),
                    'bitly'        : $("#bitly").val(),
                    'jkel'         : $("input[name='jkel']:checked").val(),
                    'alamat'       : $("#alamat").val(),
                    'desakelurahan': $("#desakelurahan").val(),
                    'pekerjaan'    : $("#pekerjaan").val(),
                    'rekening'     : $("#rekening").val(),
                    'namarekening' : $("#namarekening").val(),
                    'bank'         : $("#bank").val(),
                    'setujubiaya'  : setujutransfer,
                    'infosrc'      : infosrc
                  },
            success: function(){
                        statusupdate = 0;
                        setstatusupdate(3);
                    }
        });
    }
};
