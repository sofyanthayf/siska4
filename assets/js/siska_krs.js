var smtdefault;

function getSemesterKRS(listsemester) {
    $.get("/krs_api/semesterkrs", function (data) {
        smtdefault = data.semesterkrs[0].kodesmt;
        $.each(data.semesterkrs, function (i) {
            if (data.semesterkrs[i].kodesmt != "") {
                var smt = data.semesterkrs[i].kodesmt;
                var ssmt = "Semester " + data.semesterkrs[i].stringsmt + " (" + smt + ")";
                var opt = new Option(ssmt, smt);
                $(opt).html(ssmt);
                $(listsemester).append(opt);
            }
        });
        getKRSMahasiswa(smtdefault)
    });
}

function getKRSMahasiswa(kodesmt) {
    // kodesmt = smt;
    // alert("getKRSMahasiswa("+kodesmt+")");
    dttableres = $(dttable).DataTable({
        ajax: {
            url: "/krs_api/krsmahasiswa/smt/" + kodesmt + "/prodi/" + prodi,
            type: "GET"
        },
        sAjaxDataProp: "listkrsmhs",
        columns: [
            { data: "mahasiswa.nimhs", className: "dt-center" },
            { data: "mahasiswa.namamhs" },
            { data: "tanggal_request_slip", className: "dt-center" },
            {
                render: function (data, type, full, meta) {
                    if (full.tanggal_cetak_slip != null) {
                        return "<a title='" + full.tanggal_cetak_slip + "'>" + full.nomorslip + "</a>";
                    } else {
                        return "";
                    }
                },
                className: "dt-center"
            },
            {
                render: function (data, type, full, meta) {
                    if (full.tanggal_konfirmasi != null) {
                        return "<a title='" + full.tanggal_konfirmasi + "'><img src='/assets/img/check16.png'></a>";
                    } else {
                        if (full.tanggal_cetak_slip != null && full.nomorslip != "BEASISWA") {
                            return "<img src='/assets/img/timer16.png'>";
                        } else {
                            return "";
                        }
                    }
                },
                className: "dt-center"
            },
            {
                render: function (data, type, full, meta) {
                    if (full.tanggal_bayar != null) {
                        return "<a title='" + full.tanggal_bayar + "'><img src='/assets/img/check16.png'></a>";
                    } else {
                        return "";
                    }
                },
                className: "dt-center"
            },
            {
                render: function (data, type, full, meta) {
                    if (full.tanggal_validasi_keu != null) {
                        return "<a title='" + full.tanggal_validasi_keu + "'><img src='/assets/img/check16.png'></a>";
                    } else {
                        return "";
                    }
                },
                className: "dt-center"
            },
            { data: "tanggal_registrasi_mhs", className: "dt-center" },
            {
                render: function (data, type, full, meta) {
                    if (full.tanggal_validasi_pa != null) {
                        return "<a title='" + full.tanggal_validasi_pa + "'><img src='/assets/img/check16.png'></a>";
                    } else {
                        if (full.status != 0) {
                            return "<a title='Menunggu validasi PA ..'><img src='/assets/img/timer16.png'></a>";
                        } else {
                            return "";
                        }
                    }
                },
                className: "dt-center"
            },
            {
                render: function (data, type, full, meta) {
                    if (full.tanggal_validasi_prodi != null) {
                        return "<a title='" + full.tanggal_validasi_prodi + "'><img src='/assets/img/check16.png'></a>";
                    } else {
                        if (full.tanggal_validasi_keu != null) {
                            return "<img src='/assets/img/timer16.png'>";
                        } else {
                            return "";
                        }
                    }
                },
                className: "dt-center"
            }
        ],
        columnDefs: [
            { targets: 6, visible: roles['keu'] },
            { targets: 9, visible: roles['prd'] }
        ],
        order: [[2, 'desc']],
        language: { searchPlaceholder: "NIM / Nama Mahasiswa" },
        iDisplayLength: 25,
        deferRender: true,
        destroy: true
    });

}

function showDetailMahasiswa(data) {
    // console.log(data);
    idkrs = data.id_krs;
    nunik = parseInt(data.biaya_unik);
    console.log(nunik);

    $("#smtkrs").html("<strong>" + data.kodesmt + "</strong>");
    $("#nimhs").html("<strong>" + data.mahasiswa.nimhs + "</strong>");
    $("#namamhs").html("<strong>" + data.mahasiswa.namamhs + "</strong>");
    $("#telepon").html("<strong>" + data.mahasiswa.telepon + "</strong>");
    $("#email").html("<strong>" + data.mahasiswa.email + "</strong>");
    $("#dosenpa").html(data.mahasiswa.dosen_pa.nama);

    $("#jmlsks").html("<strong>" + data.jumlah_sks + "</strong> sks");
    $("#jmlmk").html(data.jumlah_mk);
    $("#tanggalreq").html(data.tanggal_request_slip);
    $("#opsibayar").html(((data.opsi_bayar == 0) ? '<strong>Bayar Penuh</strong>' : '<strong>Dengan Angsuran</strong><br><i>(Pastikan mahasiswa mengkonfirmasi langsung sebelum menerbitkan invoice)'));
    $("#tanggalreg").html(data.tanggal_registrasi_mhs);

    // if (data.tanggal_validasi_pa != null) {
    if (data.tanggal_request_slip != null) {
        $("#tanggalvalidasipa").html(data.tanggal_validasi_pa);
        $("#rowtgvalidasipa").show();
        $("#tdbiayaunik").html("<strong>" + formatCurrency(data.biaya_unik, 0, ".", ",") + "<strong>");

        if (data.tanggal_cetak_slip != null) {
            $("#frcetakslip").hide();
            $("#rowtgcetakslip").show();

            $("#innomorslip").html(data.nomorslip);
            $("#incetakslip").html(data.tanggal_cetak_slip);
            $("#inbiayasks").html("<strong>" + formatCurrency(data.biaya_sks, 0, ".", ",") + "</strong>");
            $("#inbiayaadm").html("<strong>" + formatCurrency(data.biaya_adm, 0, ".", ",") + "<strong>");
            $("#inbiayaunik").html("<strong>" + formatCurrency(data.biaya_unik, 0, ".", ",") + "<strong>");
            $("#intotalbiaya").html("<strong>" + formatCurrency(parseInt(data.biaya_sks) + parseInt(data.biaya_adm) + parseInt(data.biaya_unik), 0, ".", ",") + "</strong>");

            $("#frmbayar").show();
        } else {
            $("#frcetakslip").show();
            $("#rowtgcetakslip").hide();
            $("#frmbayar").hide();
        }

    } else {
        $("#rowtgvalidasipa").hide();
        $("#rowtgcetakslip").hide();
        $("#frcetakslip").hide();
        $("#frmbayar").hide();
    }

    if (data.tanggal_konfirmasi != null) {
        image_url = "/assets/dokumen/trkrs/" + data.kodesmt + "/" + data.nimhs;
        $.get(image_url + ".jpg")
            .done(function () {
                $("#pictransfer").attr("src", image_url + ".jpg");
            }).fail(function () {
                $("#pictransfer").attr("src", image_url + ".png");
            })
        $("#imgtransfer").show();

    }

    if (data.tanggal_validasi_keu != null) {
        $("#intglbayar").html(data.tanggal_bayar);
        $("#intglvalidasikeu").html(data.tanggal_validasi_keu);

        $("#injumlahbayar").html("Rp " + formatCurrency(parseInt(data.biaya_sks) + parseInt(data.biaya_adm), 0, ".", ","));
        $("#inmetodebayar").html(metode_bayar[data.metode_bayar]);

        $("#frcetakslip").hide();
        $("#frmbayar").hide();

        $("#rowtgbayar").show();
        $("#rowtgvalidasikeu").show();
        $("#rowjumlahbayar").show();
        $("#rowmetodebayar").show();

        $("#tbkonfirmprodi").show();

    } else {

        $("#intglbayar").html("");
        $("#intglvalidasikeu").html("");
        $("#injumlahbayar").html("");
        $("#inmetodebayar").html("");

        $("#rowtgbayar").hide();
        $("#rowtgvalidasikeu").hide();
        $("#rowjumlahbayar").hide();
        $("#rowmetodebayar").hide();

        $("#tbkonfirmprodi").hide();
    }

    if (data.tanggal_validasi_prodi != null) {
        $("#intglvalidasiprodi").html(data.tanggal_validasi_prodi);
        $("#tbkonfirmprodi").hide();
        $("#rowtgvalidasiprodi").show();
    } else {
        $("#intglvalidasiprodi").html("");
        $("#rowtgvalidasiprodi").hide();
    }


}

setInterval(function () {
    if ($("#chkautorefresh").is(":checked")) {
        getKRSMahasiswa(smtdefault);
    }
}, 60000);

$("#btrefresh").click(function () {
    getKRSMahasiswa(smtdefault);
})